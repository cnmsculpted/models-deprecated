Sculpted Models
===============
## Pulling Repo ##
In order to have all of the updated models and changes, you need to clone this repository. To do so, click on the top right in Bitbucket where it
says "Clone", and copy that text into your terminal or shell while you are in whatever folder you want this repository to be.
Ask around for someone to help you since this may be different between Mac and Windows.

If you want all of the changes to appear on your computer, you need to "pull". Make sure you are in the folder directory "models" and make
sure you are in the MASTER BRANCH (see below). Type
`git pull origin master`.
You should definitely do this every time you start a work session because otherwise you will be editing old files!

## To Create/Edit a Model ##
In a Git-like repository, there is a "Master" branch. The Master branch contains all of the updated work and models.

To create or make edits to model, create a new branch first. This will make it safe when you go to add your changes to the whole repo.
To create a branch, `git branch name_of_branch_here`.
For example, if I need to create a tool, I will first do `git branch jose__tool`. The `git branch` command creates a new branch.
With that, go into the branch using `git checkout jose__tool`. The `git checkout` command switches you between different branches.
You are now in the `jose__tool` branch. You can then make edit/add a new model while in this branch, and it won't harm anyone else's.

## Contributing Changes (Pushing) ##
If you are done editing your model, you must push your branch that contains all your work. We will then "merge" this branch with "master"
such that everyone will then be able to receive the changes you have made.

At any time, you can type `git status` to see that status of all of your files which have been changed from the Master branch.
GREEN files are going to be committed with `git commit`. RED files will not be committed with the next `git commit`.

To push, first commit all changes. Before you commit, you have to tell Git what files you want to change. Often, you just want to push
every file that you have changed. But `git add` lets you specify exactly which file you want to merge with the master branch.

`git add -A` means you will be "committing" every changed file in the repository, even if Git has never seen them before.

`git add -u` means you will be "committing" all changed files that were already there, not new files!

`git add <filename>` adds a specific file.

After adding the files, you should commit your changes. Use the `git commit` command. Example:
`git commit -m "This is an explanation of my changes, i.e. what this commit does."`
Always include a useful commit message!

Then push your changes to the Master branch using `git push origin <name_of_branch>`. The `<name_of_branch>` is your current branch: you can check it with `git branch`.

In BitBucket, you then need to create a "Pull Request". A "Pull Request" is a way for you to request your changes be put into the master branch.
So go to "models" website, and on the left hand side, click "Pull Requests".

Once the Pull Request has been made, me or any Admin will then merge the pull requests.

## Review of the Flow of Git ##

1. Pull most recent changes that other people have made (`git pull origin master`)

2. Create a new local "branch" for today's changes with any helpful name (`git branch <name_of_new_branch>`)

3. Change to your current branch (`git checkout <name_of_new_branch>`)

4. Make edits to the files you want, or create new files. (aka all of your actual work)

5. Once you have finished these changes and are fine with forcing everyone else to have your version of the files, you are ready to push.
Please don't push broken files. It's fine to push in-progress work, just not broken work.

6. Add all files that you want to push. (`git add`, described in detail above)

7. Commit your changes. This creates a sort of "packet" containing all of your changes this session. (`git commit -m "Change Description!"`)

8. Push your branch. (`git push origin <name_of_new_branch>`)

9. Make a pull request on Bitbucket so your branch can get merged with the master branch.
