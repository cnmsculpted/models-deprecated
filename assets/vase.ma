//Maya ASCII 2018 scene
//Name: vase.ma
//Last modified: Wed, Nov 07, 2018 02:15:30 PM
//Codeset: UTF-8
requires maya "2018";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Mac OS X 10.12.6";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "32C45087-7E45-CFBA-97C3-AF82E62BFDF1";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 14.43445018864997 3.1397178806836181 12.244437583167491 ;
	setAttr ".r" -type "double3" -0.33835273177739972 -1750.1999999993145 3.8496843281712696e-17 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "E0434C42-8D4F-DC22-1A97-458FD0FFCE41";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 18.89863452975537;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -1.1920928955078125e-07 3.0281151150233083 0.046381335471680885 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "25ACA567-0F47-89C7-A0C4-F4BA66DCA793";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "CDB0848A-1A4E-9C16-8CB2-5D886264A8BD";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 24.65883209750924;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "685C3C92-994A-2072-D890-7E97081EF2A0";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "F218F54F-184E-096F-A997-BF94FBA5938A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 6.0243328836917973;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "1448B040-B148-67F7-0748-64AF2CBD6B52";
	setAttr ".t" -type "double3" 1000.1419126424773 0.18546514798447378 0.046381573890481843 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "385C49A7-B84C-E6ED-CC23-6F8D33A559F8";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1419127616864;
	setAttr ".ow" 25.74589567980717;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" -1.1920928955078125e-07 0.18546514798447378 0.046381573890259764 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "imagePlane1";
	rename -uid "40C6793B-794F-9E76-DF70-7A861D94B117";
	setAttr ".t" -type "double3" -6.6039108238672615 3.1642609521109648 -0.0037535162878498118 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode imagePlane -n "imagePlaneShape1" -p "imagePlane1";
	rename -uid "B1B7E7BB-CC42-DF18-FF89-67A91298A191";
	setAttr -k off ".v";
	setAttr ".fc" 102;
	setAttr ".imn" -type "string" "/Users/JoseChavez/Desktop/sculpted/vase.jpg";
	setAttr ".cov" -type "short2" 512 714 ;
	setAttr ".dlc" no;
	setAttr ".w" 5.12;
	setAttr ".h" 7.1400000000000006;
	setAttr ".cs" -type "string" "sRGB";
createNode transform -n "pCylinder1";
	rename -uid "3D28B9CE-BB4A-09FB-32D6-A584888A30C0";
	setAttr ".t" -type "double3" 0 3.1075734376698172 0.046381693099549315 ;
	setAttr ".s" -type "double3" 1 3.2692591125726125 1 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	rename -uid "1F400C1E-F848-6BCF-69AE-E79622DD9134";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999998509883881 0.3442198857665062 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "5927A4B1-3E4C-847A-E1F9-42889D653D14";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "466E591E-0E42-A5F2-E8D2-A79CF4D6ACC3";
createNode displayLayer -n "defaultLayer";
	rename -uid "C819995B-414A-6A8A-A0DD-82BD7C69F379";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "0699645A-6745-0ECC-EB3F-CC9D1132FFC8";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "AAD6F37F-3D44-DFC7-0C48-0B83B10D8387";
	setAttr ".g" yes;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "E72F612B-7E4B-C6BE-F32D-42BCC90E63B4";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "B3BF9684-2848-206D-D787-94AA180B4AAE";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "4412ADE0-6B44-6C07-1A83-348E82746DB9";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 1\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1348\n            -height 599\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 1\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1348\n            -height 599\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n"
		+ "            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n"
		+ "            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n"
		+ "            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n"
		+ "                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n"
		+ "                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n"
		+ "                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n"
		+ "                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n"
		+ "                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n"
		+ "                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n"
		+ "                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n"
		+ "                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -highlightConnections 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n"
		+ "                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1348\\n    -height 599\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1348\\n    -height 599\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "A212B8CB-D346-2104-25CD-4DADCD82B2FE";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "61B5A76C-6047-8D07-8131-278495F72920";
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "7B4F2D37-044F-9476-8A82-5CB5CD002EDE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 3.2692591125726125 0 0 0 0 1 0 0 3.1075734376698172 0.046381693099549315 1;
	setAttr ".wt" 0.58926600217819214;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 15;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "153D06CB-5446-801C-E2D4-FE96A18BB959";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[620:621]" "e[623]" "e[625]" "e[627]" "e[629]" "e[631]" "e[633]" "e[635]" "e[637]" "e[639]" "e[641]" "e[643]" "e[645]" "e[647]" "e[649]" "e[651]" "e[653]" "e[655]" "e[657]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 3.2692591125726125 0 0 0 0 1 0 0 3.1075734376698172 0.046381693099549315 1;
	setAttr ".wt" 0.50699669122695923;
	setAttr ".re" 620;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 15;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "A053F463-BD4C-E87A-F256-29BECC8962A1";
	setAttr ".uopa" yes;
	setAttr -s 340 ".tk";
	setAttr ".tk[0]" -type "float3" 3.9581209e-09 -2.910383e-11 1.6880222e-09 ;
	setAttr ".tk[1]" -type "float3" -1.6298145e-09 -2.910383e-11 -1.1641532e-10 ;
	setAttr ".tk[2]" -type "float3" 1.9790605e-09 -2.910383e-11 3.3760443e-09 ;
	setAttr ".tk[3]" -type "float3" -1.7462298e-10 -2.910383e-11 -1.1641532e-09 ;
	setAttr ".tk[4]" -type "float3" -5.5511151e-17 -2.910383e-11 -6.9849193e-10 ;
	setAttr ".tk[5]" -type "float3" 1.6880222e-09 -2.910383e-11 2.3283064e-10 ;
	setAttr ".tk[6]" -type "float3" -4.0745363e-10 -2.910383e-11 1.7462298e-09 ;
	setAttr ".tk[7]" -type "float3" 1.7462298e-09 -2.910383e-11 1.1641532e-10 ;
	setAttr ".tk[8]" -type "float3" 3.0267984e-09 -2.910383e-11 2.3865141e-09 ;
	setAttr ".tk[9]" -type "float3" 2.0954758e-09 -2.910383e-11 -5.5511151e-17 ;
	setAttr ".tk[10]" -type "float3" 3.0267984e-09 -2.910383e-11 -6.9849193e-10 ;
	setAttr ".tk[11]" -type "float3" 2.5611371e-09 -2.910383e-11 1.0477379e-09 ;
	setAttr ".tk[12]" -type "float3" 1.1641532e-10 -2.910383e-11 -3.3760443e-09 ;
	setAttr ".tk[13]" -type "float3" 3.4924597e-10 -2.910383e-11 1.1641532e-09 ;
	setAttr ".tk[14]" -type "float3" -2.7755576e-17 -2.910383e-11 6.9849193e-10 ;
	setAttr ".tk[15]" -type "float3" -1.8044375e-09 -2.910383e-11 -2.3283064e-10 ;
	setAttr ".tk[16]" -type "float3" 4.0745363e-10 -2.910383e-11 -1.6298145e-09 ;
	setAttr ".tk[17]" -type "float3" -1.7462298e-09 -2.910383e-11 5.8207661e-11 ;
	setAttr ".tk[18]" -type "float3" -3.0267984e-09 -2.910383e-11 -2.3865141e-09 ;
	setAttr ".tk[19]" -type "float3" -2.0954758e-09 -2.910383e-11 -5.5511151e-17 ;
	setAttr ".tk[20]" -type "float3" 0.010486613 0.0021278872 -0.0034073049 ;
	setAttr ".tk[21]" -type "float3" 0.008920447 0.0021278872 -0.0064810794 ;
	setAttr ".tk[22]" -type "float3" 0.006481085 0.0021278872 -0.0089204451 ;
	setAttr ".tk[23]" -type "float3" 0.0034073084 0.0021278872 -0.01048661 ;
	setAttr ".tk[24]" -type "float3" 1.3144338e-09 0.0021278872 -0.01102627 ;
	setAttr ".tk[25]" -type "float3" -0.0034073053 0.0021278872 -0.01048661 ;
	setAttr ".tk[26]" -type "float3" -0.0064810794 0.0021278872 -0.0089204405 ;
	setAttr ".tk[27]" -type "float3" -0.0089204414 0.0021278872 -0.0064810771 ;
	setAttr ".tk[28]" -type "float3" -0.010486609 0.0021278872 -0.0034073042 ;
	setAttr ".tk[29]" -type "float3" -0.011026269 0.0021278872 2.1092386e-09 ;
	setAttr ".tk[30]" -type "float3" -0.010486609 0.0021278872 0.0034073084 ;
	setAttr ".tk[31]" -type "float3" -0.0089204405 0.0021278872 0.0064810836 ;
	setAttr ".tk[32]" -type "float3" -0.0064810771 0.0021278872 0.0089204451 ;
	setAttr ".tk[33]" -type "float3" -0.0034073044 0.0021278872 0.010486611 ;
	setAttr ".tk[34]" -type "float3" 9.8582542e-10 0.0021278872 0.011026271 ;
	setAttr ".tk[35]" -type "float3" 0.0034073074 0.0021278872 0.01048661 ;
	setAttr ".tk[36]" -type "float3" 0.0064810794 0.0021278872 0.0089204451 ;
	setAttr ".tk[37]" -type "float3" 0.0089204414 0.0021278872 0.0064810817 ;
	setAttr ".tk[38]" -type "float3" 0.010486609 0.0021278872 0.0034073081 ;
	setAttr ".tk[39]" -type "float3" 0.011026269 0.0021278872 2.1092386e-09 ;
	setAttr ".tk[42]" -type "float3" 0.12808457 0.0046818177 2.3019313e-08 ;
	setAttr ".tk[43]" -type "float3" 0.1218157 0.0046818177 0.039580319 ;
	setAttr ".tk[44]" -type "float3" 0.10362257 0.0046818177 0.075286239 ;
	setAttr ".tk[45]" -type "float3" 0.075286232 0.0046818177 0.10362263 ;
	setAttr ".tk[46]" -type "float3" 0.039580315 0.0046818177 0.1218157 ;
	setAttr ".tk[47]" -type "float3" 1.1451653e-08 0.0046818177 0.12808459 ;
	setAttr ".tk[48]" -type "float3" -0.039580308 0.0046818177 0.12181571 ;
	setAttr ".tk[49]" -type "float3" -0.07528621 0.0046818177 0.10362263 ;
	setAttr ".tk[50]" -type "float3" -0.10362256 0.0046818177 0.075286254 ;
	setAttr ".tk[51]" -type "float3" -0.1218157 0.0046818177 0.039580338 ;
	setAttr ".tk[52]" -type "float3" -0.12808457 0.0046818177 2.3019313e-08 ;
	setAttr ".tk[53]" -type "float3" -0.1218157 0.0046818177 -0.039580289 ;
	setAttr ".tk[54]" -type "float3" -0.10362257 0.0046818177 -0.07528621 ;
	setAttr ".tk[55]" -type "float3" -0.075286232 0.0046818177 -0.10362257 ;
	setAttr ".tk[56]" -type "float3" -0.039580312 0.0046818177 -0.1218157 ;
	setAttr ".tk[57]" -type "float3" 1.5268869e-08 0.0046818177 -0.12808459 ;
	setAttr ".tk[58]" -type "float3" 0.039580338 0.0046818177 -0.1218157 ;
	setAttr ".tk[59]" -type "float3" 0.075286277 0.0046818177 -0.10362263 ;
	setAttr ".tk[60]" -type "float3" 0.10362267 0.0046818177 -0.075286232 ;
	setAttr ".tk[61]" -type "float3" 0.12181576 0.0046818177 -0.039580312 ;
	setAttr ".tk[62]" -type "float3" 0.13306721 0.014442976 2.3093316e-08 ;
	setAttr ".tk[63]" -type "float3" 0.1265544 0.014442976 0.041120023 ;
	setAttr ".tk[64]" -type "float3" 0.1076536 0.014442976 0.078214958 ;
	setAttr ".tk[65]" -type "float3" 0.078214943 0.014442976 0.10765365 ;
	setAttr ".tk[66]" -type "float3" 0.041120011 0.014442976 0.12655438 ;
	setAttr ".tk[67]" -type "float3" 1.1897134e-08 0.014442976 0.13306722 ;
	setAttr ".tk[68]" -type "float3" -0.041120008 0.014442976 0.1265544 ;
	setAttr ".tk[69]" -type "float3" -0.078214929 0.014442976 0.10765365 ;
	setAttr ".tk[70]" -type "float3" -0.1076536 0.014442976 0.078214958 ;
	setAttr ".tk[71]" -type "float3" -0.1265544 0.014442976 0.041120026 ;
	setAttr ".tk[72]" -type "float3" -0.13306721 0.014442976 2.3093316e-08 ;
	setAttr ".tk[73]" -type "float3" -0.1265544 0.014442976 -0.041120004 ;
	setAttr ".tk[74]" -type "float3" -0.1076536 0.014442976 -0.078214943 ;
	setAttr ".tk[75]" -type "float3" -0.078214943 0.014442976 -0.10765364 ;
	setAttr ".tk[76]" -type "float3" -0.041120011 0.014442976 -0.12655437 ;
	setAttr ".tk[77]" -type "float3" 1.5862847e-08 0.014442976 -0.13306722 ;
	setAttr ".tk[78]" -type "float3" 0.04112003 0.014442976 -0.12655438 ;
	setAttr ".tk[79]" -type "float3" 0.078214981 0.014442976 -0.10765364 ;
	setAttr ".tk[80]" -type "float3" 0.10765371 0.014442976 -0.078214951 ;
	setAttr ".tk[81]" -type "float3" 0.12655444 0.014442976 -0.041120008 ;
	setAttr ".tk[82]" -type "float3" 0.22178212 0.025345081 4.7834888e-08 ;
	setAttr ".tk[83]" -type "float3" 0.21092738 0.025345081 0.068534538 ;
	setAttr ".tk[84]" -type "float3" 0.17942546 0.025345081 0.13036035 ;
	setAttr ".tk[85]" -type "float3" 0.13036034 0.025345081 0.17942549 ;
	setAttr ".tk[86]" -type "float3" 0.068534508 0.025345081 0.21092749 ;
	setAttr ".tk[87]" -type "float3" 1.9828848e-08 0.025345081 0.22178219 ;
	setAttr ".tk[88]" -type "float3" -0.068534456 0.025345081 0.21092743 ;
	setAttr ".tk[89]" -type "float3" -0.13036022 0.025345081 0.17942551 ;
	setAttr ".tk[90]" -type "float3" -0.17942545 0.025345081 0.13036035 ;
	setAttr ".tk[91]" -type "float3" -0.21092738 0.025345081 0.068534531 ;
	setAttr ".tk[92]" -type "float3" -0.22178212 0.025345081 4.7834888e-08 ;
	setAttr ".tk[93]" -type "float3" -0.21092738 0.025345081 -0.068534456 ;
	setAttr ".tk[94]" -type "float3" -0.17942546 0.025345081 -0.1303602 ;
	setAttr ".tk[95]" -type "float3" -0.13036034 0.025345081 -0.17942546 ;
	setAttr ".tk[96]" -type "float3" -0.068534464 0.025345081 -0.21092738 ;
	setAttr ".tk[97]" -type "float3" 2.6438494e-08 0.025345081 -0.22178219 ;
	setAttr ".tk[98]" -type "float3" 0.068534531 0.025345081 -0.21092743 ;
	setAttr ".tk[99]" -type "float3" 0.13036036 0.025345081 -0.17942551 ;
	setAttr ".tk[100]" -type "float3" 0.17942555 0.025345081 -0.13036032 ;
	setAttr ".tk[101]" -type "float3" 0.2109275 0.025345081 -0.068534456 ;
	setAttr ".tk[102]" -type "float3" 0.49919152 0.074926563 1.0576256e-07 ;
	setAttr ".tk[103]" -type "float3" 0.47475955 0.074926563 0.15425885 ;
	setAttr ".tk[104]" -type "float3" 0.40385482 0.074926563 0.29341754 ;
	setAttr ".tk[105]" -type "float3" 0.29341751 0.074926563 0.40385482 ;
	setAttr ".tk[106]" -type "float3" 0.1542587 0.074926563 0.47475955 ;
	setAttr ".tk[107]" -type "float3" 4.4631186e-08 0.074926563 0.49919158 ;
	setAttr ".tk[108]" -type "float3" -0.15425865 0.074926563 0.47475958 ;
	setAttr ".tk[109]" -type "float3" -0.29341751 0.074926563 0.40385485 ;
	setAttr ".tk[110]" -type "float3" -0.40385479 0.074926563 0.29341754 ;
	setAttr ".tk[111]" -type "float3" -0.47475958 0.074926563 0.15425886 ;
	setAttr ".tk[112]" -type "float3" -0.49919152 0.074926563 1.0576256e-07 ;
	setAttr ".tk[113]" -type "float3" -0.47475958 0.074926563 -0.15425865 ;
	setAttr ".tk[114]" -type "float3" -0.40385482 0.074926563 -0.29341751 ;
	setAttr ".tk[115]" -type "float3" -0.29341751 0.074926563 -0.40385476 ;
	setAttr ".tk[116]" -type "float3" -0.15425868 0.074926563 -0.47475955 ;
	setAttr ".tk[117]" -type "float3" 5.950826e-08 0.074926563 -0.49919158 ;
	setAttr ".tk[118]" -type "float3" 0.15425871 0.074926563 -0.47475961 ;
	setAttr ".tk[119]" -type "float3" 0.29341754 0.074926563 -0.40385482 ;
	setAttr ".tk[120]" -type "float3" 0.40385482 0.074926563 -0.29341751 ;
	setAttr ".tk[121]" -type "float3" 0.47475961 0.074926563 -0.15425865 ;
	setAttr ".tk[122]" -type "float3" 0.72427541 0.15166046 1.5268749e-07 ;
	setAttr ".tk[123]" -type "float3" 0.68882686 0.15166046 0.22381368 ;
	setAttr ".tk[124]" -type "float3" 0.58595103 0.15166046 0.42571813 ;
	setAttr ".tk[125]" -type "float3" 0.42571807 0.15166046 0.58595097 ;
	setAttr ".tk[126]" -type "float3" 0.22381374 0.15166046 0.68882686 ;
	setAttr ".tk[127]" -type "float3" 6.4755149e-08 0.15166046 0.72427529 ;
	setAttr ".tk[128]" -type "float3" -0.22381303 0.15166046 0.6888268 ;
	setAttr ".tk[129]" -type "float3" -0.42571804 0.15166046 0.58595115 ;
	setAttr ".tk[130]" -type "float3" -0.58595085 0.15166046 0.42571813 ;
	setAttr ".tk[131]" -type "float3" -0.6888268 0.15166046 0.22381374 ;
	setAttr ".tk[132]" -type "float3" -0.72427541 0.15166046 1.5268749e-07 ;
	setAttr ".tk[133]" -type "float3" -0.6888268 0.15166046 -0.22381301 ;
	setAttr ".tk[134]" -type "float3" -0.58595103 0.15166046 -0.42571804 ;
	setAttr ".tk[135]" -type "float3" -0.42571807 0.15166046 -0.58595097 ;
	setAttr ".tk[136]" -type "float3" -0.2238137 0.15166046 -0.68882686 ;
	setAttr ".tk[137]" -type "float3" 8.63403e-08 0.15166046 -0.72427541 ;
	setAttr ".tk[138]" -type "float3" 0.2238137 0.15166046 -0.6888268 ;
	setAttr ".tk[139]" -type "float3" 0.42571819 0.15166046 -0.58595103 ;
	setAttr ".tk[140]" -type "float3" 0.58595097 0.15166046 -0.42571807 ;
	setAttr ".tk[141]" -type "float3" 0.68882698 0.15166046 -0.22381301 ;
	setAttr ".tk[142]" -type "float3" 0.97235256 0.18061662 2.0544292e-07 ;
	setAttr ".tk[143]" -type "float3" 0.92476177 0.18061662 0.30047405 ;
	setAttr ".tk[144]" -type "float3" 0.78664875 0.18061662 0.57153642 ;
	setAttr ".tk[145]" -type "float3" 0.57153648 0.18061662 0.78664875 ;
	setAttr ".tk[146]" -type "float3" 0.30047402 0.18061662 0.92476189 ;
	setAttr ".tk[147]" -type "float3" 8.6935074e-08 0.18061662 0.97235256 ;
	setAttr ".tk[148]" -type "float3" -0.30047342 0.18061662 0.92476189 ;
	setAttr ".tk[149]" -type "float3" -0.5715366 0.18061662 0.78664875 ;
	setAttr ".tk[150]" -type "float3" -0.78664887 0.18061662 0.57153618 ;
	setAttr ".tk[151]" -type "float3" -0.92476201 0.18061662 0.30047411 ;
	setAttr ".tk[152]" -type "float3" -0.97235256 0.18061662 2.0544292e-07 ;
	setAttr ".tk[153]" -type "float3" -0.92476201 0.18061662 -0.30047336 ;
	setAttr ".tk[154]" -type "float3" -0.78664923 0.18061662 -0.57153624 ;
	setAttr ".tk[155]" -type "float3" -0.57153648 0.18061662 -0.78664875 ;
	setAttr ".tk[156]" -type "float3" -0.30047354 0.18061662 -0.92476201 ;
	setAttr ".tk[157]" -type "float3" 1.1591347e-07 0.18061662 -0.97235256 ;
	setAttr ".tk[158]" -type "float3" 0.30047402 0.18061662 -0.92476201 ;
	setAttr ".tk[159]" -type "float3" 0.57153672 0.18061662 -0.78664923 ;
	setAttr ".tk[160]" -type "float3" 0.78665 0.18061662 -0.57153618 ;
	setAttr ".tk[161]" -type "float3" 0.9247632 0.18061662 -0.30047345 ;
	setAttr ".tk[162]" -type "float3" 1.2486513 0.10473894 2.606522e-07 ;
	setAttr ".tk[163]" -type "float3" 1.1875383 0.10473894 0.38585541 ;
	setAttr ".tk[164]" -type "float3" 1.0101799 0.10473894 0.7339384 ;
	setAttr ".tk[165]" -type "float3" 0.73393798 0.10473894 1.0101801 ;
	setAttr ".tk[166]" -type "float3" 0.38585493 0.10473894 1.1875383 ;
	setAttr ".tk[167]" -type "float3" 1.1163793e-07 0.10473894 1.2486519 ;
	setAttr ".tk[168]" -type "float3" -0.38585499 0.10473894 1.1875383 ;
	setAttr ".tk[169]" -type "float3" -0.73393798 0.10473894 1.0101801 ;
	setAttr ".tk[170]" -type "float3" -1.01018 0.10473894 0.73393846 ;
	setAttr ".tk[171]" -type "float3" -1.1875366 0.10473894 0.38585544 ;
	setAttr ".tk[172]" -type "float3" -1.2486513 0.10473894 2.606522e-07 ;
	setAttr ".tk[173]" -type "float3" -1.1875366 0.10473894 -0.38585502 ;
	setAttr ".tk[174]" -type "float3" -1.0101799 0.10473894 -0.73393804 ;
	setAttr ".tk[175]" -type "float3" -0.73393798 0.10473894 -1.0101801 ;
	setAttr ".tk[176]" -type "float3" -0.38585481 0.10473894 -1.1875389 ;
	setAttr ".tk[177]" -type "float3" 1.488508e-07 0.10473894 -1.2486522 ;
	setAttr ".tk[178]" -type "float3" 0.38585514 0.10473894 -1.1875387 ;
	setAttr ".tk[179]" -type "float3" 0.73393869 0.10473894 -1.0101801 ;
	setAttr ".tk[180]" -type "float3" 1.0101805 0.10473894 -0.73393822 ;
	setAttr ".tk[181]" -type "float3" 1.1875393 0.10473894 -0.38585484 ;
	setAttr ".tk[182]" -type "float3" 1.2297754 -0.033686556 2.462092e-07 ;
	setAttr ".tk[183]" -type "float3" 1.1695857 -0.033686556 0.38002095 ;
	setAttr ".tk[184]" -type "float3" 0.99490774 -0.033686556 0.72284389 ;
	setAttr ".tk[185]" -type "float3" 0.72284436 -0.033686556 0.9949075 ;
	setAttr ".tk[186]" -type "float3" 0.38002083 -0.033686556 1.1695852 ;
	setAttr ".tk[187]" -type "float3" 1.0947481e-07 -0.033686556 1.2297753 ;
	setAttr ".tk[188]" -type "float3" -0.38002065 -0.033686556 1.1695852 ;
	setAttr ".tk[189]" -type "float3" -0.72284377 -0.033686556 0.9949075 ;
	setAttr ".tk[190]" -type "float3" -0.99490714 -0.033686556 0.72284424 ;
	setAttr ".tk[191]" -type "float3" -1.1695851 -0.033686556 0.38002074 ;
	setAttr ".tk[192]" -type "float3" -1.2297752 -0.033686556 2.462092e-07 ;
	setAttr ".tk[193]" -type "float3" -1.1695851 -0.033686556 -0.38002047 ;
	setAttr ".tk[194]" -type "float3" -0.99490786 -0.033686556 -0.72284353 ;
	setAttr ".tk[195]" -type "float3" -0.72284436 -0.033686556 -0.99490774 ;
	setAttr ".tk[196]" -type "float3" -0.38002071 -0.033686556 -1.1695861 ;
	setAttr ".tk[197]" -type "float3" 1.46125e-07 -0.033686556 -1.2297754 ;
	setAttr ".tk[198]" -type "float3" 0.38002077 -0.033686556 -1.1695855 ;
	setAttr ".tk[199]" -type "float3" 0.72284448 -0.033686556 -0.99490762 ;
	setAttr ".tk[200]" -type "float3" 0.99490833 -0.033686556 -0.72284406 ;
	setAttr ".tk[201]" -type "float3" 1.1695855 -0.033686556 -0.38002068 ;
	setAttr ".tk[202]" -type "float3" 0.97339141 -0.1233899 1.8563871e-07 ;
	setAttr ".tk[203]" -type "float3" 0.92575139 -0.1233899 0.30079448 ;
	setAttr ".tk[204]" -type "float3" 0.78749126 -0.1233899 0.57214516 ;
	setAttr ".tk[205]" -type "float3" 0.57214481 -0.1233899 0.7874915 ;
	setAttr ".tk[206]" -type "float3" 0.30079406 -0.1233899 0.92575139 ;
	setAttr ".tk[207]" -type "float3" 8.7028099e-08 -0.1233899 0.97339201 ;
	setAttr ".tk[208]" -type "float3" -0.30079412 -0.1233899 0.92575139 ;
	setAttr ".tk[209]" -type "float3" -0.57214475 -0.1233899 0.7874915 ;
	setAttr ".tk[210]" -type "float3" -0.78749138 -0.1233899 0.57214516 ;
	setAttr ".tk[211]" -type "float3" -0.92575091 -0.1233899 0.30079448 ;
	setAttr ".tk[212]" -type "float3" -0.97339141 -0.1233899 1.8563871e-07 ;
	setAttr ".tk[213]" -type "float3" -0.92575091 -0.1233899 -0.30079415 ;
	setAttr ".tk[214]" -type "float3" -0.78749132 -0.1233899 -0.57214487 ;
	setAttr ".tk[215]" -type "float3" -0.57214481 -0.1233899 -0.7874915 ;
	setAttr ".tk[216]" -type "float3" -0.30079398 -0.1233899 -0.92575192 ;
	setAttr ".tk[217]" -type "float3" 1.1603727e-07 -0.1233899 -0.97339237 ;
	setAttr ".tk[218]" -type "float3" 0.30079421 -0.1233899 -0.92575186 ;
	setAttr ".tk[219]" -type "float3" 0.5721454 -0.1233899 -0.78749156 ;
	setAttr ".tk[220]" -type "float3" 0.78749222 -0.1233899 -0.57214499 ;
	setAttr ".tk[221]" -type "float3" 0.92575258 -0.1233899 -0.30079401 ;
	setAttr ".tk[222]" -type "float3" 0.62719691 -0.15679923 1.1215153e-07 ;
	setAttr ".tk[223]" -type "float3" 0.59649885 -0.15679923 0.19381453 ;
	setAttr ".tk[224]" -type "float3" 0.50741237 -0.15679923 0.36865732 ;
	setAttr ".tk[225]" -type "float3" 0.36865762 -0.15679923 0.50741237 ;
	setAttr ".tk[226]" -type "float3" 0.1938145 -0.15679923 0.59649885 ;
	setAttr ".tk[227]" -type "float3" 5.6075766e-08 -0.15679923 0.62719691 ;
	setAttr ".tk[228]" -type "float3" -0.19381434 -0.15679923 0.59649885 ;
	setAttr ".tk[229]" -type "float3" -0.36865774 -0.15679923 0.50741237 ;
	setAttr ".tk[230]" -type "float3" -0.50741249 -0.15679923 0.36865732 ;
	setAttr ".tk[231]" -type "float3" -0.59649897 -0.15679923 0.19381459 ;
	setAttr ".tk[232]" -type "float3" -0.62719691 -0.15679923 1.1215153e-07 ;
	setAttr ".tk[233]" -type "float3" -0.59649897 -0.15679923 -0.19381426 ;
	setAttr ".tk[234]" -type "float3" -0.50741285 -0.15679923 -0.36865741 ;
	setAttr ".tk[235]" -type "float3" -0.36865762 -0.15679923 -0.50741237 ;
	setAttr ".tk[236]" -type "float3" -0.19381446 -0.15679923 -0.59649897 ;
	setAttr ".tk[237]" -type "float3" 7.4767705e-08 -0.15679923 -0.62719691 ;
	setAttr ".tk[238]" -type "float3" 0.1938145 -0.15679923 -0.59649891 ;
	setAttr ".tk[239]" -type "float3" 0.36865786 -0.15679923 -0.50741285 ;
	setAttr ".tk[240]" -type "float3" 0.50741327 -0.15679923 -0.36865732 ;
	setAttr ".tk[241]" -type "float3" 0.5965001 -0.15679923 -0.19381435 ;
	setAttr ".tk[242]" -type "float3" 0.41304111 -0.12624192 7.3857372e-08 ;
	setAttr ".tk[243]" -type "float3" 0.39282539 -0.12624192 0.12763666 ;
	setAttr ".tk[244]" -type "float3" 0.33415726 -0.12624192 0.24277928 ;
	setAttr ".tk[245]" -type "float3" 0.24277923 -0.12624192 0.33415726 ;
	setAttr ".tk[246]" -type "float3" 0.12763667 -0.12624192 0.39282539 ;
	setAttr ".tk[247]" -type "float3" 3.6928686e-08 -0.12624192 0.413041 ;
	setAttr ".tk[248]" -type "float3" -0.12763661 -0.12624192 0.39282539 ;
	setAttr ".tk[249]" -type "float3" -0.24277918 -0.12624192 0.33415726 ;
	setAttr ".tk[250]" -type "float3" -0.33415714 -0.12624192 0.24277928 ;
	setAttr ".tk[251]" -type "float3" -0.39282537 -0.12624192 0.1276367 ;
	setAttr ".tk[252]" -type "float3" -0.41304111 -0.12624192 7.3857372e-08 ;
	setAttr ".tk[253]" -type "float3" -0.39282537 -0.12624192 -0.1276366 ;
	setAttr ".tk[254]" -type "float3" -0.33415726 -0.12624192 -0.24277918 ;
	setAttr ".tk[255]" -type "float3" -0.24277923 -0.12624192 -0.33415726 ;
	setAttr ".tk[256]" -type "float3" -0.12763666 -0.12624192 -0.39282537 ;
	setAttr ".tk[257]" -type "float3" 4.9238285e-08 -0.12624192 -0.41304114 ;
	setAttr ".tk[258]" -type "float3" 0.12763667 -0.12624192 -0.39282539 ;
	setAttr ".tk[259]" -type "float3" 0.24277933 -0.12624192 -0.33415732 ;
	setAttr ".tk[260]" -type "float3" 0.33415723 -0.12624192 -0.24277923 ;
	setAttr ".tk[261]" -type "float3" 0.39282596 -0.12624192 -0.1276366 ;
	setAttr ".tk[262]" -type "float3" 0.25726572 -0.053385865 4.6002658e-08 ;
	setAttr ".tk[263]" -type "float3" 0.24467421 -0.053385865 0.079499453 ;
	setAttr ".tk[264]" -type "float3" 0.20813245 -0.053385865 0.15121695 ;
	setAttr ".tk[265]" -type "float3" 0.15121697 -0.053385865 0.20813245 ;
	setAttr ".tk[266]" -type "float3" 0.079499461 -0.053385865 0.24467421 ;
	setAttr ".tk[267]" -type "float3" 2.3001329e-08 -0.053385865 0.25726575 ;
	setAttr ".tk[268]" -type "float3" -0.079499438 -0.053385865 0.24467421 ;
	setAttr ".tk[269]" -type "float3" -0.15121697 -0.053385865 0.20813245 ;
	setAttr ".tk[270]" -type "float3" -0.20813227 -0.053385865 0.15121695 ;
	setAttr ".tk[271]" -type "float3" -0.24467421 -0.053385865 0.079499461 ;
	setAttr ".tk[272]" -type "float3" -0.25726572 -0.053385865 4.6002658e-08 ;
	setAttr ".tk[273]" -type "float3" -0.24467421 -0.053385865 -0.079499431 ;
	setAttr ".tk[274]" -type "float3" -0.20813245 -0.053385865 -0.15121694 ;
	setAttr ".tk[275]" -type "float3" -0.15121697 -0.053385865 -0.20813243 ;
	setAttr ".tk[276]" -type "float3" -0.079499453 -0.053385865 -0.24467421 ;
	setAttr ".tk[277]" -type "float3" 3.0668456e-08 -0.053385865 -0.25726575 ;
	setAttr ".tk[278]" -type "float3" 0.079499476 -0.053385865 -0.24467424 ;
	setAttr ".tk[279]" -type "float3" 0.15121706 -0.053385865 -0.20813245 ;
	setAttr ".tk[280]" -type "float3" 0.20813248 -0.053385865 -0.15121697 ;
	setAttr ".tk[281]" -type "float3" 0.24467424 -0.053385865 -0.079499438 ;
	setAttr ".tk[282]" -type "float3" 0.020185696 -0.0075071454 3.6094845e-09 ;
	setAttr ".tk[283]" -type "float3" 0.019197725 -0.0075071454 0.0062377299 ;
	setAttr ".tk[284]" -type "float3" 0.016330579 -0.0075071454 0.011864861 ;
	setAttr ".tk[285]" -type "float3" 0.011864853 -0.0075071454 0.016330585 ;
	setAttr ".tk[286]" -type "float3" 0.0062377267 -0.0075071454 0.019197732 ;
	setAttr ".tk[287]" -type "float3" 1.8047422e-09 -0.0075071454 0.020185698 ;
	setAttr ".tk[288]" -type "float3" -0.0062377201 -0.0075071454 0.019197742 ;
	setAttr ".tk[289]" -type "float3" -0.011864849 -0.0075071454 0.016330585 ;
	setAttr ".tk[290]" -type "float3" -0.016330579 -0.0075071454 0.011864861 ;
	setAttr ".tk[291]" -type "float3" -0.019197725 -0.0075071454 0.0062377257 ;
	setAttr ".tk[292]" -type "float3" -0.020185696 -0.0075071454 3.6094845e-09 ;
	setAttr ".tk[293]" -type "float3" -0.019197725 -0.0075071454 -0.0062377225 ;
	setAttr ".tk[294]" -type "float3" -0.016330581 -0.0075071454 -0.011864849 ;
	setAttr ".tk[295]" -type "float3" -0.011864853 -0.0075071454 -0.016330579 ;
	setAttr ".tk[296]" -type "float3" -0.0062377183 -0.0075071454 -0.019197732 ;
	setAttr ".tk[297]" -type "float3" 2.4063214e-09 -0.0075071454 -0.020185698 ;
	setAttr ".tk[298]" -type "float3" 0.006237736 -0.0075071454 -0.01919774 ;
	setAttr ".tk[299]" -type "float3" 0.011864867 -0.0075071454 -0.016330585 ;
	setAttr ".tk[300]" -type "float3" 0.016330589 -0.0075071454 -0.011864861 ;
	setAttr ".tk[301]" -type "float3" 0.019197758 -0.0075071454 -0.0062377197 ;
	setAttr ".tk[302]" -type "float3" -0.089178376 0.0002437794 -1.5946336e-08 ;
	setAttr ".tk[303]" -type "float3" -0.084813707 0.0002437794 -0.027557649 ;
	setAttr ".tk[304]" -type "float3" -0.072146818 0.0002437794 -0.052417755 ;
	setAttr ".tk[305]" -type "float3" -0.052417759 0.0002437794 -0.072146863 ;
	setAttr ".tk[306]" -type "float3" -0.027557636 0.0002437794 -0.084813707 ;
	setAttr ".tk[307]" -type "float3" -7.9731679e-09 0.0002437794 -0.089178376 ;
	setAttr ".tk[308]" -type "float3" 0.027557617 0.0002437794 -0.084813707 ;
	setAttr ".tk[309]" -type "float3" 0.052417748 0.0002437794 -0.072146863 ;
	setAttr ".tk[310]" -type "float3" 0.072146818 0.0002437794 -0.052417755 ;
	setAttr ".tk[311]" -type "float3" 0.084813692 0.0002437794 -0.027557652 ;
	setAttr ".tk[312]" -type "float3" 0.089178376 0.0002437794 -1.5946336e-08 ;
	setAttr ".tk[313]" -type "float3" 0.084813692 0.0002437794 0.027557615 ;
	setAttr ".tk[314]" -type "float3" 0.072146833 0.0002437794 0.052417751 ;
	setAttr ".tk[315]" -type "float3" 0.052417759 0.0002437794 0.072146818 ;
	setAttr ".tk[316]" -type "float3" 0.027557636 0.0002437794 0.084813707 ;
	setAttr ".tk[317]" -type "float3" -1.063089e-08 0.0002437794 0.089178376 ;
	setAttr ".tk[318]" -type "float3" -0.027557652 0.0002437794 0.084813707 ;
	setAttr ".tk[319]" -type "float3" -0.052417781 0.0002437794 0.07214684 ;
	setAttr ".tk[320]" -type "float3" -0.07214687 0.0002437794 0.052417759 ;
	setAttr ".tk[321]" -type "float3" -0.084813744 0.0002437794 0.027557632 ;
	setAttr ".tk[322]" -type "float3" -0.014524914 0.0018191568 -2.5972557e-09 ;
	setAttr ".tk[323]" -type "float3" -0.013814038 0.0018191568 -0.0044884477 ;
	setAttr ".tk[324]" -type "float3" -0.011750903 0.0018191568 -0.008537584 ;
	setAttr ".tk[325]" -type "float3" -0.0085375439 0.0018191568 -0.011750923 ;
	setAttr ".tk[326]" -type "float3" -0.0044884514 0.0018191568 -0.013814034 ;
	setAttr ".tk[327]" -type "float3" -1.2986279e-09 0.0018191568 -0.014524918 ;
	setAttr ".tk[328]" -type "float3" 0.0044884551 0.0018191568 -0.01381404 ;
	setAttr ".tk[329]" -type "float3" 0.0085375477 0.0018191568 -0.011750923 ;
	setAttr ".tk[330]" -type "float3" 0.011750903 0.0018191568 -0.008537584 ;
	setAttr ".tk[331]" -type "float3" 0.013814063 0.0018191568 -0.0044884486 ;
	setAttr ".tk[332]" -type "float3" 0.014524914 0.0018191568 -2.5972557e-09 ;
	setAttr ".tk[333]" -type "float3" 0.013814063 0.0018191568 0.0044884537 ;
	setAttr ".tk[334]" -type "float3" 0.011750937 0.0018191568 0.0085375477 ;
	setAttr ".tk[335]" -type "float3" 0.0085375439 0.0018191568 0.01175091 ;
	setAttr ".tk[336]" -type "float3" 0.0044884509 0.0018191568 0.013814034 ;
	setAttr ".tk[337]" -type "float3" -1.7315058e-09 0.0018191568 0.014524914 ;
	setAttr ".tk[338]" -type "float3" -0.0044884467 0.0018191568 0.013814016 ;
	setAttr ".tk[339]" -type "float3" -0.0085375411 0.0018191568 0.011750923 ;
	setAttr ".tk[340]" -type "float3" -0.011750923 0.0018191568 0.008537557 ;
	setAttr ".tk[341]" -type "float3" -0.01381403 0.0018191568 0.0044884509 ;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "D06588A4-6941-E5C2-63FC-BAB61C257E22";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[660:661]" "e[663]" "e[665]" "e[667]" "e[669]" "e[671]" "e[673]" "e[675]" "e[677]" "e[679]" "e[681]" "e[683]" "e[685]" "e[687]" "e[689]" "e[691]" "e[693]" "e[695]" "e[697]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 3.2692591125726125 0 0 0 0 1 0 0 3.1075734376698172 0.046381693099549315 1;
	setAttr ".wt" 0.4286942183971405;
	setAttr ".re" 660;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 15;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "0070803E-E44C-461F-55E6-D0BD7FD35F7E";
	setAttr ".uopa" yes;
	setAttr -s 141 ".tk";
	setAttr ".tk[0]" -type "float3" 0.10238548 -0.0096457005 -0.033267029 ;
	setAttr ".tk[1]" -type "float3" 0.087094299 -0.0096457005 -0.063277677 ;
	setAttr ".tk[2]" -type "float3" 0.063277714 -0.0096457005 -0.087094247 ;
	setAttr ".tk[3]" -type "float3" 0.033267058 -0.0096457005 -0.10238545 ;
	setAttr ".tk[4]" -type "float3" 1.2833404e-08 -0.0096457005 -0.10765444 ;
	setAttr ".tk[5]" -type "float3" -0.033267029 -0.0096457005 -0.10238543 ;
	setAttr ".tk[6]" -type "float3" -0.063277692 -0.0096457005 -0.087094232 ;
	setAttr ".tk[7]" -type "float3" -0.087094232 -0.0096457005 -0.063277669 ;
	setAttr ".tk[8]" -type "float3" -0.10238543 -0.0096457005 -0.033267021 ;
	setAttr ".tk[9]" -type "float3" -0.10765441 -0.0096457005 1.9250102e-08 ;
	setAttr ".tk[10]" -type "float3" -0.10238543 -0.0096457005 0.033267055 ;
	setAttr ".tk[11]" -type "float3" -0.087094232 -0.0096457005 0.063277699 ;
	setAttr ".tk[12]" -type "float3" -0.063277669 -0.0096457005 0.087094247 ;
	setAttr ".tk[13]" -type "float3" -0.033267025 -0.0096457005 0.10238545 ;
	setAttr ".tk[14]" -type "float3" 9.625051e-09 -0.0096457005 0.10765444 ;
	setAttr ".tk[15]" -type "float3" 0.033267044 -0.0096457005 0.10238543 ;
	setAttr ".tk[16]" -type "float3" 0.063277692 -0.0096457005 0.08709424 ;
	setAttr ".tk[17]" -type "float3" 0.087094232 -0.0096457005 0.063277677 ;
	setAttr ".tk[18]" -type "float3" 0.10238543 -0.0096457005 0.033267047 ;
	setAttr ".tk[19]" -type "float3" 0.10765441 -0.0096457005 1.9250102e-08 ;
	setAttr ".tk[20]" -type "float3" 0.23133832 -0.079859577 -0.075166322 ;
	setAttr ".tk[21]" -type "float3" 0.1967881 -0.079859577 -0.14297479 ;
	setAttr ".tk[22]" -type "float3" 0.14297494 -0.079859577 -0.19678798 ;
	setAttr ".tk[23]" -type "float3" 0.075166367 -0.079859577 -0.23133816 ;
	setAttr ".tk[24]" -type "float3" 2.8996869e-08 -0.079859577 -0.24324343 ;
	setAttr ".tk[25]" -type "float3" -0.075166322 -0.079859577 -0.23133813 ;
	setAttr ".tk[26]" -type "float3" -0.14297478 -0.079859577 -0.19678798 ;
	setAttr ".tk[27]" -type "float3" -0.19678798 -0.079859577 -0.14297478 ;
	setAttr ".tk[28]" -type "float3" -0.23133813 -0.079859577 -0.075166248 ;
	setAttr ".tk[29]" -type "float3" -0.24324337 -0.079859577 4.3528409e-08 ;
	setAttr ".tk[30]" -type "float3" -0.23133813 -0.079859577 0.07516636 ;
	setAttr ".tk[31]" -type "float3" -0.19678797 -0.079859577 0.14297491 ;
	setAttr ".tk[32]" -type "float3" -0.14297478 -0.079859577 0.19678798 ;
	setAttr ".tk[33]" -type "float3" -0.075166307 -0.079859577 0.23133816 ;
	setAttr ".tk[34]" -type "float3" 2.174764e-08 -0.079859577 0.24324343 ;
	setAttr ".tk[35]" -type "float3" 0.075166322 -0.079859577 0.23133813 ;
	setAttr ".tk[36]" -type "float3" 0.14297478 -0.079859577 0.19678798 ;
	setAttr ".tk[37]" -type "float3" 0.19678798 -0.079859577 0.1429749 ;
	setAttr ".tk[38]" -type "float3" 0.23133813 -0.079859577 0.07516636 ;
	setAttr ".tk[39]" -type "float3" 0.24324337 -0.079859577 4.3528409e-08 ;
	setAttr ".tk[41]" -type "float3" 2.8680628e-08 -0.080371402 4.3020925e-08 ;
	setAttr ".tk[282]" -type "float3" -0.014436734 -0.0007505453 -2.5814892e-09 ;
	setAttr ".tk[283]" -type "float3" -0.013730157 -0.0007505453 -0.0044612004 ;
	setAttr ".tk[284]" -type "float3" -0.011679566 -0.0007505453 -0.0084857047 ;
	setAttr ".tk[285]" -type "float3" -0.0084857028 -0.0007505453 -0.011679566 ;
	setAttr ".tk[286]" -type "float3" -0.0044611995 -0.0007505453 -0.013730157 ;
	setAttr ".tk[287]" -type "float3" -2.2028617e-09 -0.0007505453 -0.014436738 ;
	setAttr ".tk[288]" -type "float3" 0.0044611949 -0.0007505453 -0.013730154 ;
	setAttr ".tk[289]" -type "float3" 0.0084857 -0.0007505453 -0.011679566 ;
	setAttr ".tk[290]" -type "float3" 0.011679564 -0.0007505453 -0.0084857047 ;
	setAttr ".tk[291]" -type "float3" 0.013730153 -0.0007505453 -0.0044611986 ;
	setAttr ".tk[292]" -type "float3" 0.014436734 -0.0007505453 -2.5814892e-09 ;
	setAttr ".tk[293]" -type "float3" 0.013730153 -0.0007505453 0.0044611949 ;
	setAttr ".tk[294]" -type "float3" 0.011679566 -0.0007505453 0.0084857028 ;
	setAttr ".tk[295]" -type "float3" 0.0084857 -0.0007505453 0.011679566 ;
	setAttr ".tk[296]" -type "float3" 0.0044611976 -0.0007505453 0.013730157 ;
	setAttr ".tk[297]" -type "float3" -2.6331102e-09 -0.0007505453 0.014436738 ;
	setAttr ".tk[298]" -type "float3" -0.0044612023 -0.0007505453 0.013730156 ;
	setAttr ".tk[299]" -type "float3" -0.0084857084 -0.0007505453 0.011679568 ;
	setAttr ".tk[300]" -type "float3" -0.011679576 -0.0007505453 0.0084857028 ;
	setAttr ".tk[301]" -type "float3" -0.013730162 -0.0007505453 0.004461199 ;
	setAttr ".tk[302]" -type "float3" 0.049701121 0.0092911329 8.8872483e-09 ;
	setAttr ".tk[303]" -type "float3" 0.04726854 0.0092911329 0.015358495 ;
	setAttr ".tk[304]" -type "float3" 0.040209044 0.0092911329 0.029213592 ;
	setAttr ".tk[305]" -type "float3" 0.029213594 0.0092911329 0.04020907 ;
	setAttr ".tk[306]" -type "float3" 0.015358486 0.0092911329 0.047268551 ;
	setAttr ".tk[307]" -type "float3" 3.4965073e-09 0.0092911329 0.049701124 ;
	setAttr ".tk[308]" -type "float3" -0.015358482 0.0092911329 0.047268543 ;
	setAttr ".tk[309]" -type "float3" -0.029213598 0.0092911329 0.04020907 ;
	setAttr ".tk[310]" -type "float3" -0.040209044 0.0092911329 0.029213592 ;
	setAttr ".tk[311]" -type "float3" -0.047268532 0.0092911329 0.015358498 ;
	setAttr ".tk[312]" -type "float3" -0.049701121 0.0092911329 8.8872483e-09 ;
	setAttr ".tk[313]" -type "float3" -0.047268532 0.0092911329 -0.015358469 ;
	setAttr ".tk[314]" -type "float3" -0.04020907 0.0092911329 -0.029213598 ;
	setAttr ".tk[315]" -type "float3" -0.029213594 0.0092911329 -0.040209055 ;
	setAttr ".tk[316]" -type "float3" -0.015358485 0.0092911329 -0.047268551 ;
	setAttr ".tk[317]" -type "float3" 4.9777169e-09 0.0092911329 -0.049701124 ;
	setAttr ".tk[318]" -type "float3" 0.015358495 0.0092911329 -0.047268547 ;
	setAttr ".tk[319]" -type "float3" 0.029213611 0.0092911329 -0.040209055 ;
	setAttr ".tk[320]" -type "float3" 0.040209062 0.0092911329 -0.029213589 ;
	setAttr ".tk[321]" -type "float3" 0.047268599 0.0092911329 -0.015358482 ;
	setAttr ".tk[322]" -type "float3" 0.10671271 0.0039284742 1.9081721e-08 ;
	setAttr ".tk[323]" -type "float3" 0.10148986 0.0039284742 0.032976076 ;
	setAttr ".tk[324]" -type "float3" 0.086332411 0.0039284742 0.062724158 ;
	setAttr ".tk[325]" -type "float3" 0.062724181 0.0039284742 0.086332478 ;
	setAttr ".tk[326]" -type "float3" 0.032976057 0.0039284742 0.10148987 ;
	setAttr ".tk[327]" -type "float3" 9.5408605e-09 0.0039284742 0.10671271 ;
	setAttr ".tk[328]" -type "float3" -0.032976024 0.0039284742 0.10148984 ;
	setAttr ".tk[329]" -type "float3" -0.062724188 0.0039284742 0.086332478 ;
	setAttr ".tk[330]" -type "float3" -0.086332411 0.0039284742 0.062724158 ;
	setAttr ".tk[331]" -type "float3" -0.10148978 0.0039284742 0.032976091 ;
	setAttr ".tk[332]" -type "float3" -0.10671271 0.0039284742 1.9081721e-08 ;
	setAttr ".tk[333]" -type "float3" -0.10148978 0.0039284742 -0.032976024 ;
	setAttr ".tk[334]" -type "float3" -0.086332396 0.0039284742 -0.062724173 ;
	setAttr ".tk[335]" -type "float3" -0.062724181 0.0039284742 -0.086332411 ;
	setAttr ".tk[336]" -type "float3" -0.03297605 0.0039284742 -0.10148987 ;
	setAttr ".tk[337]" -type "float3" 1.2721149e-08 0.0039284742 -0.10671271 ;
	setAttr ".tk[338]" -type "float3" 0.032976083 0.0039284742 -0.10148988 ;
	setAttr ".tk[339]" -type "float3" 0.062724188 0.0039284742 -0.086332448 ;
	setAttr ".tk[340]" -type "float3" 0.086332455 0.0039284742 -0.062724151 ;
	setAttr ".tk[341]" -type "float3" 0.10148986 0.0039284742 -0.032976057 ;
	setAttr ".tk[342]" -type "float3" -0.011258791 0.0010933165 -2.0132294e-09 ;
	setAttr ".tk[343]" -type "float3" -0.010707753 0.0010933165 -0.003479158 ;
	setAttr ".tk[344]" -type "float3" -0.0091085564 0.0010933165 -0.006617751 ;
	setAttr ".tk[345]" -type "float3" -0.0066177468 0.0010933165 -0.0091085499 ;
	setAttr ".tk[346]" -type "float3" -0.0034791571 0.0010933165 -0.010707752 ;
	setAttr ".tk[347]" -type "float3" -2.4775861e-09 0.0010933165 -0.011258788 ;
	setAttr ".tk[348]" -type "float3" 0.0034791527 0.0010933165 -0.010707749 ;
	setAttr ".tk[349]" -type "float3" 0.0066177426 0.0010933165 -0.0091085499 ;
	setAttr ".tk[350]" -type "float3" 0.0091085527 0.0010933165 -0.006617751 ;
	setAttr ".tk[351]" -type "float3" 0.010707743 0.0010933165 -0.0034791585 ;
	setAttr ".tk[352]" -type "float3" 0.01125879 0.0010933165 -2.0132294e-09 ;
	setAttr ".tk[353]" -type "float3" 0.010707743 0.0010933165 0.0034791562 ;
	setAttr ".tk[354]" -type "float3" 0.0091085536 0.0010933165 0.0066177398 ;
	setAttr ".tk[355]" -type "float3" 0.0066177468 0.0010933165 0.0091085527 ;
	setAttr ".tk[356]" -type "float3" 0.0034791508 0.0010933165 0.010707752 ;
	setAttr ".tk[357]" -type "float3" -2.8131262e-09 0.0010933165 0.011258788 ;
	setAttr ".tk[358]" -type "float3" -0.0034791604 0.0010933165 0.010707757 ;
	setAttr ".tk[359]" -type "float3" -0.0066177528 0.0010933165 0.0091085592 ;
	setAttr ".tk[360]" -type "float3" -0.0091085583 0.0010933165 0.0066177463 ;
	setAttr ".tk[361]" -type "float3" -0.010707743 0.0010933165 0.0034791566 ;
	setAttr ".tk[362]" -type "float3" 0.11100958 0.0091127027 1.9850065e-08 ;
	setAttr ".tk[363]" -type "float3" 0.10557643 0.0091127027 0.034303874 ;
	setAttr ".tk[364]" -type "float3" 0.089808658 0.0091127027 0.065249875 ;
	setAttr ".tk[365]" -type "float3" 0.065249853 0.0091127027 0.089808673 ;
	setAttr ".tk[366]" -type "float3" 0.034303878 0.0091127027 0.10557646 ;
	setAttr ".tk[367]" -type "float3" 9.8884581e-09 0.0091127027 0.1110096 ;
	setAttr ".tk[368]" -type "float3" -0.034303881 0.0091127027 0.10557643 ;
	setAttr ".tk[369]" -type "float3" -0.065249808 0.0091127027 0.089808673 ;
	setAttr ".tk[370]" -type "float3" -0.089808658 0.0091127027 0.065249875 ;
	setAttr ".tk[371]" -type "float3" -0.10557649 0.0091127027 0.034303889 ;
	setAttr ".tk[372]" -type "float3" -0.11100958 0.0091127027 1.9850065e-08 ;
	setAttr ".tk[373]" -type "float3" -0.10557649 0.0091127027 -0.034303859 ;
	setAttr ".tk[374]" -type "float3" -0.089808673 0.0091127027 -0.065249793 ;
	setAttr ".tk[375]" -type "float3" -0.065249853 0.0091127027 -0.089808606 ;
	setAttr ".tk[376]" -type "float3" -0.034303878 0.0091127027 -0.10557647 ;
	setAttr ".tk[377]" -type "float3" 1.319681e-08 0.0091127027 -0.11100958 ;
	setAttr ".tk[378]" -type "float3" 0.034303889 0.0091127027 -0.10557646 ;
	setAttr ".tk[379]" -type "float3" 0.065249898 0.0091127027 -0.089808688 ;
	setAttr ".tk[380]" -type "float3" 0.089808777 0.0091127027 -0.065249845 ;
	setAttr ".tk[381]" -type "float3" 0.1055764 0.0091127027 -0.034303878 ;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "904C8D8D-CC4D-515E-A552-70B19B34A082";
	setAttr ".dc" -type "componentList" 1 "f[40:59]";
createNode polySplitRing -n "polySplitRing4";
	rename -uid "A68C9B7B-E94A-119E-EC62-179A2A40EAD3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 3.2692591125726125 0 0 0 0 1 0 0 3.1075734376698172 0.046381693099549315 1;
	setAttr ".wt" 0.12908953428268433;
	setAttr ".re" 55;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 15;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "D66BFD62-7F4B-8EF6-7897-3B8E6DE5EA6D";
	setAttr ".ics" -type "componentList" 1 "f[0:399]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 3.2692591125726125 0 0 0 0 1 0 0 3.1075734376698172 0.046381693099549315 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.1920929e-07 3.0969741 0.046381336 ;
	setAttr ".rs" 706173075;
	setAttr ".lt" -type "double3" -2.9837243786801082e-16 2.7061686225238191e-16 0.15779129482001086 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.2486512660980225 0.071240533045457965 -2.2022705266727893 ;
	setAttr ".cbx" -type "double3" 2.2486510276794434 6.1227075831950177 2.2950331976161511 ;
createNode polyTweak -n "polyTweak3";
	rename -uid "2AB31205-984A-D90C-3D65-EFB63AFBFC87";
	setAttr ".uopa" yes;
	setAttr -s 401 ".tk";
	setAttr ".tk[0:165]" -type "float3"  0.27910522 0.080893084 -0.090686537
		 0.23742096 0.080893084 -0.17249587 0.17249635 0.080893084 -0.23742057 0.090686589
		 0.080893084 -0.27910474 3.4984058e-08 0.080893084 -0.29346755 -0.090686515 0.080893084
		 -0.27910471 -0.17249584 0.080893084 -0.23742056 -0.23742056 0.080893084 -0.17249587
		 -0.27910468 0.080893084 -0.090686582 -0.29346743 0.080893084 4.5264333e-08 -0.27910468
		 0.080893084 0.09068653 -0.23742059 0.080893084 0.17249614 -0.17249589 0.080893084
		 0.23742048 -0.090686508 0.080893084 0.2791045 2.6238093e-08 0.080893084 0.29346764
		 0.090686508 0.080893084 0.27910456 0.17249584 0.080893084 0.23742047 0.23742056 0.080893084
		 0.17249614 0.27910468 0.080893084 0.090686567 0.29346743 0.080893084 4.5264333e-08
		 -2.682209e-07 -1.1175871e-08 -3.7252903e-08 -7.4505806e-08 -1.1175871e-08 4.4703484e-08
		 1.6391277e-07 -1.1175871e-08 -7.4505806e-08 -2.2351742e-08 -1.1175871e-08 1.4901161e-08
		 -1.2434498e-14 -1.1175871e-08 1.7881393e-07 7.4505806e-09 -1.1175871e-08 -5.9604645e-08
		 7.4505806e-08 -1.1175871e-08 -1.4901161e-07 -7.4505806e-08 -1.1175871e-08 0 -5.9604645e-08
		 -1.1175871e-08 4.4703484e-08 1.3411045e-07 -1.1175871e-08 7.1054274e-15 -5.9604645e-08
		 -1.1175871e-08 7.4505806e-08 -8.9406967e-08 -1.1175871e-08 1.6391277e-07 1.4901161e-08
		 -1.1175871e-08 -1.0430813e-07 -2.2351742e-08 -1.1175871e-08 -2.682209e-07 2.1316282e-14
		 -1.1175871e-08 1.3411045e-07 -7.4505806e-09 -1.1175871e-08 -1.7881393e-07 -7.4505806e-08
		 -1.1175871e-08 0 7.4505806e-08 -1.1175871e-08 1.4901161e-07 5.9604645e-08 -1.1175871e-08
		 9.6857548e-08 -1.3411045e-07 -1.1175871e-08 7.1054274e-15 2.5449847e-09 0.077387288
		 3.8175401e-09 -4.4703484e-08 0 -7.8159701e-14 1.6391277e-07 0 -2.2351742e-08 7.4505806e-08
		 0 -1.6391277e-07 -1.3411045e-07 0 7.4505806e-08 -1.0430813e-07 0 2.0861626e-07 1.4210855e-14
		 0 1.7881393e-07 6.7055225e-08 0 1.3411045e-07 5.9604645e-08 0 7.4505806e-08 -4.4703484e-08
		 0 1.3411045e-07 -1.6391277e-07 0 -2.2351742e-08 4.4703484e-08 0 -7.8159701e-14 -1.6391277e-07
		 0 1.4901161e-08 -7.4505806e-08 0 -1.4901161e-08 1.3411045e-07 0 1.3411045e-07 7.4505806e-08
		 0 -1.4901161e-07 -5.3290705e-15 0 5.9604645e-08 2.2351742e-08 0 -1.6391277e-07 1.3411045e-07
		 0 -5.9604645e-08 -1.4901161e-08 0 8.9406967e-08 2.9802322e-08 0 2.9802322e-08 3.2782555e-07
		 -7.4505806e-09 2.8421709e-14 1.1920929e-07 -7.4505806e-09 1.4901161e-08 -1.4901161e-08
		 -7.4505806e-09 -4.4703484e-08 0 -7.4505806e-09 1.0430813e-07 7.4505806e-08 -7.4505806e-09
		 1.0430813e-07 -3.5527137e-15 -7.4505806e-09 1.1920929e-07 5.9604645e-08 -7.4505806e-09
		 4.4703484e-08 -2.9802322e-08 -7.4505806e-09 1.1920929e-07 1.3411045e-07 -7.4505806e-09
		 -2.9802322e-08 -1.1920929e-07 -7.4505806e-09 0 -3.2782555e-07 -7.4505806e-09 2.8421709e-14
		 -1.1920929e-07 -7.4505806e-09 -7.4505806e-09 1.4901161e-08 -7.4505806e-09 1.0430813e-07
		 0 -7.4505806e-09 8.9406967e-08 8.9406967e-08 -7.4505806e-09 -1.937151e-07 4.6185278e-14
		 -7.4505806e-09 -3.2782555e-07 0 -7.4505806e-09 -1.1920929e-07 -4.4703484e-08 -7.4505806e-09
		 1.4901161e-08 2.9802322e-08 -7.4505806e-09 -2.9802322e-08 4.4703484e-08 -7.4505806e-09
		 2.2351742e-08 -4.4703484e-08 0 -2.1316282e-14 -5.9604645e-08 0 -5.9604645e-08 1.4901161e-07
		 0 -1.4901161e-07 1.4901161e-08 0 4.4703484e-08 2.2351742e-08 0 -7.4505806e-08 -8.8817842e-15
		 0 -2.0861626e-07 4.4703484e-08 0 -7.4505806e-08 -1.4901161e-08 0 -1.4901161e-08 -1.937151e-07
		 0 -1.4901161e-07 5.9604645e-08 0 -5.9604645e-08 4.4703484e-08 0 -2.1316282e-14 5.9604645e-08
		 0 -2.2351742e-08 -1.4901161e-07 0 -1.4901161e-07 -1.4901161e-08 0 1.4901161e-08 -8.1956387e-08
		 0 1.4901161e-08 -1.0658141e-14 0 2.9802322e-08 -1.4901161e-08 0 5.9604645e-08 -1.4901161e-07
		 0 -1.1920929e-07 0 0 -5.9604645e-08 1.6391277e-07 0 2.2351742e-08 -5.9604645e-08
		 -5.5879354e-09 -1.1368684e-13 -1.7881393e-07 -5.5879354e-09 -7.4505806e-09 -8.9406967e-08
		 -5.5879354e-09 -1.4901161e-08 7.4505806e-08 -5.5879354e-09 -7.4505806e-08 -1.0430813e-07
		 -5.5879354e-09 2.9802322e-07 -5.3290705e-15 -5.5879354e-09 0 4.4703484e-08 -5.5879354e-09
		 2.682209e-07 -1.3411045e-07 -5.5879354e-09 -1.1920929e-07 8.9406967e-08 -5.5879354e-09
		 -1.4901161e-08 1.1920929e-07 -5.5879354e-09 -5.2154064e-08 5.9604645e-08 -5.5879354e-09
		 -1.1368684e-13 1.1920929e-07 -5.5879354e-09 5.9604645e-08 8.9406967e-08 -5.5879354e-09
		 -1.3411045e-07 -7.4505806e-08 -5.5879354e-09 7.4505806e-08 1.0430813e-07 -5.5879354e-09
		 1.4901161e-07 0 -5.5879354e-09 1.1920929e-07 0 -5.5879354e-09 1.7881393e-07 -4.4703484e-08
		 -5.5879354e-09 1.937151e-07 -1.937151e-07 -5.5879354e-09 -1.0430813e-07 2.0861626e-07
		 -5.5879354e-09 -2.2351742e-08 -2.9802322e-08 -5.5879354e-09 6.3948846e-14 1.4901161e-07
		 -5.5879354e-09 -3.7252903e-08 2.9802322e-07 -5.5879354e-09 7.4505806e-08 5.9604645e-08
		 -5.5879354e-09 2.0861626e-07 4.4703484e-08 -5.5879354e-09 1.4901161e-07 -7.1054274e-15
		 -5.5879354e-09 2.9802322e-08 -7.4505806e-09 -5.5879354e-09 1.4901161e-07 -1.3411045e-07
		 -5.5879354e-09 5.9604645e-08 -1.7881393e-07 -5.5879354e-09 -2.9802322e-08 3.2782555e-07
		 -5.5879354e-09 -2.9802322e-08 2.9802322e-08 -5.5879354e-09 6.3948846e-14 3.2782555e-07
		 -5.5879354e-09 -4.4703484e-08 -2.9802322e-07 -5.5879354e-09 -1.6391277e-07 -5.9604645e-08
		 -5.5879354e-09 -1.7881393e-07 -1.1920929e-07 -5.5879354e-09 3.2782555e-07 3.1974423e-14
		 -5.5879354e-09 2.9802322e-08 0 -5.5879354e-09 3.2782555e-07 -1.4901161e-08 -5.5879354e-09
		 -2.0861626e-07 5.9604645e-08 -5.5879354e-09 -1.3411045e-07 5.9604645e-08 -5.5879354e-09
		 7.4505806e-09 -2.9802322e-08 5.5879354e-09 -2.8421709e-14 -3.2782555e-07 5.5879354e-09
		 -1.1920929e-07 -1.1920929e-07 5.5879354e-09 -1.4901161e-08 8.9406967e-08 5.5879354e-09
		 -1.1920929e-07 -6.7055225e-08 5.5879354e-09 0 3.5527137e-15 5.5879354e-09 -2.9802322e-07
		 2.9802322e-08 5.5879354e-09 0 -8.9406967e-08 5.5879354e-09 -1.1920929e-07 1.1920929e-07
		 5.5879354e-09 8.9406967e-08 4.1723251e-07 5.5879354e-09 -1.7136335e-07 2.9802322e-08
		 5.5879354e-09 -2.8421709e-14 4.1723251e-07 5.5879354e-09 7.4505806e-09 1.4901161e-07
		 5.5879354e-09 -1.7881393e-07 -8.9406967e-08 5.5879354e-09 0 -1.937151e-07 5.5879354e-09
		 3.5762787e-07 -7.1054274e-15 5.5879354e-09 0 -2.9802322e-08 5.5879354e-09 3.5762787e-07
		 -1.0430813e-07 5.5879354e-09 1.4901161e-07 2.9802322e-08 5.5879354e-09 -1.4901161e-07
		 2.9802322e-08 5.5879354e-09 5.9604645e-08 -2.682209e-07 1.8626451e-09 -1.4210855e-14
		 -2.0861626e-07 1.8626451e-09 -1.3411045e-07 -1.1920929e-07 1.8626451e-09 2.9802322e-08
		 -2.9802322e-08 1.8626451e-09 -2.682209e-07 5.9604645e-08 1.8626451e-09 -3.2782555e-07;
	setAttr ".tk[166:331]" 3.5527137e-15 1.8626451e-09 -4.7683716e-07 -1.0430813e-07
		 1.8626451e-09 -3.2782555e-07 2.9802322e-08 1.8626451e-09 -2.682209e-07 1.1920929e-07
		 1.8626451e-09 -5.9604645e-08 -5.9604645e-08 1.8626451e-09 -1.6391277e-07 2.682209e-07
		 1.8626451e-09 -1.4210855e-14 -5.9604645e-08 1.8626451e-09 2.0861626e-07 1.1920929e-07
		 1.8626451e-09 -2.9802322e-08 2.9802322e-08 1.8626451e-09 1.1920929e-07 -8.9406967e-08
		 1.8626451e-09 4.4703484e-07 -7.1054274e-15 1.8626451e-09 4.7683716e-07 1.4901161e-08
		 1.8626451e-09 4.4703484e-07 -2.3841858e-07 1.8626451e-09 2.682209e-07 1.4901161e-07
		 1.8626451e-09 5.9604645e-08 5.6624413e-07 1.8626451e-09 1.937151e-07 -1.1920929e-07
		 3.4924597e-10 1.9184654e-13 2.682209e-07 3.4924597e-10 -8.9406967e-08 2.9802322e-08
		 3.4924597e-10 4.4703484e-07 3.5762787e-07 3.4924597e-10 2.9802322e-08 2.9802322e-08
		 3.4924597e-10 4.1723251e-07 -2.4868996e-14 3.4924597e-10 -2.0861626e-07 -5.9604645e-08
		 3.4924597e-10 4.1723251e-07 1.7881393e-07 3.4924597e-10 2.9802322e-08 -2.0861626e-07
		 3.4924597e-10 3.2782555e-07 -3.2782555e-07 3.4924597e-10 -1.4901161e-08 -5.9604645e-08
		 3.4924597e-10 1.9184654e-13 -3.2782555e-07 3.4924597e-10 1.6391277e-07 -2.9802322e-08
		 3.4924597e-10 0 -3.5762787e-07 3.4924597e-10 -8.9406967e-08 -1.4901161e-08 3.4924597e-10
		 0 -6.3948846e-14 3.4924597e-10 1.1920929e-07 -5.9604645e-08 3.4924597e-10 -4.1723251e-07
		 1.7881393e-07 3.4924597e-10 -8.9406967e-08 1.1920929e-07 3.4924597e-10 2.0861626e-07
		 1.1920929e-07 3.4924597e-10 -8.9406967e-08 1.4901161e-07 1.8626451e-09 -1.4210855e-13
		 2.682209e-07 1.8626451e-09 -6.7055225e-08 -8.9406967e-08 1.8626451e-09 2.5331974e-07
		 -1.937151e-07 1.8626451e-09 -2.682209e-07 8.9406967e-08 1.8626451e-09 2.682209e-07
		 2.8421709e-14 1.8626451e-09 1.4901161e-07 -8.9406967e-08 1.8626451e-09 2.682209e-07
		 1.4901161e-07 1.8626451e-09 -2.682209e-07 8.9406967e-08 1.8626451e-09 2.5331974e-07
		 1.7881393e-07 1.8626451e-09 -8.1956387e-08 -1.4901161e-07 1.8626451e-09 -1.4210855e-13
		 1.7881393e-07 1.8626451e-09 8.9406967e-08 8.9406967e-08 1.8626451e-09 1.1920929e-07
		 1.937151e-07 1.8626451e-09 8.9406967e-08 -1.2665987e-07 1.8626451e-09 -5.9604645e-08
		 3.5527137e-14 1.8626451e-09 -1.4901161e-07 1.0430813e-07 1.8626451e-09 -5.9604645e-08
		 1.7881393e-07 1.8626451e-09 2.0861626e-07 2.0861626e-07 1.8626451e-09 2.8312206e-07
		 -8.9406967e-08 1.8626451e-09 8.9406967e-08 0 -3.7252903e-09 2.8421709e-14 5.9604645e-08
		 -3.7252903e-09 -7.4505806e-08 -8.9406967e-08 -3.7252903e-09 -1.1920929e-07 -1.3411045e-07
		 -3.7252903e-09 -2.0861626e-07 2.2351742e-08 -3.7252903e-09 -2.9802322e-08 -5.3290705e-15
		 -3.7252903e-09 -2.0861626e-07 -9.6857548e-08 -3.7252903e-09 -2.9802322e-08 1.3411045e-07
		 -3.7252903e-09 -2.0861626e-07 8.9406967e-08 -3.7252903e-09 -1.1920929e-07 -5.9604645e-08
		 -3.7252903e-09 -1.0430813e-07 0 -3.7252903e-09 2.8421709e-14 -5.9604645e-08 -3.7252903e-09
		 -5.2154064e-08 2.3841858e-07 -3.7252903e-09 -7.4505806e-08 1.3411045e-07 -3.7252903e-09
		 2.9802322e-08 -6.7055225e-08 -3.7252903e-09 -5.9604645e-08 0 -3.7252903e-09 0 -4.4703484e-08
		 -3.7252903e-09 -5.9604645e-08 4.4703484e-08 -3.7252903e-09 2.3841858e-07 2.9802322e-08
		 -3.7252903e-09 -7.4505806e-08 2.3841858e-07 -3.7252903e-09 -3.7252903e-08 2.9802322e-08
		 9.3132257e-09 4.2632564e-14 1.1920929e-07 9.3132257e-09 3.7252903e-08 -1.3411045e-07
		 9.3132257e-09 1.4901161e-07 -5.9604645e-08 9.3132257e-09 -2.2351742e-07 -5.2154064e-08
		 9.3132257e-09 0 0 9.3132257e-09 -5.9604645e-08 -7.4505806e-09 9.3132257e-09 0 5.9604645e-08
		 9.3132257e-09 -2.2351742e-07 7.4505806e-08 9.3132257e-09 1.4901161e-07 -1.1920929e-07
		 9.3132257e-09 0 -2.9802322e-08 9.3132257e-09 4.2632564e-14 -1.1920929e-07 9.3132257e-09
		 7.4505806e-09 1.3411045e-07 9.3132257e-09 0 5.9604645e-08 9.3132257e-09 1.0430813e-07
		 4.4703484e-08 9.3132257e-09 -1.1920929e-07 -7.1054274e-15 9.3132257e-09 -2.9802322e-08
		 2.9802322e-08 9.3132257e-09 -8.9406967e-08 1.0430813e-07 9.3132257e-09 1.0430813e-07
		 -2.682209e-07 9.3132257e-09 1.0430813e-07 -1.1920929e-07 9.3132257e-09 0 -2.9802322e-07
		 9.3132257e-09 -6.3948846e-14 1.7881393e-07 9.3132257e-09 5.2154064e-08 2.9802322e-07
		 9.3132257e-09 7.4505806e-08 1.4901161e-08 9.3132257e-09 2.5331974e-07 -8.1956387e-08
		 9.3132257e-09 1.7881393e-07 7.1054274e-15 9.3132257e-09 2.9802322e-07 2.2351742e-08
		 9.3132257e-09 1.7881393e-07 1.937151e-07 9.3132257e-09 2.5331974e-07 -3.1292439e-07
		 9.3132257e-09 7.4505806e-08 -2.0861626e-07 9.3132257e-09 2.2351742e-08 2.9802322e-07
		 9.3132257e-09 -6.3948846e-14 -2.0861626e-07 9.3132257e-09 -5.9604645e-08 -2.9802322e-07
		 9.3132257e-09 7.4505806e-08 -1.4901161e-08 9.3132257e-09 -3.1292439e-07 5.9604645e-08
		 9.3132257e-09 -2.0861626e-07 -2.8421709e-14 9.3132257e-09 -3.5762787e-07 7.4505806e-08
		 9.3132257e-09 -1.7881393e-07 2.9802322e-08 9.3132257e-09 -2.9802322e-07 1.3411045e-07
		 9.3132257e-09 1.937151e-07 4.4703484e-08 9.3132257e-09 -1.4901161e-08 0.053214192
		 7.7821314e-06 9.5153254e-09 0.050609425 7.7821314e-06 0.016444035 0.043050855 7.7821314e-06
		 0.031278424 0.031278439 7.7821314e-06 0.043051094 0.01644408 7.7821314e-06 0.050609574
		 4.7177267e-09 7.7821314e-06 0.053214103 -0.016443942 7.7821314e-06 0.050609514 -0.031278268
		 7.7821314e-06 0.043051094 -0.04305087 7.7821314e-06 0.031278424 -0.050609499 7.7821314e-06
		 0.016444024 -0.053214192 7.7821314e-06 9.5153254e-09 -0.050609499 7.7821314e-06 -0.01644399
		 -0.043050922 7.7821314e-06 -0.031278312 -0.031278439 7.7821314e-06 -0.04305093 -0.016444068
		 7.7821314e-06 -0.050609499 6.3036119e-09 7.7821314e-06 -0.053214192 0.016444042 7.7821314e-06
		 -0.05060941 0.031278424 7.7821314e-06 -0.043050952 0.04305099 7.7821314e-06 -0.031278268
		 0.050609469 7.7821314e-06 -0.016444001 0.026937589 0.00020275638 4.8168403e-09 0.02561903
		 0.00020275638 0.0083241425 0.021792874 0.00020275638 0.015833408 0.015833415 0.00020275638
		 0.021792799 0.0083240569 0.00020275638 0.025619015 2.4315101e-09 0.00020275638 0.02693741
		 -0.0083240755 0.00020275638 0.02561906 -0.01583346 0.00020275638 0.021792799 -0.021792874
		 0.00020275638 0.015833408 -0.025619105 0.00020275638 0.0083241276 -0.026937589 0.00020275638
		 4.8168403e-09 -0.025619105 0.00020275638 -0.0083241053 -0.021792829 0.00020275638
		 -0.015833549 -0.015833415 0.00020275638 -0.021792948 -0.0083240457 0.00020275638
		 -0.025619045 3.2342928e-09 0.00020275638 -0.026937306 0.0083241835 0.00020275638
		 -0.02561903 0.0158334 0.00020275638 -0.021792859 0.021792814 0.00020275638 -0.01583346
		 0.025618955 0.00020275638 -0.0083241053 0.14841124 0.03384953 2.4375993e-08 0.1411472
		 0.03384953 0.045861442 0.12006713 0.03384953 0.087233774 0.087233812 0.03384953 0.12006754
		 0.045861498 0.03384953 0.14114717 1.3269009e-08 0.03384953 0.14841111 -0.045861535
		 0.03384953 0.14114718 -0.087233841 0.03384953 0.12006754 -0.12006713 0.03384953 0.087233774
		 -0.14114708 0.03384953 0.045861579 -0.14841124 0.03384953 2.4375993e-08;
	setAttr ".tk[332:400]" -0.14114708 0.03384953 -0.045861594 -0.12006719 0.03384953
		 -0.087233923 -0.087233812 0.03384953 -0.12006721 -0.04586149 0.03384953 -0.14114723
		 1.7691997e-08 0.03384953 -0.14841124 0.045861471 0.03384953 -0.14114723 0.087234125
		 0.03384953 -0.12006735 0.12006752 0.03384953 -0.087234005 0.14114743 0.03384953 -0.045861565
		 0.011540562 0.00041456148 2.0636008e-09 0.010975659 0.00041456148 0.0035661906 0.0093364865
		 0.00041456148 0.0067835078 0.0067832991 0.00041456148 0.0093363822 0.0035662055 0.00041456148
		 0.010975808 1.2525163e-09 0.00041456148 0.011540428 -0.0035662465 0.00041456148 0.010975718
		 -0.006783329 0.00041456148 0.0093363822 -0.0093364865 0.00041456148 0.0067835078
		 -0.010975659 0.00041456148 0.0035661757 -0.011540532 0.00041456148 2.0636008e-09
		 -0.010975659 0.00041456148 -0.0035661869 -0.0093364716 0.00041456148 -0.0067834035
		 -0.0067832991 0.00041456148 -0.0093364865 -0.0035662241 0.00041456148 -0.010975674
		 1.5964492e-09 0.00041456148 -0.011540532 0.0035662353 0.00041456148 -0.010975614
		 0.0067835078 0.00041456148 -0.0093364865 0.0093363822 0.00041456148 -0.006783329
		 0.010975614 0.00041456148 -0.0035661794 0.070757911 0.0010357015 1.2597127e-08 0.06729494
		 0.0010357015 0.021865491 0.057244495 0.0010357015 0.041590437 0.041590549 0.0010357015
		 0.057244569 0.021865398 0.0010357015 0.067294806 6.323817e-09 0.0010357015 0.070757791
		 -0.021865457 0.0010357015 0.067294806 -0.041590609 0.0010357015 0.057244569 -0.057244495
		 0.0010357015 0.041590437 -0.06729494 0.0010357015 0.021865495 -0.070757911 0.0010357015
		 1.2597127e-08 -0.06729494 0.0010357015 -0.021865338 -0.057244465 0.0010357015 -0.041590601
		 -0.041590549 0.0010357052 -0.057244271 -0.021865424 0.0010357052 -0.06729494 8.4325862e-09
		 0.0010357052 -0.070757911 0.021865536 0.0010357052 -0.06729494 0.041590437 0.0010357052
		 -0.057244465 0.057244495 0.0010357052 -0.041590564 0.067294836 0.0010357052 -0.021865323
		 0.096375212 0.10078778 0.29661238 2.788399e-08 0.10078778 0.31187677 -0.096375093
		 0.10078778 0.29661235 -0.18331712 0.10078778 0.25231346 -0.25231361 0.10078778 0.18331711
		 -0.2966122 0.10078778 0.096375152 -0.31187686 0.10078778 4.8791229e-08 -0.2966122
		 0.10078778 -0.096375152 -0.25231355 0.10078778 -0.18331707 -0.18331718 0.10078778
		 -0.25231358 -0.09637522 0.10078778 -0.2966122 3.7178623e-08 0.10078778 -0.31187686
		 0.096375175 0.10078778 -0.29661214 0.18331711 0.10078778 -0.25231358 0.25231344 0.10078778
		 -0.18331726 0.29661241 0.10078778 -0.096375138 0.31187686 0.10078778 4.8791229e-08
		 0.2966122 0.10078778 0.096375152 0.25231355 0.10078778 0.18331714 0.18331715 0.10078778
		 0.25231358;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "E115F4B4-F34C-5FF4-198C-55A3BA2E2676";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 20 "e[801]" "e[803]" "e[805]" "e[808]" "e[811]" "e[814]" "e[817]" "e[820]" "e[823]" "e[826]" "e[829]" "e[832]" "e[835]" "e[838]" "e[841]" "e[844]" "e[847]" "e[850]" "e[853]" "e[856]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 3.2692591125726125 0 0 0 0 1 0 0 3.1075734376698172 0.046381693099549315 1;
	setAttr ".wt" 0.12803517282009125;
	setAttr ".re" 847;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 15;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "C7E599CB-7B41-C180-4063-AD818AF57C1D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[1620:1621]" "e[1623]" "e[1625]" "e[1627]" "e[1629]" "e[1631]" "e[1633]" "e[1635]" "e[1637]" "e[1639]" "e[1641]" "e[1643]" "e[1645]" "e[1647]" "e[1649]" "e[1651]" "e[1653]" "e[1655]" "e[1657]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 3.2692591125726125 0 0 0 0 1 0 0 3.1075734376698172 0.046381693099549315 1;
	setAttr ".wt" 0.86707621812820435;
	setAttr ".dr" no;
	setAttr ".re" 1620;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 15;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "7852E7B5-2A40-E462-4A14-AD8EBB66692E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1600:1619]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 3.2692591125726125 0 0 0 0 1 0 0 3.1075734376698172 0.046381693099549315 1;
	setAttr ".wt" 0.15060828626155853;
	setAttr ".re" 1619;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 15;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr ":defaultColorMgtGlobals.cme" "imagePlaneShape1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "imagePlaneShape1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "imagePlaneShape1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "imagePlaneShape1.ws";
connectAttr ":sideShape.msg" "imagePlaneShape1.ltc";
connectAttr "polySplitRing7.out" "pCylinderShape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCylinder1.out" "polySplitRing1.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing1.mp";
connectAttr "polyTweak1.out" "polySplitRing2.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing1.out" "polyTweak1.ip";
connectAttr "polySplitRing2.out" "polySplitRing3.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "polySplitRing4.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing4.mp";
connectAttr "polyTweak3.out" "polyExtrudeFace1.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polySplitRing4.out" "polyTweak3.ip";
connectAttr "polyExtrudeFace1.out" "polySplitRing5.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing6.mp";
connectAttr "polySplitRing6.out" "polySplitRing7.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing7.mp";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCylinderShape1.iog" ":initialShadingGroup.dsm" -na;
// End of vase.ma
