//Maya ASCII 2018 scene
//Name: ladder.ma
//Last modified: Mon, Oct 15, 2018 03:50:58 AM
//Codeset: UTF-8
requires maya "2018";
requires -nodeType "PxrSurface" -nodeType "rmanDisplayChannel" -nodeType "PxrOSL"
		 -nodeType "PxrDisney" -nodeType "d_openexr" -nodeType "rmanGlobals" -nodeType "PxrRectLight"
		 -nodeType "PxrPathTracer" -nodeType "rmanDisplay" "RenderMan_for_Maya.py" "1.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Mac OS X 10.13.5";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "31FFFE91-4641-D27D-4FD9-C5BE0F336302";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.74278912313472456 14.682196420316849 55.529808108266515 ;
	setAttr ".r" -type "double3" -2.1383527295959013 0.60000000000099807 1.5530903724888981e-18 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "8A8FDD8C-7246-F56A-86F5-E0A952CF5B5C";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 55.911188580086417;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -1.1127310055115593 2.6043084977774136 0 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "8C4D3D53-D444-7B42-8721-DD9695E33D10";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "5DB5FCC5-6D43-5A1C-4115-D1855EB2EBCB";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "8F460C7B-554A-06C0-BB00-6CAA0B949CD3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "DFD253F2-9C40-17AE-7177-B98B8CD94320";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "D70D0A24-9A48-D889-B059-8FBE36E419AC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "73905F8D-474C-1AAD-7D10-92BD0DA0B10A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCube1";
	rename -uid "E981B88A-D245-5F9E-2171-E181E277C10A";
	setAttr ".t" -type "double3" -3.6223224268748213 12.177456321962971 0 ;
	setAttr ".s" -type "double3" 0.72368219284792923 24.561124555897987 0.72368219284792923 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "974052A9-C341-85C0-648C-39AA0DA29324";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.21748419106006622 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 2 ".clst";
	setAttr ".clst[0].clsn" -type "string" "SculptFreezeColorTemp";
	setAttr ".clst[1].clsn" -type "string" "SculptMaskColorTemp";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 311 ".pt";
	setAttr ".pt[1:166]" -type "float3"  0.12423575 -0.00060299039 0.014676392 
		0 0 0 -0.028847367 0.00014311075 -0.0058533549 0 0 0 0 0 0 0 0 0 0 0 0 -0.21401149 
		0.0010615289 -0.043424606 -0.001843065 9.149313e-06 -0.00037395954 0 0 0 0 0 0 0.14986199 
		-0.00072735548 0.017703712 0 0 0 0 0 0 0 0 0 -0.070733279 -0.0017116317 -0.14750427 
		0 0 0 0 0 0 -0.31699532 -0.0080476701 -0.017195301 -0.0071602166 9.8556047e-05 -0.30640551 
		0.0078723133 5.9604645e-08 0.00051766634 0 0 0 -0.14521354 0.0069367597 -0.014000773 
		-0.14633286 -0.0060284138 -0.27299172 0.011621952 -0.0010801256 8.5890293e-05 0.00049707294 
		-4.3809414e-05 9.6410513e-05 4.1723251e-07 0 -5.9604645e-08 -0.051947445 0.00068807602 
		-0.073894322 0 0 0 0 0 0 -0.12393942 4.5463443e-05 0.13457742 -0.088872433 -0.0035959482 
		-0.16206726 0.052708417 0.00077951699 0.0027397871 0.01384154 0.0002047047 0.00071948767 
		0.0034201145 5.0581992e-05 0.00017777085 -0.16337723 -0.00049652159 -0.22834995 0 
		0 0 0 0 0 0 0 0 -0.041675955 -0.0012324676 -0.033854932 0 0 0 0 0 0 -0.18011749 -0.0023650229 
		0.28692073 -0.18056655 -0.001858443 -0.23428643 8.5532665e-06 1.7136335e-07 -1.1146069e-05 
		0 0 0 0 0 0 -0.071835697 -0.005041644 -0.13594812 0 0 0 0 0 0 0 0 0 -0.080346942 
		0.0049318671 -0.096755624 0 0 0 0 0 0 -0.10355434 0.0080217719 0.1829018 0 0 0 -2.3841858e-07 
		-4.9471855e-06 1.6689301e-06 0 0 0 -9.5367432e-06 -0.00024554133 8.3148479e-05 0 
		0 0 0 0 0 0 0 0 -0.029727936 -0.0010741651 -0.038738489 -0.086561441 -5.3457916e-05 
		0.049094737 -0.015568972 0.00023411214 -0.020482779 0.0087995529 -0.0066579953 -0.046324015 
		0.0035364628 -0.0026758388 -0.01861757 0 0 0 0 0 0 -0.0078492165 -0.0004292056 0.013803601 
		0 0 0 0 0 0 -0.073833704 0.0024395287 -0.078981936 0 0 0 0 0 0 0 0 0 -0.12582779 
		-0.00052818656 -0.022399127 -0.14559174 -0.00061112642 -0.025917411 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 -0.020843506 -8.7499619e-05 -0.0037104487 -0.015440941 -6.4820051e-05 
		-0.0027486682 0 0 0 0.11491871 -5.5134296e-06 0.020203978 2.8133392e-05 0 4.9173832e-06 
		0 0 0 0 0 0 0.025453568 0.0023016334 -0.041576296 0.0061807632 5.9604645e-08 -0.00074443221 
		0 0 0 0.0029792786 0.00026941299 0.0044038892 0.075118542 -0.0029650037 0.0063893199 
		0 0 0 0 0 0 0 4.6566129e-10 0 0.092854023 -0.0029752553 0.006182909 0.0013918877 
		-5.8561563e-05 -0.0013647974 0.00094509125 -1.3679266e-05 9.8079443e-05 0.088944435 
		0.0017297268 0.0046074986 0.21380758 -0.0039732456 -0.18422201 0.0079226494 -0.0089168251 
		-0.018311262 0.0056262016 -0.00048980117 -8.3684921e-05 0.034264565 -6.6757202e-06 
		-0.00062209368 0.13844824 0.0025862306 -0.18887681 -0.004714489 9.3713403e-05 -0.0047397316 
		0 0 0 0 0 0 0.12409973 -0.0043366477 -0.16396415 -0.017859459 -5.2280724e-05 -0.0011788309 
		-9.5367432e-06 -2.9802322e-08 -6.5565109e-07 9.4413757e-05 -9.1642141e-07 1.1116266e-05 
		0.178792 -0.00071904808 -0.223113 0.0036425591 -0.00039759278 -1.6391277e-05 1.8119812e-05 
		-1.9967556e-06 3.6358833e-06 0 0 0 0.090087891 -0.0033908188 -0.15783027 0 0 0 0 
		0 0 0 0 0 0.065275669 0.00039571524 -0.091210306 0 0 0 0 0 0 0.041307926 0.00050104409 
		0.046953321 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -2.2411346e-05 -0.0005710125 0.00019341707 
		0 0 0 -8.4638596e-05 -0.0021530986 0.00072926283 0 0 0 0.018197298 0.00038935244 
		-0.014106184 0 0 0 0.23272657 -0.0060369074 -0.22256464 0.00071549416 -0.00054131448 
		-0.0037662983 0.27083111 0.0062926114 -0.19200632 0.0049946308 -0.0037790537 -0.026293337 
		0.001231432 -0.0009316653 -0.0064822137 0 0 0 0 0 0 0.24080467 -0.0058017373 0.18306056 
		0.0080478191 -0.00028701127 -0.0092779994 0 0 0 0.21022892 0.0072392523 -0.23109305 
		0 0 0 0 0 0 -0.00073325634 -1.5199184e-06 0.0053700805 -2.2649765e-06 1.1920929e-07 
		0.00082033873 -0.13174111 -0.0034043193 -0.0071117878 -0.029164433 0.0015094876 -0.0028789639 
		0.014503956 -0.00057211518 0.0012334585 0.017949581 0.00035324693 0.00092577934 0 
		0 0 0.01402998 -0.00055378675 0.0011933446 0.35832536 -0.0077058971 -0.28082836 0 
		0 0 0.0414325 0.00078289211 0.028099477 0.0044647455 4.7832727e-05 -0.0029399693 
		-0.050593376 0.00074678659 -0.034594238 0 0 0 0.0043263435 -1.1920929e-07 -0.00017619133;
	setAttr ".pt[167:311]" -0.14140034 -0.0030471087 -0.11210522 0 0 0 0.16005242 
		-0.0037491918 -0.11344102 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.20363808 -0.0046121851 
		-0.13931233 0 0 0 0.025768995 -0.00051939487 -0.010614634 0.29060984 -0.0067703277 
		0.22185206 0 0 0 -0.0044937134 8.8341534e-05 0.0027839839 0 0 0 -0.26129389 -0.005759798 
		-0.19193783 -0.013318539 0.00026825815 -0.0086604953 0.19146264 0.002976656 -0.11772406 
		0.23925221 -0.006000787 -0.18231323 0 0 0 0.19824302 0.0042344034 0.15958342 0.020938396 
		0.0023047328 -0.14784589 0.0045537949 0.00051501393 0.026720881 0 0 0 0 0 0 -0.060534567 
		-0.0013468564 0.0095741749 0.024300426 -0.001825273 0.0013385415 0.0048304796 -0.00043126941 
		0.00093564391 -0.007533282 -7.5012445e-05 0.0011435151 -0.035660326 -0.0037455857 
		-0.03601104 0 0 0 0 0 0 -0.16082424 -0.017364353 0.14969474 0.072980106 -0.0098940134 
		-0.38063174 0 0 0 0.0017797947 0.0034479052 0.12653482 0.0085331202 0.0010011494 
		-0.075591475 0.14463043 0.0038922727 -0.020281225 0.24475622 -0.00027903914 -0.035811722 
		0.082135201 0.00083491206 0.00035354495 0.028264999 0.00129807 0.00045540929 -0.0012021065 
		-0.00013080239 -0.0012023449 0.16684484 -0.016070336 -0.17772576 0 0 0 0 0 0 0 0 
		0 -0.02805692 -0.0066864267 -0.2104778 0.00024604797 3.6358833e-06 1.2785196e-05 
		0 0 0 0 0 0 0 0 0 4.2915344e-06 -3.7252903e-08 4.7683716e-07 0.014219761 -0.007167533 
		-0.22177476 0.035075366 0.00051931292 0.0018180609 0.071546346 0.0010587424 0.0037133694 
		0.015716463 0.00036217272 -0.00036114454 -0.0041913688 0.00011078268 -0.0017878413 
		-0.1839807 -0.019681968 -0.18398082 0 0 0 0 0 0 0 0 0 -0.062624454 -0.00018332154 
		-0.0041335523 -0.0079674721 -2.3320317e-05 -0.00052589178 0.015149117 -0.00014661252 
		0.0017873049 -0.006942749 -2.0325184e-05 -0.00045830011 0.013494015 -0.0014617741 
		-8.8691711e-05 0.32605791 -0.022914786 -0.047670841 0 0 0 0.0018000603 -0.0001934506 
		0.00035870075 0.001584053 -0.00012799725 -2.6851892e-05 0 0 0 3.8146973e-06 -3.837049e-07 
		7.7486038e-07 0.0045685768 -0.00032525137 0.0015499294 0.11705828 -0.01387012 -0.11620903 
		0.077541828 -0.0092282742 0.061939776 0 0 0 0 0 0 0.0034651756 0.001994513 -0.068769991 
		0.001188755 0.0011357144 0.033275157 0 0 0 -0.0011892319 -0.0056665689 -0.20799476 
		0 0 0 -0.00038331747 -0.0021783486 -0.03667134 0.011603236 -0.009309344 0.33845031 
		0 0 0 -0.060966596 0.0046089217 -0.039729476 0 0 0 0 0 0 -0.27960771 0.025386333 
		0.28811049 -0.39553922 -0.030929789 -0.33343041 0.0025151372 -0.00051093102 -0.0046264231 
		2.9623508e-05 -3.4347177e-06 2.977252e-05 -0.0064326525 -0.00043990463 0.0011992455 
		-0.30148917 -0.0077908039 -0.016275346 0 0 0 0 0 0 -0.0030001998 -7.7515841e-05 -0.00016194582 
		-0.037478626 6.967783e-05 0.0074107051 0.044353515 -0.00011599064 0.0033211708 0.0045025349 
		0 0.00029590726 -0.15532055 0.0080389977 -0.015332341 0.019721031 -0.00077840686 
		0.0016773939 0 0 0 0 0 0 0 0 0 0.02540493 -0.0031260848 -0.0039069653 0.056978226 
		-0.00074842572 -0.00056472421 0.11331654 0.0020062029 0.00605914 0.0083036423 -0.00031894445 
		0.0011186898 -0.10701424 0.0052056611 -0.010371804 -0.004306972 1.1205673e-05 0.00047683716 
		-0.0019986629 -5.1647425e-05 -0.00010788441 -0.28885031 -0.0071914196 -0.015750289 
		0.066586018 -0.0022900403 0.0052092075 0.06912899 0.0013584793 0.003567338 0 0 0 
		0.057423592 -0.002266556 0.004884243 0.053308964 -0.0059542954 -0.22423226 0.01341486 
		0.0032958984 -0.18979454 0 0 0 0.020622253 -6.2584877e-07 -0.00053757429 -0.026390433 
		0.0094749629 -0.19961107 -0.007276535 -0.0078206509 -0.19603613 0 0 0 -0.0073559284 
		0.011682659 0.291677 -0.14880943 0.0029837489 -0.1011754 -0.026650906 0.00050392747 
		0.013308406 0 0 0 0 0 0 0 0 0 0.0013537407 3.5762787e-07 0.00035366416 0.003370285 
		2.0414591e-05 0.00085562468 0 0 0 0.038105965 4.4703484e-07 -0.0045895875 0.0019884109 
		-4.3809414e-05 -0.00072184205 0 0 0 0 0 0 -0.0096756816 -0.00070160627 -0.009157151 
		-0.025455654 -0.00025200844 0.0030446053 0 0 0 -0.036965996 -0.002614975 0.039005339 
		0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "PxrRectLight";
	rename -uid "B049CE72-324F-76DA-2A50-4DBFDF3188C4";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 17.461208529274195 12.385199697088744 ;
	setAttr ".s" -type "double3" 18.035213599362642 18.035213599362642 18.035213599362642 ;
createNode PxrRectLight -n "PxrRectLightShape" -p "PxrRectLight";
	rename -uid "546D765B-2F46-16A4-AA4F-E2B4828F62DA";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".isc" no;
	setAttr ".bbx" no;
	setAttr ".icn" -type "string" "";
	setAttr ".vwm" 2;
	setAttr ".tpv" 0;
	setAttr ".uit" 0;
	setAttr -k off ".v" yes;
	setAttr ".io" no;
	setAttr ".tmp" no;
	setAttr ".gh" no;
	setAttr ".obcc" -type "float3" 0 0 0 ;
	setAttr ".wfcc" -type "float3" 0 0 0 ;
	setAttr ".uoc" 0;
	setAttr ".oc" 0;
	setAttr ".ovdt" 0;
	setAttr ".ovlod" 0;
	setAttr ".ovs" no;
	setAttr ".ovt" yes;
	setAttr ".ovp" yes;
	setAttr ".ove" yes;
	setAttr ".ovv" yes;
	setAttr ".hpb" no;
	setAttr ".ovrgbf" no;
	setAttr ".ovc" 0;
	setAttr ".ovrgb" -type "float3" 0 0 0 ;
	setAttr ".lodv" yes;
	setAttr ".sech" yes;
	setAttr ".rlid" 0;
	setAttr ".rndr" yes;
	setAttr ".lovc" 0;
	setAttr ".gc" 0;
	setAttr ".gpr" 3;
	setAttr ".gps" 3;
	setAttr ".gss" 1;
	setAttr ".gap" 1;
	setAttr ".gcp" -type "float3" 0.447 1 1 ;
	setAttr ".gla" 1;
	setAttr ".gac" -type "float3" 0.87800002 0.67799997 0.66299999 ;
	setAttr ".grs" 0;
	setAttr ".gre" 100;
	setAttr ".rt" 0;
	setAttr ".rv" no;
	setAttr ".vf" 1;
	setAttr ".hfm" 1;
	setAttr ".mb" yes;
	setAttr ".vir" no;
	setAttr ".vif" no;
	setAttr ".csh" yes;
	setAttr ".rcsh" yes;
	setAttr ".asbg" no;
	setAttr ".vbo" no;
	setAttr ".mvs" 1;
	setAttr ".gao" no;
	setAttr ".gal" 1;
	setAttr ".sso" no;
	setAttr ".ssa" 1;
	setAttr ".msa" 1;
	setAttr ".vso" no;
	setAttr ".vss" 1;
	setAttr ".dej" no;
	setAttr ".iss" no;
	setAttr ".vis" no;
	setAttr ".tw" no;
	setAttr ".rtw" yes;
	setAttr ".pv" -type "double2" 0 0 ;
	setAttr ".di" no;
	setAttr ".dcol" no;
	setAttr ".dcc" -type "string" "color";
	setAttr ".ih" no;
	setAttr ".ds" yes;
	setAttr ".op" no;
	setAttr ".hot" no;
	setAttr ".smo" yes;
	setAttr ".bbs" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".fbda" yes;
	setAttr ".dsr" 6;
	setAttr ".xsr" 5;
	setAttr ".fth" 0;
	setAttr ".nat" 30;
	setAttr ".dhe" no;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".intensity" 1;
	setAttr ".exposure" 0.51948052644729614;
	setAttr ".lightColor" -type "float3" 1 1 1 ;
	setAttr ".lightColorMap" -type "string" "";
	setAttr ".colorMapGamma" -type "float3" 1 1 1 ;
	setAttr ".colorMapSaturation" 1;
	setAttr ".enableTemperature" no;
	setAttr ".temperature" 6500;
	setAttr ".emissionFocus" 0;
	setAttr ".emissionFocusTint" -type "float3" 0 0 0 ;
	setAttr ".specular" 1;
	setAttr ".diffuse" 1;
	setAttr ".intensityNearDist" 0;
	setAttr ".coneAngle" 90;
	setAttr ".coneSoftness" 0;
	setAttr ".iesProfile" -type "string" "";
	setAttr ".iesProfileScale" 0;
	setAttr ".iesProfileNormalize" no;
	setAttr ".enableShadows" yes;
	setAttr ".shadowColor" -type "float3" 0 0 0 ;
	setAttr ".shadowDistance" -1;
	setAttr ".shadowFalloff" -1;
	setAttr ".shadowFalloffGamma" 1;
	setAttr ".shadowSubset" -type "string" "";
	setAttr ".shadowExcludeSubset" -type "string" "";
	setAttr ".areaNormalize" no;
	setAttr ".traceLightPaths" no;
	setAttr ".thinShadow" yes;
	setAttr ".visibleInRefractionPath" no;
	setAttr ".cheapCaustics" no;
	setAttr ".cheapCausticsExcludeGroup" -type "string" "";
	setAttr ".fixedSampleCount" 0;
	setAttr ".lightGroup" -type "string" "";
	setAttr ".importanceMultiplier" 1;
	setAttr ".rman__lightfilters[0]" -type "float3"  0 0 0;
	setAttr ".cl" -type "float3" 1 1 1 ;
	setAttr ".ed" yes;
	setAttr ".sn" yes;
	setAttr ".lls" 1;
	setAttr ".de" 2;
	setAttr ".urs" yes;
	setAttr ".col" 5;
	setAttr ".hio" no;
	setAttr ".uocol" no;
	setAttr ".oclr" -type "float3" 0 0 0 ;
	setAttr ".rman_coneAngleDepth" 10;
	setAttr ".rman_coneAngleOpacity" 0.5;
createNode place3dTexture -n "place3dTexture1";
	rename -uid "4361BC34-CA43-6631-E39F-A787307D16F1";
	setAttr ".v" no;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "4299C781-E144-2107-72EB-2AB24A9DE1C6";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "05DBFC53-C745-E8F5-1F1C-26AE1311CAAA";
createNode displayLayer -n "defaultLayer";
	rename -uid "976DEBBE-5249-C185-33B8-E1A48F0BDDB1";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "1D339421-BA42-DAF5-04C4-799AAB7BC5C1";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "958C2903-E749-C7CC-CC94-F4BB5E3E7509";
	setAttr ".g" yes;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "22CA6A04-4B4B-F68B-EB00-EF95A8F9E694";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "F4FB946D-C641-1DAA-49AA-2CA498CFFA7C";
createNode polyCube -n "polyCube1";
	rename -uid "4A0D0C50-6540-9DB6-3FC1-CDA30622195E";
	setAttr ".cuv" 4;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "9D24F249-5A4D-CACB-AEA9-34856CE99DC2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[4:5]" "e[8:9]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.97465628385543823;
	setAttr ".dr" no;
	setAttr ".re" 5;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "80E21B3C-2C4B-21F3-36A4-92881093879D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[4:5]" "e[15]" "e[17]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.024827685207128525;
	setAttr ".re" 5;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "8C9CD23D-1949-72E5-F433-21ADC1E1C1BF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[15]" "e[17]" "e[20:21]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.91613560914993286;
	setAttr ".dr" no;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "18F4D1E6-FC48-B051-C46E-3EB92A82A331";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20:21]" "e[31]" "e[33]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.97127765417098999;
	setAttr ".dr" no;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "FB0233CB-5048-1B83-FCFE-97A92CE68FB5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20:21]" "e[39]" "e[41]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.86919605731964111;
	setAttr ".dr" no;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "FF428A40-2944-1B89-1418-61B27E7AC84D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20:21]" "e[47]" "e[49]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.96216988563537598;
	setAttr ".dr" no;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "6B3CA1C9-E44D-761D-78DB-7AA4AA4B4A56";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20:21]" "e[55]" "e[57]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.81299149990081787;
	setAttr ".dr" no;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "02359B60-B646-729D-3476-679B5AE3D133";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20:21]" "e[63]" "e[65]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.94212222099304199;
	setAttr ".dr" no;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing9";
	rename -uid "98A18BF9-B143-0931-9EB3-B986440CD4CC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20:21]" "e[71]" "e[73]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.75745421648025513;
	setAttr ".dr" no;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing10";
	rename -uid "B9C7DD11-9C4B-ACAA-A257-FA9540FE8E20";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20:21]" "e[79]" "e[81]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.92418241500854492;
	setAttr ".dr" no;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing11";
	rename -uid "6D48DA68-5D46-376F-A848-238D25D85343";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20:21]" "e[87]" "e[89]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.60070681571960449;
	setAttr ".dr" no;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing12";
	rename -uid "F3D8C285-C844-F420-CAB5-3BAFB4E58581";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20:21]" "e[95]" "e[97]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.86152404546737671;
	setAttr ".dr" no;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "04EF5625-834A-59A9-5773-0C9E4EFE7EF1";
	setAttr ".ics" -type "componentList" 5 "f[21]" "f[29]" "f[37]" "f[45]" "f[53]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.2604814 13.594758 0 ;
	setAttr ".rs" 1204631406;
	setAttr ".lt" -type "double3" -4.2236753121826886e-16 -6.8724781961724401e-15 1.9021742562079933 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.2604813304508569 5.3117244154979542 -0.36184109642396461 ;
	setAttr ".cbx" -type "double3" -3.2604813304508569 21.877792007403585 0.36184109642396461 ;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "EDC01B47-464E-62BA-2FD7-CB97811F5E39";
	setAttr ".dc" -type "componentList" 5 "f[21]" "f[29]" "f[37]" "f[45]" "f[53]";
createNode polyMirror -n "polyMirror1";
	rename -uid "5C0EF6EC-7342-4C66-A3DE-3993A8BEBC14";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".ws" yes;
	setAttr ".p" -type "double3" -1.112730860710144 0 0 ;
	setAttr ".ad" 0;
	setAttr ".mps" -1.112730860710144;
	setAttr ".mm" 2;
	setAttr ".cm" yes;
	setAttr ".fnf" 69;
	setAttr ".lnf" 157;
	setAttr ".pc" -type "double3" -1.112730860710144 0 0 ;
createNode polySplitRing -n "polySplitRing13";
	rename -uid "5971BB88-B949-4942-CA3A-03A1D2AEBB35";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[108:109]" "e[111]" "e[113]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.22981365025043488;
	setAttr ".re" 109;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing14";
	rename -uid "60FBC34B-354F-B661-5FB3-5B9E4232DC75";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[257]" "e[260]" "e[262]" "e[265]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.2239651083946228;
	setAttr ".re" 260;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing15";
	rename -uid "F8C93B47-C74A-EC3E-C81B-DE983616EEFA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[116:117]" "e[119]" "e[121]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.23152288794517517;
	setAttr ".re" 121;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing16";
	rename -uid "6D567BC8-CA4B-AF8A-4C81-22838607E015";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[269]" "e[272]" "e[274]" "e[277]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.21477684378623962;
	setAttr ".re" 272;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing17";
	rename -uid "58924463-834B-E18C-38A4-2DAF0539CD76";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[124:125]" "e[127]" "e[129]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.233594611287117;
	setAttr ".re" 125;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing18";
	rename -uid "A483E183-AA43-A09C-B9F2-ACB0FC04F13F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[281]" "e[284]" "e[286]" "e[289]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.20381937921047211;
	setAttr ".re" 284;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing19";
	rename -uid "310818DC-A74D-5696-956B-97905010FAE6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[132:133]" "e[135]" "e[137]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.23568667471408844;
	setAttr ".re" 133;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing20";
	rename -uid "D1CB9084-9B4E-E286-1A65-098364EFFEF3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[293]" "e[296]" "e[298]" "e[301]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.24904114007949829;
	setAttr ".re" 293;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing21";
	rename -uid "6D12BD0E-BB4B-C4A1-7D7E-9FBFC09E0C84";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[140:141]" "e[143]" "e[145]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.27143201231956482;
	setAttr ".re" 141;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing22";
	rename -uid "FA20996A-0C46-E683-820C-E7A6F5FE4FA4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[305]" "e[308]" "e[310]" "e[313]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.14753668010234833;
	setAttr ".re" 308;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode createColorSet -n "createColorSet1";
	rename -uid "FA930AEA-CE43-C1D2-F481-038F0A4E2623";
	setAttr ".colos" -type "string" "SculptFreezeColorTemp";
	setAttr ".clam" no;
createNode createColorSet -n "createColorSet2";
	rename -uid "1B6A7C77-7E48-FDA5-7A67-3CB4538FFB88";
	setAttr ".colos" -type "string" "SculptMaskColorTemp";
	setAttr ".clam" no;
createNode polySplitRing -n "polySplitRing23";
	rename -uid "52897D04-EE49-AB3A-269F-1986BDEC5AAD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[39]" "e[41]" "e[44:45]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.11155874282121658;
	setAttr ".re" 44;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "4F1C9FB1-CA42-5601-5ECD-3287FED54A76";
	setAttr ".uopa" yes;
	setAttr -s 166 ".tk[0:165]" -type "float3"  0 0.080929503 0 0 0.080929503
		 0 -2.9802322e-08 0 -2.9802322e-08 2.9802322e-08 0 -2.9802322e-08 -2.9802322e-08 0
		 2.9802322e-08 2.9802322e-08 0 2.9802322e-08 0 0.080929503 0 0 0.080929503 0 2.9802322e-08
		 0 -2.9802322e-08 -2.9802322e-08 0 -2.9802322e-08 -2.9802322e-08 0 2.9802322e-08 2.9802322e-08
		 0 2.9802322e-08 0 0.070113525 0 0 0.070113525 0 0 0.070113525 0 0 0.070113525 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.017610963 0 0 -0.017610963
		 0 0 -0.017610963 0 0 -0.017610963 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.080929503
		 0 0 0.080929503 0 0 0.070113525 0 0 0.070113525 0 2.9802322e-08 0 -2.9802322e-08
		 0 0 -2.9802322e-08 0 0 2.9802322e-08 2.9802322e-08 0 2.9802322e-08 0 0.070113525
		 0 0 0.070113525 0 0 0.080929503 0 0 0.080929503 0 0 0 -2.9802322e-08 2.9802322e-08
		 0 -2.9802322e-08 2.9802322e-08 0 2.9802322e-08 0 0 2.9802322e-08 0 -0.017610963 0
		 0 -0.017610963 0 0 -0.017610963 0 0 -0.017610963 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode polySplitRing -n "polySplitRing24";
	rename -uid "EBD9375B-AD44-4FD3-DC46-719DE2F409E3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[55]" "e[57]" "e[60:61]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.92531538009643555;
	setAttr ".dr" no;
	setAttr ".re" 60;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing25";
	rename -uid "4005A3A9-0A4F-B9A6-ABD0-FAB56671DED5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[116:117]" "e[119]" "e[121]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.39294081926345825;
	setAttr ".dr" no;
	setAttr ".re" 121;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing26";
	rename -uid "14BE7396-A94D-C028-124A-84A88D1DA726";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[196:197]" "e[199:200]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.2369910329580307;
	setAttr ".re" 197;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing27";
	rename -uid "DF7A5C51-A140-0A39-9A32-65AAB34E8831";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[212:213]" "e[215:216]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.90575951337814331;
	setAttr ".dr" no;
	setAttr ".re" 213;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing28";
	rename -uid "8F50414D-C04F-897D-E1F6-5B94F62CA588";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[124:125]" "e[127]" "e[129]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.55137515068054199;
	setAttr ".dr" no;
	setAttr ".re" 125;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing29";
	rename -uid "62749FD4-0143-6125-7235-719F033C66B0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[281]" "e[284]" "e[286]" "e[289]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.45383170247077942;
	setAttr ".re" 284;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing30";
	rename -uid "A19CCA9D-914F-99C5-35C0-45B34225DC51";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[60:61]" "e[407]" "e[409]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.077669575810432434;
	setAttr ".re" 60;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing31";
	rename -uid "0F6693D3-FD4D-1CD3-31B6-52A258F626D3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[71]" "e[73]" "e[76:77]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.93306261301040649;
	setAttr ".dr" no;
	setAttr ".re" 76;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing32";
	rename -uid "226DABBE-3440-4C72-FC8B-8DAFEB02CB3F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[212:213]" "e[431]" "e[433]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.057030722498893738;
	setAttr ".re" 213;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing33";
	rename -uid "5CCE9745-4048-87C1-4743-D9A7A529F396";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[228:229]" "e[231:232]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.94272220134735107;
	setAttr ".dr" no;
	setAttr ".re" 229;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing34";
	rename -uid "C2F1960C-F641-CF82-29AE-4F952FECF510";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[228:229]" "e[479]" "e[481]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.06838236004114151;
	setAttr ".re" 228;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing35";
	rename -uid "46845F9B-174E-6E43-3D4F-DA9538A1AE6A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[244:245]" "e[247:248]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.93253129720687866;
	setAttr ".dr" no;
	setAttr ".re" 244;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing36";
	rename -uid "99CCACF5-9C4F-0DC7-B265-95B214414C83";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[293]" "e[296]" "e[298]" "e[301]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.49268549680709839;
	setAttr ".re" 296;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing37";
	rename -uid "0316D503-824C-B8A0-49F3-8F896692F533";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[132:133]" "e[135]" "e[137]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.48568615317344666;
	setAttr ".dr" no;
	setAttr ".re" 133;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing38";
	rename -uid "FAAC0D12-F442-5547-DD78-7BB284308401";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[76:77]" "e[463]" "e[465]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.055705860257148743;
	setAttr ".re" 76;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing39";
	rename -uid "A7DBBE8B-BC47-FE01-9FCF-8B922125EA8F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[87]" "e[89]" "e[92:93]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.92147409915924072;
	setAttr ".dr" no;
	setAttr ".re" 92;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing40";
	rename -uid "1F829201-EA41-E940-E6C4-3294FCD93A33";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[15]" "e[17]" "e[28:29]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.94323670864105225;
	setAttr ".dr" no;
	setAttr ".re" 17;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "B0F0E840-E743-4B0E-750F-BF923A9A1B15";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[192]" -type "float3" 0 -0.005764964 0 ;
	setAttr ".tk[193]" -type "float3" 0 -0.005764964 0 ;
	setAttr ".tk[194]" -type "float3" 0 -0.005764964 0 ;
	setAttr ".tk[195]" -type "float3" 0 -0.005764964 0 ;
	setAttr ".tk[196]" -type "float3" 0 0.0025337355 0 ;
	setAttr ".tk[197]" -type "float3" 0 0.0025337355 0 ;
	setAttr ".tk[198]" -type "float3" 0 0.0025337355 0 ;
	setAttr ".tk[199]" -type "float3" 0 0.0025337355 0 ;
	setAttr ".tk[204]" -type "float3" 0 -0.02194404 0 ;
	setAttr ".tk[205]" -type "float3" 0 -0.02194404 0 ;
	setAttr ".tk[206]" -type "float3" 0 -0.02194404 0 ;
	setAttr ".tk[207]" -type "float3" 0 -0.02194404 0 ;
	setAttr ".tk[208]" -type "float3" 0 0.0044482648 0 ;
	setAttr ".tk[209]" -type "float3" 0 0.0044482648 0 ;
	setAttr ".tk[210]" -type "float3" 0 0.0044482648 0 ;
	setAttr ".tk[211]" -type "float3" 0 0.0044482648 0 ;
createNode polySplitRing -n "polySplitRing41";
	rename -uid "42399020-0644-9BF2-09E5-129900186F3B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[39]" "e[41]" "e[396:397]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.91736799478530884;
	setAttr ".dr" no;
	setAttr ".re" 396;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing42";
	rename -uid "A95B726B-F946-6BEB-34E4-F698C773F6AB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[181:182]" "e[184:185]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.09463222324848175;
	setAttr ".re" 181;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing43";
	rename -uid "F1B4298A-1842-0C00-D173-2FBED28242D2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[199:200]" "e[420:421]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.93087601661682129;
	setAttr ".dr" no;
	setAttr ".re" 420;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing44";
	rename -uid "3C1BAA8F-B240-3FD9-7EC5-1D96BC70CD1B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[108:109]" "e[111]" "e[113]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.51923668384552002;
	setAttr ".re" 108;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing45";
	rename -uid "3F4205A2-7C49-D118-350B-3396A661111F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[257]" "e[260]" "e[262]" "e[265]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.44945356249809265;
	setAttr ".re" 260;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing46";
	rename -uid "47D05F25-3C4B-EDBC-0073-8E9A81322D1D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[269]" "e[272]" "e[274]" "e[277]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.45546078681945801;
	setAttr ".re" 277;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing47";
	rename -uid "6E76D615-FF4F-546D-91A4-D5AF4E707C4B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[140:141]" "e[143]" "e[145]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.5260130763053894;
	setAttr ".dr" no;
	setAttr ".re" 141;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing48";
	rename -uid "A2DF7A7C-3E44-5028-362D-E5ADDFE6FB2D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[388:389]" "e[391]" "e[393]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.11859696358442307;
	setAttr ".re" 388;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing49";
	rename -uid "2E777016-C649-9523-723A-C394BDD9B47F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[244:245]" "e[493]" "e[495]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.084864236414432526;
	setAttr ".re" 245;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing50";
	rename -uid "BF32DDAC-C949-1D17-73F4-A69DDB7E328F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[172]" "e[174]" "e[176]" "e[178]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.89489078521728516;
	setAttr ".dr" no;
	setAttr ".re" 174;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing51";
	rename -uid "D6E9231A-524A-2B2B-0A4D-BDAC1C41F75D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20:21]" "e[103]" "e[105]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.88397419452667236;
	setAttr ".dr" no;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing52";
	rename -uid "D6F0B1C9-854F-8042-27A6-388654678635";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[92:93]" "e[527]" "e[529]";
	setAttr ".ix" -type "matrix" 0.72368219284792923 0 0 0 0 24.561124555897987 0 0 0 0 0.72368219284792923 0
		 -3.6223224268748213 12.177456321962971 0 1;
	setAttr ".wt" 0.090463466942310333;
	setAttr ".re" 92;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "99CBF4B3-9C4F-D1C0-137C-2E828D0A83C1";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 1\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1170\n            -height 555\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n"
		+ "            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n"
		+ "            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n"
		+ "            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n"
		+ "                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n"
		+ "                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n"
		+ "                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n"
		+ "                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -highlightConnections 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n"
		+ "                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 1\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1170\\n    -height 555\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 1\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1170\\n    -height 555\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "3A24180D-634B-D6CD-224D-5383305BD324";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode rmanGlobals -s -n "rmanGlobals";
	rename -uid "F8B2C2F8-1F47-39A1-CE44-FBBDAD7E2CEC";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".hider_minSamples" 0;
	setAttr ".hider_maxSamples" 128;
	setAttr ".ri_pixelVariance" 0.0099999997764825821;
	setAttr ".hider_darkfalloff" 0.02500000037252903;
	setAttr ".hider_incremental" yes;
	setAttr ".ipr_hider_maxSamples" 64;
	setAttr ".ipr_ri_pixelVariance" 0.05000000074505806;
	setAttr ".ri_maxSpecularDepth" 4;
	setAttr ".ri_maxDiffuseDepth" 1;
	setAttr ".ri_displayFilter" -type "string" "gaussian";
	setAttr ".ri_displayFilterSize" -type "float2" 2 2 ;
	setAttr ".motionBlur" 0;
	setAttr ".cameraBlur" no;
	setAttr ".shutterAngle" 180;
	setAttr ".shutterOpenEnd" 0;
	setAttr ".shutterCloseStart" 1;
	setAttr ".shutterTiming" 0;
	setAttr ".motionSamples" 2;
	setAttr ".displayFilters[0]" -type "string" "";
	setAttr ".sampleFilters[0]" -type "string" "";
	setAttr ".outputAllShaders" no;
	setAttr ".nestedInstancing" no;
	setAttr ".reentrantProcedurals" yes;
	setAttr ".outputShadowAOV" 0;
	setAttr ".enableImagePlaneFilter" yes;
	setAttr ".learnLightSelection" no;
	setAttr ".ri_hider_adaptAll" no;
	setAttr ".osl_batched" 0;
	setAttr ".adaptiveSampler" 0;
	setAttr ".opt_bucket_order" -type "string" "circle";
	setAttr ".limits_bucketsize" -type "long2" 16 16 ;
	setAttr ".limits_othreshold" -type "float3" 0.99599999 0.99599999 0.99599999 ;
	setAttr ".rfm_referenceFrame" 0;
	setAttr ".dice_micropolygonlength" 1;
	setAttr ".dice_watertight" no;
	setAttr ".dice_referenceCameraType" 0;
	setAttr ".dice_referenceCamera" -type "string" "";
	setAttr ".hair_minWidth" 0.5;
	setAttr ".trace_autobias" yes;
	setAttr ".trace_bias" 0.0010000000474974513;
	setAttr ".trace_worldorigin" -type "string" "camera";
	setAttr ".trace_worldoffset" -type "float3" 0 0 0 ;
	setAttr ".opt_cropWindowEnable" no;
	setAttr ".opt_cropWindowTopLeft" -type "float2" 0 0 ;
	setAttr ".opt_cropWindowBottomRight" -type "float2" 1 1 ;
	setAttr ".user_sceneUnits" 1;
	setAttr ".user_iesIgnoreWatts" yes;
	setAttr ".limits_texturememory" 4096;
	setAttr ".limits_geocachememory" 4096;
	setAttr ".limits_opacitycachememory" 2048;
	setAttr ".statistics_level" 1;
	setAttr ".statistics_xmlfilename" -type "string" "";
	setAttr ".lpe_diffuse2" -type "string" "Diffuse,HairDiffuse";
	setAttr ".lpe_diffuse3" -type "string" "Subsurface";
	setAttr ".lpe_specular2" -type "string" "Specular,HairSpecularR";
	setAttr ".lpe_specular3" -type "string" "RoughSpecular,HairSpecularTRT";
	setAttr ".lpe_specular4" -type "string" "Clearcoat";
	setAttr ".lpe_specular5" -type "string" "Iridescence";
	setAttr ".lpe_specular6" -type "string" "Fuzz,HairSpecularGLINTS";
	setAttr ".lpe_specular7" -type "string" "SingleScatter,HairSpecularTT";
	setAttr ".lpe_specular8" -type "string" "Glass";
	setAttr ".lpe_user2" -type "string" "Albedo,DiffuseAlbedo,SubsurfaceAlbedo,HairAlbedo";
	setAttr ".lpe_user3" -type "string" "";
	setAttr ".lpe_user4" -type "string" "";
	setAttr ".lpe_user5" -type "string" "";
	setAttr ".lpe_user6" -type "string" "";
	setAttr ".lpe_user7" -type "string" "";
	setAttr ".lpe_user8" -type "string" "";
	setAttr ".lpe_user9" -type "string" "";
	setAttr ".lpe_user10" -type "string" "";
	setAttr ".lpe_user11" -type "string" "";
	setAttr ".lpe_user12" -type "string" "";
	setAttr ".imageFileFormat" -type "string" "<scene>_<layer>_<camera>_<aov>.<f4>.<ext>";
	setAttr ".ribFileFormat" -type "string" "<camera><layer>.<f4>.rib";
	setAttr ".version" 1;
	setAttr ".take" 1;
	setAttr ".imageOutputDir" -type "string" "<ws>/images/<scene>_v<version>_t<take>";
	setAttr ".ribOutputDir" -type "string" "<ws>/renderman/rib/<scene>/v<version>_t<take>";
	setAttr -s 10 ".UserTokens";
	setAttr ".UserTokens[0].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[0].userTokenValues" -type "string" "";
	setAttr ".UserTokens[1].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[1].userTokenValues" -type "string" "";
	setAttr ".UserTokens[2].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[2].userTokenValues" -type "string" "";
	setAttr ".UserTokens[3].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[3].userTokenValues" -type "string" "";
	setAttr ".UserTokens[4].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[4].userTokenValues" -type "string" "";
	setAttr ".UserTokens[5].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[5].userTokenValues" -type "string" "";
	setAttr ".UserTokens[6].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[6].userTokenValues" -type "string" "";
	setAttr ".UserTokens[7].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[7].userTokenValues" -type "string" "";
	setAttr ".UserTokens[8].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[8].userTokenValues" -type "string" "";
	setAttr ".UserTokens[9].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[9].userTokenValues" -type "string" "";
	setAttr ".rlfData" -type "string" "init";
createNode rmanDisplay -s -n "rmanDefaultDisplay";
	rename -uid "82A1592B-364F-6174-7321-B69383B19ED1";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".enable" yes;
	setAttr ".denoise" no;
	setAttr ".frameMode" 0;
	setAttr ".remapBreakPoint" 0;
	setAttr ".remapMaxValue" 0;
	setAttr ".remapSmoothness" 0;
	setAttr -s 2 ".displayChannels";
	setAttr -l on ".name" -type "string" "beauty";
createNode d_openexr -n "d_openexr";
	rename -uid "3019C0BA-F84E-B6BB-BC23-D596EE91A2FD";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".asrgba" yes;
	setAttr ".storage" -type "string" "scanline";
	setAttr ".exrpixeltype" -type "string" "half";
	setAttr ".compression" -type "string" "zips";
	setAttr ".compressionlevel" 45;
createNode rmanDisplayChannel -n "Ci";
	rename -uid "B2A36485-A542-317F-F5E2-E88F14681DF0";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".enable" yes;
	setAttr ".channelType" -type "string" "color";
	setAttr ".channelSource" -type "string" "Ci";
	setAttr ".lpeLightGroup" -type "string" "";
	setAttr ".filter" -type "string" "inherit from display";
	setAttr ".filterwidth" -type "float2" -1 -1 ;
	setAttr ".statistics" -type "string" "";
	setAttr ".remapBreakPoint" 0;
	setAttr ".remapMaxValue" 0;
	setAttr ".remapSmoothness" 0;
	setAttr -l on ".name" -type "string" "Ci";
createNode rmanDisplayChannel -n "a";
	rename -uid "614406EC-E744-3BA1-5C6C-4AB7350DA56B";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".enable" yes;
	setAttr ".channelType" -type "string" "float";
	setAttr ".channelSource" -type "string" "a";
	setAttr ".lpeLightGroup" -type "string" "";
	setAttr ".filter" -type "string" "inherit from display";
	setAttr ".filterwidth" -type "float2" -1 -1 ;
	setAttr ".statistics" -type "string" "";
	setAttr ".remapBreakPoint" 0;
	setAttr ".remapMaxValue" 0;
	setAttr ".remapSmoothness" 0;
	setAttr -l on ".name" -type "string" "a";
createNode PxrPathTracer -s -n "PxrPathTracer";
	rename -uid "4F37572F-FF40-E903-8EC4-AD94106C41C7";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".maxPathLength" 10;
	setAttr ".maxContinuationLength" -1;
	setAttr ".maxNonStochasticOpacityEvents" 0;
	setAttr ".sampleMode" -type "string" "bxdf";
	setAttr ".numLightSamples" 1;
	setAttr ".numBxdfSamples" 1;
	setAttr ".numIndirectSamples" 1;
	setAttr ".numDiffuseSamples" 1;
	setAttr ".numSpecularSamples" 1;
	setAttr ".numSubsurfaceSamples" 1;
	setAttr ".numRefractionSamples" 1;
	setAttr ".allowCaustics" no;
	setAttr ".accumOpacity" no;
	setAttr ".rouletteDepth" 4;
	setAttr ".rouletteThreshold" 0.20000000298023224;
	setAttr ".clampDepth" 2;
	setAttr ".clampLuminance" 10;
createNode PxrDisney -n "PxrDisney1";
	rename -uid "298B321B-BB45-B481-F0E6-7790CA3A1293";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".baseColor" -type "float3" 0.2 0.5 0.80000001 ;
	setAttr ".emitColor" -type "float3" 0 0 0 ;
	setAttr ".subsurface" 0;
	setAttr ".subsurfaceColor" -type "float3" 0 0 0 ;
	setAttr ".metallic" 0;
	setAttr ".specular" 0.5;
	setAttr ".specularTint" 0;
	setAttr ".roughness" 0.25;
	setAttr ".anisotropic" 0;
	setAttr ".sheen" 0;
	setAttr ".sheenTint" 0.5;
	setAttr ".clearcoat" 0;
	setAttr ".clearcoatGloss" 1;
	setAttr ".bumpNormal" -type "float3" 0 0 0 ;
	setAttr ".presence" 1;
	setAttr ".inputAOV" 0;
createNode shadingEngine -n "PxrDisney1SG";
	rename -uid "14A4F754-1F48-D37B-AF1C-4E9DAFB3027E";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "CBB2FA25-7346-BF2D-E46E-CE8BB8427C41";
createNode lambert -n "lambert2";
	rename -uid "4920A30B-3548-F243-E158-5DB6C6A6D042";
createNode PxrOSL -n "PxrOSL1";
	rename -uid "D973F63A-B247-89A6-AA00-79B18C030A22";
	addAttr -ci true -sn "Pos" -ln "Pos" -ct "osl" -at "float3" -nc 3;
	addAttr -ci true -sn "PosX" -ln "PosX" -at "float" -p "Pos";
	addAttr -ci true -sn "PosY" -ln "PosY" -at "float" -p "Pos";
	addAttr -ci true -sn "PosZ" -ln "PosZ" -at "float" -p "Pos";
	addAttr -ci true -sn "globalScale" -ln "globalScale" -ct "osl" -dv 1 -at "float";
	addAttr -ci true -sn "Sharpness" -ln "Sharpness" -ct "osl" -dv 0.01 -at "float";
	addAttr -ci true -sn "ringfreq" -ln "ringfreq" -ct "osl" -dv 8 -at "float";
	addAttr -ci true -sn "ringunevenness" -ln "ringunevenness" -ct "osl" -dv 0.5 -at "float";
	addAttr -ci true -sn "ringnoise" -ln "ringnoise" -ct "osl" -dv 0.02 -at "float";
	addAttr -ci true -sn "ringnoisefreq" -ln "ringnoisefreq" -ct "osl" -dv 1 -at "float";
	addAttr -ci true -sn "grainfreq" -ln "grainfreq" -ct "osl" -dv 25 -at "float";
	addAttr -ci true -sn "trunkwobble" -ln "trunkwobble" -ct "osl" -dv 0.15 -at "float";
	addAttr -ci true -sn "trunkwobblefreq" -ln "trunkwobblefreq" -ct "osl" -dv 0.025 
		-at "float";
	addAttr -ci true -sn "angularwobble" -ln "angularwobble" -ct "osl" -dv 1 -at "float";
	addAttr -ci true -sn "angularwobblefreq" -ln "angularwobblefreq" -ct "osl" -dv 1.5 
		-at "float";
	addAttr -ci true -uac -sn "Clightwood" -ln "Clightwood" -ct "osl" -at "float3" -nc 
		3;
	addAttr -ci true -sn "ClightwoodR" -ln "ClightwoodR" -dv 0.5 -at "float" -p "Clightwood";
	addAttr -ci true -sn "ClightwoodG" -ln "ClightwoodG" -dv 0.2 -at "float" -p "Clightwood";
	addAttr -ci true -sn "ClightwoodB" -ln "ClightwoodB" -dv 0.067 -at "float" -p "Clightwood";
	addAttr -ci true -uac -sn "Cdarkwood" -ln "Cdarkwood" -ct "osl" -at "float3" -nc 
		3;
	addAttr -ci true -sn "CdarkwoodR" -ln "CdarkwoodR" -dv 0.15 -at "float" -p "Cdarkwood";
	addAttr -ci true -sn "CdarkwoodG" -ln "CdarkwoodG" -dv 0.077 -at "float" -p "Cdarkwood";
	addAttr -ci true -sn "CdarkwoodB" -ln "CdarkwoodB" -dv 0.028 -at "float" -p "Cdarkwood";
	addAttr -ci true -sn "ringy" -ln "ringy" -ct "osl" -dv 1 -at "float";
	addAttr -ci true -sn "grainy" -ln "grainy" -ct "osl" -dv 1 -at "float";
	addAttr -w false -uac -sn "Color" -ln "Color" -ct "osl" -at "float3" -nc 3;
	addAttr -w false -sn "ColorR" -ln "ColorR" -at "float" -p "Color";
	addAttr -w false -sn "ColorG" -ln "ColorG" -at "float" -p "Color";
	addAttr -w false -sn "ColorB" -ln "ColorB" -at "float" -p "Color";
	addAttr -w false -uac -sn "Spec" -ln "Spec" -ct "osl" -at "float3" -nc 3;
	addAttr -w false -sn "SpecR" -ln "SpecR" -dv 0.1 -at "float" -p "Spec";
	addAttr -w false -sn "SpecG" -ln "SpecG" -dv 0.1 -at "float" -p "Spec";
	addAttr -w false -sn "SpecB" -ln "SpecB" -dv 0.1 -at "float" -p "Spec";
	addAttr -w false -sn "Roughness" -ln "Roughness" -ct "osl" -at "float";
	addAttr -w false -sn "Disp" -ln "Disp" -ct "osl" -at "float";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".shadername" -type "string" "/Applications/Pixar/RenderManProServer-22.1/lib/RenderManAssetLibrary/Materials/Wood/Gritz_Oak.rma/wood.oso";
	setAttr ".oslCompile" -type "string" "";
	setAttr ".shaderSource" -type "string" "/Applications/Pixar/RenderManProServer-22.1/lib/RenderManAssetLibrary/Materials/Wood/Gritz_Oak.rma/wood.osl";
	setAttr ".oslCode" -type "string" (
		"// for the original renderman shader, check http://www.larrygritz.com/arman/materials.html\n// adapted from larry gritz advanced renderman patterns.h\n// Credit: Michel @ Small Blender Things\n// Modified by plp @ pixar\n\nfloat smoothpulse (float e0, float e1, float e2, float e3, float x)\n{\n    return smoothstep(e0,e1,x) - smoothstep(e2,e3,x);\n}\n\n/* A pulse train of smoothsteps: a signal that repeats with a given\n * period, and is 0 when 0 <= mod(x/period,1) < edge, and 1 when\n * mod(x/period,1) > edge.\n */\nfloat smoothpulsetrain (float e0, float e1, float e2, float e3, float period, float x)\n{\n    return smoothpulse (e0, e1, e2, e3, mod(x,period));\n}\n\n// adapted from larry gritz advanced renderman noises.h\n/* fractional Brownian motion\n * Inputs:\n *    p              position\n *    octaves        max # of octaves to calculate\n *    lacunarity     frequency spacing between successive octaves\n *    gain           scaling factor between successive octaves\n */\n\n/* A vector-valued antialiased fBm. */\nvector vfBm (point p, float octaves, float lacunarity, float gain)\n"
		+ "{\n    float amp = 1;\n    point pp = p;\n    vector sum = 0;\n    float i;\n\n    for (i = 0;  i < octaves;  i += 1) {\n        vector d = snoise(pp);\n        sum += amp * d;\n        amp *= gain;\n        pp *= lacunarity;\n    }\n    return sum;\n}\n\n// adapted from larry gritz oak.sl and oak.h\n// original comments between /* ... */\n// my comments start with //\n// note that I dropped the whole filterwidth stuff, partly\n// because I don't think it necessary in Blender Cycles, partly\n// because the derivatives and area() function doesn't seem to work (yet)\n// all specialized snoise defines are replaced by snoise() function calls\nfloat oaktexture (point Pshad,\n                  float dPshad,\n                  float ringfreq,\n                  float ringunevenness,\n                  float grainfreq,\n                  float ringnoise,\n                  float ringnoisefreq,\n                  float trunkwobble,\n                  float trunkwobblefreq,\n                  float angularwobble,\n                  float angularwobblefreq,\n"
		+ "                  float ringy,\n                  float grainy)\n{\n    /* We shade based on Pshad, but we add several layers of warping: */\n    /* Some general warping of the domain */\n    vector offset = vfBm(Pshad*ringnoisefreq, 2, 4, 0.5);\n\n    point Pring = Pshad + ringnoise*offset;\n    /* The trunk isn't totally steady xy as you go up in z */\n    vector d = snoise(Pshad[2]*trunkwobblefreq) ;\n    Pring += trunkwobble * d * vector(1,1,0);\n\n    /* Calculate the radius from the center. */\n    float r = hypot(Pring[0], Pring[1]) * ringfreq;\n    /* Add some noise around the trunk */\n    r += angularwobble * smoothstep(0,5,r)\n    * snoise (angularwobblefreq*(Pring)*vector(1,1,0.1));\n\n    /* Now add some noise so all rings are not equal width */\n    r += ringunevenness*snoise(r);\n\n    float inring = smoothpulsetrain (.1, .55, .7, .95, 1, r);\n\n    point Pgrain = Pshad*grainfreq*vector(1,1,.05);\n    float dPgrain = dPshad; //dropped filterwidthp(Pgrain);\n    float grain = 0;\n    float i, amp=1;\n    for (i = 0;  i < 2;  i += 1) {\n"
		+ "        float grain1valid = 1-smoothstep(.2,.6,dPgrain);\n        if (grain1valid > 0) {\n            float g = grain1valid * snoise (Pgrain);\n            g *= (0.3 + 0.7*inring);\n            g = pow(clamp(0.8 - (g),0,1),2);\n            g = grainy * smoothstep (0.5, 1, g);\n            if (i == 0)\n                inring *= (1-0.4*grain1valid);\n            grain = max (grain, g);\n        }\n        Pgrain *= 2;\n        dPgrain *= 2;\n        amp *= 0.5;\n    }\n\n    return mix (inring*ringy, 1, grain);\n}\n\n// larry gritz' original shader was a closure but this shader\n// provides different outputs that you can plug into your own\n// closures/shaders\nshader wood(\n    point Pos = P\n    [[int lockgeom = 1]],\n    float globalScale = 1.0,\n    float Sharpness = 0.01, // sharpness of the grain. hand tweaked because we lack derivatives.\n    float ringfreq = 8,\n    float ringunevenness = 0.5,\n    float ringnoise = 0.02,\n    float ringnoisefreq = 1,\n    float grainfreq = 25,\n    float trunkwobble = 0.15,\n    float trunkwobblefreq = 0.025,\n"
		+ "    float angularwobble = 1,\n    float angularwobblefreq = 1.5,\n    color Clightwood = color(.5, .2, .067),\n    color Cdarkwood = color(0.15, 0.077, 0.028),\n    float ringy = 1,\n    float grainy = 1,\n    output color Color = 0,\n    output color Spec = 0.1,\n    output float Roughness = 0.1,\n    output float Disp = 0\n    )\n{\n    point Pshad = transform (\"object\", Pos/max(0.0001, globalScale));\n    float wood = oaktexture (Pshad, Sharpness, ringfreq, ringunevenness, grainfreq,\n         ringnoise, ringnoisefreq, trunkwobble, trunkwobblefreq,\n         angularwobble, angularwobblefreq, ringy, grainy);\n\n    Color = mix (Clightwood, Cdarkwood, wood);\n    Disp = -wood;  // lightwood = 0, darkwood is deeper/lower = -1\n    Spec = 0.2*(1-0.5*wood); // darkwood is less specular\n    Roughness = 0.2+0.1*wood; // and rougher\n}\n");
	setAttr ".oslRefresh" -type "string" "";
	setAttr ".oslRebuildUI" 0;
	setAttr ".Pos" -type "float3" 0 0 0 ;
	setAttr ".globalScale" 1;
	setAttr ".Sharpness" 0.0099999997764825821;
	setAttr ".ringfreq" 8;
	setAttr ".ringunevenness" 0.5;
	setAttr ".ringnoise" 0.019999999552965164;
	setAttr ".ringnoisefreq" 1;
	setAttr ".grainfreq" 25;
	setAttr ".trunkwobble" 0.15000000596046448;
	setAttr ".trunkwobblefreq" 0.02500000037252903;
	setAttr ".angularwobble" 1;
	setAttr ".angularwobblefreq" 1.5;
	setAttr ".Clightwood" -type "float3" 0.22400001 0.089601427 0.030016001 ;
	setAttr ".Cdarkwood" -type "float3" 0.5 0.5 0.5 ;
	setAttr ".ringy" 1;
	setAttr ".grainy" 1;
createNode shadingEngine -n "Gritz_Oak_SG";
	rename -uid "1A967510-6F44-899C-D45C-CAAC0F4CFD44";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "5C330FC5-9641-24E4-46E5-55A13711122A";
createNode PxrSurface -n "Gritz_Oak";
	rename -uid "7EDA3077-5940-78F2-09DF-72B57286E306";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".inputMaterial" 0;
	setAttr ".diffuseGain" 1;
	setAttr ".diffuseColor" -type "float3" 0 0 0 ;
	setAttr ".diffuseRoughness" 0;
	setAttr ".diffuseExponent" 1;
	setAttr ".diffuseBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".diffuseDoubleSided" no;
	setAttr ".diffuseBackUseDiffuseColor" yes;
	setAttr ".diffuseBackColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".diffuseTransmitGain" 0;
	setAttr ".diffuseTransmitColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".specularFresnelMode" 1;
	setAttr ".specularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".specularEdgeColor" -type "float3" 0.1 0.1 0.1 ;
	setAttr ".specularFresnelShape" 5;
	setAttr ".specularIor" -type "float3" 1.4 1.4 1.4 ;
	setAttr ".specularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".specularRoughness" 0;
	setAttr ".specularModelType" 0;
	setAttr ".specularAnisotropy" 0;
	setAttr ".specularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".specularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".specularDoubleSided" no;
	setAttr ".roughSpecularFresnelMode" 0;
	setAttr ".roughSpecularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularFresnelShape" 5;
	setAttr ".roughSpecularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".roughSpecularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularRoughness" 0.60000002384185791;
	setAttr ".roughSpecularModelType" 0;
	setAttr ".roughSpecularAnisotropy" 0;
	setAttr ".roughSpecularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularDoubleSided" no;
	setAttr ".clearcoatFresnelMode" 0;
	setAttr ".clearcoatFaceColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatFresnelShape" 5;
	setAttr ".clearcoatIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".clearcoatExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".clearcoatThickness" 0;
	setAttr ".clearcoatAbsorptionTint" -type "float3" 0 0 0 ;
	setAttr ".clearcoatRoughness" 0;
	setAttr ".clearcoatModelType" 0;
	setAttr ".clearcoatAnisotropy" 0;
	setAttr ".clearcoatAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".clearcoatBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".clearcoatDoubleSided" no;
	setAttr ".specularEnergyCompensation" 0;
	setAttr ".clearcoatEnergyCompensation" 0;
	setAttr ".iridescenceFaceGain" 0;
	setAttr ".iridescenceEdgeGain" 0;
	setAttr ".iridescenceFresnelShape" 5;
	setAttr ".iridescenceMode" 0;
	setAttr ".iridescencePrimaryColor" -type "float3" 1 0 0 ;
	setAttr ".iridescenceSecondaryColor" -type "float3" 0 0 1 ;
	setAttr ".iridescenceRoughness" 0.20000000298023224;
	setAttr ".iridescenceAnisotropy" 0;
	setAttr ".iridescenceAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".iridescenceCurve" 1;
	setAttr ".iridescenceScale" 1;
	setAttr ".iridescenceFlip" no;
	setAttr ".iridescenceThickness" 800;
	setAttr ".iridescenceDoubleSided" no;
	setAttr ".fuzzGain" 0;
	setAttr ".fuzzColor" -type "float3" 1 1 1 ;
	setAttr ".fuzzConeAngle" 8;
	setAttr ".fuzzBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".fuzzDoubleSided" no;
	setAttr ".subsurfaceType" 0;
	setAttr ".subsurfaceGain" 0;
	setAttr ".subsurfaceColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".subsurfaceDmfp" 10;
	setAttr ".subsurfaceDmfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".shortSubsurfaceGain" 0;
	setAttr ".shortSubsurfaceColor" -type "float3" 0.89999998 0.89999998 0.89999998 ;
	setAttr ".shortSubsurfaceDmfp" 5;
	setAttr ".longSubsurfaceGain" 0;
	setAttr ".longSubsurfaceColor" -type "float3" 0.80000001 0 0 ;
	setAttr ".longSubsurfaceDmfp" 20;
	setAttr ".subsurfaceDirectionality" 0;
	setAttr ".subsurfaceBleed" 0;
	setAttr ".subsurfaceDiffuseBlend" 0;
	setAttr ".subsurfaceResolveSelfIntersections" no;
	setAttr ".subsurfaceIor" 1.3999999761581421;
	setAttr ".subsurfacePostTint" -type "float3" 1 1 1 ;
	setAttr ".subsurfaceDiffuseSwitch" 1;
	setAttr ".subsurfaceDoubleSided" no;
	setAttr ".subsurfaceTransmitGain" 0;
	setAttr ".considerBackside" yes;
	setAttr ".continuationRayMode" 0;
	setAttr ".maxContinuationHits" 2;
	setAttr ".followTopology" 0;
	setAttr ".subsurfaceSubset" -type "string" "";
	setAttr ".singlescatterGain" 0;
	setAttr ".singlescatterColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".singlescatterMfp" 10;
	setAttr ".singlescatterMfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".singlescatterDirectionality" 0;
	setAttr ".singlescatterIor" 1.2999999523162842;
	setAttr ".singlescatterBlur" 0;
	setAttr ".singlescatterDirectGain" 0;
	setAttr ".singlescatterDirectGainTint" -type "float3" 1 1 1 ;
	setAttr ".singlescatterDoubleSided" no;
	setAttr ".singlescatterConsiderBackside" yes;
	setAttr ".singlescatterContinuationRayMode" 0;
	setAttr ".singlescatterMaxContinuationHits" 2;
	setAttr ".singlescatterDirectGainMode" 0;
	setAttr ".singlescatterSubset" -type "string" "";
	setAttr ".irradianceTint" -type "float3" 1 1 1 ;
	setAttr ".irradianceRoughness" 0;
	setAttr ".unitLength" 0.10000000149011612;
	setAttr ".refractionGain" 0;
	setAttr ".reflectionGain" 0;
	setAttr ".refractionColor" -type "float3" 1 1 1 ;
	setAttr ".glassRoughness" 0.10000000149011612;
	setAttr ".glassAnisotropy" 0;
	setAttr ".glassAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".glassIor" 1.5;
	setAttr ".mwWalkable" no;
	setAttr ".mwIor" -1;
	setAttr ".thinGlass" no;
	setAttr ".ignoreFresnel" no;
	setAttr ".ignoreAccumOpacity" no;
	setAttr ".blocksVolumes" no;
	setAttr ".ssAlbedo" -type "float3" 0 0 0 ;
	setAttr ".extinction" -type "float3" 0 0 0 ;
	setAttr ".g" 0;
	setAttr ".multiScatter" no;
	setAttr ".enableOverlappingVolumes" no;
	setAttr ".glowGain" 0;
	setAttr ".glowColor" -type "float3" 1 1 1 ;
	setAttr ".bumpNormal" -type "float3" 1 1 1 ;
	setAttr ".shadowColor" -type "float3" 0 0 0 ;
	setAttr ".shadowMode" 0;
	setAttr ".presence" 1;
	setAttr ".presenceCached" 1;
	setAttr ".mwStartable" no;
	setAttr ".roughnessMollificationClamp" 32;
	setAttr ".utilityPattern[0]"  0;
createNode wood -n "wood1";
	rename -uid "BD34DCB9-F949-AF26-FC35-09BAF9F00D44";
	setAttr ".fc" -type "float3" 0.18831168 0.14329062 0.10763933 ;
	setAttr ".vc" -type "float3" 0.006493506 0.0032260749 0.0016130348 ;
	setAttr ".v" 0.4632352888584137;
	setAttr ".ls" 0.063235290348529816;
	setAttr ".rd" 0.64705884456634521;
	setAttr ".a" 44.117645263671875;
	setAttr ".gc" -type "float3" 0.11688311 0.038630903 0 ;
	setAttr ".gx" 0.66176468133926392;
	setAttr ".gs" 0.033088237047195435;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
	setAttr -s 5 ".r";
select -ne :lightList1;
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "renderman";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultLightSet;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polySplitRing52.out" "pCubeShape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "PxrDisney1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Gritz_Oak_SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "PxrDisney1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Gritz_Oak_SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube1.out" "polySplitRing1.ip";
connectAttr "pCubeShape1.wm" "polySplitRing1.mp";
connectAttr "polySplitRing1.out" "polySplitRing2.ip";
connectAttr "pCubeShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing2.out" "polySplitRing3.ip";
connectAttr "pCubeShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCubeShape1.wm" "polySplitRing4.mp";
connectAttr "polySplitRing4.out" "polySplitRing5.ip";
connectAttr "pCubeShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCubeShape1.wm" "polySplitRing6.mp";
connectAttr "polySplitRing6.out" "polySplitRing7.ip";
connectAttr "pCubeShape1.wm" "polySplitRing7.mp";
connectAttr "polySplitRing7.out" "polySplitRing8.ip";
connectAttr "pCubeShape1.wm" "polySplitRing8.mp";
connectAttr "polySplitRing8.out" "polySplitRing9.ip";
connectAttr "pCubeShape1.wm" "polySplitRing9.mp";
connectAttr "polySplitRing9.out" "polySplitRing10.ip";
connectAttr "pCubeShape1.wm" "polySplitRing10.mp";
connectAttr "polySplitRing10.out" "polySplitRing11.ip";
connectAttr "pCubeShape1.wm" "polySplitRing11.mp";
connectAttr "polySplitRing11.out" "polySplitRing12.ip";
connectAttr "pCubeShape1.wm" "polySplitRing12.mp";
connectAttr "polySplitRing12.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyExtrudeFace1.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "polyMirror1.ip";
connectAttr "pCube1.sp" "polyMirror1.sp";
connectAttr "pCubeShape1.wm" "polyMirror1.mp";
connectAttr "polyMirror1.out" "polySplitRing13.ip";
connectAttr "pCubeShape1.wm" "polySplitRing13.mp";
connectAttr "polySplitRing13.out" "polySplitRing14.ip";
connectAttr "pCubeShape1.wm" "polySplitRing14.mp";
connectAttr "polySplitRing14.out" "polySplitRing15.ip";
connectAttr "pCubeShape1.wm" "polySplitRing15.mp";
connectAttr "polySplitRing15.out" "polySplitRing16.ip";
connectAttr "pCubeShape1.wm" "polySplitRing16.mp";
connectAttr "polySplitRing16.out" "polySplitRing17.ip";
connectAttr "pCubeShape1.wm" "polySplitRing17.mp";
connectAttr "polySplitRing17.out" "polySplitRing18.ip";
connectAttr "pCubeShape1.wm" "polySplitRing18.mp";
connectAttr "polySplitRing18.out" "polySplitRing19.ip";
connectAttr "pCubeShape1.wm" "polySplitRing19.mp";
connectAttr "polySplitRing19.out" "polySplitRing20.ip";
connectAttr "pCubeShape1.wm" "polySplitRing20.mp";
connectAttr "polySplitRing20.out" "polySplitRing21.ip";
connectAttr "pCubeShape1.wm" "polySplitRing21.mp";
connectAttr "polySplitRing21.out" "polySplitRing22.ip";
connectAttr "pCubeShape1.wm" "polySplitRing22.mp";
connectAttr "polySplitRing22.out" "createColorSet1.ig";
connectAttr "createColorSet1.og" "createColorSet2.ig";
connectAttr "polyTweak1.out" "polySplitRing23.ip";
connectAttr "pCubeShape1.wm" "polySplitRing23.mp";
connectAttr "createColorSet2.og" "polyTweak1.ip";
connectAttr "polySplitRing23.out" "polySplitRing24.ip";
connectAttr "pCubeShape1.wm" "polySplitRing24.mp";
connectAttr "polySplitRing24.out" "polySplitRing25.ip";
connectAttr "pCubeShape1.wm" "polySplitRing25.mp";
connectAttr "polySplitRing25.out" "polySplitRing26.ip";
connectAttr "pCubeShape1.wm" "polySplitRing26.mp";
connectAttr "polySplitRing26.out" "polySplitRing27.ip";
connectAttr "pCubeShape1.wm" "polySplitRing27.mp";
connectAttr "polySplitRing27.out" "polySplitRing28.ip";
connectAttr "pCubeShape1.wm" "polySplitRing28.mp";
connectAttr "polySplitRing28.out" "polySplitRing29.ip";
connectAttr "pCubeShape1.wm" "polySplitRing29.mp";
connectAttr "polySplitRing29.out" "polySplitRing30.ip";
connectAttr "pCubeShape1.wm" "polySplitRing30.mp";
connectAttr "polySplitRing30.out" "polySplitRing31.ip";
connectAttr "pCubeShape1.wm" "polySplitRing31.mp";
connectAttr "polySplitRing31.out" "polySplitRing32.ip";
connectAttr "pCubeShape1.wm" "polySplitRing32.mp";
connectAttr "polySplitRing32.out" "polySplitRing33.ip";
connectAttr "pCubeShape1.wm" "polySplitRing33.mp";
connectAttr "polySplitRing33.out" "polySplitRing34.ip";
connectAttr "pCubeShape1.wm" "polySplitRing34.mp";
connectAttr "polySplitRing34.out" "polySplitRing35.ip";
connectAttr "pCubeShape1.wm" "polySplitRing35.mp";
connectAttr "polySplitRing35.out" "polySplitRing36.ip";
connectAttr "pCubeShape1.wm" "polySplitRing36.mp";
connectAttr "polySplitRing36.out" "polySplitRing37.ip";
connectAttr "pCubeShape1.wm" "polySplitRing37.mp";
connectAttr "polySplitRing37.out" "polySplitRing38.ip";
connectAttr "pCubeShape1.wm" "polySplitRing38.mp";
connectAttr "polySplitRing38.out" "polySplitRing39.ip";
connectAttr "pCubeShape1.wm" "polySplitRing39.mp";
connectAttr "polyTweak2.out" "polySplitRing40.ip";
connectAttr "pCubeShape1.wm" "polySplitRing40.mp";
connectAttr "polySplitRing39.out" "polyTweak2.ip";
connectAttr "polySplitRing40.out" "polySplitRing41.ip";
connectAttr "pCubeShape1.wm" "polySplitRing41.mp";
connectAttr "polySplitRing41.out" "polySplitRing42.ip";
connectAttr "pCubeShape1.wm" "polySplitRing42.mp";
connectAttr "polySplitRing42.out" "polySplitRing43.ip";
connectAttr "pCubeShape1.wm" "polySplitRing43.mp";
connectAttr "polySplitRing43.out" "polySplitRing44.ip";
connectAttr "pCubeShape1.wm" "polySplitRing44.mp";
connectAttr "polySplitRing44.out" "polySplitRing45.ip";
connectAttr "pCubeShape1.wm" "polySplitRing45.mp";
connectAttr "polySplitRing45.out" "polySplitRing46.ip";
connectAttr "pCubeShape1.wm" "polySplitRing46.mp";
connectAttr "polySplitRing46.out" "polySplitRing47.ip";
connectAttr "pCubeShape1.wm" "polySplitRing47.mp";
connectAttr "polySplitRing47.out" "polySplitRing48.ip";
connectAttr "pCubeShape1.wm" "polySplitRing48.mp";
connectAttr "polySplitRing48.out" "polySplitRing49.ip";
connectAttr "pCubeShape1.wm" "polySplitRing49.mp";
connectAttr "polySplitRing49.out" "polySplitRing50.ip";
connectAttr "pCubeShape1.wm" "polySplitRing50.mp";
connectAttr "polySplitRing50.out" "polySplitRing51.ip";
connectAttr "pCubeShape1.wm" "polySplitRing51.mp";
connectAttr "polySplitRing51.out" "polySplitRing52.ip";
connectAttr "pCubeShape1.wm" "polySplitRing52.mp";
connectAttr ":rmanDefaultDisplay.msg" ":rmanGlobals.displays[0]";
connectAttr ":PxrPathTracer.msg" ":rmanGlobals.ri_integrator";
connectAttr "d_openexr.msg" ":rmanDefaultDisplay.displayType";
connectAttr "Ci.msg" ":rmanDefaultDisplay.displayChannels[0]";
connectAttr "a.msg" ":rmanDefaultDisplay.displayChannels[1]";
connectAttr "PxrDisney1.oc" "PxrDisney1SG.rman__surface";
connectAttr "lambert2.oc" "PxrDisney1SG.ss";
connectAttr "PxrDisney1SG.msg" "materialInfo1.sg";
connectAttr "lambert2.msg" "materialInfo1.m";
connectAttr "wood1.oc" "PxrOSL1.Cdarkwood";
connectAttr "Gritz_Oak.oc" "Gritz_Oak_SG.ss";
connectAttr "pCubeShape1.iog" "Gritz_Oak_SG.dsm" -na;
connectAttr "Gritz_Oak_SG.msg" "materialInfo2.sg";
connectAttr "Gritz_Oak.msg" "materialInfo2.m";
connectAttr "Gritz_Oak.msg" "materialInfo2.t" -na;
connectAttr "PxrOSL1.Color" "Gritz_Oak.diffuseColor";
connectAttr "PxrOSL1.Spec" "Gritz_Oak.specularEdgeColor";
connectAttr "PxrOSL1.Roughness" "Gritz_Oak.specularRoughness";
connectAttr "place3dTexture1.wim" "wood1.pm";
connectAttr "PxrDisney1SG.pa" ":renderPartition.st" -na;
connectAttr "Gritz_Oak_SG.pa" ":renderPartition.st" -na;
connectAttr "PxrDisney1.msg" ":defaultShaderList1.s" -na;
connectAttr "Gritz_Oak.msg" ":defaultShaderList1.s" -na;
connectAttr "place3dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr ":rmanGlobals.msg" ":defaultRenderingList1.r" -na;
connectAttr ":rmanDefaultDisplay.msg" ":defaultRenderingList1.r" -na;
connectAttr "d_openexr.msg" ":defaultRenderingList1.r" -na;
connectAttr ":PxrPathTracer.msg" ":defaultRenderingList1.r" -na;
connectAttr "PxrRectLightShape.msg" ":lightList1.l" -na;
connectAttr "PxrOSL1.msg" ":defaultTextureList1.tx" -na;
connectAttr "wood1.msg" ":defaultTextureList1.tx" -na;
connectAttr "PxrRectLight.iog" ":defaultLightSet.dsm" -na;
// End of ladder.ma
