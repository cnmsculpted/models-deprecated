//Maya ASCII 2018 scene
//Name: bottega-uv.ma
//Last modified: Sat, Nov 10, 2018 12:21:59 AM
//Codeset: UTF-8
requires maya "2018";
requires -nodeType "displayPoints" "Type" "2.0a";
requires "stereoCamera" "10.0";
requires -nodeType "PxrSurface" -nodeType "rmanDisplayChannel" -nodeType "d_openexr"
		 -nodeType "rmanGlobals" -nodeType "PxrPathTracer" -nodeType "PxrSphereLight" -nodeType "rmanDisplay"
		 "RenderMan_for_Maya.py" "1.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Mac OS X 10.14.1";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "C270CC9B-2445-5912-2C50-5C820F1137A4";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.7591159028839183 2.9990077283650862 6.4167757850477347 ;
	setAttr ".r" -type "double3" 350.66164726996334 -690.99999999997101 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "B3B9821A-7E4C-248B-CB5B-728BFA7F55C8";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 10.337205562775178;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.91003373599186865 1.962768902423317 -2.9011842735015589 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "A4ECAAE5-004D-221E-ECCB-45A84559B1D0";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.98407330529478743 1000.1 -3.6398949071614641 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "2AF13C9B-3945-5663-DA6B-F6B8D3BC8E75";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 2.2516097179091741;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "DB9A5EF0-AF49-4BA8-8E85-759A94C84A56";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.3495458970976868 0.28963430491223185 1000.101046419402 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "093CCAB8-3940-ECAC-0323-C69706BC0946";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 996.51516256732305;
	setAttr ".ow" 3.9942078399340795;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 2.8451905626664962 1.3578873562295586 3.5858838520789247 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "5232A728-8D47-3C38-9B57-DDAD144F57BB";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1000614681579 1.3809535516644569 1.9871506626828026 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "E744E83C-4042-3668-4D1F-B9BA64604ECB";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 997.30432513963149;
	setAttr ".ow" 3.0575881592811234;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 2.7957363285262686 1.3986615911969729 0.44064854350950267 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	rename -uid "9EFED24B-2247-7983-A205-5A8ECAB6647B";
	setAttr ".t" -type "double3" 0 0.5 0 ;
	setAttr ".s" -type "double3" 2.0502456776410818 2.0502456776410818 5.8539501523725681 ;
	setAttr ".rp" -type "double3" 0 -0.5 0 ;
	setAttr ".sp" -type "double3" 0 -0.5 0 ;
createNode transform -n "polySurface1" -p "pCube1";
	rename -uid "E80D4293-5749-E594-FCA2-70981D1DDFB7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 0.16036265464520669 ;
	setAttr ".rp" -type "double3" 0 0 0.72023093700408936 ;
	setAttr ".sp" -type "double3" 0 0 0.72023093700408936 ;
createNode mesh -n "polySurfaceShape1" -p "polySurface1";
	rename -uid "1EF9C3B8-B942-C291-19A9-068B8FEE95CE";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -2.0198634e-09 -2.9802322e-08 
		-0.1071713 0.88773161 1.4901161e-08 -0.10717133 0.88773155 0.040413637 -0.10717133 
		-2.2561366e-08 0.040413573 -0.10717133;
createNode transform -n "polySurface2" -p "pCube1";
	rename -uid "5BF0F182-5F41-4A6A-2CBE-1EBA21BB1828";
	setAttr ".s" -type "double3" 1 1 1.226119279100836 ;
createNode transform -n "polySurface3" -p "polySurface2";
	rename -uid "7B2C1CFA-2444-95C3-ED27-9CBE0F3D7A75";
createNode transform -n "polySurface5" -p "polySurface3";
	rename -uid "94EAF9B0-9241-5343-200F-91A52250B935";
createNode mesh -n "polySurfaceShape6" -p "polySurface5";
	rename -uid "988DDE79-2742-5894-D526-96A7FF2F2CDD";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface6" -p "polySurface3";
	rename -uid "C21670CB-954C-E47B-A02E-56A5E73DE76D";
	setAttr ".t" -type "double3" 0 0 -0.26163131739322015 ;
createNode transform -n "transform12" -p "|pCube1|polySurface2|polySurface3|polySurface6";
	rename -uid "03FE7FBC-4144-408F-60B1-79B2A1DE1A35";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape7" -p "transform12";
	rename -uid "FB87EE86-C544-715B-D259-759C747850EB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[4:7]" -type "float3"  0 0 0.10885981 0 0 0.10885981 
		0 0 0.10885981 0 0 0.10885981;
createNode transform -n "transform10" -p "polySurface3";
	rename -uid "BDB5D36E-2642-DCC3-8546-0ABE49EC0A12";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape4" -p "transform10";
	rename -uid "C1ED3E7F-1541-03B7-6E11-9E9FC0DA1395";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface4" -p "polySurface2";
	rename -uid "64F52286-0945-6434-1539-81AE30449943";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.20253147292925699 0 0 ;
createNode mesh -n "polySurfaceShape5" -p "polySurface4";
	rename -uid "9F7F490E-E643-E203-D710-458122A26F20";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0.37705776 0 0 0.37705776 
		0 0 0.37705776 0 0 0.37705776 0 0;
createNode transform -n "transform9" -p "polySurface2";
	rename -uid "88164B92-404D-96C3-554C-08B0C352D8C6";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape2" -p "transform9";
	rename -uid "196DF428-2A41-C0EE-920B-9985C387CD17";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode transform -n "transform1" -p "pCube1";
	rename -uid "8C9F10F5-3348-ED45-0765-98BD2097EA38";
	setAttr ".v" no;
createNode mesh -n "pCubeShape1" -p "transform1";
	rename -uid "BB9A2BC3-874D-C271-9807-059C83500952";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pTorus1";
	rename -uid "7F714773-D246-2CE8-AA09-E08254CA678C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.3248204499287206 1.1142577440074855 1.2834600340007283 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 1.8722901894521309 1.5152858838336571 0.84913650370180194 ;
	setAttr ".rp" -type "double3" 1.6224328189310211 0 0 ;
	setAttr ".sp" -type "double3" 1.0707107063033443 0 0 ;
	setAttr ".spt" -type "double3" 0.55172211262767323 0 0 ;
createNode mesh -n "pTorusShape1" -p "pTorus1";
	rename -uid "BC063B8B-0B43-2554-4E52-4CBC3EF88BC3";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999979138374329 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 94 ".pt";
	setAttr ".pt[0]" -type "float3" -2.7474016e-08 0 -4.6566129e-09 ;
	setAttr ".pt[1]" -type "float3" 5.2154064e-08 0 1.7695129e-08 ;
	setAttr ".pt[2]" -type "float3" 1.3783574e-07 0 -2.2351742e-08 ;
	setAttr ".pt[3]" -type "float3" 5.9604645e-08 0 -6.3329935e-08 ;
	setAttr ".pt[4]" -type "float3" 1.4156103e-07 0 -4.0978193e-08 ;
	setAttr ".pt[5]" -type "float3" -2.2351742e-08 0 1.4901161e-08 ;
	setAttr ".pt[6]" -type "float3" -2.9057264e-07 0 0 ;
	setAttr ".pt[7]" -type "float3" 1.8626451e-08 0 -1.8626451e-08 ;
	setAttr ".pt[8]" -type "float3" -1.7136335e-07 0 -1.7881393e-07 ;
	setAttr ".pt[9]" -type "float3" -2.6728958e-07 0 4.0978193e-08 ;
	setAttr ".pt[10]" -type "float3" 7.7998266e-09 0 1.3469253e-07 ;
	setAttr ".pt[11]" -type "float3" -5.1688403e-08 0 -1.4901161e-08 ;
	setAttr ".pt[12]" -type "float3" 4.1909516e-09 0 4.4703484e-08 ;
	setAttr ".pt[13]" -type "float3" 2.9773219e-08 0 1.5308615e-07 ;
	setAttr ".pt[14]" -type "float3" 2.9150397e-07 0 2.6077032e-08 ;
	setAttr ".pt[15]" -type "float3" 2.4214387e-07 0 -1.7974526e-07 ;
	setAttr ".pt[16]" -type "float3" -8.9406967e-08 0 -1.8626451e-08 ;
	setAttr ".pt[17]" -type "float3" 3.054738e-07 0 0 ;
	setAttr ".pt[18]" -type "float3" -9.6857548e-08 0 -1.4901161e-08 ;
	setAttr ".pt[19]" -type "float3" 0 0 7.4505806e-08 ;
	setAttr ".pt[20]" -type "float3" -1.5646219e-07 0 2.2351742e-08 ;
	setAttr ".pt[21]" -type "float3" 2.9802322e-08 0 -2.9802322e-08 ;
	setAttr ".pt[22]" -type "float3" -5.5879354e-09 0 4.6566129e-09 ;
	setAttr ".pt[23]" -type "float3" 2.0489097e-08 0 0 ;
	setAttr ".pt[24]" -type "float3" -2.3283064e-10 0 -3.8999133e-09 ;
	setAttr ".pt[25]" -type "float3" -1.4668331e-08 0 -3.6670826e-09 ;
	setAttr ".pt[27]" -type "float3" -1.8626451e-09 0 -9.3132257e-10 ;
	setAttr ".pt[29]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".pt[30]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".pt[31]" -type "float3" -4.6566129e-10 0 0 ;
	setAttr ".pt[32]" -type "float3" 1.8067658e-07 0 -1.9324943e-08 ;
	setAttr ".pt[33]" -type "float3" 3.981404e-08 0 -6.2864274e-09 ;
	setAttr ".pt[34]" -type "float3" 1.4901161e-08 0 5.9604645e-08 ;
	setAttr ".pt[35]" -type "float3" -3.6414713e-07 0 -9.5926225e-08 ;
	setAttr ".pt[36]" -type "float3" 7.9162419e-08 0 -1.4901161e-08 ;
	setAttr ".pt[37]" -type "float3" 1.1920929e-07 0 -4.4703484e-08 ;
	setAttr ".pt[38]" -type "float3" -1.8253922e-07 0 -1.4901161e-08 ;
	setAttr ".pt[39]" -type "float3" -8.2887709e-08 0 -2.9802322e-08 ;
	setAttr ".pt[40]" -type "float3" 2.7194619e-07 0 9.406358e-08 ;
	setAttr ".pt[41]" -type "float3" -6.519258e-08 0 7.8231096e-08 ;
	setAttr ".pt[42]" -type "float3" 1.3504177e-08 0 -2.910383e-09 ;
	setAttr ".pt[51]" -type "float3" 4.6566129e-10 0 9.3132257e-10 ;
	setAttr ".pt[52]" -type "float3" 9.3132257e-10 0 -1.3969839e-09 ;
	setAttr ".pt[53]" -type "float3" 0 0 9.3132257e-10 ;
	setAttr ".pt[54]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".pt[55]" -type "float3" 0 0 -1.8626451e-09 ;
	setAttr ".pt[56]" -type "float3" -1.8626451e-09 0 9.3132257e-10 ;
	setAttr ".pt[57]" -type "float3" -3.4924597e-10 0 1.1641532e-10 ;
	setAttr ".pt[58]" -type "float3" 1.7695129e-07 0 -1.5599653e-08 ;
	setAttr ".pt[59]" -type "float3" 4.353933e-08 0 1.1641532e-09 ;
	setAttr ".pt[60]" -type "float3" 4.4703484e-08 0 5.9604645e-08 ;
	setAttr ".pt[61]" -type "float3" -3.6414713e-07 0 -9.5926225e-08 ;
	setAttr ".pt[62]" -type "float3" 7.9162419e-08 0 -1.4901161e-08 ;
	setAttr ".pt[63]" -type "float3" 1.1920929e-07 0 -4.4703484e-08 ;
	setAttr ".pt[64]" -type "float3" -1.8253922e-07 0 -1.4901161e-08 ;
	setAttr ".pt[65]" -type "float3" -1.1269003e-07 0 -2.9802322e-08 ;
	setAttr ".pt[66]" -type "float3" 2.6449561e-07 0 9.406358e-08 ;
	setAttr ".pt[67]" -type "float3" -9.8720193e-08 0 7.8231096e-08 ;
	setAttr ".pt[68]" -type "float3" -2.3748726e-08 0 4.5401976e-09 ;
	setAttr ".pt[77]" -type "float3" 4.6566129e-10 0 0 ;
	setAttr ".pt[78]" -type "float3" -1.3969839e-09 0 2.7939677e-09 ;
	setAttr ".pt[79]" -type "float3" 2.6077032e-08 0 1.0244548e-08 ;
	setAttr ".pt[80]" -type "float3" 1.0803342e-07 0 -1.4901161e-08 ;
	setAttr ".pt[81]" -type "float3" 4.4703484e-08 0 -5.5879354e-08 ;
	setAttr ".pt[82]" -type "float3" 9.6857548e-08 0 -2.6077032e-08 ;
	setAttr ".pt[83]" -type "float3" -3.7252903e-08 0 -1.4901161e-08 ;
	setAttr ".pt[84]" -type "float3" -3.5017729e-07 0 1.4901161e-08 ;
	setAttr ".pt[85]" -type "float3" -1.1175871e-08 0 -1.8626451e-08 ;
	setAttr ".pt[86]" -type "float3" -8.1956387e-08 0 -1.7881393e-07 ;
	setAttr ".pt[87]" -type "float3" -1.7788261e-07 0 4.0978193e-08 ;
	setAttr ".pt[88]" -type "float3" 7.7998266e-09 0 1.3469253e-07 ;
	setAttr ".pt[89]" -type "float3" -5.1688403e-08 0 -1.4901161e-08 ;
	setAttr ".pt[90]" -type "float3" 4.1909516e-09 0 4.4703484e-08 ;
	setAttr ".pt[91]" -type "float3" 5.9575541e-08 0 1.5308615e-07 ;
	setAttr ".pt[92]" -type "float3" 1.7229468e-07 0 2.6077032e-08 ;
	setAttr ".pt[93]" -type "float3" 1.2293458e-07 0 -1.4994293e-07 ;
	setAttr ".pt[94]" -type "float3" -4.4703484e-08 0 -1.8626451e-08 ;
	setAttr ".pt[95]" -type "float3" 3.3527613e-07 0 1.4901161e-08 ;
	setAttr ".pt[96]" -type "float3" -8.1956387e-08 0 -4.4703484e-08 ;
	setAttr ".pt[97]" -type "float3" 7.4505806e-08 0 8.9406967e-08 ;
	setAttr ".pt[98]" -type "float3" -4.4703484e-08 0 0 ;
	setAttr ".pt[99]" -type "float3" 5.2154064e-08 0 -3.7252903e-08 ;
	setAttr ".pt[100]" -type "float3" 1.8626451e-09 0 8.3819032e-09 ;
	setAttr ".pt[101]" -type "float3" 2.514571e-08 0 3.7252903e-09 ;
	setAttr ".pt[102]" -type "float3" 3.608875e-09 0 -3.8999133e-09 ;
	setAttr ".pt[103]" -type "float3" 2.3283064e-10 0 -7.3923729e-09 ;
	setAttr ".pt[104]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[105]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[106]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[107]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[108]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[109]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[110]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[111]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".dr" 1;
createNode transform -n "pTorus2";
	rename -uid "54D20A4F-6343-DB06-6B5E-8FBEAC3F83C1";
	setAttr ".t" -type "double3" 0.22144663179391605 0.90012333193416383 1.8586220401509119 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 2.3209442420421267 2.3209442420421267 2.3209442420421267 ;
createNode mesh -n "pTorusShape2" -p "pTorus2";
	rename -uid "022FB0AB-074A-D95C-7536-618A001F88AA";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.23333333432674408 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode transform -n "PxrSphereLight";
	rename -uid "1C319F51-3743-E680-3282-BE9BECC96CE6";
	setAttr ".t" -type "double3" 0.87355979062890599 0.94393635246008467 -0.093621399966719032 ;
createNode PxrSphereLight -n "PxrSphereLightShape" -p "PxrSphereLight";
	rename -uid "FDD91B64-AB4E-655E-2DA4-FAA8A3909284";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".isc" no;
	setAttr ".bbx" no;
	setAttr ".icn" -type "string" "";
	setAttr ".vwm" 2;
	setAttr ".tpv" 0;
	setAttr ".uit" 0;
	setAttr -k off ".v" yes;
	setAttr ".io" no;
	setAttr ".tmp" no;
	setAttr ".gh" no;
	setAttr ".obcc" -type "float3" 0 0 0 ;
	setAttr ".wfcc" -type "float3" 0 0 0 ;
	setAttr ".uoc" 0;
	setAttr ".oc" 0;
	setAttr ".ovdt" 0;
	setAttr ".ovlod" 0;
	setAttr ".ovs" no;
	setAttr ".ovt" yes;
	setAttr ".ovp" yes;
	setAttr ".ove" yes;
	setAttr ".ovv" yes;
	setAttr ".hpb" no;
	setAttr ".ovrgbf" no;
	setAttr ".ovc" 0;
	setAttr ".ovrgb" -type "float3" 0 0 0 ;
	setAttr ".lodv" yes;
	setAttr ".sech" yes;
	setAttr ".rlid" 0;
	setAttr ".rndr" yes;
	setAttr ".lovc" 0;
	setAttr ".gc" 0;
	setAttr ".gpr" 3;
	setAttr ".gps" 3;
	setAttr ".gss" 1;
	setAttr ".gap" 1;
	setAttr ".gcp" -type "float3" 0.447 1 1 ;
	setAttr ".gla" 1;
	setAttr ".gac" -type "float3" 0.87800002 0.67799997 0.66299999 ;
	setAttr ".grs" 0;
	setAttr ".gre" 100;
	setAttr ".rt" 0;
	setAttr ".rv" no;
	setAttr ".vf" 1;
	setAttr ".hfm" 1;
	setAttr ".mb" yes;
	setAttr ".vir" no;
	setAttr ".vif" no;
	setAttr ".csh" yes;
	setAttr ".rcsh" yes;
	setAttr ".asbg" no;
	setAttr ".vbo" no;
	setAttr ".mvs" 1;
	setAttr ".gao" no;
	setAttr ".gal" 1;
	setAttr ".sso" no;
	setAttr ".ssa" 1;
	setAttr ".msa" 1;
	setAttr ".vso" no;
	setAttr ".vss" 1;
	setAttr ".dej" no;
	setAttr ".iss" no;
	setAttr ".vis" no;
	setAttr ".tw" no;
	setAttr ".rtw" yes;
	setAttr ".pv" -type "double2" 0 0 ;
	setAttr ".di" no;
	setAttr ".dcol" no;
	setAttr ".dcc" -type "string" "color";
	setAttr ".ih" no;
	setAttr ".ds" yes;
	setAttr ".op" no;
	setAttr ".hot" no;
	setAttr ".smo" yes;
	setAttr ".bbs" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".fbda" yes;
	setAttr ".dsr" 6;
	setAttr ".xsr" 5;
	setAttr ".fth" 0;
	setAttr ".nat" 30;
	setAttr ".dhe" no;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".intensity" 5;
	setAttr ".exposure" 0;
	setAttr ".lightColor" -type "float3" 1 1 1 ;
	setAttr ".enableTemperature" no;
	setAttr ".temperature" 6500;
	setAttr ".emissionFocus" 0;
	setAttr ".emissionFocusTint" -type "float3" 0 0 0 ;
	setAttr ".specular" 1;
	setAttr ".diffuse" 1;
	setAttr ".intensityNearDist" 0;
	setAttr ".coneAngle" 90;
	setAttr ".coneSoftness" 0;
	setAttr ".iesProfile" -type "string" "";
	setAttr ".iesProfileScale" 0;
	setAttr ".iesProfileNormalize" no;
	setAttr ".enableShadows" yes;
	setAttr ".shadowColor" -type "float3" 0 0 0 ;
	setAttr ".shadowDistance" -1;
	setAttr ".shadowFalloff" -1;
	setAttr ".shadowFalloffGamma" 1;
	setAttr ".shadowSubset" -type "string" "";
	setAttr ".shadowExcludeSubset" -type "string" "";
	setAttr ".areaNormalize" no;
	setAttr ".traceLightPaths" no;
	setAttr ".thinShadow" yes;
	setAttr ".visibleInRefractionPath" no;
	setAttr ".cheapCaustics" no;
	setAttr ".cheapCausticsExcludeGroup" -type "string" "";
	setAttr ".fixedSampleCount" 0;
	setAttr ".lightGroup" -type "string" "";
	setAttr ".importanceMultiplier" 1;
	setAttr ".rman__lightfilters[0]" -type "float3"  0 0 0;
	setAttr ".cl" -type "float3" 1 1 1 ;
	setAttr ".ed" yes;
	setAttr ".sn" yes;
	setAttr ".lls" 1;
	setAttr ".de" 2;
	setAttr ".urs" yes;
	setAttr ".col" 5;
	setAttr ".hio" no;
	setAttr ".uocol" no;
	setAttr ".oclr" -type "float3" 0 0 0 ;
	setAttr ".rman_coneAngleDepth" 10;
	setAttr ".rman_coneAngleOpacity" 0.5;
createNode transform -n "PxrSphereLight1";
	rename -uid "4CDE5F1B-3646-C86A-2B00-A59E0219B53D";
	setAttr ".t" -type "double3" 0 0.66763259666593378 2.1298540206304231 ;
createNode PxrSphereLight -n "PxrSphereLight1Shape" -p "PxrSphereLight1";
	rename -uid "CAA4D469-F543-C92C-9167-3E95309C3B56";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".isc" no;
	setAttr ".bbx" no;
	setAttr ".icn" -type "string" "";
	setAttr ".vwm" 2;
	setAttr ".tpv" 0;
	setAttr ".uit" 0;
	setAttr -k off ".v" yes;
	setAttr ".io" no;
	setAttr ".tmp" no;
	setAttr ".gh" no;
	setAttr ".obcc" -type "float3" 0 0 0 ;
	setAttr ".wfcc" -type "float3" 0 0 0 ;
	setAttr ".uoc" 0;
	setAttr ".oc" 0;
	setAttr ".ovdt" 0;
	setAttr ".ovlod" 0;
	setAttr ".ovs" no;
	setAttr ".ovt" yes;
	setAttr ".ovp" yes;
	setAttr ".ove" yes;
	setAttr ".ovv" yes;
	setAttr ".hpb" no;
	setAttr ".ovrgbf" no;
	setAttr ".ovc" 0;
	setAttr ".ovrgb" -type "float3" 0 0 0 ;
	setAttr ".lodv" yes;
	setAttr ".sech" yes;
	setAttr ".rlid" 0;
	setAttr ".rndr" yes;
	setAttr ".lovc" 0;
	setAttr ".gc" 0;
	setAttr ".gpr" 3;
	setAttr ".gps" 3;
	setAttr ".gss" 1;
	setAttr ".gap" 1;
	setAttr ".gcp" -type "float3" 0.447 1 1 ;
	setAttr ".gla" 1;
	setAttr ".gac" -type "float3" 0.87800002 0.67799997 0.66299999 ;
	setAttr ".grs" 0;
	setAttr ".gre" 100;
	setAttr ".rt" 0;
	setAttr ".rv" no;
	setAttr ".vf" 1;
	setAttr ".hfm" 1;
	setAttr ".mb" yes;
	setAttr ".vir" no;
	setAttr ".vif" no;
	setAttr ".csh" yes;
	setAttr ".rcsh" yes;
	setAttr ".asbg" no;
	setAttr ".vbo" no;
	setAttr ".mvs" 1;
	setAttr ".gao" no;
	setAttr ".gal" 1;
	setAttr ".sso" no;
	setAttr ".ssa" 1;
	setAttr ".msa" 1;
	setAttr ".vso" no;
	setAttr ".vss" 1;
	setAttr ".dej" no;
	setAttr ".iss" no;
	setAttr ".vis" no;
	setAttr ".tw" no;
	setAttr ".rtw" yes;
	setAttr ".pv" -type "double2" 0 0 ;
	setAttr ".di" no;
	setAttr ".dcol" no;
	setAttr ".dcc" -type "string" "color";
	setAttr ".ih" no;
	setAttr ".ds" yes;
	setAttr ".op" no;
	setAttr ".hot" no;
	setAttr ".smo" yes;
	setAttr ".bbs" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".fbda" yes;
	setAttr ".dsr" 6;
	setAttr ".xsr" 5;
	setAttr ".fth" 0;
	setAttr ".nat" 30;
	setAttr ".dhe" no;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".intensity" 2;
	setAttr ".exposure" 0;
	setAttr ".lightColor" -type "float3" 1 1 1 ;
	setAttr ".enableTemperature" no;
	setAttr ".temperature" 6500;
	setAttr ".emissionFocus" 0;
	setAttr ".emissionFocusTint" -type "float3" 0 0 0 ;
	setAttr ".specular" 1;
	setAttr ".diffuse" 1;
	setAttr ".intensityNearDist" 0;
	setAttr ".coneAngle" 90;
	setAttr ".coneSoftness" 0;
	setAttr ".iesProfile" -type "string" "";
	setAttr ".iesProfileScale" 0;
	setAttr ".iesProfileNormalize" no;
	setAttr ".enableShadows" yes;
	setAttr ".shadowColor" -type "float3" 0 0 0 ;
	setAttr ".shadowDistance" -1;
	setAttr ".shadowFalloff" -1;
	setAttr ".shadowFalloffGamma" 1;
	setAttr ".shadowSubset" -type "string" "";
	setAttr ".shadowExcludeSubset" -type "string" "";
	setAttr ".areaNormalize" no;
	setAttr ".traceLightPaths" no;
	setAttr ".thinShadow" yes;
	setAttr ".visibleInRefractionPath" no;
	setAttr ".cheapCaustics" no;
	setAttr ".cheapCausticsExcludeGroup" -type "string" "";
	setAttr ".fixedSampleCount" 0;
	setAttr ".lightGroup" -type "string" "";
	setAttr ".importanceMultiplier" 1;
	setAttr ".rman__lightfilters[0]" -type "float3"  0 0 0;
	setAttr ".cl" -type "float3" 1 1 1 ;
	setAttr ".ed" yes;
	setAttr ".sn" yes;
	setAttr ".lls" 1;
	setAttr ".de" 2;
	setAttr ".urs" yes;
	setAttr ".col" 5;
	setAttr ".hio" no;
	setAttr ".uocol" no;
	setAttr ".oclr" -type "float3" 0 0 0 ;
	setAttr ".rman_coneAngleDepth" 10;
	setAttr ".rman_coneAngleOpacity" 0.5;
createNode transform -n "transform2";
	rename -uid "F4DB40B2-7742-A6D5-FA5D-D6B9E0539084";
	setAttr ".hio" yes;
createNode displayPoints -n "displayPoints2" -p "transform2";
	rename -uid "914178C8-104B-8BFC-AB2A-BFB23CE2992F";
	setAttr -k off ".v";
	setAttr ".boundingBoxes" -type "vectorArray" 0 ;
	setAttr ".hio" yes;
createNode transform -n "pTorus3";
	rename -uid "7A47B1F1-BA45-6212-86EF-94A00D2B62EA";
	setAttr ".t" -type "double3" 2.7957363266636235 1.3578873562295586 2.5990325734684618 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
	setAttr ".rpt" -type "double3" 0.3941378593444822 -0.39413785934448242 0 ;
	setAttr ".sp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
createNode transform -n "transform8" -p "pTorus3";
	rename -uid "5E1BAC85-2C4E-66C0-B670-F1B891654593";
	setAttr ".v" no;
createNode mesh -n "pTorusShape3" -p "transform8";
	rename -uid "DBACC96A-374F-7E9D-1C97-C49471F75E13";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 10 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.85833317041397095 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".pt";
	setAttr ".pt[48]" -type "float3" 0.03513477 -0.064907849 -0.031166604 ;
	setAttr ".pt[49]" -type "float3" 0.031876974 -0.064907849 -0.03099589 ;
	setAttr ".pt[50]" -type "float3" 0.028654853 -0.064907849 -0.030485559 ;
	setAttr ".pt[51]" -type "float3" 0.025503734 -0.064907849 -0.02964123 ;
	setAttr ".pt[52]" -type "float3" 0.022458173 -0.064907849 -0.028472122 ;
	setAttr ".pt[53]" -type "float3" 0.019551437 -0.064907849 -0.026991103 ;
	setAttr ".pt[54]" -type "float3" 0.01681548 -0.064907849 -0.025214329 ;
	setAttr ".pt[55]" -type "float3" 0.014280196 -0.064907849 -0.023161316 ;
	setAttr ".pt[56]" -type "float3" 0.01197345 -0.064907849 -0.020854538 ;
	setAttr ".pt[57]" -type "float3" 0.0099204574 -0.064907849 -0.018319288 ;
	setAttr ".pt[58]" -type "float3" 0.0081436727 -0.064907849 -0.015583325 ;
	setAttr ".pt[59]" -type "float3" 0.0066626295 -0.064907849 -0.012676611 ;
	setAttr ".pt[60]" -type "float3" 0.0054935142 -0.064907849 -0.0096310163 ;
	setAttr ".pt[61]" -type "float3" 0.0046492144 -0.064907849 -0.0064799134 ;
	setAttr ".pt[62]" -type "float3" 0.0041388338 -0.064907849 -0.0032578055 ;
	setAttr ".pt[63]" -type "float3" 0.0039681112 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[66]" -type "float3" 0.085886188 -0.064907849 -0.031166604 ;
	setAttr ".pt[72]" -type "float3" 0.085886188 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[121]" -type "float3" 0.03513477 -0.064907849 0.031166604 ;
	setAttr ".pt[122]" -type "float3" 0.031876974 -0.064907849 0.030995879 ;
	setAttr ".pt[123]" -type "float3" 0.028654853 -0.064907849 0.030485567 ;
	setAttr ".pt[124]" -type "float3" 0.025503734 -0.064907849 0.029641215 ;
	setAttr ".pt[125]" -type "float3" 0.022458173 -0.064907849 0.028472107 ;
	setAttr ".pt[126]" -type "float3" 0.019551437 -0.064907849 0.026991079 ;
	setAttr ".pt[127]" -type "float3" 0.01681548 -0.064907849 0.025214313 ;
	setAttr ".pt[128]" -type "float3" 0.014280196 -0.064907849 0.023161313 ;
	setAttr ".pt[129]" -type "float3" 0.01197345 -0.064907849 0.020854548 ;
	setAttr ".pt[130]" -type "float3" 0.0099204574 -0.064907849 0.018319286 ;
	setAttr ".pt[131]" -type "float3" 0.0081436727 -0.064907849 0.015583316 ;
	setAttr ".pt[132]" -type "float3" 0.0066626295 -0.064907849 0.012676615 ;
	setAttr ".pt[133]" -type "float3" 0.0054935142 -0.064907849 0.0096310098 ;
	setAttr ".pt[134]" -type "float3" 0.0046492144 -0.064907849 0.0064799096 ;
	setAttr ".pt[135]" -type "float3" 0.0041388338 -0.064907849 0.003257805 ;
	setAttr ".pt[138]" -type "float3" 0.085886188 -0.064907849 0.031166604 ;
createNode transform -n "pTorus4";
	rename -uid "F4C5511F-094B-5698-F98A-9DA340FD823A";
	setAttr ".t" -type "double3" 2.7957363266636235 1.3578873562295586 1.1176843793335578 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
	setAttr ".rpt" -type "double3" 0.3941378593444822 -0.39413785934448242 0 ;
	setAttr ".sp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
createNode transform -n "transform6" -p "pTorus4";
	rename -uid "4EC366F6-3C47-C837-AD72-6DB5AFF5B42D";
	setAttr ".v" no;
createNode mesh -n "pTorusShape4" -p "transform6";
	rename -uid "476B6D48-B549-6963-A9E5-F598C6510CCB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 5 ".iog[0].og";
	setAttr ".iog[0].og[4].gcl" -type "componentList" 8 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]";
	setAttr ".iog[0].og[5].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[6].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[7].gcl" -type "componentList" 3 "e[142]" "e[144]" "e[146:147]";
	setAttr ".iog[0].og[10].gcl" -type "componentList" 1 "f[0:143]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.85833317041397095 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 184 ".uvst[0].uvsp[0:183]" -type "float2" 0.73333329 1 0.74999994
		 1 0.76666659 1 0.78333324 1 0.79999989 1 0.81666654 1 0.83333319 1 0.84999985 1 0.8666665
		 1 0.88333315 1 0.8999998 1 0.91666645 1 0.9333331 1 0.94999975 1 0.9666664 1 0.98333305
		 1 0.73333329 0.75 0.74999994 0.75 0.76666659 0.75 0.78333324 0.75 0.79999989 0.75
		 0.81666654 0.75 0.83333319 0.75 0.84999985 0.75 0.8666665 0.75 0.88333315 0.75 0.8999998
		 0.75 0.91666645 0.75 0.9333331 0.75 0.94999975 0.75 0.9666664 0.75 0.98333305 0.75
		 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989 0.5 0.81666654
		 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998 0.5 0.91666645
		 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329 0.25 0.74999994
		 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25 0.83333319 0.25
		 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645 0.25 0.9333331
		 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994 0 0.76666659
		 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665 0 0.88333315
		 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305 0 0.73333329
		 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0.75 0.73333329 0 0.73333329
		 0.25 0.73333329 0.5 0.73333329 0 0.73333329 0.25 0.73333329 0.25 0.73333329 0 0.74999994
		 1 0.73333329 1 0.73333329 0.75 0.74999994 0.75 0.76666659 1 0.76666659 0.75 0.78333324
		 1 0.78333324 0.75 0.79999989 1 0.79999989 0.75 0.81666654 1 0.81666654 0.75 0.83333319
		 1 0.83333319 0.75 0.84999985 1 0.84999985 0.75 0.8666665 1 0.8666665 0.75 0.88333315
		 1 0.88333315 0.75 0.8999998 1 0.8999998 0.75 0.91666645 1 0.91666645 0.75 0.9333331
		 1 0.9333331 0.75 0.94999975 1 0.94999975 0.75 0.9666664 1 0.9666664 0.75 0.98333305
		 1 0.98333305 0.75 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989
		 0.5 0.81666654 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998
		 0.5 0.91666645 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329
		 0.25 0.74999994 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25
		 0.83333319 0.25 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645
		 0.25 0.9333331 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994
		 0 0.76666659 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665
		 0 0.88333315 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305
		 0 0.73333329 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0 0.73333329
		 0.75 0.73333329 0.25 0.73333329 0.5 0.73333329 0.25 0.73333329 0 0.73333329 0.25
		 0.73333329 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".pt";
	setAttr ".pt[48]" -type "float3" 0.03513477 -0.064907849 -0.031166604 ;
	setAttr ".pt[49]" -type "float3" 0.031876974 -0.064907849 -0.03099589 ;
	setAttr ".pt[50]" -type "float3" 0.028654853 -0.064907849 -0.030485559 ;
	setAttr ".pt[51]" -type "float3" 0.025503734 -0.064907849 -0.02964123 ;
	setAttr ".pt[52]" -type "float3" 0.022458173 -0.064907849 -0.028472122 ;
	setAttr ".pt[53]" -type "float3" 0.019551437 -0.064907849 -0.026991103 ;
	setAttr ".pt[54]" -type "float3" 0.01681548 -0.064907849 -0.025214329 ;
	setAttr ".pt[55]" -type "float3" 0.014280196 -0.064907849 -0.023161316 ;
	setAttr ".pt[56]" -type "float3" 0.01197345 -0.064907849 -0.020854538 ;
	setAttr ".pt[57]" -type "float3" 0.0099204574 -0.064907849 -0.018319288 ;
	setAttr ".pt[58]" -type "float3" 0.0081436727 -0.064907849 -0.015583325 ;
	setAttr ".pt[59]" -type "float3" 0.0066626295 -0.064907849 -0.012676611 ;
	setAttr ".pt[60]" -type "float3" 0.0054935142 -0.064907849 -0.0096310163 ;
	setAttr ".pt[61]" -type "float3" 0.0046492144 -0.064907849 -0.0064799134 ;
	setAttr ".pt[62]" -type "float3" 0.0041388338 -0.064907849 -0.0032578055 ;
	setAttr ".pt[63]" -type "float3" 0.0039681112 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[66]" -type "float3" 0.085886188 -0.064907849 -0.031166604 ;
	setAttr ".pt[72]" -type "float3" 0.085886188 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[121]" -type "float3" 0.03513477 -0.064907849 0.031166604 ;
	setAttr ".pt[122]" -type "float3" 0.031876974 -0.064907849 0.030995879 ;
	setAttr ".pt[123]" -type "float3" 0.028654853 -0.064907849 0.030485567 ;
	setAttr ".pt[124]" -type "float3" 0.025503734 -0.064907849 0.029641215 ;
	setAttr ".pt[125]" -type "float3" 0.022458173 -0.064907849 0.028472107 ;
	setAttr ".pt[126]" -type "float3" 0.019551437 -0.064907849 0.026991079 ;
	setAttr ".pt[127]" -type "float3" 0.01681548 -0.064907849 0.025214313 ;
	setAttr ".pt[128]" -type "float3" 0.014280196 -0.064907849 0.023161313 ;
	setAttr ".pt[129]" -type "float3" 0.01197345 -0.064907849 0.020854548 ;
	setAttr ".pt[130]" -type "float3" 0.0099204574 -0.064907849 0.018319286 ;
	setAttr ".pt[131]" -type "float3" 0.0081436727 -0.064907849 0.015583316 ;
	setAttr ".pt[132]" -type "float3" 0.0066626295 -0.064907849 0.012676615 ;
	setAttr ".pt[133]" -type "float3" 0.0054935142 -0.064907849 0.0096310098 ;
	setAttr ".pt[134]" -type "float3" 0.0046492144 -0.064907849 0.0064799096 ;
	setAttr ".pt[135]" -type "float3" 0.0041388338 -0.064907849 0.003257805 ;
	setAttr ".pt[138]" -type "float3" 0.085886188 -0.064907849 0.031166604 ;
	setAttr -s 144 ".vt[0:143]"  0 0.03535533 0.31464475 0.032889366 0.03535533 0.31292099
		 0.065418363 0.03535533 0.307769 0.097230554 0.03535533 0.29924482 0.12797761 0.03535533 0.28744215
		 0.15732241 0.03535533 0.27249032 0.18494356 0.03535533 0.2545529 0.21053839 0.03535533 0.23382658
		 0.23382664 0.03535533 0.21053833 0.25455284 0.03535533 0.1849435 0.27249026 0.03535533 0.15732235
		 0.28744221 0.03535533 0.12797755 0.29924488 0.03535533 0.097230613 0.30776894 0.03535533 0.065418303
		 0.31292105 0.03535533 0.032889307 0.31464469 0.03535533 -7.5649616e-08 0 0.03535533 0.37460828
		 0.0402807 0.03535533 0.37460828 0.080119967 0.03535533 0.37460828 0.11908138 0.03535533 0.37460828
		 0.15673816 0.03535533 0.37460828 0.19267774 0.03535533 0.37460828 0.2740916 0.03535533 0.37460828
		 0.56961167 0.03535533 0.37460828 0.56961167 0.03535533 0.30112225 0.56961167 0.03535533 0.24240094
		 0.56961167 0.03535533 0.19267768 0.56961167 0.03535533 0.1567381 0.56961167 0.03535533 0.11908132
		 0.56961167 0.03535533 0.080119908 0.56961167 0.03535533 0.042046666 0.56961167 0.03535533 -7.5649616e-08
		 0 -0.03535533 0.37460828 0.0402807 -0.03535533 0.37460828 0.080119967 -0.03535533 0.37460828
		 0.11908138 -0.03535533 0.37460828 0.15673816 -0.03535533 0.37460828 0.19267774 -0.03535533 0.37460828
		 0.2740916 -0.03535533 0.37460828 0.56961167 -0.03535533 0.37460828 0.56961167 -0.03535533 0.30112225
		 0.56961167 -0.03535533 0.24240094 0.56961167 -0.03535533 0.19267768 0.56961167 -0.03535533 0.1567381
		 0.56961167 -0.03535533 0.11908132 0.56961167 -0.03535533 0.080119908 0.56961167 -0.03535533 0.042046666
		 0.56961167 -0.03535533 -7.5649616e-08 0 -0.03535533 0.31464463 0.032889366 -0.03535533 0.31292099
		 0.065418363 -0.03535533 0.30776888 0.097230554 -0.03535533 0.29924482 0.12797749 -0.03535533 0.28744215
		 0.15732241 -0.03535533 0.2724902 0.18494356 -0.03535533 0.2545529 0.21053839 -0.03535533 0.23382658
		 0.23382652 -0.03535533 0.21053833 0.25455284 -0.03535533 0.1849435 0.27249026 -0.03535533 0.15732235
		 0.28744221 -0.03535533 0.12797755 0.29924488 -0.03535533 0.097230494 0.30776894 -0.03535533 0.065418303
		 0.31292105 -0.03535533 0.032889307 0.31464458 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 0.31464475
		 -0.51236534 0.03535533 0.37460828 -0.51236534 -0.03535533 0.31464463 -0.51236534 -0.03535533 0.37460828
		 -1.35788739 0.03535533 0.31464475 -1.35788739 0.03535533 0.37460828 -1.35788739 -0.03535533 0.31464463
		 -1.35788739 -0.03535533 0.37460828 -0.51236534 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 -7.5649616e-08
		 -1.35788739 -0.03535533 -7.5649616e-08 -1.35788739 0.03535533 -7.5649616e-08 -3.2154015e-19 0.03535533 -0.3146449
		 0.032889366 0.03535533 -0.31292114 0.065418363 0.03535533 -0.30776915 0.097230554 0.03535533 -0.29924497
		 0.12797761 0.03535533 -0.2874423 0.15732241 0.03535533 -0.27249047 0.18494356 0.03535533 -0.25455305
		 0.21053839 0.03535533 -0.23382673 0.23382664 0.03535533 -0.21053848 0.25455284 0.03535533 -0.18494365
		 0.27249026 0.03535533 -0.1573225 0.28744221 0.03535533 -0.1279777 0.29924488 0.03535533 -0.097230762
		 0.30776894 0.03535533 -0.065418452 0.31292105 0.03535533 -0.032889459 0 0.03535533 -0.37460843
		 0.0402807 0.03535533 -0.37460843 0.080119967 0.03535533 -0.37460843 0.11908138 0.03535533 -0.37460843
		 0.15673816 0.03535533 -0.37460843 0.19267774 0.03535533 -0.37460843 0.2740916 0.03535533 -0.37460843
		 0.56961167 0.03535533 -0.37460843 0.56961167 0.03535533 -0.3011224 0.56961167 0.03535533 -0.24240109
		 0.56961167 0.03535533 -0.19267783 0.56961167 0.03535533 -0.15673825 0.56961167 0.03535533 -0.11908147
		 0.56961167 0.03535533 -0.080120057 0.56961167 0.03535533 -0.042046819 0 -0.03535533 -0.37460843
		 0.0402807 -0.03535533 -0.37460843 0.080119967 -0.03535533 -0.37460843 0.11908138 -0.03535533 -0.37460843
		 0.15673816 -0.03535533 -0.37460843 0.19267774 -0.03535533 -0.37460843 0.2740916 -0.03535533 -0.37460843
		 0.56961167 -0.03535533 -0.37460843 0.56961167 -0.03535533 -0.3011224 0.56961167 -0.03535533 -0.24240109
		 0.56961167 -0.03535533 -0.19267783 0.56961167 -0.03535533 -0.15673825 0.56961167 -0.03535533 -0.11908147
		 0.56961167 -0.03535533 -0.080120057 0.56961167 -0.03535533 -0.042046819 -3.2154025e-19 -0.03535533 -0.31464478
		 0.032889366 -0.03535533 -0.31292114 0.065418363 -0.03535533 -0.30776903 0.097230554 -0.03535533 -0.29924497
		 0.12797749 -0.03535533 -0.2874423 0.15732241 -0.03535533 -0.27249035 0.18494356 -0.03535533 -0.25455305
		 0.21053839 -0.03535533 -0.23382673 0.23382652 -0.03535533 -0.21053848 0.25455284 -0.03535533 -0.18494365
		 0.27249026 -0.03535533 -0.1573225 0.28744221 -0.03535533 -0.1279777 0.29924488 -0.03535533 -0.097230643
		 0.30776894 -0.03535533 -0.065418452 0.31292105 -0.03535533 -0.032889459 -0.51236534 0.03535533 -0.3146449
		 -0.51236534 0.03535533 -0.37460843 -0.51236534 -0.03535533 -0.31464478 -0.51236534 -0.03535533 -0.37460843
		 -1.35788739 0.03535533 -0.3146449 -1.35788739 0.03535533 -0.37460843 -1.35788739 -0.03535533 -0.31464478
		 -1.35788739 -0.03535533 -0.37460843;
	setAttr -s 288 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 16 17 0 17 18 0 18 19 0 19 20 0 20 21 0
		 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0 32 33 0
		 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 42 0 42 43 0 43 44 0
		 44 45 0 45 46 0 46 47 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0
		 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 0 16 0 1 17 1 2 18 1 3 19 1
		 4 20 1 5 21 1 6 22 1 7 23 1 8 24 1 9 25 1 10 26 1 11 27 1 12 28 1 13 29 1 14 30 1
		 15 31 1 16 32 0 17 33 1 18 34 1 19 35 1 20 36 1 21 37 1 22 38 1 23 39 1 24 40 1 25 41 1
		 26 42 1 27 43 1 28 44 1 29 45 1 30 46 1 31 47 1 32 48 0 33 49 1 34 50 1 35 51 1 36 52 1
		 37 53 1 38 54 1 39 55 1 40 56 1 41 57 1 42 58 1 43 59 1 44 60 1 45 61 1 46 62 1 47 63 1
		 48 0 0 49 1 1 50 2 1 51 3 1 52 4 1 53 5 1 54 6 1 55 7 1 56 8 1 57 9 1 58 10 1 59 11 1
		 60 12 1 61 13 1 62 14 1 63 15 1 0 64 0 16 65 0 64 65 0 48 66 0 66 64 0 32 67 0 67 66 0
		 65 67 0 64 68 0 65 69 0 68 69 0 66 70 0 70 68 0 67 71 0 71 70 0 69 71 0 66 72 0 64 73 0
		 72 73 1 70 74 0 72 74 1 68 75 0 74 75 1 73 75 1 76 77 0 76 91 0 91 92 0 77 92 1 77 78 0
		 92 93 0 78 93 1 78 79 0 93 94 0 79 94 1 79 80 0 94 95 0 80 95 1 80 81 0 95 96 0 81 96 1
		 81 82 0 96 97 0;
	setAttr ".ed[166:287]" 82 97 1 82 83 0 97 98 0 83 98 1 83 84 0 98 99 0 84 99 1
		 84 85 0 99 100 0 85 100 1 85 86 0 100 101 0 86 101 1 86 87 0 101 102 0 87 102 1 87 88 0
		 102 103 0 88 103 1 88 89 0 103 104 0 89 104 1 89 90 0 104 105 0 90 105 1 90 15 0
		 105 31 0 91 106 0 106 107 0 92 107 1 107 108 0 93 108 1 108 109 0 94 109 1 109 110 0
		 95 110 1 110 111 0 96 111 1 111 112 0 97 112 1 112 113 0 98 113 1 113 114 0 99 114 1
		 114 115 0 100 115 1 115 116 0 101 116 1 116 117 0 102 117 1 117 118 0 103 118 1 118 119 0
		 104 119 1 119 120 0 105 120 1 120 47 0 106 121 0 121 122 0 107 122 1 122 123 0 108 123 1
		 123 124 0 109 124 1 124 125 0 110 125 1 125 126 0 111 126 1 126 127 0 112 127 1 127 128 0
		 113 128 1 128 129 0 114 129 1 129 130 0 115 130 1 130 131 0 116 131 1 131 132 0 117 132 1
		 132 133 0 118 133 1 133 134 0 119 134 1 134 135 0 120 135 1 135 63 0 121 76 0 122 77 1
		 123 78 1 124 79 1 125 80 1 126 81 1 127 82 1 128 83 1 129 84 1 130 85 1 131 86 1
		 132 87 1 133 88 1 134 89 1 135 90 1 140 141 0 142 140 0 143 142 0 141 143 0 76 136 0
		 136 137 0 91 137 0 121 138 0 138 136 0 106 139 0 139 138 0 137 139 0 136 140 0 137 141 0
		 139 143 0 138 142 0 138 72 0 136 73 0 142 74 0 140 75 0;
	setAttr -s 144 -ch 576 ".fc[0:143]" -type "polyFaces" 
		f 4 -1 60 15 -62
		mu 0 4 1 0 16 17
		f 4 -2 61 16 -63
		mu 0 4 2 1 17 18
		f 4 -3 62 17 -64
		mu 0 4 3 2 18 19
		f 4 -4 63 18 -65
		mu 0 4 4 3 19 20
		f 4 -5 64 19 -66
		mu 0 4 5 4 20 21
		f 4 -6 65 20 -67
		mu 0 4 6 5 21 22
		f 4 -7 66 21 -68
		mu 0 4 7 6 22 23
		f 4 -8 67 22 -69
		mu 0 4 8 7 23 24
		f 4 -9 68 23 -70
		mu 0 4 9 8 24 25
		f 4 -10 69 24 -71
		mu 0 4 10 9 25 26
		f 4 -11 70 25 -72
		mu 0 4 11 10 26 27
		f 4 -12 71 26 -73
		mu 0 4 12 11 27 28
		f 4 -13 72 27 -74
		mu 0 4 13 12 28 29
		f 4 -14 73 28 -75
		mu 0 4 14 13 29 30
		f 4 -15 74 29 -76
		mu 0 4 15 14 30 31
		f 4 -16 76 30 -78
		mu 0 4 17 16 32 33
		f 4 -17 77 31 -79
		mu 0 4 18 17 33 34
		f 4 -18 78 32 -80
		mu 0 4 19 18 34 35
		f 4 -19 79 33 -81
		mu 0 4 20 19 35 36
		f 4 -20 80 34 -82
		mu 0 4 21 20 36 37
		f 4 -21 81 35 -83
		mu 0 4 22 21 37 38
		f 4 -22 82 36 -84
		mu 0 4 23 22 38 39
		f 4 -23 83 37 -85
		mu 0 4 24 23 39 40
		f 4 -24 84 38 -86
		mu 0 4 25 24 40 41
		f 4 -25 85 39 -87
		mu 0 4 26 25 41 42
		f 4 -26 86 40 -88
		mu 0 4 27 26 42 43
		f 4 -27 87 41 -89
		mu 0 4 28 27 43 44
		f 4 -28 88 42 -90
		mu 0 4 29 28 44 45
		f 4 -29 89 43 -91
		mu 0 4 30 29 45 46
		f 4 -30 90 44 -92
		mu 0 4 31 30 46 47
		f 4 -31 92 45 -94
		mu 0 4 33 32 48 49
		f 4 -32 93 46 -95
		mu 0 4 34 33 49 50
		f 4 -33 94 47 -96
		mu 0 4 35 34 50 51
		f 4 -34 95 48 -97
		mu 0 4 36 35 51 52
		f 4 -35 96 49 -98
		mu 0 4 37 36 52 53
		f 4 -36 97 50 -99
		mu 0 4 38 37 53 54
		f 4 -37 98 51 -100
		mu 0 4 39 38 54 55
		f 4 -38 99 52 -101
		mu 0 4 40 39 55 56
		f 4 -39 100 53 -102
		mu 0 4 41 40 56 57
		f 4 -40 101 54 -103
		mu 0 4 42 41 57 58
		f 4 -41 102 55 -104
		mu 0 4 43 42 58 59
		f 4 -42 103 56 -105
		mu 0 4 44 43 59 60
		f 4 -43 104 57 -106
		mu 0 4 45 44 60 61
		f 4 -44 105 58 -107
		mu 0 4 46 45 61 62
		f 4 -45 106 59 -108
		mu 0 4 47 46 62 63
		f 4 -46 108 0 -110
		mu 0 4 49 48 64 65
		f 4 -47 109 1 -111
		mu 0 4 50 49 65 66
		f 4 -48 110 2 -112
		mu 0 4 51 50 66 67
		f 4 -49 111 3 -113
		mu 0 4 52 51 67 68
		f 4 -50 112 4 -114
		mu 0 4 53 52 68 69
		f 4 -51 113 5 -115
		mu 0 4 54 53 69 70
		f 4 -52 114 6 -116
		mu 0 4 55 54 70 71
		f 4 -53 115 7 -117
		mu 0 4 56 55 71 72
		f 4 -54 116 8 -118
		mu 0 4 57 56 72 73
		f 4 -55 117 9 -119
		mu 0 4 58 57 73 74
		f 4 -56 118 10 -120
		mu 0 4 59 58 74 75
		f 4 -57 119 11 -121
		mu 0 4 60 59 75 76
		f 4 -58 120 12 -122
		mu 0 4 61 60 76 77
		f 4 -59 121 13 -123
		mu 0 4 62 61 77 78
		f 4 -60 122 14 -124
		mu 0 4 63 62 78 79
		f 4 -135 -137 -139 -140
		mu 0 4 84 85 86 87
		f 4 -61 124 126 -126
		mu 0 4 16 64 81 80
		f 4 -109 127 128 -125
		mu 0 4 64 48 82 81
		f 4 -93 129 130 -128
		mu 0 4 48 32 83 82
		f 4 -77 125 131 -130
		mu 0 4 32 16 80 83
		f 4 -127 132 134 -134
		mu 0 4 80 81 85 84
		f 4 -131 137 138 -136
		mu 0 4 82 83 87 86
		f 4 -132 133 139 -138
		mu 0 4 83 80 84 87
		f 4 -129 140 142 -142
		mu 0 4 81 82 89 88
		f 4 135 143 -145 -141
		mu 0 4 82 86 90 89
		f 4 136 145 -147 -144
		mu 0 4 86 85 91 90
		f 4 -133 141 147 -146
		mu 0 4 85 81 88 91
		f 4 151 -151 -150 148
		mu 0 4 92 95 94 93
		f 4 154 -154 -152 152
		mu 0 4 96 97 95 92
		f 4 157 -157 -155 155
		mu 0 4 98 99 97 96
		f 4 160 -160 -158 158
		mu 0 4 100 101 99 98
		f 4 163 -163 -161 161
		mu 0 4 102 103 101 100
		f 4 166 -166 -164 164
		mu 0 4 104 105 103 102
		f 4 169 -169 -167 167
		mu 0 4 106 107 105 104
		f 4 172 -172 -170 170
		mu 0 4 108 109 107 106
		f 4 175 -175 -173 173
		mu 0 4 110 111 109 108
		f 4 178 -178 -176 176
		mu 0 4 112 113 111 110
		f 4 181 -181 -179 179
		mu 0 4 114 115 113 112
		f 4 184 -184 -182 182
		mu 0 4 116 117 115 114
		f 4 187 -187 -185 185
		mu 0 4 118 119 117 116
		f 4 190 -190 -188 188
		mu 0 4 120 121 119 118
		f 4 75 -193 -191 191
		mu 0 4 122 123 121 120
		f 4 195 -195 -194 150
		mu 0 4 95 125 124 94
		f 4 197 -197 -196 153
		mu 0 4 97 126 125 95
		f 4 199 -199 -198 156
		mu 0 4 99 127 126 97
		f 4 201 -201 -200 159
		mu 0 4 101 128 127 99
		f 4 203 -203 -202 162
		mu 0 4 103 129 128 101
		f 4 205 -205 -204 165
		mu 0 4 105 130 129 103
		f 4 207 -207 -206 168
		mu 0 4 107 131 130 105
		f 4 209 -209 -208 171
		mu 0 4 109 132 131 107
		f 4 211 -211 -210 174
		mu 0 4 111 133 132 109
		f 4 213 -213 -212 177
		mu 0 4 113 134 133 111
		f 4 215 -215 -214 180
		mu 0 4 115 135 134 113
		f 4 217 -217 -216 183
		mu 0 4 117 136 135 115
		f 4 219 -219 -218 186
		mu 0 4 119 137 136 117
		f 4 221 -221 -220 189
		mu 0 4 121 138 137 119
		f 4 91 -223 -222 192
		mu 0 4 123 139 138 121
		f 4 225 -225 -224 194
		mu 0 4 125 141 140 124
		f 4 227 -227 -226 196
		mu 0 4 126 142 141 125
		f 4 229 -229 -228 198
		mu 0 4 127 143 142 126
		f 4 231 -231 -230 200
		mu 0 4 128 144 143 127
		f 4 233 -233 -232 202
		mu 0 4 129 145 144 128
		f 4 235 -235 -234 204
		mu 0 4 130 146 145 129
		f 4 237 -237 -236 206
		mu 0 4 131 147 146 130
		f 4 239 -239 -238 208
		mu 0 4 132 148 147 131
		f 4 241 -241 -240 210
		mu 0 4 133 149 148 132
		f 4 243 -243 -242 212
		mu 0 4 134 150 149 133
		f 4 245 -245 -244 214
		mu 0 4 135 151 150 134
		f 4 247 -247 -246 216
		mu 0 4 136 152 151 135
		f 4 249 -249 -248 218
		mu 0 4 137 153 152 136
		f 4 251 -251 -250 220
		mu 0 4 138 154 153 137
		f 4 107 -253 -252 222
		mu 0 4 139 155 154 138
		f 4 254 -149 -254 224
		mu 0 4 141 157 156 140
		f 4 255 -153 -255 226
		mu 0 4 142 158 157 141
		f 4 256 -156 -256 228
		mu 0 4 143 159 158 142
		f 4 257 -159 -257 230
		mu 0 4 144 160 159 143
		f 4 258 -162 -258 232
		mu 0 4 145 161 160 144
		f 4 259 -165 -259 234
		mu 0 4 146 162 161 145
		f 4 260 -168 -260 236
		mu 0 4 147 163 162 146
		f 4 261 -171 -261 238
		mu 0 4 148 164 163 147
		f 4 262 -174 -262 240
		mu 0 4 149 165 164 148
		f 4 263 -177 -263 242
		mu 0 4 150 166 165 149
		f 4 264 -180 -264 244
		mu 0 4 151 167 166 150
		f 4 265 -183 -265 246
		mu 0 4 152 168 167 151
		f 4 266 -186 -266 248
		mu 0 4 153 169 168 152
		f 4 267 -189 -267 250
		mu 0 4 154 170 169 153
		f 4 123 -192 -268 252
		mu 0 4 155 171 170 154
		f 4 271 270 269 268
		mu 0 4 172 175 174 173
		f 4 274 -274 -273 149
		mu 0 4 94 177 176 156
		f 4 272 -277 -276 253
		mu 0 4 156 176 178 140
		f 4 275 -279 -278 223
		mu 0 4 140 178 179 124
		f 4 277 -280 -275 193
		mu 0 4 124 179 177 94
		f 4 281 -269 -281 273
		mu 0 4 177 172 173 176
		f 4 283 -271 -283 278
		mu 0 4 178 174 175 179
		f 4 282 -272 -282 279
		mu 0 4 179 175 172 177
		f 4 285 -143 -285 276
		mu 0 4 176 181 180 178
		f 4 284 144 -287 -284
		mu 0 4 178 180 182 174
		f 4 286 146 -288 -270
		mu 0 4 174 182 183 173
		f 4 287 -148 -286 280
		mu 0 4 173 183 181 176;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pTorus5";
	rename -uid "C1CF6683-3847-4502-6BE7-FA9A1131A23B";
	setAttr ".t" -type "double3" 2.7957363266636226 1.3578873562295588 -0.3636638148013458 ;
	setAttr ".r" -type "double3" 0 0 89.999999999999986 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
	setAttr ".rp" -type "double3" -0.39413785934448253 0 -7.5649615682849003e-08 ;
	setAttr ".rpt" -type "double3" 0.39413785934448287 -0.39413785934448264 0 ;
	setAttr ".sp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
	setAttr ".spt" -type "double3" -1.1102230246251568e-16 0 0 ;
createNode transform -n "transform3" -p "pTorus5";
	rename -uid "5ED83EFF-7F4A-BC15-7049-20A7A29F3455";
	setAttr ".v" no;
createNode mesh -n "pTorusShape5" -p "transform3";
	rename -uid "2D43F3C3-2C46-E8CC-9E46-D8A85F322283";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 5 ".iog[0].og";
	setAttr ".iog[0].og[4].gcl" -type "componentList" 8 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]";
	setAttr ".iog[0].og[5].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[6].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[7].gcl" -type "componentList" 3 "e[142]" "e[144]" "e[146:147]";
	setAttr ".iog[0].og[8].gcl" -type "componentList" 1 "f[0:143]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.85833317041397095 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 184 ".uvst[0].uvsp[0:183]" -type "float2" 0.73333329 1 0.74999994
		 1 0.76666659 1 0.78333324 1 0.79999989 1 0.81666654 1 0.83333319 1 0.84999985 1 0.8666665
		 1 0.88333315 1 0.8999998 1 0.91666645 1 0.9333331 1 0.94999975 1 0.9666664 1 0.98333305
		 1 0.73333329 0.75 0.74999994 0.75 0.76666659 0.75 0.78333324 0.75 0.79999989 0.75
		 0.81666654 0.75 0.83333319 0.75 0.84999985 0.75 0.8666665 0.75 0.88333315 0.75 0.8999998
		 0.75 0.91666645 0.75 0.9333331 0.75 0.94999975 0.75 0.9666664 0.75 0.98333305 0.75
		 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989 0.5 0.81666654
		 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998 0.5 0.91666645
		 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329 0.25 0.74999994
		 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25 0.83333319 0.25
		 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645 0.25 0.9333331
		 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994 0 0.76666659
		 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665 0 0.88333315
		 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305 0 0.73333329
		 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0.75 0.73333329 0 0.73333329
		 0.25 0.73333329 0.5 0.73333329 0 0.73333329 0.25 0.73333329 0.25 0.73333329 0 0.74999994
		 1 0.73333329 1 0.73333329 0.75 0.74999994 0.75 0.76666659 1 0.76666659 0.75 0.78333324
		 1 0.78333324 0.75 0.79999989 1 0.79999989 0.75 0.81666654 1 0.81666654 0.75 0.83333319
		 1 0.83333319 0.75 0.84999985 1 0.84999985 0.75 0.8666665 1 0.8666665 0.75 0.88333315
		 1 0.88333315 0.75 0.8999998 1 0.8999998 0.75 0.91666645 1 0.91666645 0.75 0.9333331
		 1 0.9333331 0.75 0.94999975 1 0.94999975 0.75 0.9666664 1 0.9666664 0.75 0.98333305
		 1 0.98333305 0.75 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989
		 0.5 0.81666654 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998
		 0.5 0.91666645 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329
		 0.25 0.74999994 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25
		 0.83333319 0.25 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645
		 0.25 0.9333331 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994
		 0 0.76666659 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665
		 0 0.88333315 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305
		 0 0.73333329 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0 0.73333329
		 0.75 0.73333329 0.25 0.73333329 0.5 0.73333329 0.25 0.73333329 0 0.73333329 0.25
		 0.73333329 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".pt";
	setAttr ".pt[48]" -type "float3" 0.03513477 -0.064907849 -0.031166604 ;
	setAttr ".pt[49]" -type "float3" 0.031876974 -0.064907849 -0.03099589 ;
	setAttr ".pt[50]" -type "float3" 0.028654853 -0.064907849 -0.030485559 ;
	setAttr ".pt[51]" -type "float3" 0.025503734 -0.064907849 -0.02964123 ;
	setAttr ".pt[52]" -type "float3" 0.022458173 -0.064907849 -0.028472122 ;
	setAttr ".pt[53]" -type "float3" 0.019551437 -0.064907849 -0.026991103 ;
	setAttr ".pt[54]" -type "float3" 0.01681548 -0.064907849 -0.025214329 ;
	setAttr ".pt[55]" -type "float3" 0.014280196 -0.064907849 -0.023161316 ;
	setAttr ".pt[56]" -type "float3" 0.01197345 -0.064907849 -0.020854538 ;
	setAttr ".pt[57]" -type "float3" 0.0099204574 -0.064907849 -0.018319288 ;
	setAttr ".pt[58]" -type "float3" 0.0081436727 -0.064907849 -0.015583325 ;
	setAttr ".pt[59]" -type "float3" 0.0066626295 -0.064907849 -0.012676611 ;
	setAttr ".pt[60]" -type "float3" 0.0054935142 -0.064907849 -0.0096310163 ;
	setAttr ".pt[61]" -type "float3" 0.0046492144 -0.064907849 -0.0064799134 ;
	setAttr ".pt[62]" -type "float3" 0.0041388338 -0.064907849 -0.0032578055 ;
	setAttr ".pt[63]" -type "float3" 0.0039681112 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[66]" -type "float3" 0.085886188 -0.064907849 -0.031166604 ;
	setAttr ".pt[72]" -type "float3" 0.085886188 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[121]" -type "float3" 0.03513477 -0.064907849 0.031166604 ;
	setAttr ".pt[122]" -type "float3" 0.031876974 -0.064907849 0.030995879 ;
	setAttr ".pt[123]" -type "float3" 0.028654853 -0.064907849 0.030485567 ;
	setAttr ".pt[124]" -type "float3" 0.025503734 -0.064907849 0.029641215 ;
	setAttr ".pt[125]" -type "float3" 0.022458173 -0.064907849 0.028472107 ;
	setAttr ".pt[126]" -type "float3" 0.019551437 -0.064907849 0.026991079 ;
	setAttr ".pt[127]" -type "float3" 0.01681548 -0.064907849 0.025214313 ;
	setAttr ".pt[128]" -type "float3" 0.014280196 -0.064907849 0.023161313 ;
	setAttr ".pt[129]" -type "float3" 0.01197345 -0.064907849 0.020854548 ;
	setAttr ".pt[130]" -type "float3" 0.0099204574 -0.064907849 0.018319286 ;
	setAttr ".pt[131]" -type "float3" 0.0081436727 -0.064907849 0.015583316 ;
	setAttr ".pt[132]" -type "float3" 0.0066626295 -0.064907849 0.012676615 ;
	setAttr ".pt[133]" -type "float3" 0.0054935142 -0.064907849 0.0096310098 ;
	setAttr ".pt[134]" -type "float3" 0.0046492144 -0.064907849 0.0064799096 ;
	setAttr ".pt[135]" -type "float3" 0.0041388338 -0.064907849 0.003257805 ;
	setAttr ".pt[138]" -type "float3" 0.085886188 -0.064907849 0.031166604 ;
	setAttr -s 144 ".vt[0:143]"  0 0.03535533 0.31464475 0.032889366 0.03535533 0.31292099
		 0.065418363 0.03535533 0.307769 0.097230554 0.03535533 0.29924482 0.12797761 0.03535533 0.28744215
		 0.15732241 0.03535533 0.27249032 0.18494356 0.03535533 0.2545529 0.21053839 0.03535533 0.23382658
		 0.23382664 0.03535533 0.21053833 0.25455284 0.03535533 0.1849435 0.27249026 0.03535533 0.15732235
		 0.28744221 0.03535533 0.12797755 0.29924488 0.03535533 0.097230613 0.30776894 0.03535533 0.065418303
		 0.31292105 0.03535533 0.032889307 0.31464469 0.03535533 -7.5649616e-08 0 0.03535533 0.37460828
		 0.0402807 0.03535533 0.37460828 0.080119967 0.03535533 0.37460828 0.11908138 0.03535533 0.37460828
		 0.15673816 0.03535533 0.37460828 0.19267774 0.03535533 0.37460828 0.2740916 0.03535533 0.37460828
		 0.56961167 0.03535533 0.37460828 0.56961167 0.03535533 0.30112225 0.56961167 0.03535533 0.24240094
		 0.56961167 0.03535533 0.19267768 0.56961167 0.03535533 0.1567381 0.56961167 0.03535533 0.11908132
		 0.56961167 0.03535533 0.080119908 0.56961167 0.03535533 0.042046666 0.56961167 0.03535533 -7.5649616e-08
		 0 -0.03535533 0.37460828 0.0402807 -0.03535533 0.37460828 0.080119967 -0.03535533 0.37460828
		 0.11908138 -0.03535533 0.37460828 0.15673816 -0.03535533 0.37460828 0.19267774 -0.03535533 0.37460828
		 0.2740916 -0.03535533 0.37460828 0.56961167 -0.03535533 0.37460828 0.56961167 -0.03535533 0.30112225
		 0.56961167 -0.03535533 0.24240094 0.56961167 -0.03535533 0.19267768 0.56961167 -0.03535533 0.1567381
		 0.56961167 -0.03535533 0.11908132 0.56961167 -0.03535533 0.080119908 0.56961167 -0.03535533 0.042046666
		 0.56961167 -0.03535533 -7.5649616e-08 0 -0.03535533 0.31464463 0.032889366 -0.03535533 0.31292099
		 0.065418363 -0.03535533 0.30776888 0.097230554 -0.03535533 0.29924482 0.12797749 -0.03535533 0.28744215
		 0.15732241 -0.03535533 0.2724902 0.18494356 -0.03535533 0.2545529 0.21053839 -0.03535533 0.23382658
		 0.23382652 -0.03535533 0.21053833 0.25455284 -0.03535533 0.1849435 0.27249026 -0.03535533 0.15732235
		 0.28744221 -0.03535533 0.12797755 0.29924488 -0.03535533 0.097230494 0.30776894 -0.03535533 0.065418303
		 0.31292105 -0.03535533 0.032889307 0.31464458 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 0.31464475
		 -0.51236534 0.03535533 0.37460828 -0.51236534 -0.03535533 0.31464463 -0.51236534 -0.03535533 0.37460828
		 -1.35788739 0.03535533 0.31464475 -1.35788739 0.03535533 0.37460828 -1.35788739 -0.03535533 0.31464463
		 -1.35788739 -0.03535533 0.37460828 -0.51236534 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 -7.5649616e-08
		 -1.35788739 -0.03535533 -7.5649616e-08 -1.35788739 0.03535533 -7.5649616e-08 -3.2154015e-19 0.03535533 -0.3146449
		 0.032889366 0.03535533 -0.31292114 0.065418363 0.03535533 -0.30776915 0.097230554 0.03535533 -0.29924497
		 0.12797761 0.03535533 -0.2874423 0.15732241 0.03535533 -0.27249047 0.18494356 0.03535533 -0.25455305
		 0.21053839 0.03535533 -0.23382673 0.23382664 0.03535533 -0.21053848 0.25455284 0.03535533 -0.18494365
		 0.27249026 0.03535533 -0.1573225 0.28744221 0.03535533 -0.1279777 0.29924488 0.03535533 -0.097230762
		 0.30776894 0.03535533 -0.065418452 0.31292105 0.03535533 -0.032889459 0 0.03535533 -0.37460843
		 0.0402807 0.03535533 -0.37460843 0.080119967 0.03535533 -0.37460843 0.11908138 0.03535533 -0.37460843
		 0.15673816 0.03535533 -0.37460843 0.19267774 0.03535533 -0.37460843 0.2740916 0.03535533 -0.37460843
		 0.56961167 0.03535533 -0.37460843 0.56961167 0.03535533 -0.3011224 0.56961167 0.03535533 -0.24240109
		 0.56961167 0.03535533 -0.19267783 0.56961167 0.03535533 -0.15673825 0.56961167 0.03535533 -0.11908147
		 0.56961167 0.03535533 -0.080120057 0.56961167 0.03535533 -0.042046819 0 -0.03535533 -0.37460843
		 0.0402807 -0.03535533 -0.37460843 0.080119967 -0.03535533 -0.37460843 0.11908138 -0.03535533 -0.37460843
		 0.15673816 -0.03535533 -0.37460843 0.19267774 -0.03535533 -0.37460843 0.2740916 -0.03535533 -0.37460843
		 0.56961167 -0.03535533 -0.37460843 0.56961167 -0.03535533 -0.3011224 0.56961167 -0.03535533 -0.24240109
		 0.56961167 -0.03535533 -0.19267783 0.56961167 -0.03535533 -0.15673825 0.56961167 -0.03535533 -0.11908147
		 0.56961167 -0.03535533 -0.080120057 0.56961167 -0.03535533 -0.042046819 -3.2154025e-19 -0.03535533 -0.31464478
		 0.032889366 -0.03535533 -0.31292114 0.065418363 -0.03535533 -0.30776903 0.097230554 -0.03535533 -0.29924497
		 0.12797749 -0.03535533 -0.2874423 0.15732241 -0.03535533 -0.27249035 0.18494356 -0.03535533 -0.25455305
		 0.21053839 -0.03535533 -0.23382673 0.23382652 -0.03535533 -0.21053848 0.25455284 -0.03535533 -0.18494365
		 0.27249026 -0.03535533 -0.1573225 0.28744221 -0.03535533 -0.1279777 0.29924488 -0.03535533 -0.097230643
		 0.30776894 -0.03535533 -0.065418452 0.31292105 -0.03535533 -0.032889459 -0.51236534 0.03535533 -0.3146449
		 -0.51236534 0.03535533 -0.37460843 -0.51236534 -0.03535533 -0.31464478 -0.51236534 -0.03535533 -0.37460843
		 -1.35788739 0.03535533 -0.3146449 -1.35788739 0.03535533 -0.37460843 -1.35788739 -0.03535533 -0.31464478
		 -1.35788739 -0.03535533 -0.37460843;
	setAttr -s 288 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 16 17 0 17 18 0 18 19 0 19 20 0 20 21 0
		 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0 32 33 0
		 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 42 0 42 43 0 43 44 0
		 44 45 0 45 46 0 46 47 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0
		 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 0 16 0 1 17 1 2 18 1 3 19 1
		 4 20 1 5 21 1 6 22 1 7 23 1 8 24 1 9 25 1 10 26 1 11 27 1 12 28 1 13 29 1 14 30 1
		 15 31 1 16 32 0 17 33 1 18 34 1 19 35 1 20 36 1 21 37 1 22 38 1 23 39 1 24 40 1 25 41 1
		 26 42 1 27 43 1 28 44 1 29 45 1 30 46 1 31 47 1 32 48 0 33 49 1 34 50 1 35 51 1 36 52 1
		 37 53 1 38 54 1 39 55 1 40 56 1 41 57 1 42 58 1 43 59 1 44 60 1 45 61 1 46 62 1 47 63 1
		 48 0 0 49 1 1 50 2 1 51 3 1 52 4 1 53 5 1 54 6 1 55 7 1 56 8 1 57 9 1 58 10 1 59 11 1
		 60 12 1 61 13 1 62 14 1 63 15 1 0 64 0 16 65 0 64 65 0 48 66 0 66 64 0 32 67 0 67 66 0
		 65 67 0 64 68 0 65 69 0 68 69 0 66 70 0 70 68 0 67 71 0 71 70 0 69 71 0 66 72 0 64 73 0
		 72 73 1 70 74 0 72 74 1 68 75 0 74 75 1 73 75 1 76 77 0 76 91 0 91 92 0 77 92 1 77 78 0
		 92 93 0 78 93 1 78 79 0 93 94 0 79 94 1 79 80 0 94 95 0 80 95 1 80 81 0 95 96 0 81 96 1
		 81 82 0 96 97 0;
	setAttr ".ed[166:287]" 82 97 1 82 83 0 97 98 0 83 98 1 83 84 0 98 99 0 84 99 1
		 84 85 0 99 100 0 85 100 1 85 86 0 100 101 0 86 101 1 86 87 0 101 102 0 87 102 1 87 88 0
		 102 103 0 88 103 1 88 89 0 103 104 0 89 104 1 89 90 0 104 105 0 90 105 1 90 15 0
		 105 31 0 91 106 0 106 107 0 92 107 1 107 108 0 93 108 1 108 109 0 94 109 1 109 110 0
		 95 110 1 110 111 0 96 111 1 111 112 0 97 112 1 112 113 0 98 113 1 113 114 0 99 114 1
		 114 115 0 100 115 1 115 116 0 101 116 1 116 117 0 102 117 1 117 118 0 103 118 1 118 119 0
		 104 119 1 119 120 0 105 120 1 120 47 0 106 121 0 121 122 0 107 122 1 122 123 0 108 123 1
		 123 124 0 109 124 1 124 125 0 110 125 1 125 126 0 111 126 1 126 127 0 112 127 1 127 128 0
		 113 128 1 128 129 0 114 129 1 129 130 0 115 130 1 130 131 0 116 131 1 131 132 0 117 132 1
		 132 133 0 118 133 1 133 134 0 119 134 1 134 135 0 120 135 1 135 63 0 121 76 0 122 77 1
		 123 78 1 124 79 1 125 80 1 126 81 1 127 82 1 128 83 1 129 84 1 130 85 1 131 86 1
		 132 87 1 133 88 1 134 89 1 135 90 1 140 141 0 142 140 0 143 142 0 141 143 0 76 136 0
		 136 137 0 91 137 0 121 138 0 138 136 0 106 139 0 139 138 0 137 139 0 136 140 0 137 141 0
		 139 143 0 138 142 0 138 72 0 136 73 0 142 74 0 140 75 0;
	setAttr -s 144 -ch 576 ".fc[0:143]" -type "polyFaces" 
		f 4 -1 60 15 -62
		mu 0 4 1 0 16 17
		f 4 -2 61 16 -63
		mu 0 4 2 1 17 18
		f 4 -3 62 17 -64
		mu 0 4 3 2 18 19
		f 4 -4 63 18 -65
		mu 0 4 4 3 19 20
		f 4 -5 64 19 -66
		mu 0 4 5 4 20 21
		f 4 -6 65 20 -67
		mu 0 4 6 5 21 22
		f 4 -7 66 21 -68
		mu 0 4 7 6 22 23
		f 4 -8 67 22 -69
		mu 0 4 8 7 23 24
		f 4 -9 68 23 -70
		mu 0 4 9 8 24 25
		f 4 -10 69 24 -71
		mu 0 4 10 9 25 26
		f 4 -11 70 25 -72
		mu 0 4 11 10 26 27
		f 4 -12 71 26 -73
		mu 0 4 12 11 27 28
		f 4 -13 72 27 -74
		mu 0 4 13 12 28 29
		f 4 -14 73 28 -75
		mu 0 4 14 13 29 30
		f 4 -15 74 29 -76
		mu 0 4 15 14 30 31
		f 4 -16 76 30 -78
		mu 0 4 17 16 32 33
		f 4 -17 77 31 -79
		mu 0 4 18 17 33 34
		f 4 -18 78 32 -80
		mu 0 4 19 18 34 35
		f 4 -19 79 33 -81
		mu 0 4 20 19 35 36
		f 4 -20 80 34 -82
		mu 0 4 21 20 36 37
		f 4 -21 81 35 -83
		mu 0 4 22 21 37 38
		f 4 -22 82 36 -84
		mu 0 4 23 22 38 39
		f 4 -23 83 37 -85
		mu 0 4 24 23 39 40
		f 4 -24 84 38 -86
		mu 0 4 25 24 40 41
		f 4 -25 85 39 -87
		mu 0 4 26 25 41 42
		f 4 -26 86 40 -88
		mu 0 4 27 26 42 43
		f 4 -27 87 41 -89
		mu 0 4 28 27 43 44
		f 4 -28 88 42 -90
		mu 0 4 29 28 44 45
		f 4 -29 89 43 -91
		mu 0 4 30 29 45 46
		f 4 -30 90 44 -92
		mu 0 4 31 30 46 47
		f 4 -31 92 45 -94
		mu 0 4 33 32 48 49
		f 4 -32 93 46 -95
		mu 0 4 34 33 49 50
		f 4 -33 94 47 -96
		mu 0 4 35 34 50 51
		f 4 -34 95 48 -97
		mu 0 4 36 35 51 52
		f 4 -35 96 49 -98
		mu 0 4 37 36 52 53
		f 4 -36 97 50 -99
		mu 0 4 38 37 53 54
		f 4 -37 98 51 -100
		mu 0 4 39 38 54 55
		f 4 -38 99 52 -101
		mu 0 4 40 39 55 56
		f 4 -39 100 53 -102
		mu 0 4 41 40 56 57
		f 4 -40 101 54 -103
		mu 0 4 42 41 57 58
		f 4 -41 102 55 -104
		mu 0 4 43 42 58 59
		f 4 -42 103 56 -105
		mu 0 4 44 43 59 60
		f 4 -43 104 57 -106
		mu 0 4 45 44 60 61
		f 4 -44 105 58 -107
		mu 0 4 46 45 61 62
		f 4 -45 106 59 -108
		mu 0 4 47 46 62 63
		f 4 -46 108 0 -110
		mu 0 4 49 48 64 65
		f 4 -47 109 1 -111
		mu 0 4 50 49 65 66
		f 4 -48 110 2 -112
		mu 0 4 51 50 66 67
		f 4 -49 111 3 -113
		mu 0 4 52 51 67 68
		f 4 -50 112 4 -114
		mu 0 4 53 52 68 69
		f 4 -51 113 5 -115
		mu 0 4 54 53 69 70
		f 4 -52 114 6 -116
		mu 0 4 55 54 70 71
		f 4 -53 115 7 -117
		mu 0 4 56 55 71 72
		f 4 -54 116 8 -118
		mu 0 4 57 56 72 73
		f 4 -55 117 9 -119
		mu 0 4 58 57 73 74
		f 4 -56 118 10 -120
		mu 0 4 59 58 74 75
		f 4 -57 119 11 -121
		mu 0 4 60 59 75 76
		f 4 -58 120 12 -122
		mu 0 4 61 60 76 77
		f 4 -59 121 13 -123
		mu 0 4 62 61 77 78
		f 4 -60 122 14 -124
		mu 0 4 63 62 78 79
		f 4 -135 -137 -139 -140
		mu 0 4 84 85 86 87
		f 4 -61 124 126 -126
		mu 0 4 16 64 81 80
		f 4 -109 127 128 -125
		mu 0 4 64 48 82 81
		f 4 -93 129 130 -128
		mu 0 4 48 32 83 82
		f 4 -77 125 131 -130
		mu 0 4 32 16 80 83
		f 4 -127 132 134 -134
		mu 0 4 80 81 85 84
		f 4 -131 137 138 -136
		mu 0 4 82 83 87 86
		f 4 -132 133 139 -138
		mu 0 4 83 80 84 87
		f 4 -129 140 142 -142
		mu 0 4 81 82 89 88
		f 4 135 143 -145 -141
		mu 0 4 82 86 90 89
		f 4 136 145 -147 -144
		mu 0 4 86 85 91 90
		f 4 -133 141 147 -146
		mu 0 4 85 81 88 91
		f 4 151 -151 -150 148
		mu 0 4 92 95 94 93
		f 4 154 -154 -152 152
		mu 0 4 96 97 95 92
		f 4 157 -157 -155 155
		mu 0 4 98 99 97 96
		f 4 160 -160 -158 158
		mu 0 4 100 101 99 98
		f 4 163 -163 -161 161
		mu 0 4 102 103 101 100
		f 4 166 -166 -164 164
		mu 0 4 104 105 103 102
		f 4 169 -169 -167 167
		mu 0 4 106 107 105 104
		f 4 172 -172 -170 170
		mu 0 4 108 109 107 106
		f 4 175 -175 -173 173
		mu 0 4 110 111 109 108
		f 4 178 -178 -176 176
		mu 0 4 112 113 111 110
		f 4 181 -181 -179 179
		mu 0 4 114 115 113 112
		f 4 184 -184 -182 182
		mu 0 4 116 117 115 114
		f 4 187 -187 -185 185
		mu 0 4 118 119 117 116
		f 4 190 -190 -188 188
		mu 0 4 120 121 119 118
		f 4 75 -193 -191 191
		mu 0 4 122 123 121 120
		f 4 195 -195 -194 150
		mu 0 4 95 125 124 94
		f 4 197 -197 -196 153
		mu 0 4 97 126 125 95
		f 4 199 -199 -198 156
		mu 0 4 99 127 126 97
		f 4 201 -201 -200 159
		mu 0 4 101 128 127 99
		f 4 203 -203 -202 162
		mu 0 4 103 129 128 101
		f 4 205 -205 -204 165
		mu 0 4 105 130 129 103
		f 4 207 -207 -206 168
		mu 0 4 107 131 130 105
		f 4 209 -209 -208 171
		mu 0 4 109 132 131 107
		f 4 211 -211 -210 174
		mu 0 4 111 133 132 109
		f 4 213 -213 -212 177
		mu 0 4 113 134 133 111
		f 4 215 -215 -214 180
		mu 0 4 115 135 134 113
		f 4 217 -217 -216 183
		mu 0 4 117 136 135 115
		f 4 219 -219 -218 186
		mu 0 4 119 137 136 117
		f 4 221 -221 -220 189
		mu 0 4 121 138 137 119
		f 4 91 -223 -222 192
		mu 0 4 123 139 138 121
		f 4 225 -225 -224 194
		mu 0 4 125 141 140 124
		f 4 227 -227 -226 196
		mu 0 4 126 142 141 125
		f 4 229 -229 -228 198
		mu 0 4 127 143 142 126
		f 4 231 -231 -230 200
		mu 0 4 128 144 143 127
		f 4 233 -233 -232 202
		mu 0 4 129 145 144 128
		f 4 235 -235 -234 204
		mu 0 4 130 146 145 129
		f 4 237 -237 -236 206
		mu 0 4 131 147 146 130
		f 4 239 -239 -238 208
		mu 0 4 132 148 147 131
		f 4 241 -241 -240 210
		mu 0 4 133 149 148 132
		f 4 243 -243 -242 212
		mu 0 4 134 150 149 133
		f 4 245 -245 -244 214
		mu 0 4 135 151 150 134
		f 4 247 -247 -246 216
		mu 0 4 136 152 151 135
		f 4 249 -249 -248 218
		mu 0 4 137 153 152 136
		f 4 251 -251 -250 220
		mu 0 4 138 154 153 137
		f 4 107 -253 -252 222
		mu 0 4 139 155 154 138
		f 4 254 -149 -254 224
		mu 0 4 141 157 156 140
		f 4 255 -153 -255 226
		mu 0 4 142 158 157 141
		f 4 256 -156 -256 228
		mu 0 4 143 159 158 142
		f 4 257 -159 -257 230
		mu 0 4 144 160 159 143
		f 4 258 -162 -258 232
		mu 0 4 145 161 160 144
		f 4 259 -165 -259 234
		mu 0 4 146 162 161 145
		f 4 260 -168 -260 236
		mu 0 4 147 163 162 146
		f 4 261 -171 -261 238
		mu 0 4 148 164 163 147
		f 4 262 -174 -262 240
		mu 0 4 149 165 164 148
		f 4 263 -177 -263 242
		mu 0 4 150 166 165 149
		f 4 264 -180 -264 244
		mu 0 4 151 167 166 150
		f 4 265 -183 -265 246
		mu 0 4 152 168 167 151
		f 4 266 -186 -266 248
		mu 0 4 153 169 168 152
		f 4 267 -189 -267 250
		mu 0 4 154 170 169 153
		f 4 123 -192 -268 252
		mu 0 4 155 171 170 154
		f 4 271 270 269 268
		mu 0 4 172 175 174 173
		f 4 274 -274 -273 149
		mu 0 4 94 177 176 156
		f 4 272 -277 -276 253
		mu 0 4 156 176 178 140
		f 4 275 -279 -278 223
		mu 0 4 140 178 179 124
		f 4 277 -280 -275 193
		mu 0 4 124 179 177 94
		f 4 281 -269 -281 273
		mu 0 4 177 172 173 176
		f 4 283 -271 -283 278
		mu 0 4 178 174 175 179
		f 4 282 -272 -282 279
		mu 0 4 179 175 172 177
		f 4 285 -143 -285 276
		mu 0 4 176 181 180 178
		f 4 284 144 -287 -284
		mu 0 4 178 180 182 174
		f 4 286 146 -288 -270
		mu 0 4 174 182 183 173
		f 4 287 -148 -286 280
		mu 0 4 173 183 181 176;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pTorus6";
	rename -uid "52AAC7F5-7A40-7A10-38CA-4E86F15864C5";
	setAttr ".t" -type "double3" 2.7957363266636226 1.357887356229559 -1.8450120089362494 ;
	setAttr ".r" -type "double3" 0 0 89.999999999999986 ;
	setAttr ".s" -type "double3" 1.0000000000000007 1.0000000000000002 1 ;
	setAttr ".rp" -type "double3" -0.3941378593444827 0 -7.5649615682849003e-08 ;
	setAttr ".rpt" -type "double3" 0.39413785934448303 -0.39413785934448281 0 ;
	setAttr ".sp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
	setAttr ".spt" -type "double3" -2.7755575615628933e-16 0 0 ;
createNode transform -n "transform4" -p "pTorus6";
	rename -uid "31A8DE53-5840-BE40-7553-A4A495D50D41";
	setAttr ".v" no;
createNode mesh -n "pTorusShape6" -p "transform4";
	rename -uid "1C926EDC-6D46-E1A3-454C-278E41CB3BA8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 5 ".iog[0].og";
	setAttr ".iog[0].og[4].gcl" -type "componentList" 8 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]";
	setAttr ".iog[0].og[5].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[6].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[7].gcl" -type "componentList" 3 "e[142]" "e[144]" "e[146:147]";
	setAttr ".iog[0].og[8].gcl" -type "componentList" 1 "f[0:143]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.79166656732559204 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 184 ".uvst[0].uvsp[0:183]" -type "float2" 0.73333329 1 0.74999994
		 1 0.76666659 1 0.78333324 1 0.79999989 1 0.81666654 1 0.83333319 1 0.84999985 1 0.8666665
		 1 0.88333315 1 0.8999998 1 0.91666645 1 0.9333331 1 0.94999975 1 0.9666664 1 0.98333305
		 1 0.73333329 0.75 0.74999994 0.75 0.76666659 0.75 0.78333324 0.75 0.79999989 0.75
		 0.81666654 0.75 0.83333319 0.75 0.84999985 0.75 0.8666665 0.75 0.88333315 0.75 0.8999998
		 0.75 0.91666645 0.75 0.9333331 0.75 0.94999975 0.75 0.9666664 0.75 0.98333305 0.75
		 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989 0.5 0.81666654
		 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998 0.5 0.91666645
		 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329 0.25 0.74999994
		 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25 0.83333319 0.25
		 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645 0.25 0.9333331
		 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994 0 0.76666659
		 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665 0 0.88333315
		 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305 0 0.73333329
		 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0.75 0.73333329 0 0.73333329
		 0.25 0.73333329 0.5 0.73333329 0 0.73333329 0.25 0.73333329 0.25 0.73333329 0 0.74999994
		 1 0.73333329 1 0.73333329 0.75 0.74999994 0.75 0.76666659 1 0.76666659 0.75 0.78333324
		 1 0.78333324 0.75 0.79999989 1 0.79999989 0.75 0.81666654 1 0.81666654 0.75 0.83333319
		 1 0.83333319 0.75 0.84999985 1 0.84999985 0.75 0.8666665 1 0.8666665 0.75 0.88333315
		 1 0.88333315 0.75 0.8999998 1 0.8999998 0.75 0.91666645 1 0.91666645 0.75 0.9333331
		 1 0.9333331 0.75 0.94999975 1 0.94999975 0.75 0.9666664 1 0.9666664 0.75 0.98333305
		 1 0.98333305 0.75 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989
		 0.5 0.81666654 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998
		 0.5 0.91666645 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329
		 0.25 0.74999994 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25
		 0.83333319 0.25 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645
		 0.25 0.9333331 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994
		 0 0.76666659 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665
		 0 0.88333315 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305
		 0 0.73333329 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0 0.73333329
		 0.75 0.73333329 0.25 0.73333329 0.5 0.73333329 0.25 0.73333329 0 0.73333329 0.25
		 0.73333329 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".pt";
	setAttr ".pt[48]" -type "float3" 0.03513477 -0.064907849 -0.031166604 ;
	setAttr ".pt[49]" -type "float3" 0.031876974 -0.064907849 -0.03099589 ;
	setAttr ".pt[50]" -type "float3" 0.028654853 -0.064907849 -0.030485559 ;
	setAttr ".pt[51]" -type "float3" 0.025503734 -0.064907849 -0.02964123 ;
	setAttr ".pt[52]" -type "float3" 0.022458173 -0.064907849 -0.028472122 ;
	setAttr ".pt[53]" -type "float3" 0.019551437 -0.064907849 -0.026991103 ;
	setAttr ".pt[54]" -type "float3" 0.01681548 -0.064907849 -0.025214329 ;
	setAttr ".pt[55]" -type "float3" 0.014280196 -0.064907849 -0.023161316 ;
	setAttr ".pt[56]" -type "float3" 0.01197345 -0.064907849 -0.020854538 ;
	setAttr ".pt[57]" -type "float3" 0.0099204574 -0.064907849 -0.018319288 ;
	setAttr ".pt[58]" -type "float3" 0.0081436727 -0.064907849 -0.015583325 ;
	setAttr ".pt[59]" -type "float3" 0.0066626295 -0.064907849 -0.012676611 ;
	setAttr ".pt[60]" -type "float3" 0.0054935142 -0.064907849 -0.0096310163 ;
	setAttr ".pt[61]" -type "float3" 0.0046492144 -0.064907849 -0.0064799134 ;
	setAttr ".pt[62]" -type "float3" 0.0041388338 -0.064907849 -0.0032578055 ;
	setAttr ".pt[63]" -type "float3" 0.0039681112 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[66]" -type "float3" 0.085886188 -0.064907849 -0.031166604 ;
	setAttr ".pt[72]" -type "float3" 0.085886188 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[121]" -type "float3" 0.03513477 -0.064907849 0.031166604 ;
	setAttr ".pt[122]" -type "float3" 0.031876974 -0.064907849 0.030995879 ;
	setAttr ".pt[123]" -type "float3" 0.028654853 -0.064907849 0.030485567 ;
	setAttr ".pt[124]" -type "float3" 0.025503734 -0.064907849 0.029641215 ;
	setAttr ".pt[125]" -type "float3" 0.022458173 -0.064907849 0.028472107 ;
	setAttr ".pt[126]" -type "float3" 0.019551437 -0.064907849 0.026991079 ;
	setAttr ".pt[127]" -type "float3" 0.01681548 -0.064907849 0.025214313 ;
	setAttr ".pt[128]" -type "float3" 0.014280196 -0.064907849 0.023161313 ;
	setAttr ".pt[129]" -type "float3" 0.01197345 -0.064907849 0.020854548 ;
	setAttr ".pt[130]" -type "float3" 0.0099204574 -0.064907849 0.018319286 ;
	setAttr ".pt[131]" -type "float3" 0.0081436727 -0.064907849 0.015583316 ;
	setAttr ".pt[132]" -type "float3" 0.0066626295 -0.064907849 0.012676615 ;
	setAttr ".pt[133]" -type "float3" 0.0054935142 -0.064907849 0.0096310098 ;
	setAttr ".pt[134]" -type "float3" 0.0046492144 -0.064907849 0.0064799096 ;
	setAttr ".pt[135]" -type "float3" 0.0041388338 -0.064907849 0.003257805 ;
	setAttr ".pt[138]" -type "float3" 0.085886188 -0.064907849 0.031166604 ;
	setAttr -s 144 ".vt[0:143]"  0 0.03535533 0.31464475 0.032889366 0.03535533 0.31292099
		 0.065418363 0.03535533 0.307769 0.097230554 0.03535533 0.29924482 0.12797761 0.03535533 0.28744215
		 0.15732241 0.03535533 0.27249032 0.18494356 0.03535533 0.2545529 0.21053839 0.03535533 0.23382658
		 0.23382664 0.03535533 0.21053833 0.25455284 0.03535533 0.1849435 0.27249026 0.03535533 0.15732235
		 0.28744221 0.03535533 0.12797755 0.29924488 0.03535533 0.097230613 0.30776894 0.03535533 0.065418303
		 0.31292105 0.03535533 0.032889307 0.31464469 0.03535533 -7.5649616e-08 0 0.03535533 0.37460828
		 0.0402807 0.03535533 0.37460828 0.080119967 0.03535533 0.37460828 0.11908138 0.03535533 0.37460828
		 0.15673816 0.03535533 0.37460828 0.19267774 0.03535533 0.37460828 0.2740916 0.03535533 0.37460828
		 0.56961167 0.03535533 0.37460828 0.56961167 0.03535533 0.30112225 0.56961167 0.03535533 0.24240094
		 0.56961167 0.03535533 0.19267768 0.56961167 0.03535533 0.1567381 0.56961167 0.03535533 0.11908132
		 0.56961167 0.03535533 0.080119908 0.56961167 0.03535533 0.042046666 0.56961167 0.03535533 -7.5649616e-08
		 0 -0.03535533 0.37460828 0.0402807 -0.03535533 0.37460828 0.080119967 -0.03535533 0.37460828
		 0.11908138 -0.03535533 0.37460828 0.15673816 -0.03535533 0.37460828 0.19267774 -0.03535533 0.37460828
		 0.2740916 -0.03535533 0.37460828 0.56961167 -0.03535533 0.37460828 0.56961167 -0.03535533 0.30112225
		 0.56961167 -0.03535533 0.24240094 0.56961167 -0.03535533 0.19267768 0.56961167 -0.03535533 0.1567381
		 0.56961167 -0.03535533 0.11908132 0.56961167 -0.03535533 0.080119908 0.56961167 -0.03535533 0.042046666
		 0.56961167 -0.03535533 -7.5649616e-08 0 -0.03535533 0.31464463 0.032889366 -0.03535533 0.31292099
		 0.065418363 -0.03535533 0.30776888 0.097230554 -0.03535533 0.29924482 0.12797749 -0.03535533 0.28744215
		 0.15732241 -0.03535533 0.2724902 0.18494356 -0.03535533 0.2545529 0.21053839 -0.03535533 0.23382658
		 0.23382652 -0.03535533 0.21053833 0.25455284 -0.03535533 0.1849435 0.27249026 -0.03535533 0.15732235
		 0.28744221 -0.03535533 0.12797755 0.29924488 -0.03535533 0.097230494 0.30776894 -0.03535533 0.065418303
		 0.31292105 -0.03535533 0.032889307 0.31464458 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 0.31464475
		 -0.51236534 0.03535533 0.37460828 -0.51236534 -0.03535533 0.31464463 -0.51236534 -0.03535533 0.37460828
		 -1.35788739 0.03535533 0.31464475 -1.35788739 0.03535533 0.37460828 -1.35788739 -0.03535533 0.31464463
		 -1.35788739 -0.03535533 0.37460828 -0.51236534 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 -7.5649616e-08
		 -1.35788739 -0.03535533 -7.5649616e-08 -1.35788739 0.03535533 -7.5649616e-08 -3.2154015e-19 0.03535533 -0.3146449
		 0.032889366 0.03535533 -0.31292114 0.065418363 0.03535533 -0.30776915 0.097230554 0.03535533 -0.29924497
		 0.12797761 0.03535533 -0.2874423 0.15732241 0.03535533 -0.27249047 0.18494356 0.03535533 -0.25455305
		 0.21053839 0.03535533 -0.23382673 0.23382664 0.03535533 -0.21053848 0.25455284 0.03535533 -0.18494365
		 0.27249026 0.03535533 -0.1573225 0.28744221 0.03535533 -0.1279777 0.29924488 0.03535533 -0.097230762
		 0.30776894 0.03535533 -0.065418452 0.31292105 0.03535533 -0.032889459 0 0.03535533 -0.37460843
		 0.0402807 0.03535533 -0.37460843 0.080119967 0.03535533 -0.37460843 0.11908138 0.03535533 -0.37460843
		 0.15673816 0.03535533 -0.37460843 0.19267774 0.03535533 -0.37460843 0.2740916 0.03535533 -0.37460843
		 0.56961167 0.03535533 -0.37460843 0.56961167 0.03535533 -0.3011224 0.56961167 0.03535533 -0.24240109
		 0.56961167 0.03535533 -0.19267783 0.56961167 0.03535533 -0.15673825 0.56961167 0.03535533 -0.11908147
		 0.56961167 0.03535533 -0.080120057 0.56961167 0.03535533 -0.042046819 0 -0.03535533 -0.37460843
		 0.0402807 -0.03535533 -0.37460843 0.080119967 -0.03535533 -0.37460843 0.11908138 -0.03535533 -0.37460843
		 0.15673816 -0.03535533 -0.37460843 0.19267774 -0.03535533 -0.37460843 0.2740916 -0.03535533 -0.37460843
		 0.56961167 -0.03535533 -0.37460843 0.56961167 -0.03535533 -0.3011224 0.56961167 -0.03535533 -0.24240109
		 0.56961167 -0.03535533 -0.19267783 0.56961167 -0.03535533 -0.15673825 0.56961167 -0.03535533 -0.11908147
		 0.56961167 -0.03535533 -0.080120057 0.56961167 -0.03535533 -0.042046819 -3.2154025e-19 -0.03535533 -0.31464478
		 0.032889366 -0.03535533 -0.31292114 0.065418363 -0.03535533 -0.30776903 0.097230554 -0.03535533 -0.29924497
		 0.12797749 -0.03535533 -0.2874423 0.15732241 -0.03535533 -0.27249035 0.18494356 -0.03535533 -0.25455305
		 0.21053839 -0.03535533 -0.23382673 0.23382652 -0.03535533 -0.21053848 0.25455284 -0.03535533 -0.18494365
		 0.27249026 -0.03535533 -0.1573225 0.28744221 -0.03535533 -0.1279777 0.29924488 -0.03535533 -0.097230643
		 0.30776894 -0.03535533 -0.065418452 0.31292105 -0.03535533 -0.032889459 -0.51236534 0.03535533 -0.3146449
		 -0.51236534 0.03535533 -0.37460843 -0.51236534 -0.03535533 -0.31464478 -0.51236534 -0.03535533 -0.37460843
		 -1.35788739 0.03535533 -0.3146449 -1.35788739 0.03535533 -0.37460843 -1.35788739 -0.03535533 -0.31464478
		 -1.35788739 -0.03535533 -0.37460843;
	setAttr -s 288 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 16 17 0 17 18 0 18 19 0 19 20 0 20 21 0
		 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0 32 33 0
		 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 42 0 42 43 0 43 44 0
		 44 45 0 45 46 0 46 47 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0
		 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 0 16 0 1 17 1 2 18 1 3 19 1
		 4 20 1 5 21 1 6 22 1 7 23 1 8 24 1 9 25 1 10 26 1 11 27 1 12 28 1 13 29 1 14 30 1
		 15 31 1 16 32 0 17 33 1 18 34 1 19 35 1 20 36 1 21 37 1 22 38 1 23 39 1 24 40 1 25 41 1
		 26 42 1 27 43 1 28 44 1 29 45 1 30 46 1 31 47 1 32 48 0 33 49 1 34 50 1 35 51 1 36 52 1
		 37 53 1 38 54 1 39 55 1 40 56 1 41 57 1 42 58 1 43 59 1 44 60 1 45 61 1 46 62 1 47 63 1
		 48 0 0 49 1 1 50 2 1 51 3 1 52 4 1 53 5 1 54 6 1 55 7 1 56 8 1 57 9 1 58 10 1 59 11 1
		 60 12 1 61 13 1 62 14 1 63 15 1 0 64 0 16 65 0 64 65 0 48 66 0 66 64 0 32 67 0 67 66 0
		 65 67 0 64 68 0 65 69 0 68 69 0 66 70 0 70 68 0 67 71 0 71 70 0 69 71 0 66 72 0 64 73 0
		 72 73 1 70 74 0 72 74 1 68 75 0 74 75 1 73 75 1 76 77 0 76 91 0 91 92 0 77 92 1 77 78 0
		 92 93 0 78 93 1 78 79 0 93 94 0 79 94 1 79 80 0 94 95 0 80 95 1 80 81 0 95 96 0 81 96 1
		 81 82 0 96 97 0;
	setAttr ".ed[166:287]" 82 97 1 82 83 0 97 98 0 83 98 1 83 84 0 98 99 0 84 99 1
		 84 85 0 99 100 0 85 100 1 85 86 0 100 101 0 86 101 1 86 87 0 101 102 0 87 102 1 87 88 0
		 102 103 0 88 103 1 88 89 0 103 104 0 89 104 1 89 90 0 104 105 0 90 105 1 90 15 0
		 105 31 0 91 106 0 106 107 0 92 107 1 107 108 0 93 108 1 108 109 0 94 109 1 109 110 0
		 95 110 1 110 111 0 96 111 1 111 112 0 97 112 1 112 113 0 98 113 1 113 114 0 99 114 1
		 114 115 0 100 115 1 115 116 0 101 116 1 116 117 0 102 117 1 117 118 0 103 118 1 118 119 0
		 104 119 1 119 120 0 105 120 1 120 47 0 106 121 0 121 122 0 107 122 1 122 123 0 108 123 1
		 123 124 0 109 124 1 124 125 0 110 125 1 125 126 0 111 126 1 126 127 0 112 127 1 127 128 0
		 113 128 1 128 129 0 114 129 1 129 130 0 115 130 1 130 131 0 116 131 1 131 132 0 117 132 1
		 132 133 0 118 133 1 133 134 0 119 134 1 134 135 0 120 135 1 135 63 0 121 76 0 122 77 1
		 123 78 1 124 79 1 125 80 1 126 81 1 127 82 1 128 83 1 129 84 1 130 85 1 131 86 1
		 132 87 1 133 88 1 134 89 1 135 90 1 140 141 0 142 140 0 143 142 0 141 143 0 76 136 0
		 136 137 0 91 137 0 121 138 0 138 136 0 106 139 0 139 138 0 137 139 0 136 140 0 137 141 0
		 139 143 0 138 142 0 138 72 0 136 73 0 142 74 0 140 75 0;
	setAttr -s 144 -ch 576 ".fc[0:143]" -type "polyFaces" 
		f 4 -1 60 15 -62
		mu 0 4 1 0 16 17
		f 4 -2 61 16 -63
		mu 0 4 2 1 17 18
		f 4 -3 62 17 -64
		mu 0 4 3 2 18 19
		f 4 -4 63 18 -65
		mu 0 4 4 3 19 20
		f 4 -5 64 19 -66
		mu 0 4 5 4 20 21
		f 4 -6 65 20 -67
		mu 0 4 6 5 21 22
		f 4 -7 66 21 -68
		mu 0 4 7 6 22 23
		f 4 -8 67 22 -69
		mu 0 4 8 7 23 24
		f 4 -9 68 23 -70
		mu 0 4 9 8 24 25
		f 4 -10 69 24 -71
		mu 0 4 10 9 25 26
		f 4 -11 70 25 -72
		mu 0 4 11 10 26 27
		f 4 -12 71 26 -73
		mu 0 4 12 11 27 28
		f 4 -13 72 27 -74
		mu 0 4 13 12 28 29
		f 4 -14 73 28 -75
		mu 0 4 14 13 29 30
		f 4 -15 74 29 -76
		mu 0 4 15 14 30 31
		f 4 -16 76 30 -78
		mu 0 4 17 16 32 33
		f 4 -17 77 31 -79
		mu 0 4 18 17 33 34
		f 4 -18 78 32 -80
		mu 0 4 19 18 34 35
		f 4 -19 79 33 -81
		mu 0 4 20 19 35 36
		f 4 -20 80 34 -82
		mu 0 4 21 20 36 37
		f 4 -21 81 35 -83
		mu 0 4 22 21 37 38
		f 4 -22 82 36 -84
		mu 0 4 23 22 38 39
		f 4 -23 83 37 -85
		mu 0 4 24 23 39 40
		f 4 -24 84 38 -86
		mu 0 4 25 24 40 41
		f 4 -25 85 39 -87
		mu 0 4 26 25 41 42
		f 4 -26 86 40 -88
		mu 0 4 27 26 42 43
		f 4 -27 87 41 -89
		mu 0 4 28 27 43 44
		f 4 -28 88 42 -90
		mu 0 4 29 28 44 45
		f 4 -29 89 43 -91
		mu 0 4 30 29 45 46
		f 4 -30 90 44 -92
		mu 0 4 31 30 46 47
		f 4 -31 92 45 -94
		mu 0 4 33 32 48 49
		f 4 -32 93 46 -95
		mu 0 4 34 33 49 50
		f 4 -33 94 47 -96
		mu 0 4 35 34 50 51
		f 4 -34 95 48 -97
		mu 0 4 36 35 51 52
		f 4 -35 96 49 -98
		mu 0 4 37 36 52 53
		f 4 -36 97 50 -99
		mu 0 4 38 37 53 54
		f 4 -37 98 51 -100
		mu 0 4 39 38 54 55
		f 4 -38 99 52 -101
		mu 0 4 40 39 55 56
		f 4 -39 100 53 -102
		mu 0 4 41 40 56 57
		f 4 -40 101 54 -103
		mu 0 4 42 41 57 58
		f 4 -41 102 55 -104
		mu 0 4 43 42 58 59
		f 4 -42 103 56 -105
		mu 0 4 44 43 59 60
		f 4 -43 104 57 -106
		mu 0 4 45 44 60 61
		f 4 -44 105 58 -107
		mu 0 4 46 45 61 62
		f 4 -45 106 59 -108
		mu 0 4 47 46 62 63
		f 4 -46 108 0 -110
		mu 0 4 49 48 64 65
		f 4 -47 109 1 -111
		mu 0 4 50 49 65 66
		f 4 -48 110 2 -112
		mu 0 4 51 50 66 67
		f 4 -49 111 3 -113
		mu 0 4 52 51 67 68
		f 4 -50 112 4 -114
		mu 0 4 53 52 68 69
		f 4 -51 113 5 -115
		mu 0 4 54 53 69 70
		f 4 -52 114 6 -116
		mu 0 4 55 54 70 71
		f 4 -53 115 7 -117
		mu 0 4 56 55 71 72
		f 4 -54 116 8 -118
		mu 0 4 57 56 72 73
		f 4 -55 117 9 -119
		mu 0 4 58 57 73 74
		f 4 -56 118 10 -120
		mu 0 4 59 58 74 75
		f 4 -57 119 11 -121
		mu 0 4 60 59 75 76
		f 4 -58 120 12 -122
		mu 0 4 61 60 76 77
		f 4 -59 121 13 -123
		mu 0 4 62 61 77 78
		f 4 -60 122 14 -124
		mu 0 4 63 62 78 79
		f 4 -135 -137 -139 -140
		mu 0 4 84 85 86 87
		f 4 -61 124 126 -126
		mu 0 4 16 64 81 80
		f 4 -109 127 128 -125
		mu 0 4 64 48 82 81
		f 4 -93 129 130 -128
		mu 0 4 48 32 83 82
		f 4 -77 125 131 -130
		mu 0 4 32 16 80 83
		f 4 -127 132 134 -134
		mu 0 4 80 81 85 84
		f 4 -131 137 138 -136
		mu 0 4 82 83 87 86
		f 4 -132 133 139 -138
		mu 0 4 83 80 84 87
		f 4 -129 140 142 -142
		mu 0 4 81 82 89 88
		f 4 135 143 -145 -141
		mu 0 4 82 86 90 89
		f 4 136 145 -147 -144
		mu 0 4 86 85 91 90
		f 4 -133 141 147 -146
		mu 0 4 85 81 88 91
		f 4 151 -151 -150 148
		mu 0 4 92 95 94 93
		f 4 154 -154 -152 152
		mu 0 4 96 97 95 92
		f 4 157 -157 -155 155
		mu 0 4 98 99 97 96
		f 4 160 -160 -158 158
		mu 0 4 100 101 99 98
		f 4 163 -163 -161 161
		mu 0 4 102 103 101 100
		f 4 166 -166 -164 164
		mu 0 4 104 105 103 102
		f 4 169 -169 -167 167
		mu 0 4 106 107 105 104
		f 4 172 -172 -170 170
		mu 0 4 108 109 107 106
		f 4 175 -175 -173 173
		mu 0 4 110 111 109 108
		f 4 178 -178 -176 176
		mu 0 4 112 113 111 110
		f 4 181 -181 -179 179
		mu 0 4 114 115 113 112
		f 4 184 -184 -182 182
		mu 0 4 116 117 115 114
		f 4 187 -187 -185 185
		mu 0 4 118 119 117 116
		f 4 190 -190 -188 188
		mu 0 4 120 121 119 118
		f 4 75 -193 -191 191
		mu 0 4 122 123 121 120
		f 4 195 -195 -194 150
		mu 0 4 95 125 124 94
		f 4 197 -197 -196 153
		mu 0 4 97 126 125 95
		f 4 199 -199 -198 156
		mu 0 4 99 127 126 97
		f 4 201 -201 -200 159
		mu 0 4 101 128 127 99
		f 4 203 -203 -202 162
		mu 0 4 103 129 128 101
		f 4 205 -205 -204 165
		mu 0 4 105 130 129 103
		f 4 207 -207 -206 168
		mu 0 4 107 131 130 105
		f 4 209 -209 -208 171
		mu 0 4 109 132 131 107
		f 4 211 -211 -210 174
		mu 0 4 111 133 132 109
		f 4 213 -213 -212 177
		mu 0 4 113 134 133 111
		f 4 215 -215 -214 180
		mu 0 4 115 135 134 113
		f 4 217 -217 -216 183
		mu 0 4 117 136 135 115
		f 4 219 -219 -218 186
		mu 0 4 119 137 136 117
		f 4 221 -221 -220 189
		mu 0 4 121 138 137 119
		f 4 91 -223 -222 192
		mu 0 4 123 139 138 121
		f 4 225 -225 -224 194
		mu 0 4 125 141 140 124
		f 4 227 -227 -226 196
		mu 0 4 126 142 141 125
		f 4 229 -229 -228 198
		mu 0 4 127 143 142 126
		f 4 231 -231 -230 200
		mu 0 4 128 144 143 127
		f 4 233 -233 -232 202
		mu 0 4 129 145 144 128
		f 4 235 -235 -234 204
		mu 0 4 130 146 145 129
		f 4 237 -237 -236 206
		mu 0 4 131 147 146 130
		f 4 239 -239 -238 208
		mu 0 4 132 148 147 131
		f 4 241 -241 -240 210
		mu 0 4 133 149 148 132
		f 4 243 -243 -242 212
		mu 0 4 134 150 149 133
		f 4 245 -245 -244 214
		mu 0 4 135 151 150 134
		f 4 247 -247 -246 216
		mu 0 4 136 152 151 135
		f 4 249 -249 -248 218
		mu 0 4 137 153 152 136
		f 4 251 -251 -250 220
		mu 0 4 138 154 153 137
		f 4 107 -253 -252 222
		mu 0 4 139 155 154 138
		f 4 254 -149 -254 224
		mu 0 4 141 157 156 140
		f 4 255 -153 -255 226
		mu 0 4 142 158 157 141
		f 4 256 -156 -256 228
		mu 0 4 143 159 158 142
		f 4 257 -159 -257 230
		mu 0 4 144 160 159 143
		f 4 258 -162 -258 232
		mu 0 4 145 161 160 144
		f 4 259 -165 -259 234
		mu 0 4 146 162 161 145
		f 4 260 -168 -260 236
		mu 0 4 147 163 162 146
		f 4 261 -171 -261 238
		mu 0 4 148 164 163 147
		f 4 262 -174 -262 240
		mu 0 4 149 165 164 148
		f 4 263 -177 -263 242
		mu 0 4 150 166 165 149
		f 4 264 -180 -264 244
		mu 0 4 151 167 166 150
		f 4 265 -183 -265 246
		mu 0 4 152 168 167 151
		f 4 266 -186 -266 248
		mu 0 4 153 169 168 152
		f 4 267 -189 -267 250
		mu 0 4 154 170 169 153
		f 4 123 -192 -268 252
		mu 0 4 155 171 170 154
		f 4 271 270 269 268
		mu 0 4 172 175 174 173
		f 4 274 -274 -273 149
		mu 0 4 94 177 176 156
		f 4 272 -277 -276 253
		mu 0 4 156 176 178 140
		f 4 275 -279 -278 223
		mu 0 4 140 178 179 124
		f 4 277 -280 -275 193
		mu 0 4 124 179 177 94
		f 4 281 -269 -281 273
		mu 0 4 177 172 173 176
		f 4 283 -271 -283 278
		mu 0 4 178 174 175 179
		f 4 282 -272 -282 279
		mu 0 4 179 175 172 177
		f 4 285 -143 -285 276
		mu 0 4 176 181 180 178
		f 4 284 144 -287 -284
		mu 0 4 178 180 182 174
		f 4 286 146 -288 -270
		mu 0 4 174 182 183 173
		f 4 287 -148 -286 280
		mu 0 4 173 183 181 176;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pTorus7";
	rename -uid "F0D3015B-3C41-0B0C-1925-00B2223343ED";
	setAttr ".t" -type "double3" 2.2341316878919182 1.3578873562295586 5.7049203515048568 ;
	setAttr ".r" -type "double3" -90 0 90 ;
	setAttr ".rp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
	setAttr ".rpt" -type "double3" 0.3941378593444822 -0.39413785934448242 0 ;
	setAttr ".sp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
createNode mesh -n "polySurfaceShape3" -p "pTorus7";
	rename -uid "2A04FB6D-9747-B914-EFDE-7DBAD8BEF0E6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".iog[0].og[4].gcl" -type "componentList" 8 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]";
	setAttr ".iog[0].og[5].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[6].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[7].gcl" -type "componentList" 3 "e[142]" "e[144]" "e[146:147]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.79166656732559204 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 184 ".uvst[0].uvsp[0:183]" -type "float2" 0.73333329 1 0.74999994
		 1 0.76666659 1 0.78333324 1 0.79999989 1 0.81666654 1 0.83333319 1 0.84999985 1 0.8666665
		 1 0.88333315 1 0.8999998 1 0.91666645 1 0.9333331 1 0.94999975 1 0.9666664 1 0.98333305
		 1 0.73333329 0.75 0.74999994 0.75 0.76666659 0.75 0.78333324 0.75 0.79999989 0.75
		 0.81666654 0.75 0.83333319 0.75 0.84999985 0.75 0.8666665 0.75 0.88333315 0.75 0.8999998
		 0.75 0.91666645 0.75 0.9333331 0.75 0.94999975 0.75 0.9666664 0.75 0.98333305 0.75
		 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989 0.5 0.81666654
		 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998 0.5 0.91666645
		 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329 0.25 0.74999994
		 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25 0.83333319 0.25
		 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645 0.25 0.9333331
		 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994 0 0.76666659
		 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665 0 0.88333315
		 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305 0 0.73333329
		 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0.75 0.73333329 0 0.73333329
		 0.25 0.73333329 0.5 0.73333329 0 0.73333329 0.25 0.73333329 0.25 0.73333329 0 0.74999994
		 1 0.73333329 1 0.73333329 0.75 0.74999994 0.75 0.76666659 1 0.76666659 0.75 0.78333324
		 1 0.78333324 0.75 0.79999989 1 0.79999989 0.75 0.81666654 1 0.81666654 0.75 0.83333319
		 1 0.83333319 0.75 0.84999985 1 0.84999985 0.75 0.8666665 1 0.8666665 0.75 0.88333315
		 1 0.88333315 0.75 0.8999998 1 0.8999998 0.75 0.91666645 1 0.91666645 0.75 0.9333331
		 1 0.9333331 0.75 0.94999975 1 0.94999975 0.75 0.9666664 1 0.9666664 0.75 0.98333305
		 1 0.98333305 0.75 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989
		 0.5 0.81666654 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998
		 0.5 0.91666645 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329
		 0.25 0.74999994 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25
		 0.83333319 0.25 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645
		 0.25 0.9333331 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994
		 0 0.76666659 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665
		 0 0.88333315 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305
		 0 0.73333329 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0 0.73333329
		 0.75 0.73333329 0.25 0.73333329 0.5 0.73333329 0.25 0.73333329 0 0.73333329 0.25
		 0.73333329 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 141 ".pt[3:143]" -type "float3"  7.4505806e-09 0 0 0 0 0 7.4505806e-09 
		0 0 -7.4505806e-09 0 0 7.4505806e-09 0 0 0 0 0 0 0 0 0 0 0 -7.4505806e-09 0 0 7.4505806e-09 
		0 0 -7.4505806e-09 0 0 -2.2351742e-08 0 0 -1.4901161e-08 0 0 0 0 0 0 0 0 7.4505806e-09 
		0 0 0 0 0 -7.4505806e-09 0 0 0 0 0 -7.4505806e-09 0 0 2.9802322e-08 0 0 -7.4505806e-09 
		0 0 -1.4901161e-08 0 0 0 0 0 -7.4505806e-09 0 0 0 0 0 0 0 0 1.4901161e-08 0 0 0 0 
		0 0 0 0 0 0 0 7.4505806e-09 0 0 0 0 0 -7.4505806e-09 0 0 0 0 0 -7.4505806e-09 0 0 
		2.9802322e-08 0 0 -7.4505806e-09 0 0 -1.4901161e-08 0 0 0 0 0 -7.4505806e-09 0 0 
		0 0 0 0 0 0 1.4901161e-08 0 0 0 0 0 2.9802322e-08 -6.9388939e-18 0 2.2351742e-08 
		-7.4505806e-09 0.0017236248 4.4703484e-08 -7.4505806e-09 3.7252903e-09 2.9802322e-08 
		-7.4505806e-09 -1.4528632e-07 1.5646219e-07 -7.4505806e-09 -1.527369e-07 3.7252903e-08 
		-7.4505806e-09 9.3132257e-08 3.7252903e-08 -7.4505806e-09 -2.2351742e-08 2.2351742e-08 
		-6.9388939e-18 -1.3411045e-07 1.4156103e-07 -6.9388939e-18 -1.1734664e-07 5.2154064e-08 
		0 -1.4901161e-08 5.9604645e-08 0 -1.3038516e-07 2.2351742e-08 0 -1.2665987e-07 1.4901161e-08 
		0 -1.4901161e-08 2.2351742e-08 0 -7.4505806e-09 3.7252903e-08 0 -1.3038516e-07 1.3411045e-07 
		0 -2.4108175e-09 -3.7252903e-09 0 0 -3.7252903e-09 0 0 9.0650786e-08 -6.9388939e-18 
		0 -3.7252903e-09 0 0 0 0 0 0 0 0 0 0 0 0 0 0 3.7252903e-08 -7.4505806e-09 -2.4108173e-09 
		3.7252903e-09 0 0 0 0 0 -3.7252903e-09 0 0 -3.7252903e-09 0 0 0 0 0 3.7252903e-09 
		0 0 0 0 0 7.4505806e-09 0 0 0 0 0 0 0 0 0 0 0 -7.4505806e-09 0 0 7.4505806e-09 0 
		0 0 0 0 0 0 0 0 0 0 1.4901161e-08 0 0 0 0 0 3.7252903e-09 -4.8572257e-17 -0.2364507 
		0 -4.8572257e-17 -0.2364507 3.7252903e-09 -4.8572257e-17 -0.2364507 -3.7252903e-09 
		-4.8572257e-17 -0.2364507 0 -4.8572257e-17 -0.23645072 -3.7252903e-09 -4.8572257e-17 
		-0.2364507 -3.7252903e-09 -4.8572257e-17 -0.23645073 3.7252903e-09 -0.0029366016 
		-0.23645069 0 0 0 0 0 0 7.4505806e-09 0 0 7.4505806e-09 0 0 0 0 0 -7.4505806e-09 
		0 0 0 0 0 3.7252903e-09 -4.8572257e-17 -0.2364507 0 -4.8572257e-17 -0.2364507 3.7252903e-09 
		-4.8572257e-17 -0.2364507 -3.7252903e-09 -4.8572257e-17 -0.2364507 0 -4.8572257e-17 
		-0.23645072 -3.7252903e-09 -4.8572257e-17 -0.2364507 -3.7252903e-09 -4.8572257e-17 
		-0.23645073 3.7252903e-09 -0.0029366016 -0.23645069 0 0 0 0 0 0 7.4505806e-09 0 0 
		7.4505806e-09 0 0 0 0 0 -7.4505806e-09 0 0 0 0 0 2.9802322e-08 -6.9388939e-18 0 2.9802322e-08 
		-7.4505806e-09 -0.0017236359 3.3527613e-08 -7.4505806e-09 3.7252903e-09 2.9802322e-08 
		-7.4505806e-09 1.3038516e-07 1.6391277e-07 -7.4505806e-09 1.3783574e-07 2.2351742e-08 
		-7.4505806e-09 -1.1920929e-07 2.9802322e-08 -7.4505806e-09 7.4505806e-09 3.7252903e-08 
		-6.9388939e-18 1.3411045e-07 1.4156103e-07 -6.9388939e-18 1.2479722e-07 3.7252903e-08 
		0 1.4901161e-08 3.7252903e-08 0 1.1920929e-07 2.9802322e-08 0 1.3038516e-07 2.2351742e-08 
		0 7.4505806e-09 2.9802322e-08 0 7.4505806e-09 2.9802322e-08 0 1.3038516e-07 0 0 0 
		0 -4.8572257e-17 -0.23645073 3.4771428e-08 -7.4505806e-09 0 0 -4.8572257e-17 -0.23645073 
		0 0 0 0 -5.5511151e-17 -0.23645073 -3.7252903e-09 0 0 0 -5.5511151e-17 -0.23645073;
	setAttr -s 144 ".vt[0:143]"  0 0.03535533 0.31464475 0.032889366 0.03535533 0.31292099
		 0.065418363 0.03535533 0.307769 0.097230554 0.03535533 0.29924482 0.12797761 0.03535533 0.28744215
		 0.15732241 0.03535533 0.27249032 0.18494356 0.03535533 0.2545529 0.21053839 0.03535533 0.23382658
		 0.23382664 0.03535533 0.21053833 0.25455284 0.03535533 0.1849435 0.27249026 0.03535533 0.15732235
		 0.28744221 0.03535533 0.12797755 0.29924488 0.03535533 0.097230613 0.30776894 0.03535533 0.065418303
		 0.31292105 0.03535533 0.032889307 0.31464469 0.03535533 -7.5649616e-08 0 0.03535533 0.37460828
		 0.0402807 0.03535533 0.37460828 0.080119967 0.03535533 0.37460828 0.11908138 0.03535533 0.37460828
		 0.15673816 0.03535533 0.37460828 0.19267774 0.03535533 0.37460828 0.2740916 0.03535533 0.37460828
		 0.56961167 0.03535533 0.37460828 0.56961167 0.03535533 0.30112225 0.56961167 0.03535533 0.24240094
		 0.56961167 0.03535533 0.19267768 0.56961167 0.03535533 0.1567381 0.56961167 0.03535533 0.11908132
		 0.56961167 0.03535533 0.080119908 0.56961167 0.03535533 0.042046666 0.56961167 0.03535533 -7.5649616e-08
		 0 -0.03535533 0.37460828 0.0402807 -0.03535533 0.37460828 0.080119967 -0.03535533 0.37460828
		 0.11908138 -0.03535533 0.37460828 0.15673816 -0.03535533 0.37460828 0.19267774 -0.03535533 0.37460828
		 0.2740916 -0.03535533 0.37460828 0.56961167 -0.03535533 0.37460828 0.56961167 -0.03535533 0.30112225
		 0.56961167 -0.03535533 0.24240094 0.56961167 -0.03535533 0.19267768 0.56961167 -0.03535533 0.1567381
		 0.56961167 -0.03535533 0.11908132 0.56961167 -0.03535533 0.080119908 0.56961167 -0.03535533 0.042046666
		 0.56961167 -0.03535533 -7.5649616e-08 0 -0.03535533 0.31464463 0.032889366 -0.03535533 0.31292099
		 0.065418363 -0.03535533 0.30776888 0.097230554 -0.03535533 0.29924482 0.12797749 -0.03535533 0.28744215
		 0.15732241 -0.03535533 0.2724902 0.18494356 -0.03535533 0.2545529 0.21053839 -0.03535533 0.23382658
		 0.23382652 -0.03535533 0.21053833 0.25455284 -0.03535533 0.1849435 0.27249026 -0.03535533 0.15732235
		 0.28744221 -0.03535533 0.12797755 0.29924488 -0.03535533 0.097230494 0.30776894 -0.03535533 0.065418303
		 0.31292105 -0.03535533 0.032889307 0.31464458 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 0.31464475
		 -0.51236534 0.03535533 0.37460828 -0.51236534 -0.03535533 0.31464463 -0.51236534 -0.03535533 0.37460828
		 -1.35788739 0.03535533 0.31464475 -1.35788739 0.03535533 0.37460828 -1.35788739 -0.03535533 0.31464463
		 -1.35788739 -0.03535533 0.37460828 -0.51236534 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 -7.5649616e-08
		 -1.35788739 -0.03535533 -7.5649616e-08 -1.35788739 0.03535533 -7.5649616e-08 -3.2154015e-19 0.03535533 -0.3146449
		 0.032889366 0.03535533 -0.31292114 0.065418363 0.03535533 -0.30776915 0.097230554 0.03535533 -0.29924497
		 0.12797761 0.03535533 -0.2874423 0.15732241 0.03535533 -0.27249047 0.18494356 0.03535533 -0.25455305
		 0.21053839 0.03535533 -0.23382673 0.23382664 0.03535533 -0.21053848 0.25455284 0.03535533 -0.18494365
		 0.27249026 0.03535533 -0.1573225 0.28744221 0.03535533 -0.1279777 0.29924488 0.03535533 -0.097230762
		 0.30776894 0.03535533 -0.065418452 0.31292105 0.03535533 -0.032889459 0 0.03535533 -0.37460843
		 0.0402807 0.03535533 -0.37460843 0.080119967 0.03535533 -0.37460843 0.11908138 0.03535533 -0.37460843
		 0.15673816 0.03535533 -0.37460843 0.19267774 0.03535533 -0.37460843 0.2740916 0.03535533 -0.37460843
		 0.56961167 0.03535533 -0.37460843 0.56961167 0.03535533 -0.3011224 0.56961167 0.03535533 -0.24240109
		 0.56961167 0.03535533 -0.19267783 0.56961167 0.03535533 -0.15673825 0.56961167 0.03535533 -0.11908147
		 0.56961167 0.03535533 -0.080120057 0.56961167 0.03535533 -0.042046819 0 -0.03535533 -0.37460843
		 0.0402807 -0.03535533 -0.37460843 0.080119967 -0.03535533 -0.37460843 0.11908138 -0.03535533 -0.37460843
		 0.15673816 -0.03535533 -0.37460843 0.19267774 -0.03535533 -0.37460843 0.2740916 -0.03535533 -0.37460843
		 0.56961167 -0.03535533 -0.37460843 0.56961167 -0.03535533 -0.3011224 0.56961167 -0.03535533 -0.24240109
		 0.56961167 -0.03535533 -0.19267783 0.56961167 -0.03535533 -0.15673825 0.56961167 -0.03535533 -0.11908147
		 0.56961167 -0.03535533 -0.080120057 0.56961167 -0.03535533 -0.042046819 -3.2154025e-19 -0.03535533 -0.31464478
		 0.032889366 -0.03535533 -0.31292114 0.065418363 -0.03535533 -0.30776903 0.097230554 -0.03535533 -0.29924497
		 0.12797749 -0.03535533 -0.2874423 0.15732241 -0.03535533 -0.27249035 0.18494356 -0.03535533 -0.25455305
		 0.21053839 -0.03535533 -0.23382673 0.23382652 -0.03535533 -0.21053848 0.25455284 -0.03535533 -0.18494365
		 0.27249026 -0.03535533 -0.1573225 0.28744221 -0.03535533 -0.1279777 0.29924488 -0.03535533 -0.097230643
		 0.30776894 -0.03535533 -0.065418452 0.31292105 -0.03535533 -0.032889459 -0.51236534 0.03535533 -0.3146449
		 -0.51236534 0.03535533 -0.37460843 -0.51236534 -0.03535533 -0.31464478 -0.51236534 -0.03535533 -0.37460843
		 -1.35788739 0.03535533 -0.3146449 -1.35788739 0.03535533 -0.37460843 -1.35788739 -0.03535533 -0.31464478
		 -1.35788739 -0.03535533 -0.37460843;
	setAttr -s 288 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 16 17 0 17 18 0 18 19 0 19 20 0 20 21 0
		 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0 32 33 0
		 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 42 0 42 43 0 43 44 0
		 44 45 0 45 46 0 46 47 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0
		 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 0 16 0 1 17 1 2 18 1 3 19 1
		 4 20 1 5 21 1 6 22 1 7 23 1 8 24 1 9 25 1 10 26 1 11 27 1 12 28 1 13 29 1 14 30 1
		 15 31 1 16 32 0 17 33 1 18 34 1 19 35 1 20 36 1 21 37 1 22 38 1 23 39 1 24 40 1 25 41 1
		 26 42 1 27 43 1 28 44 1 29 45 1 30 46 1 31 47 1 32 48 0 33 49 1 34 50 1 35 51 1 36 52 1
		 37 53 1 38 54 1 39 55 1 40 56 1 41 57 1 42 58 1 43 59 1 44 60 1 45 61 1 46 62 1 47 63 1
		 48 0 0 49 1 1 50 2 1 51 3 1 52 4 1 53 5 1 54 6 1 55 7 1 56 8 1 57 9 1 58 10 1 59 11 1
		 60 12 1 61 13 1 62 14 1 63 15 1 0 64 0 16 65 0 64 65 0 48 66 0 66 64 0 32 67 0 67 66 0
		 65 67 0 64 68 0 65 69 0 68 69 0 66 70 0 70 68 0 67 71 0 71 70 0 69 71 0 66 72 0 64 73 0
		 72 73 1 70 74 0 72 74 1 68 75 0 74 75 1 73 75 1 76 77 0 76 91 0 91 92 0 77 92 1 77 78 0
		 92 93 0 78 93 1 78 79 0 93 94 0 79 94 1 79 80 0 94 95 0 80 95 1 80 81 0 95 96 0 81 96 1
		 81 82 0 96 97 0;
	setAttr ".ed[166:287]" 82 97 1 82 83 0 97 98 0 83 98 1 83 84 0 98 99 0 84 99 1
		 84 85 0 99 100 0 85 100 1 85 86 0 100 101 0 86 101 1 86 87 0 101 102 0 87 102 1 87 88 0
		 102 103 0 88 103 1 88 89 0 103 104 0 89 104 1 89 90 0 104 105 0 90 105 1 90 15 0
		 105 31 0 91 106 0 106 107 0 92 107 1 107 108 0 93 108 1 108 109 0 94 109 1 109 110 0
		 95 110 1 110 111 0 96 111 1 111 112 0 97 112 1 112 113 0 98 113 1 113 114 0 99 114 1
		 114 115 0 100 115 1 115 116 0 101 116 1 116 117 0 102 117 1 117 118 0 103 118 1 118 119 0
		 104 119 1 119 120 0 105 120 1 120 47 0 106 121 0 121 122 0 107 122 1 122 123 0 108 123 1
		 123 124 0 109 124 1 124 125 0 110 125 1 125 126 0 111 126 1 126 127 0 112 127 1 127 128 0
		 113 128 1 128 129 0 114 129 1 129 130 0 115 130 1 130 131 0 116 131 1 131 132 0 117 132 1
		 132 133 0 118 133 1 133 134 0 119 134 1 134 135 0 120 135 1 135 63 0 121 76 0 122 77 1
		 123 78 1 124 79 1 125 80 1 126 81 1 127 82 1 128 83 1 129 84 1 130 85 1 131 86 1
		 132 87 1 133 88 1 134 89 1 135 90 1 140 141 0 142 140 0 143 142 0 141 143 0 76 136 0
		 136 137 0 91 137 0 121 138 0 138 136 0 106 139 0 139 138 0 137 139 0 136 140 0 137 141 0
		 139 143 0 138 142 0 138 72 0 136 73 0 142 74 0 140 75 0;
	setAttr -s 144 -ch 576 ".fc[0:143]" -type "polyFaces" 
		f 4 -1 60 15 -62
		mu 0 4 1 0 16 17
		f 4 -2 61 16 -63
		mu 0 4 2 1 17 18
		f 4 -3 62 17 -64
		mu 0 4 3 2 18 19
		f 4 -4 63 18 -65
		mu 0 4 4 3 19 20
		f 4 -5 64 19 -66
		mu 0 4 5 4 20 21
		f 4 -6 65 20 -67
		mu 0 4 6 5 21 22
		f 4 -7 66 21 -68
		mu 0 4 7 6 22 23
		f 4 -8 67 22 -69
		mu 0 4 8 7 23 24
		f 4 -9 68 23 -70
		mu 0 4 9 8 24 25
		f 4 -10 69 24 -71
		mu 0 4 10 9 25 26
		f 4 -11 70 25 -72
		mu 0 4 11 10 26 27
		f 4 -12 71 26 -73
		mu 0 4 12 11 27 28
		f 4 -13 72 27 -74
		mu 0 4 13 12 28 29
		f 4 -14 73 28 -75
		mu 0 4 14 13 29 30
		f 4 -15 74 29 -76
		mu 0 4 15 14 30 31
		f 4 -16 76 30 -78
		mu 0 4 17 16 32 33
		f 4 -17 77 31 -79
		mu 0 4 18 17 33 34
		f 4 -18 78 32 -80
		mu 0 4 19 18 34 35
		f 4 -19 79 33 -81
		mu 0 4 20 19 35 36
		f 4 -20 80 34 -82
		mu 0 4 21 20 36 37
		f 4 -21 81 35 -83
		mu 0 4 22 21 37 38
		f 4 -22 82 36 -84
		mu 0 4 23 22 38 39
		f 4 -23 83 37 -85
		mu 0 4 24 23 39 40
		f 4 -24 84 38 -86
		mu 0 4 25 24 40 41
		f 4 -25 85 39 -87
		mu 0 4 26 25 41 42
		f 4 -26 86 40 -88
		mu 0 4 27 26 42 43
		f 4 -27 87 41 -89
		mu 0 4 28 27 43 44
		f 4 -28 88 42 -90
		mu 0 4 29 28 44 45
		f 4 -29 89 43 -91
		mu 0 4 30 29 45 46
		f 4 -30 90 44 -92
		mu 0 4 31 30 46 47
		f 4 -31 92 45 -94
		mu 0 4 33 32 48 49
		f 4 -32 93 46 -95
		mu 0 4 34 33 49 50
		f 4 -33 94 47 -96
		mu 0 4 35 34 50 51
		f 4 -34 95 48 -97
		mu 0 4 36 35 51 52
		f 4 -35 96 49 -98
		mu 0 4 37 36 52 53
		f 4 -36 97 50 -99
		mu 0 4 38 37 53 54
		f 4 -37 98 51 -100
		mu 0 4 39 38 54 55
		f 4 -38 99 52 -101
		mu 0 4 40 39 55 56
		f 4 -39 100 53 -102
		mu 0 4 41 40 56 57
		f 4 -40 101 54 -103
		mu 0 4 42 41 57 58
		f 4 -41 102 55 -104
		mu 0 4 43 42 58 59
		f 4 -42 103 56 -105
		mu 0 4 44 43 59 60
		f 4 -43 104 57 -106
		mu 0 4 45 44 60 61
		f 4 -44 105 58 -107
		mu 0 4 46 45 61 62
		f 4 -45 106 59 -108
		mu 0 4 47 46 62 63
		f 4 -46 108 0 -110
		mu 0 4 49 48 64 65
		f 4 -47 109 1 -111
		mu 0 4 50 49 65 66
		f 4 -48 110 2 -112
		mu 0 4 51 50 66 67
		f 4 -49 111 3 -113
		mu 0 4 52 51 67 68
		f 4 -50 112 4 -114
		mu 0 4 53 52 68 69
		f 4 -51 113 5 -115
		mu 0 4 54 53 69 70
		f 4 -52 114 6 -116
		mu 0 4 55 54 70 71
		f 4 -53 115 7 -117
		mu 0 4 56 55 71 72
		f 4 -54 116 8 -118
		mu 0 4 57 56 72 73
		f 4 -55 117 9 -119
		mu 0 4 58 57 73 74
		f 4 -56 118 10 -120
		mu 0 4 59 58 74 75
		f 4 -57 119 11 -121
		mu 0 4 60 59 75 76
		f 4 -58 120 12 -122
		mu 0 4 61 60 76 77
		f 4 -59 121 13 -123
		mu 0 4 62 61 77 78
		f 4 -60 122 14 -124
		mu 0 4 63 62 78 79
		f 4 -135 -137 -139 -140
		mu 0 4 84 85 86 87
		f 4 -61 124 126 -126
		mu 0 4 16 64 81 80
		f 4 -109 127 128 -125
		mu 0 4 64 48 82 81
		f 4 -93 129 130 -128
		mu 0 4 48 32 83 82
		f 4 -77 125 131 -130
		mu 0 4 32 16 80 83
		f 4 -127 132 134 -134
		mu 0 4 80 81 85 84
		f 4 -131 137 138 -136
		mu 0 4 82 83 87 86
		f 4 -132 133 139 -138
		mu 0 4 83 80 84 87
		f 4 -129 140 142 -142
		mu 0 4 81 82 89 88
		f 4 135 143 -145 -141
		mu 0 4 82 86 90 89
		f 4 136 145 -147 -144
		mu 0 4 86 85 91 90
		f 4 -133 141 147 -146
		mu 0 4 85 81 88 91
		f 4 151 -151 -150 148
		mu 0 4 92 95 94 93
		f 4 154 -154 -152 152
		mu 0 4 96 97 95 92
		f 4 157 -157 -155 155
		mu 0 4 98 99 97 96
		f 4 160 -160 -158 158
		mu 0 4 100 101 99 98
		f 4 163 -163 -161 161
		mu 0 4 102 103 101 100
		f 4 166 -166 -164 164
		mu 0 4 104 105 103 102
		f 4 169 -169 -167 167
		mu 0 4 106 107 105 104
		f 4 172 -172 -170 170
		mu 0 4 108 109 107 106
		f 4 175 -175 -173 173
		mu 0 4 110 111 109 108
		f 4 178 -178 -176 176
		mu 0 4 112 113 111 110
		f 4 181 -181 -179 179
		mu 0 4 114 115 113 112
		f 4 184 -184 -182 182
		mu 0 4 116 117 115 114
		f 4 187 -187 -185 185
		mu 0 4 118 119 117 116
		f 4 190 -190 -188 188
		mu 0 4 120 121 119 118
		f 4 75 -193 -191 191
		mu 0 4 122 123 121 120
		f 4 195 -195 -194 150
		mu 0 4 95 125 124 94
		f 4 197 -197 -196 153
		mu 0 4 97 126 125 95
		f 4 199 -199 -198 156
		mu 0 4 99 127 126 97
		f 4 201 -201 -200 159
		mu 0 4 101 128 127 99
		f 4 203 -203 -202 162
		mu 0 4 103 129 128 101
		f 4 205 -205 -204 165
		mu 0 4 105 130 129 103
		f 4 207 -207 -206 168
		mu 0 4 107 131 130 105
		f 4 209 -209 -208 171
		mu 0 4 109 132 131 107
		f 4 211 -211 -210 174
		mu 0 4 111 133 132 109
		f 4 213 -213 -212 177
		mu 0 4 113 134 133 111
		f 4 215 -215 -214 180
		mu 0 4 115 135 134 113
		f 4 217 -217 -216 183
		mu 0 4 117 136 135 115
		f 4 219 -219 -218 186
		mu 0 4 119 137 136 117
		f 4 221 -221 -220 189
		mu 0 4 121 138 137 119
		f 4 91 -223 -222 192
		mu 0 4 123 139 138 121
		f 4 225 -225 -224 194
		mu 0 4 125 141 140 124
		f 4 227 -227 -226 196
		mu 0 4 126 142 141 125
		f 4 229 -229 -228 198
		mu 0 4 127 143 142 126
		f 4 231 -231 -230 200
		mu 0 4 128 144 143 127
		f 4 233 -233 -232 202
		mu 0 4 129 145 144 128
		f 4 235 -235 -234 204
		mu 0 4 130 146 145 129
		f 4 237 -237 -236 206
		mu 0 4 131 147 146 130
		f 4 239 -239 -238 208
		mu 0 4 132 148 147 131
		f 4 241 -241 -240 210
		mu 0 4 133 149 148 132
		f 4 243 -243 -242 212
		mu 0 4 134 150 149 133
		f 4 245 -245 -244 214
		mu 0 4 135 151 150 134
		f 4 247 -247 -246 216
		mu 0 4 136 152 151 135
		f 4 249 -249 -248 218
		mu 0 4 137 153 152 136
		f 4 251 -251 -250 220
		mu 0 4 138 154 153 137
		f 4 107 -253 -252 222
		mu 0 4 139 155 154 138
		f 4 254 -149 -254 224
		mu 0 4 141 157 156 140
		f 4 255 -153 -255 226
		mu 0 4 142 158 157 141
		f 4 256 -156 -256 228
		mu 0 4 143 159 158 142
		f 4 257 -159 -257 230
		mu 0 4 144 160 159 143
		f 4 258 -162 -258 232
		mu 0 4 145 161 160 144
		f 4 259 -165 -259 234
		mu 0 4 146 162 161 145
		f 4 260 -168 -260 236
		mu 0 4 147 163 162 146
		f 4 261 -171 -261 238
		mu 0 4 148 164 163 147
		f 4 262 -174 -262 240
		mu 0 4 149 165 164 148
		f 4 263 -177 -263 242
		mu 0 4 150 166 165 149
		f 4 264 -180 -264 244
		mu 0 4 151 167 166 150
		f 4 265 -183 -265 246
		mu 0 4 152 168 167 151
		f 4 266 -186 -266 248
		mu 0 4 153 169 168 152
		f 4 267 -189 -267 250
		mu 0 4 154 170 169 153
		f 4 123 -192 -268 252
		mu 0 4 155 171 170 154
		f 4 271 270 269 268
		mu 0 4 172 175 174 173
		f 4 274 -274 -273 149
		mu 0 4 94 177 176 156
		f 4 272 -277 -276 253
		mu 0 4 156 176 178 140
		f 4 275 -279 -278 223
		mu 0 4 140 178 179 124
		f 4 277 -280 -275 193
		mu 0 4 124 179 177 94
		f 4 281 -269 -281 273
		mu 0 4 177 172 173 176
		f 4 283 -271 -283 278
		mu 0 4 178 174 175 179
		f 4 282 -272 -282 279
		mu 0 4 179 175 172 177
		f 4 285 -143 -285 276
		mu 0 4 176 181 180 178
		f 4 284 144 -287 -284
		mu 0 4 178 180 182 174
		f 4 286 146 -288 -270
		mu 0 4 174 182 183 173
		f 4 287 -148 -286 280
		mu 0 4 173 183 181 176;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform14" -p "pTorus7";
	rename -uid "EE509D3D-C04E-447E-516C-B8AB72882585";
	setAttr ".v" no;
createNode mesh -n "pTorusShape7" -p "transform14";
	rename -uid "60C2756B-FC45-0DD4-F7E4-F5883A9FCBE5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 10 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.92499977350234985 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".pt";
	setAttr ".pt[23]" -type "float3" 0.052705795 0 0 ;
	setAttr ".pt[24]" -type "float3" 0.048222259 0 -0.00039944588 ;
	setAttr ".pt[25]" -type "float3" 0.045198679 0 -0.00032155181 ;
	setAttr ".pt[26]" -type "float3" 0.042638399 0 -0.00025559359 ;
	setAttr ".pt[27]" -type "float3" 0.040787835 0 -0.00020791695 ;
	setAttr ".pt[28]" -type "float3" 0.038848899 0 -0.00015796372 ;
	setAttr ".pt[29]" -type "float3" 0.036842752 0 -0.00010628485 ;
	setAttr ".pt[30]" -type "float3" 0.034882288 0 -5.5776287e-05 ;
	setAttr ".pt[31]" -type "float3" 0.032717276 0 2.3979494e-09 ;
	setAttr ".pt[39]" -type "float3" 0.052705795 0 0 ;
	setAttr ".pt[40]" -type "float3" 0.048222259 0 -0.00039944588 ;
	setAttr ".pt[41]" -type "float3" 0.045198679 0 -0.00032155181 ;
	setAttr ".pt[42]" -type "float3" 0.042638399 0 -0.00025559359 ;
	setAttr ".pt[43]" -type "float3" 0.040787835 0 -0.00020791695 ;
	setAttr ".pt[44]" -type "float3" 0.038848899 0 -0.00015796372 ;
	setAttr ".pt[45]" -type "float3" 0.036842752 0 -0.00010628485 ;
	setAttr ".pt[46]" -type "float3" 0.034882288 0 -5.5776287e-05 ;
	setAttr ".pt[47]" -type "float3" 0.032717276 0 2.3979494e-09 ;
	setAttr ".pt[99]" -type "float3" 0.017212318 0 0.00039944542 ;
	setAttr ".pt[100]" -type "float3" 0.020235918 0 0.00032154931 ;
	setAttr ".pt[101]" -type "float3" 0.022796221 0 0.00025558952 ;
	setAttr ".pt[102]" -type "float3" 0.024646748 0 0.00020791712 ;
	setAttr ".pt[103]" -type "float3" 0.02658575 0 0.00015796123 ;
	setAttr ".pt[104]" -type "float3" 0.028591868 0 0.00010628047 ;
	setAttr ".pt[105]" -type "float3" 0.030552309 0 5.577525e-05 ;
	setAttr ".pt[114]" -type "float3" 0.017212318 0 0.00039944542 ;
	setAttr ".pt[115]" -type "float3" 0.020235918 0 0.00032154931 ;
	setAttr ".pt[116]" -type "float3" 0.022796221 0 0.00025558952 ;
	setAttr ".pt[117]" -type "float3" 0.024646748 0 0.00020791712 ;
	setAttr ".pt[118]" -type "float3" 0.02658575 0 0.00015796123 ;
	setAttr ".pt[119]" -type "float3" 0.028591868 0 0.00010628047 ;
	setAttr ".pt[120]" -type "float3" 0.030552309 0 5.577525e-05 ;
	setAttr ".pt[144]" -type "float3" 3.586806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[145]" -type "float3" 3.469447e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[146]" -type "float3" 3.586806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[147]" -type "float3" 3.469447e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[148]" -type "float3" 3.469447e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[149]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[150]" -type "float3" 3.469447e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[151]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[152]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[153]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[154]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[155]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[156]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[157]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[158]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[159]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[160]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[161]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[162]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[163]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[164]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[165]" -type "float3" 3.8857806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[166]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[167]" -type "float3" 3.8857806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[168]" -type "float3" 3.8857806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[169]" -type "float3" 0.20560434 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[170]" -type "float3" 3.8857806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[171]" -type "float3" 0.20560434 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[172]" -type "float3" 3.586806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[173]" -type "float3" 3.586806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[174]" -type "float3" 3.3306691e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[175]" -type "float3" 3.3306691e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[176]" -type "float3" 3.3306691e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[177]" -type "float3" 3.3306691e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[178]" -type "float3" 2.220446e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[179]" -type "float3" 2.220446e-16 -1.1241008e-15 2.8846464 ;
createNode transform -n "pTorus8";
	rename -uid "BA350EDC-FE4B-15BF-2699-3EA3233DCD1F";
	setAttr ".rp" -type "double3" 2.8281902510811183 0.96374949688507627 -1.1043379863746037 ;
	setAttr ".sp" -type "double3" 2.8281902510811183 0.96374949688507627 -1.1043379863746037 ;
createNode transform -n "transform5" -p "pTorus8";
	rename -uid "A9EEB24F-6E4A-33B9-FDC8-A999D7B53D1A";
	setAttr ".v" no;
createNode mesh -n "pTorus8Shape" -p "transform5";
	rename -uid "836A1555-2D41-7BA0-6661-069C7D6EF1E4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 10 ".iog[0].og";
	setAttr -av ".iog[0].og[5].gid";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.7333332896232605 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pTorus9";
	rename -uid "1433FE10-3843-5786-FC73-68824455B374";
	setAttr ".rp" -type "double3" 2.8281902441824127 0.96374951240814477 -0.36366390431210294 ;
	setAttr ".sp" -type "double3" 2.8281902441824127 0.96374951240814477 -0.36366390431210294 ;
createNode transform -n "transform7" -p "pTorus9";
	rename -uid "9AAD588E-E541-D34D-26DB-E182FDA3255A";
	setAttr ".v" no;
createNode mesh -n "pTorus9Shape" -p "transform7";
	rename -uid "724E979F-DF42-B256-5D41-01A1C1669E36";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 10 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pTorus10";
	rename -uid "FE9ACA2B-A346-77D6-D32A-DDB29E31E9FD";
	setAttr ".t" -type "double3" 0.031323216042642876 0 0 ;
	setAttr ".rp" -type "double3" 2.8281902441824127 0.96374951240814477 0.37701019275534886 ;
	setAttr ".sp" -type "double3" 2.8281902441824127 0.96374951240814477 0.37701019275534886 ;
createNode mesh -n "pTorus10Shape" -p "pTorus10";
	rename -uid "708074E5-F349-5A61-F97D-E2B5B98BE287";
	setAttr -k off ".v";
	setAttr -s 10 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.79166656732559204 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "beam_4";
	rename -uid "8C7BD452-7848-25B9-D8D1-6BA3AEB9009F";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590074 -5.8908842803442214 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "beam_Shape4" -p "beam_4";
	rename -uid "7D351539-0247-537B-9A13-58AE2245E1F8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape12" -p "beam_4";
	rename -uid "7175DBB2-8941-2EC1-8BE5-5BA4F12C169F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "crossbeam_1";
	rename -uid "342D258D-5D4F-4E09-ED59-4B8ADA5DCE1D";
createNode transform -n "beam_7" -p "crossbeam_1";
	rename -uid "3A886835-BB44-FA7B-9A5D-66BEB5698D01";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590074 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "beam_7Shape" -p "beam_7";
	rename -uid "F35CAF4D-974D-324C-2DFE-5982482876B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "support_1" -p "crossbeam_1";
	rename -uid "570F8427-A446-C77B-8074-18B3CAA59FED";
	setAttr ".t" -type "double3" 1.5117128267902782 1.2049841028367543 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -2.341959037613659 0.52240791879256165 5.7707669087464355 ;
	setAttr ".sp" -type "double3" -0.54627722501754761 2.6910045146942139 31.939294815063477 ;
	setAttr ".spt" -type "double3" -1.7956818125961114 -2.1685965959016524 -26.168527906317042 ;
createNode mesh -n "support_Shape1" -p "support_1";
	rename -uid "B2518956-6E4E-1A45-8390-B9A3436245BC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape8" -p "support_1";
	rename -uid "52CA2917-DF47-D276-83FB-F1B48CD101AA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 2.684999 31.770536 
		-0.9695735 3.7968943 31.786789 -0.091733545 2.339 31.770536 -0.96957374 3.4508946 
		31.786789 -0.091733545 2.339 32.091801 -0.96957374 3.4508946 32.108051 -0.091733545 
		2.684999 32.091801 -0.9695735 3.7968943 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "vertical_1" -p "crossbeam_1";
	rename -uid "D755020A-484B-9BC8-A720-5F808D483F2F";
	setAttr ".t" -type "double3" -0.46340017732298922 0.76944171692621266 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 0.13512841007918389 3.1171321414407878 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840964 0.29397593623357654 5.7707669087464355 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.4911302328109741 31.939294815063477 ;
	setAttr ".spt" -type "double3" -0.46133351737311667 -2.1971542965773976 -26.168527906317042 ;
createNode mesh -n "vertical_Shape1" -p "vertical_1";
	rename -uid "BEF44E57-544C-46CB-6EB0-6DA6BC03BFA5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5000000522704795 0.41959086304996163 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape15" -p "vertical_1";
	rename -uid "A221F0F5-2D40-77C8-E072-93886C316501";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 2.6432605 31.770536 
		-0.18895763 2.6432607 31.786789 -0.091733545 2.339 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 2.339 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		2.6432605 32.091801 -0.18895763 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "crossbeam_2";
	rename -uid "3E485399-8D44-F039-2D7A-83B2D36F49D4";
	setAttr ".t" -type "double3" 0 0 -1.3858250209387617 ;
createNode transform -n "beam_8" -p "crossbeam_2";
	rename -uid "805C643A-834D-9F03-D0A9-92838AAE1D8A";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590074 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "beam_Shape8" -p "beam_8";
	rename -uid "969001A0-9545-97AC-AB52-3FB7F8F77906";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape16" -p "beam_8";
	rename -uid "DAF3BD5E-9744-7831-4B0B-FFB0CA4893F8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "support_2" -p "crossbeam_2";
	rename -uid "1334BE82-BC47-FBFA-B412-BD95C2F34F56";
	setAttr ".t" -type "double3" 1.5117128267902782 1.2049841028367543 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -2.341959037613659 0.52240791879256165 5.7707669087464355 ;
	setAttr ".sp" -type "double3" -0.54627722501754761 2.6910045146942139 31.939294815063477 ;
	setAttr ".spt" -type "double3" -1.7956818125961114 -2.1685965959016524 -26.168527906317042 ;
createNode mesh -n "support_Shape2" -p "support_2";
	rename -uid "38657C3D-B340-8728-BD69-0CA05DF4D4EA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape8" -p "support_2";
	rename -uid "65109C96-7148-062C-9708-7D988259B885";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 2.684999 31.770536 
		-0.9695735 3.7968943 31.786789 -0.091733545 2.339 31.770536 -0.96957374 3.4508946 
		31.786789 -0.091733545 2.339 32.091801 -0.96957374 3.4508946 32.108051 -0.091733545 
		2.684999 32.091801 -0.9695735 3.7968943 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape17" -p "support_2";
	rename -uid "1CE71394-C245-18BB-8EFC-52AF56F60FCC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 18 ".uvst[0].uvsp[0:17]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[8:11]" -type "float3"  0.052375901 0.49401012 0 
		0.052375901 0.49401012 0 0.052375901 -0.046530426 0 0.052375901 -0.046530426 0;
	setAttr -s 12 ".vt[0:11]"  -0.59173357 2.18499851 32.27053833 -0.46957353 3.29689407 32.28678894
		 -0.59173357 2.83899975 32.27053833 -0.46957374 3.95089436 32.28678894 -0.59173357 2.83899975 31.59179878
		 -0.46957374 3.95089436 31.60804939 -0.59173357 2.18499851 31.59179878 -0.46957353 3.29689407 31.60804939
		 -0.46957353 3.29689407 31.60804939 -0.46957353 3.29689407 32.28678894 -0.46957374 3.95089436 31.60804939
		 -0.46957374 3.95089436 32.28678894;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -15 -17 -19 -20
		mu 0 4 14 15 16 17
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "vertical_2" -p "crossbeam_2";
	rename -uid "5A7B5A52-534E-D396-C358-BEA18151BE89";
	setAttr ".t" -type "double3" -0.46340017732298922 0.76944171692621266 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 0.13512841007918389 3.1171321414407878 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840964 0.29397593623357654 5.7707669087464355 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.4911302328109741 31.939294815063477 ;
	setAttr ".spt" -type "double3" -0.46133351737311667 -2.1971542965773976 -26.168527906317042 ;
createNode mesh -n "vertical_Shape2" -p "vertical_2";
	rename -uid "080CEA05-934D-7A38-BF3F-0EABB6AEE2FE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape18" -p "vertical_2";
	rename -uid "B32BC146-2840-7C99-C801-D387941A61AF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 2.6432605 31.770536 
		-0.18895763 2.6432607 31.786789 -0.091733545 2.339 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 2.339 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		2.6432605 32.091801 -0.18895763 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "crossbeam_3";
	rename -uid "42DDC9E8-124B-29D4-00DD-6AB426D0328D";
	setAttr ".t" -type "double3" 0 0 -2.7716500418775234 ;
createNode transform -n "beam_9" -p "crossbeam_3";
	rename -uid "29436657-F74F-56D9-C072-C19FDB677B9B";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590074 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "beam_Shape9" -p "beam_9";
	rename -uid "6DFCB259-0C44-ACF8-015E-62A3D0F7F2BD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999985098838806 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape19" -p "beam_9";
	rename -uid "E62F4472-2A41-D837-4376-268CCFB5D2FA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999985098838806 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "support_3" -p "crossbeam_3";
	rename -uid "F8B233F3-B94E-D79F-5FD9-60BB5BD7F4B5";
	setAttr ".rp" -type "double3" -2.341959037613659 0.52240791879256165 5.7707669087464355 ;
	setAttr ".sp" -type "double3" -0.54627722501754761 2.6910045146942139 31.939294815063477 ;
	setAttr ".spt" -type "double3" -1.7956818125961114 -2.1685965959016524 -26.168527906317042 ;
createNode mesh -n "support_Shape3" -p "support_3";
	rename -uid "C26AC793-6847-9E59-2DE5-E0BCA27B1C0F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape8" -p "support_3";
	rename -uid "D81DD632-7A4D-1853-5925-C2B7078D41D5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 2.684999 31.770536 
		-0.9695735 3.7968943 31.786789 -0.091733545 2.339 31.770536 -0.96957374 3.4508946 
		31.786789 -0.091733545 2.339 32.091801 -0.96957374 3.4508946 32.108051 -0.091733545 
		2.684999 32.091801 -0.9695735 3.7968943 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape20" -p "support_3";
	rename -uid "9F5C7144-E547-869E-6775-B5AD0AB51D95";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 18 ".uvst[0].uvsp[0:17]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[8:11]" -type "float3"  0.052375901 0.49401012 0 
		0.052375901 0.49401012 0 0.052375901 -0.046530426 0 0.052375901 -0.046530426 0;
	setAttr -s 12 ".vt[0:11]"  -0.59173357 2.18499851 32.27053833 -0.46957353 3.29689407 32.28678894
		 -0.59173357 2.83899975 32.27053833 -0.46957374 3.95089436 32.28678894 -0.59173357 2.83899975 31.59179878
		 -0.46957374 3.95089436 31.60804939 -0.59173357 2.18499851 31.59179878 -0.46957353 3.29689407 31.60804939
		 -0.46957353 3.29689407 31.60804939 -0.46957353 3.29689407 32.28678894 -0.46957374 3.95089436 31.60804939
		 -0.46957374 3.95089436 32.28678894;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -15 -17 -19 -20
		mu 0 4 14 15 16 17
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "vertical_3" -p "crossbeam_3";
	rename -uid "C87A2D36-A943-34DA-2557-4E93F87D2902";
	setAttr ".t" -type "double3" -0.46340017732298922 0.76944171692621266 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 0.13512841007918389 3.1171321414407878 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840964 0.29397593623357654 5.7707669087464355 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.4911302328109741 31.939294815063477 ;
	setAttr ".spt" -type "double3" -0.46133351737311667 -2.1971542965773976 -26.168527906317042 ;
createNode mesh -n "vertical_Shape3" -p "vertical_3";
	rename -uid "AC14F227-7D44-307D-2FF5-80BCF0E46A1E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape21" -p "vertical_3";
	rename -uid "FE2BCD38-6A4E-17D7-CD1C-E29B52464E8A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 2.6432605 31.770536 
		-0.18895763 2.6432607 31.786789 -0.091733545 2.339 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 2.339 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		2.6432605 32.091801 -0.18895763 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "beam_5";
	rename -uid "3D5B22A2-8A46-8737-FB9E-399BFB4F3C6B";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590074 -7.2814177312961057 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "beam_Shape5" -p "beam_5";
	rename -uid "9F3A86B3-7E44-301A-24D0-B9ABCE156A2C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape13" -p "beam_5";
	rename -uid "151C5137-3A40-A787-F6C2-2082EACCE071";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "beam_6";
	rename -uid "F27C7053-8649-E6F0-7816-18B6EA634100";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590076 -8.6719511822479944 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "beam_Shape6" -p "beam_6";
	rename -uid "76B18A46-6742-F871-1128-FA8A796879A6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape14" -p "beam_6";
	rename -uid "FDA043B6-4843-28CA-9A54-8986C28022DC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "beam_3";
	rename -uid "BEC8D5BC-EF4F-53EB-C067-25A6697564D5";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590076 -4.6232519705704798 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "beam_Shape3" -p "beam_3";
	rename -uid "3BD0F464-6A4B-FEDC-46FD-8A86E9B11C5A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape11" -p "beam_3";
	rename -uid "6E24EA2D-8247-AFE1-1DD2-2B90C031150D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "beam_2";
	rename -uid "BB394927-3C42-371F-3FC7-62A3FB605A80";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590076 -3.2394831485831355 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "beam_Shape2" -p "beam_2";
	rename -uid "6B019239-ED4F-49E7-1E0B-67B840FE90CC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape10" -p "beam_2";
	rename -uid "0CA98B20-4D42-44D8-486D-D9BCD3FAA442";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "beam_1";
	rename -uid "92354BAD-9945-2CD0-AD47-BBA43AD9330B";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590076 -2.6812261722653337 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "beam_Shape1" -p "beam_1";
	rename -uid "DF538F06-D34A-DB95-3AF7-4FADCF11148B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape9" -p "beam_1";
	rename -uid "A2D29833-7341-6AEC-30E1-148B0EFFB9C2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube12";
	rename -uid "FA5635A3-5549-8FA5-549F-C3BDF3986DAB";
	setAttr ".t" -type "double3" 1 0.97203420014792541 -4.8383535755986182 ;
	setAttr ".s" -type "double3" 1.963645924412623 1.7453586463521469 1 ;
createNode transform -n "transform11" -p "pCube12";
	rename -uid "7D290D6D-C848-884A-0691-C69DDC29E316";
	setAttr ".v" no;
createNode mesh -n "pCubeShape12" -p "transform11";
	rename -uid "BCF9885A-524F-24C6-9A50-E3B66D9C438D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".ciog[0].cog";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface6";
	rename -uid "09185C59-C445-AE9A-AFCD-638B28E15E73";
	setAttr ".t" -type "double3" 0 0 1.1073583521137715 ;
	setAttr ".rp" -type "double3" 0.9100339412689209 1.066551685333252 -5.0760374069213867 ;
	setAttr ".sp" -type "double3" 0.9100339412689209 1.066551685333252 -5.0760374069213867 ;
createNode mesh -n "polySurface6Shape" -p "|polySurface6";
	rename -uid "EAFB5391-CF4E-17E0-8150-299615AFDAA8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube13";
	rename -uid "7DF5F1A6-B747-2BF6-8814-02B3E38CEFC8";
	setAttr ".t" -type "double3" 1.0084052820347926 1.4309032019969368 -3.886400835139725 ;
	setAttr ".r" -type "double3" 4.9266084587331997 -5.6903867138305806 -1.7475347212643246 ;
	setAttr ".s" -type "double3" 1.9217587176736648 0.069082615773987305 0.28140965139401375 ;
createNode mesh -n "pCubeShape13" -p "pCube13";
	rename -uid "D74EBD53-6C4F-E045-9D1F-E581DE184DA1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube14";
	rename -uid "1BC08223-8749-F55B-367A-4EA46691C5BC";
	setAttr ".t" -type "double3" 1.0084052820347926 0.95485656076918235 -3.886400835139725 ;
	setAttr ".r" -type "double3" 0 0 0.79557885289769759 ;
	setAttr ".s" -type "double3" 1.9217587176736648 0.069082615773987305 0.28140965139401375 ;
createNode mesh -n "pCubeShape14" -p "pCube14";
	rename -uid "3133AFEC-F842-8F9D-6715-4F9E04B4917C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube15";
	rename -uid "EA25C94D-6340-E3EA-1C00-88AB8A4A0CF3";
	setAttr ".t" -type "double3" 1.0084052820347926 0.44919522348530827 -3.886400835139725 ;
	setAttr ".r" -type "double3" 0 0 1.1495696060504879 ;
	setAttr ".s" -type "double3" 1.9217587176736648 0.069082615773987305 0.28140965139401375 ;
createNode mesh -n "pCubeShape15" -p "pCube15";
	rename -uid "7374F50F-444A-D90B-8045-D18D37B87C1F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder1";
	rename -uid "FDBF4AA5-B14C-C1F6-E360-3999CD366012";
	setAttr ".t" -type "double3" 0.6475261419110796 1.3515995436811981 5.7234510386978865 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.58643527331698797 0.088488097954319833 0.58643527331698797 ;
	setAttr ".rp" -type "double3" 0 -3.0011538669354304e-16 1.3515995436811981 ;
	setAttr ".rpt" -type "double3" 0 -1.3515995436811978 -1.3515995436811981 ;
	setAttr ".sp" -type "double3" 0 -3.2357138593114948e-15 2.1988452161399024 ;
	setAttr ".spt" -type "double3" 0 2.9355984726179506e-15 -0.84724567245870297 ;
createNode transform -n "transform13" -p "pCylinder1";
	rename -uid "F66AF80D-2A4E-6795-BE9E-559C61A59EB1";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape1" -p "transform13";
	rename -uid "B2E980A4-C44E-6219-464F-B2B02D7D3AC2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999997019767761 0.49687500298023224 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt";
	setAttr ".pt[54]" -type "float3" 0 -2.6645353e-15 2.1988454 ;
	setAttr ".pt[55]" -type "float3" 0 -3.4416914e-15 2.1988454 ;
	setAttr ".pt[56]" -type "float3" 0 -2.6645353e-15 2.1988454 ;
	setAttr ".pt[57]" -type "float3" 0 -2.6645353e-15 2.1988454 ;
	setAttr ".pt[58]" -type "float3" 0 -3.4416914e-15 2.1988454 ;
	setAttr ".pt[59]" -type "float3" 0 -3.4416914e-15 2.1988454 ;
createNode transform -n "pTorus11";
	rename -uid "4251B943-184D-95C6-E4E1-3CB7E6143423";
	setAttr ".t" -type "double3" 0 0 -2.1230010641746091 ;
	setAttr ".rp" -type "double3" 0.91003382205963135 1.066551685333252 5.7063887119293213 ;
	setAttr ".sp" -type "double3" 0.91003382205963135 1.066551685333252 5.7063887119293213 ;
createNode mesh -n "pTorus11Shape" -p "pTorus11";
	rename -uid "8D77249F-FD4A-93F0-BF57-CC94CD2C4C26";
	setAttr -k off ".v";
	setAttr -s 12 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "DC0FFF83-DE43-1116-4D6A-809C56506ECF";
	setAttr -s 5 ".lnk";
	setAttr -s 5 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "04A3942F-914D-60CE-C08E-B8BB0E806463";
createNode displayLayer -n "defaultLayer";
	rename -uid "9A45B3DE-A743-876B-5539-A1A5575971B9";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "8836811B-1747-FC60-0768-F68221C8D521";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "C777EBB5-7B4B-CA47-47D1-D995DFA8A922";
	setAttr ".g" yes;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "7B501789-3542-5702-4F4A-2CBD0283FD53";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "BB1E6CAC-A44C-B8E8-85B6-A5A0B17E2DEA";
createNode polyCube -n "polyCube1";
	rename -uid "3D47C0EB-D348-A583-7811-019266459F08";
	setAttr ".cuv" 4;
createNode polyChipOff -n "polyChipOff1";
	rename -uid "982E4B51-9641-352C-8AF5-CB8A5045A441";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 2.0502456776410818 0 0 0 0 2.0502456776410818 0 0 0 0 5.8539501523725681 0
		 0 1.0251228388205409 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.0251229 0 ;
	setAttr ".rs" 349826676;
	setAttr ".lt" -type "double3" 0 0 1.2892211252127423 ;
	setAttr ".kft" no;
	setAttr ".dup" no;
createNode polyTweak -n "polyTweak1";
	rename -uid "0E44A603-B24C-484F-5D51-2097A1A0459C";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[3]" -type "float3" 0 -0.10028286 0 ;
	setAttr ".tk[5]" -type "float3" 0 -0.10028286 0 ;
createNode polySeparate -n "polySeparate1";
	rename -uid "16946972-C54A-264E-3648-D6853FDB193C";
	setAttr ".ic" 2;
	setAttr -s 2 ".out";
createNode groupId -n "groupId1";
	rename -uid "6E923FD1-9A4C-72F8-61E7-E9868BCB7F3C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "A322674D-0048-54C1-B89B-9D9F988E7398";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId2";
	rename -uid "A8664A45-C946-99C1-7D32-07B8B72E8CAE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "3E712400-C24A-A2B2-2759-7BB11765FD62";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "D9F39E81-EA4E-F18E-2DF3-FA998A9E51A4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0]";
createNode groupId -n "groupId4";
	rename -uid "E51F7B3E-384D-204A-262A-3E925586DB0D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "3FB0CA36-AF48-AE3E-7C78-75B73AC3EDE5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:4]";
createNode polyNormal -n "polyNormal1";
	rename -uid "2AB33DA0-AB4E-1524-8B43-EB86DEDF8E8C";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".unm" no;
createNode polyNormal -n "polyNormal2";
	rename -uid "2BEC2326-B040-88AA-AC67-C29BE8BA1BAC";
	setAttr ".ics" -type "componentList" 1 "f[0:4]";
createNode polyTorus -n "polyTorus1";
	rename -uid "CA175CBB-FF41-6EC1-5F3B-D5A84EEDFCCE";
	setAttr ".sr" 0.1;
	setAttr ".tw" 45;
	setAttr ".sa" 50;
	setAttr ".sh" 4;
createNode objectSet -n "set1";
	rename -uid "C7D4B60C-6A44-AB29-DC08-5EB0453733E3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "AB940A88-2846-6B08-6B08-DF93465FE161";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "2FD2F3BE-024A-612C-E9C1-4DA541922147";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[24:48]" "e[74:98]" "e[124:148]" "e[174:198]" "e[224:249]" "e[274:299]" "e[324:349]" "e[374:399]";
createNode deleteComponent -n "deleteComponent1";
	rename -uid "5D956465-3043-F8FE-6EAA-BB885C4C6ADF";
	setAttr ".dc" -type "componentList" 4 "f[24:48]" "f[74:98]" "f[124:148]" "f[174:198]";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "20514BB4-4C49-DC07-E5A5-6C8AEA64FB87";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 562\n            -height 622\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 315\n            -height 240\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 316\n            -height 240\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 627\n            -height 0\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n"
		+ "            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n"
		+ "            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n"
		+ "            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n"
		+ "                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n"
		+ "                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n"
		+ "                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n"
		+ "                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -highlightConnections 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n"
		+ "                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n"
		+ "                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"persp\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 562\\n    -height 622\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"persp\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 562\\n    -height 622\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "3ED733B4-2540-872F-4C59-A4B74EDBC17B";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCloseBorder -n "polyCloseBorder1";
	rename -uid "5C1A7D5E-594B-9F1F-B0BC-AEAC5001C275";
	setAttr ".ics" -type "componentList" 1 "e[*]";
createNode polyTweak -n "polyTweak2";
	rename -uid "D3EE625D-D445-2A1C-D0BF-E69A7C909E23";
	setAttr ".uopa" yes;
	setAttr -s 52 ".tk[26:77]" -type "float3"  0.09099222 0 0 0.1161877 0
		 0 0.15773851 0 0 0.21498932 0 0 0.28703716 0 0 0.37274602 2.220446e-16 -0.67477077
		 7.4505806e-09 1.2490009e-16 -0.58272582 0 1.110223e-16 -0.50369155 1.4901161e-08
		 9.7144515e-17 -0.43891448 0 8.3266727e-17 -0.38941613 -8.9406967e-08 8.3266727e-17
		 -0.35597703 -2.9802322e-08 6.9388939e-17 -0.33912468 0 6.9388939e-17 -0.3391248 0
		 8.3266727e-17 -0.35597715 0 9.7144515e-17 -0.38941631 0 9.7144515e-17 -0.43891481
		 0 1.110223e-16 -0.50369191 -0.84723085 5.5511151e-17 -0.58272612 -0.74921274 -6.9388939e-17
		 0 -0.66350412 -6.9388939e-17 0 -0.59145641 -6.9388939e-17 0 -0.53420562 -6.9388939e-17
		 0 -0.49265486 -6.9388939e-17 0 -0.46745956 -6.9388939e-17 0 -0.4590168 -6.9388939e-17
		 0 0.082550414 0 0 0.09099222 0 0 0.1161877 0 0 0.15773851 0 0 0.21498932 0 0 0.28703716
		 0 0 0.37274602 2.220446e-16 -0.67477077 7.4505806e-09 1.2490009e-16 -0.58272582 0
		 1.110223e-16 -0.50369155 1.4901161e-08 9.7144515e-17 -0.43891448 0 8.3266727e-17
		 -0.38941613 -2.9802322e-08 8.3266727e-17 -0.35597703 -2.9802322e-08 6.9388939e-17
		 -0.33912468 0 6.9388939e-17 -0.3391248 0 8.3266727e-17 -0.35597715 0 9.7144515e-17
		 -0.38941631 0 9.7144515e-17 -0.43891481 0 1.110223e-16 -0.50369191 -0.84723085 5.5511151e-17
		 -0.58272612 -0.74921274 -6.9388939e-17 0 -0.66350412 -6.9388939e-17 0 -0.59145641
		 -6.9388939e-17 0 -0.53420562 -6.9388939e-17 0 -0.49265486 -6.9388939e-17 0 -0.46745956
		 -6.9388939e-17 0 -0.4590168 -6.9388939e-17 0 0.082550414 0 0;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "D2CF4832-2A4F-533B-87EE-3C8D9881EE7B";
	setAttr ".ics" -type "componentList" 1 "f[100:101]";
	setAttr ".ix" -type "matrix" 1.5152858838336571 0 0 0 0 3.3646105542432129e-16 1.5152858838336571 0
		 0 -1.5152858838336571 3.3646105542432129e-16 0 1.2928521643586794 0.47063593114229862 0.93431024359174064 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.0076246 0.47063586 0.93431026 ;
	setAttr ".rs" 1897297934;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.0251231634875992 0.47063577399743589 0.82716333497236327 ;
	setAttr ".cbx" -type "double3" 3.0403723772601738 0.47063593114229862 1.0414571409213584 ;
createNode polyTorus -n "polyTorus2";
	rename -uid "A7B757E4-7846-E4DC-1E1F-ECB06A8002E7";
	setAttr ".r" 0.5;
	setAttr ".sr" 0.1;
	setAttr ".tw" 45;
	setAttr ".sa" 60;
	setAttr ".sh" 4;
createNode objectSet -n "set2";
	rename -uid "0E6CA0E7-BA42-5550-518C-5C89FF17863B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6";
	rename -uid "43DEF63B-3F49-30D6-A60B-54AAFBC77241";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "5F1D5765-5C40-D729-A89C-1B986844CDC3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[29:58]" "e[89:118]" "e[149:178]" "e[209:238]" "e[269:299]" "e[329:359]" "e[389:419]" "e[449:479]";
createNode deleteComponent -n "deleteComponent2";
	rename -uid "E707DA4B-8841-4958-B977-1385B7E7AE8F";
	setAttr ".dc" -type "componentList" 4 "f[29:58]" "f[89:118]" "f[149:178]" "f[209:238]";
createNode polySplitEdge -n "polySplitEdge1";
	rename -uid "FED6FC96-3A44-ED8A-6206-ABA7674B2C19";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "e[134]" "e[150]" "e[165]" "e[181]" "e[196]" "e[212]" "e[227]" "e[243]";
createNode polyMergeVert -n "polyMergeVert1";
	rename -uid "D90FF8C0-BE4D-9541-4E77-DEAE7C9CFEA6";
	setAttr ".ics" -type "componentList" 2 "vtx[45]" "vtx[125]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 -3.7583501234906547 0 5.1188395462216825 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak3";
	rename -uid "C45E9E0E-4947-CC6F-36EB-56B3EEA312A1";
	setAttr ".uopa" yes;
	setAttr -s 128 ".tk[0:127]" -type "float3"  0.6020183 0 0 0.6020183 0
		 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.30100915 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.6020183 0 0 0.6020183 0 1.5832484e-08 0.30100915
		 0 1.5832484e-08 0.6020183 0 1.5832484e-08 0.6020183 0 1.5832484e-08;
createNode polyMergeVert -n "polyMergeVert2";
	rename -uid "81BABF8C-CB4A-B358-DC43-5FB24BC999B8";
	setAttr ".ics" -type "componentList" 2 "vtx[14]" "vtx[124]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 -3.7583501234906547 0 5.1188395462216825 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak4";
	rename -uid "E35A8AF0-CC43-29AC-CDD2-64BA82FC47ED";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[14]" -type "float3" 0.30100912 0 2.9802322e-08 ;
	setAttr ".tk[124]" -type "float3" -0.30100915 0 0 ;
createNode polyMergeVert -n "polyMergeVert3";
	rename -uid "915B7F3B-1A4F-1C34-D232-2E8A3967CD6D";
	setAttr ".ics" -type "componentList" 2 "vtx[107]" "vtx[124]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 -3.7583501234906547 0 5.1188395462216825 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak5";
	rename -uid "F57E36AB-B847-6E92-DB51-B289934CC23F";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[107]" -type "float3" 0.30100912 0 0 ;
	setAttr ".tk[124]" -type "float3" -0.30100915 0 -2.9802322e-08 ;
createNode polyMergeVert -n "polyMergeVert4";
	rename -uid "C424AA88-A449-2BD5-2A03-5EA2CA25CD8C";
	setAttr ".ics" -type "componentList" 2 "vtx[76]" "vtx[124]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 -3.7583501234906547 0 5.1188395462216825 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak6";
	rename -uid "F8CB25B8-BC4E-D55F-F081-76B234BFCFDB";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[76]" -type "float3" 0.30100915 0 0 ;
	setAttr ".tk[124]" -type "float3" -0.30100915 0 0 ;
createNode polyCloseBorder -n "polyCloseBorder2";
	rename -uid "9564B322-FA4B-EEC4-F594-C0A2A34B935C";
	setAttr ".ics" -type "componentList" 1 "e[*]";
createNode polyTweak -n "polyTweak7";
	rename -uid "D0759381-6A49-6B19-BE14-5B915DEE7B39";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[14]" -type "float3" 0 0 -0.0036479675 ;
	setAttr ".tk[45]" -type "float3" 0 0 -0.0036479675 ;
	setAttr ".tk[76]" -type "float3" 0 0 -0.0036479675 ;
	setAttr ".tk[107]" -type "float3" 0 0 -0.0036479675 ;
createNode rmanGlobals -s -n "rmanGlobals";
	rename -uid "58AAA6E0-484A-1207-DA4D-209DB397B41D";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".hider_minSamples" 0;
	setAttr ".hider_maxSamples" 128;
	setAttr ".ri_pixelVariance" 0.0099999997764825821;
	setAttr ".hider_darkfalloff" 0.02500000037252903;
	setAttr ".hider_incremental" yes;
	setAttr ".ipr_hider_maxSamples" 64;
	setAttr ".ipr_ri_pixelVariance" 0.05000000074505806;
	setAttr ".ri_maxSpecularDepth" 4;
	setAttr ".ri_maxDiffuseDepth" 1;
	setAttr ".ri_displayFilter" -type "string" "gaussian";
	setAttr ".ri_displayFilterSize" -type "float2" 2 2 ;
	setAttr ".motionBlur" 0;
	setAttr ".cameraBlur" no;
	setAttr ".shutterAngle" 180;
	setAttr ".shutterOpenEnd" 0;
	setAttr ".shutterCloseStart" 1;
	setAttr ".shutterTiming" 0;
	setAttr ".motionSamples" 2;
	setAttr ".displayFilters[0]" -type "string" "";
	setAttr ".sampleFilters[0]" -type "string" "";
	setAttr ".outputAllShaders" no;
	setAttr ".nestedInstancing" no;
	setAttr ".reentrantProcedurals" yes;
	setAttr ".outputShadowAOV" 0;
	setAttr ".enableImagePlaneFilter" yes;
	setAttr ".learnLightSelection" no;
	setAttr ".ri_hider_adaptAll" no;
	setAttr ".osl_batched" 0;
	setAttr ".adaptiveSampler" 0;
	setAttr ".opt_bucket_order" -type "string" "circle";
	setAttr ".limits_bucketsize" -type "long2" 16 16 ;
	setAttr ".limits_othreshold" -type "float3" 0.99599999 0.99599999 0.99599999 ;
	setAttr ".rfm_referenceFrame" 0;
	setAttr ".dice_micropolygonlength" 1;
	setAttr ".dice_watertight" no;
	setAttr ".dice_referenceCameraType" 0;
	setAttr ".dice_referenceCamera" -type "string" "";
	setAttr ".hair_minWidth" 0.5;
	setAttr ".trace_autobias" yes;
	setAttr ".trace_bias" 0.0010000000474974513;
	setAttr ".trace_worldorigin" -type "string" "camera";
	setAttr ".trace_worldoffset" -type "float3" 0 0 0 ;
	setAttr ".opt_cropWindowEnable" no;
	setAttr ".opt_cropWindowTopLeft" -type "float2" 0 0 ;
	setAttr ".opt_cropWindowBottomRight" -type "float2" 1 1 ;
	setAttr ".user_sceneUnits" 1;
	setAttr ".user_iesIgnoreWatts" yes;
	setAttr ".limits_texturememory" 4096;
	setAttr ".limits_geocachememory" 4096;
	setAttr ".limits_opacitycachememory" 2048;
	setAttr ".statistics_level" 1;
	setAttr ".statistics_xmlfilename" -type "string" "";
	setAttr ".lpe_diffuse2" -type "string" "Diffuse,HairDiffuse";
	setAttr ".lpe_diffuse3" -type "string" "Subsurface";
	setAttr ".lpe_specular2" -type "string" "Specular,HairSpecularR";
	setAttr ".lpe_specular3" -type "string" "RoughSpecular,HairSpecularTRT";
	setAttr ".lpe_specular4" -type "string" "Clearcoat";
	setAttr ".lpe_specular5" -type "string" "Iridescence";
	setAttr ".lpe_specular6" -type "string" "Fuzz,HairSpecularGLINTS";
	setAttr ".lpe_specular7" -type "string" "SingleScatter,HairSpecularTT";
	setAttr ".lpe_specular8" -type "string" "Glass";
	setAttr ".lpe_user2" -type "string" "Albedo,DiffuseAlbedo,SubsurfaceAlbedo,HairAlbedo";
	setAttr ".lpe_user3" -type "string" "";
	setAttr ".lpe_user4" -type "string" "";
	setAttr ".lpe_user5" -type "string" "";
	setAttr ".lpe_user6" -type "string" "";
	setAttr ".lpe_user7" -type "string" "";
	setAttr ".lpe_user8" -type "string" "";
	setAttr ".lpe_user9" -type "string" "";
	setAttr ".lpe_user10" -type "string" "";
	setAttr ".lpe_user11" -type "string" "";
	setAttr ".lpe_user12" -type "string" "";
	setAttr ".imageFileFormat" -type "string" "<scene>_<layer>_<camera>_<aov>.<f4>.<ext>";
	setAttr ".ribFileFormat" -type "string" "<camera><layer>.<f4>.rib";
	setAttr ".version" 1;
	setAttr ".take" 1;
	setAttr ".imageOutputDir" -type "string" "<ws>/images/<scene>_v<version>_t<take>";
	setAttr ".ribOutputDir" -type "string" "<ws>/renderman/rib/<scene>/v<version>_t<take>";
	setAttr -s 10 ".UserTokens";
	setAttr ".UserTokens[0].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[0].userTokenValues" -type "string" "";
	setAttr ".UserTokens[1].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[1].userTokenValues" -type "string" "";
	setAttr ".UserTokens[2].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[2].userTokenValues" -type "string" "";
	setAttr ".UserTokens[3].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[3].userTokenValues" -type "string" "";
	setAttr ".UserTokens[4].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[4].userTokenValues" -type "string" "";
	setAttr ".UserTokens[5].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[5].userTokenValues" -type "string" "";
	setAttr ".UserTokens[6].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[6].userTokenValues" -type "string" "";
	setAttr ".UserTokens[7].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[7].userTokenValues" -type "string" "";
	setAttr ".UserTokens[8].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[8].userTokenValues" -type "string" "";
	setAttr ".UserTokens[9].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[9].userTokenValues" -type "string" "";
	setAttr ".rlfData" -type "string" "init";
createNode rmanDisplay -s -n "rmanDefaultDisplay";
	rename -uid "D9EEC36F-A648-2F4C-BA89-29ADD0D2693B";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".enable" yes;
	setAttr ".denoise" no;
	setAttr ".frameMode" 0;
	setAttr ".remapBreakPoint" 0;
	setAttr ".remapMaxValue" 0;
	setAttr ".remapSmoothness" 0;
	setAttr -s 2 ".displayChannels";
	setAttr -l on ".name" -type "string" "beauty";
createNode d_openexr -n "d_openexr";
	rename -uid "FC5B0CAF-E846-1C93-F730-4D9B2C40F141";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".asrgba" yes;
	setAttr ".storage" -type "string" "scanline";
	setAttr ".exrpixeltype" -type "string" "half";
	setAttr ".compression" -type "string" "zips";
	setAttr ".compressionlevel" 45;
createNode rmanDisplayChannel -n "Ci";
	rename -uid "10105B6C-9D4F-3B67-929C-3BB35233FB54";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".enable" yes;
	setAttr ".channelType" -type "string" "color";
	setAttr ".channelSource" -type "string" "Ci";
	setAttr ".lpeLightGroup" -type "string" "";
	setAttr ".filter" -type "string" "inherit from display";
	setAttr ".filterwidth" -type "float2" -1 -1 ;
	setAttr ".statistics" -type "string" "";
	setAttr ".remapBreakPoint" 0;
	setAttr ".remapMaxValue" 0;
	setAttr ".remapSmoothness" 0;
	setAttr -l on ".name" -type "string" "Ci";
createNode rmanDisplayChannel -n "a";
	rename -uid "8D128D32-FA41-A287-441B-9798B2FD96CD";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".enable" yes;
	setAttr ".channelType" -type "string" "float";
	setAttr ".channelSource" -type "string" "a";
	setAttr ".lpeLightGroup" -type "string" "";
	setAttr ".filter" -type "string" "inherit from display";
	setAttr ".filterwidth" -type "float2" -1 -1 ;
	setAttr ".statistics" -type "string" "";
	setAttr ".remapBreakPoint" 0;
	setAttr ".remapMaxValue" 0;
	setAttr ".remapSmoothness" 0;
	setAttr -l on ".name" -type "string" "a";
createNode PxrPathTracer -s -n "PxrPathTracer";
	rename -uid "F481761F-B54A-6FB2-C982-D5B07DFBE1DE";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".maxPathLength" 10;
	setAttr ".maxContinuationLength" -1;
	setAttr ".maxNonStochasticOpacityEvents" 0;
	setAttr ".sampleMode" -type "string" "bxdf";
	setAttr ".numLightSamples" 1;
	setAttr ".numBxdfSamples" 1;
	setAttr ".numIndirectSamples" 1;
	setAttr ".numDiffuseSamples" 1;
	setAttr ".numSpecularSamples" 1;
	setAttr ".numSubsurfaceSamples" 1;
	setAttr ".numRefractionSamples" 1;
	setAttr ".allowCaustics" no;
	setAttr ".accumOpacity" no;
	setAttr ".rouletteDepth" 4;
	setAttr ".rouletteThreshold" 0.20000000298023224;
	setAttr ".clampDepth" 2;
	setAttr ".clampLuminance" 10;
createNode blinn -n "svgBlinn1";
	rename -uid "26BD5703-8B4B-ABB0-4089-EAB0F6142C9D";
	setAttr ".c" -type "float3" 1 1 1 ;
createNode shadingEngine -n "svgBlinn1SG";
	rename -uid "2D475104-DC4B-1236-8F98-A8B99A8256B7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "08CFE0F9-0C4E-5B43-4809-33A8DF2EE2CD";
createNode PxrSurface -n "PxrSurface1";
	rename -uid "C8FC649B-F24C-4E7F-2EBB-B184C2F8FE45";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".inputMaterial" 0;
	setAttr ".diffuseGain" 1;
	setAttr ".diffuseColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".diffuseRoughness" 0;
	setAttr ".diffuseExponent" 1;
	setAttr ".diffuseBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".diffuseDoubleSided" no;
	setAttr ".diffuseBackUseDiffuseColor" yes;
	setAttr ".diffuseBackColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".diffuseTransmitGain" 0;
	setAttr ".diffuseTransmitColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".specularFresnelMode" 0;
	setAttr ".specularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".specularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".specularFresnelShape" 5;
	setAttr ".specularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".specularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".specularRoughness" 0.20000000298023224;
	setAttr ".specularModelType" 0;
	setAttr ".specularAnisotropy" 0;
	setAttr ".specularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".specularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".specularDoubleSided" no;
	setAttr ".roughSpecularFresnelMode" 0;
	setAttr ".roughSpecularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularFresnelShape" 5;
	setAttr ".roughSpecularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".roughSpecularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularRoughness" 0.60000002384185791;
	setAttr ".roughSpecularModelType" 0;
	setAttr ".roughSpecularAnisotropy" 0;
	setAttr ".roughSpecularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularDoubleSided" no;
	setAttr ".clearcoatFresnelMode" 0;
	setAttr ".clearcoatFaceColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatFresnelShape" 5;
	setAttr ".clearcoatIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".clearcoatExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".clearcoatThickness" 0;
	setAttr ".clearcoatAbsorptionTint" -type "float3" 0 0 0 ;
	setAttr ".clearcoatRoughness" 0;
	setAttr ".clearcoatModelType" 0;
	setAttr ".clearcoatAnisotropy" 0;
	setAttr ".clearcoatAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".clearcoatBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".clearcoatDoubleSided" no;
	setAttr ".specularEnergyCompensation" 0;
	setAttr ".clearcoatEnergyCompensation" 0;
	setAttr ".iridescenceFaceGain" 0;
	setAttr ".iridescenceEdgeGain" 0;
	setAttr ".iridescenceFresnelShape" 5;
	setAttr ".iridescenceMode" 0;
	setAttr ".iridescencePrimaryColor" -type "float3" 1 0 0 ;
	setAttr ".iridescenceSecondaryColor" -type "float3" 0 0 1 ;
	setAttr ".iridescenceRoughness" 0.20000000298023224;
	setAttr ".iridescenceAnisotropy" 0;
	setAttr ".iridescenceAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".iridescenceCurve" 1;
	setAttr ".iridescenceScale" 1;
	setAttr ".iridescenceFlip" no;
	setAttr ".iridescenceThickness" 800;
	setAttr ".iridescenceDoubleSided" no;
	setAttr ".fuzzGain" 0;
	setAttr ".fuzzColor" -type "float3" 1 1 1 ;
	setAttr ".fuzzConeAngle" 8;
	setAttr ".fuzzBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".fuzzDoubleSided" no;
	setAttr ".subsurfaceType" 0;
	setAttr ".subsurfaceGain" 0;
	setAttr ".subsurfaceColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".subsurfaceDmfp" 10;
	setAttr ".subsurfaceDmfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".shortSubsurfaceGain" 0;
	setAttr ".shortSubsurfaceColor" -type "float3" 0.89999998 0.89999998 0.89999998 ;
	setAttr ".shortSubsurfaceDmfp" 5;
	setAttr ".longSubsurfaceGain" 0;
	setAttr ".longSubsurfaceColor" -type "float3" 0.80000001 0 0 ;
	setAttr ".longSubsurfaceDmfp" 20;
	setAttr ".subsurfaceDirectionality" 0;
	setAttr ".subsurfaceBleed" 0;
	setAttr ".subsurfaceDiffuseBlend" 0;
	setAttr ".subsurfaceResolveSelfIntersections" no;
	setAttr ".subsurfaceIor" 1.3999999761581421;
	setAttr ".subsurfacePostTint" -type "float3" 1 1 1 ;
	setAttr ".subsurfaceDiffuseSwitch" 1;
	setAttr ".subsurfaceDoubleSided" no;
	setAttr ".subsurfaceTransmitGain" 0;
	setAttr ".considerBackside" yes;
	setAttr ".continuationRayMode" 0;
	setAttr ".maxContinuationHits" 2;
	setAttr ".followTopology" 0;
	setAttr ".subsurfaceSubset" -type "string" "";
	setAttr ".singlescatterGain" 0;
	setAttr ".singlescatterColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".singlescatterMfp" 10;
	setAttr ".singlescatterMfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".singlescatterDirectionality" 0;
	setAttr ".singlescatterIor" 1.2999999523162842;
	setAttr ".singlescatterBlur" 0;
	setAttr ".singlescatterDirectGain" 0;
	setAttr ".singlescatterDirectGainTint" -type "float3" 1 1 1 ;
	setAttr ".singlescatterDoubleSided" no;
	setAttr ".singlescatterConsiderBackside" yes;
	setAttr ".singlescatterContinuationRayMode" 0;
	setAttr ".singlescatterMaxContinuationHits" 2;
	setAttr ".singlescatterDirectGainMode" 0;
	setAttr ".singlescatterSubset" -type "string" "";
	setAttr ".irradianceTint" -type "float3" 1 1 1 ;
	setAttr ".irradianceRoughness" 0;
	setAttr ".unitLength" 0.10000000149011612;
	setAttr ".refractionGain" 0;
	setAttr ".reflectionGain" 0;
	setAttr ".refractionColor" -type "float3" 1 1 1 ;
	setAttr ".glassRoughness" 0.10000000149011612;
	setAttr ".glassAnisotropy" 0;
	setAttr ".glassAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".glassIor" 1.5;
	setAttr ".mwWalkable" no;
	setAttr ".mwIor" -1;
	setAttr ".thinGlass" no;
	setAttr ".ignoreFresnel" no;
	setAttr ".ignoreAccumOpacity" no;
	setAttr ".blocksVolumes" no;
	setAttr ".ssAlbedo" -type "float3" 0 0 0 ;
	setAttr ".extinction" -type "float3" 0 0 0 ;
	setAttr ".g" 0;
	setAttr ".multiScatter" no;
	setAttr ".enableOverlappingVolumes" no;
	setAttr ".glowGain" 0;
	setAttr ".glowColor" -type "float3" 1 1 1 ;
	setAttr ".bumpNormal" -type "float3" 0 0 0 ;
	setAttr ".shadowColor" -type "float3" 0 0 0 ;
	setAttr ".shadowMode" 0;
	setAttr ".presence" 1;
	setAttr ".presenceCached" 1;
	setAttr ".mwStartable" no;
	setAttr ".roughnessMollificationClamp" 32;
	setAttr ".utilityPattern[0]"  0;
createNode shadingEngine -n "PxrSurface1SG";
	rename -uid "16003889-0740-B21C-AE94-8A88E6D2E667";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "41A1BCFC-0E4D-86E3-EDBF-F38E0440CE5F";
createNode lambert -n "lambert2";
	rename -uid "AC633215-324C-0932-3291-3CA1043003C1";
createNode PxrSurface -n "PxrSurface2";
	rename -uid "F47065C6-924B-A848-8FB0-308428CE44EC";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".inputMaterial" 0;
	setAttr ".diffuseGain" 1;
	setAttr ".diffuseColor" -type "float3" 0.2013889 0.2013889 0.2013889 ;
	setAttr ".diffuseRoughness" 0;
	setAttr ".diffuseExponent" 1;
	setAttr ".diffuseBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".diffuseDoubleSided" no;
	setAttr ".diffuseBackUseDiffuseColor" yes;
	setAttr ".diffuseBackColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".diffuseTransmitGain" 0;
	setAttr ".diffuseTransmitColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".specularFresnelMode" 0;
	setAttr ".specularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".specularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".specularFresnelShape" 5;
	setAttr ".specularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".specularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".specularRoughness" 0.20000000298023224;
	setAttr ".specularModelType" 0;
	setAttr ".specularAnisotropy" 0;
	setAttr ".specularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".specularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".specularDoubleSided" no;
	setAttr ".roughSpecularFresnelMode" 0;
	setAttr ".roughSpecularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularFresnelShape" 5;
	setAttr ".roughSpecularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".roughSpecularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularRoughness" 0.60000002384185791;
	setAttr ".roughSpecularModelType" 0;
	setAttr ".roughSpecularAnisotropy" 0;
	setAttr ".roughSpecularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularDoubleSided" no;
	setAttr ".clearcoatFresnelMode" 0;
	setAttr ".clearcoatFaceColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatFresnelShape" 5;
	setAttr ".clearcoatIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".clearcoatExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".clearcoatThickness" 0;
	setAttr ".clearcoatAbsorptionTint" -type "float3" 0 0 0 ;
	setAttr ".clearcoatRoughness" 0;
	setAttr ".clearcoatModelType" 0;
	setAttr ".clearcoatAnisotropy" 0;
	setAttr ".clearcoatAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".clearcoatBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".clearcoatDoubleSided" no;
	setAttr ".specularEnergyCompensation" 0;
	setAttr ".clearcoatEnergyCompensation" 0;
	setAttr ".iridescenceFaceGain" 0;
	setAttr ".iridescenceEdgeGain" 0;
	setAttr ".iridescenceFresnelShape" 5;
	setAttr ".iridescenceMode" 0;
	setAttr ".iridescencePrimaryColor" -type "float3" 1 0 0 ;
	setAttr ".iridescenceSecondaryColor" -type "float3" 0 0 1 ;
	setAttr ".iridescenceRoughness" 0.20000000298023224;
	setAttr ".iridescenceAnisotropy" 0;
	setAttr ".iridescenceAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".iridescenceCurve" 1;
	setAttr ".iridescenceScale" 1;
	setAttr ".iridescenceFlip" no;
	setAttr ".iridescenceThickness" 800;
	setAttr ".iridescenceDoubleSided" no;
	setAttr ".fuzzGain" 0;
	setAttr ".fuzzColor" -type "float3" 1 1 1 ;
	setAttr ".fuzzConeAngle" 8;
	setAttr ".fuzzBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".fuzzDoubleSided" no;
	setAttr ".subsurfaceType" 0;
	setAttr ".subsurfaceGain" 0;
	setAttr ".subsurfaceColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".subsurfaceDmfp" 10;
	setAttr ".subsurfaceDmfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".shortSubsurfaceGain" 0;
	setAttr ".shortSubsurfaceColor" -type "float3" 0.89999998 0.89999998 0.89999998 ;
	setAttr ".shortSubsurfaceDmfp" 5;
	setAttr ".longSubsurfaceGain" 0;
	setAttr ".longSubsurfaceColor" -type "float3" 0.80000001 0 0 ;
	setAttr ".longSubsurfaceDmfp" 20;
	setAttr ".subsurfaceDirectionality" 0;
	setAttr ".subsurfaceBleed" 0;
	setAttr ".subsurfaceDiffuseBlend" 0;
	setAttr ".subsurfaceResolveSelfIntersections" no;
	setAttr ".subsurfaceIor" 1.3999999761581421;
	setAttr ".subsurfacePostTint" -type "float3" 1 1 1 ;
	setAttr ".subsurfaceDiffuseSwitch" 1;
	setAttr ".subsurfaceDoubleSided" no;
	setAttr ".subsurfaceTransmitGain" 0;
	setAttr ".considerBackside" yes;
	setAttr ".continuationRayMode" 0;
	setAttr ".maxContinuationHits" 2;
	setAttr ".followTopology" 0;
	setAttr ".subsurfaceSubset" -type "string" "";
	setAttr ".singlescatterGain" 0;
	setAttr ".singlescatterColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".singlescatterMfp" 10;
	setAttr ".singlescatterMfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".singlescatterDirectionality" 0;
	setAttr ".singlescatterIor" 1.2999999523162842;
	setAttr ".singlescatterBlur" 0;
	setAttr ".singlescatterDirectGain" 0;
	setAttr ".singlescatterDirectGainTint" -type "float3" 1 1 1 ;
	setAttr ".singlescatterDoubleSided" no;
	setAttr ".singlescatterConsiderBackside" yes;
	setAttr ".singlescatterContinuationRayMode" 0;
	setAttr ".singlescatterMaxContinuationHits" 2;
	setAttr ".singlescatterDirectGainMode" 0;
	setAttr ".singlescatterSubset" -type "string" "";
	setAttr ".irradianceTint" -type "float3" 1 1 1 ;
	setAttr ".irradianceRoughness" 0;
	setAttr ".unitLength" 0.10000000149011612;
	setAttr ".refractionGain" 0;
	setAttr ".reflectionGain" 0;
	setAttr ".refractionColor" -type "float3" 1 1 1 ;
	setAttr ".glassRoughness" 0.10000000149011612;
	setAttr ".glassAnisotropy" 0;
	setAttr ".glassAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".glassIor" 1.5;
	setAttr ".mwWalkable" no;
	setAttr ".mwIor" -1;
	setAttr ".thinGlass" no;
	setAttr ".ignoreFresnel" no;
	setAttr ".ignoreAccumOpacity" no;
	setAttr ".blocksVolumes" no;
	setAttr ".ssAlbedo" -type "float3" 0 0 0 ;
	setAttr ".extinction" -type "float3" 0 0 0 ;
	setAttr ".g" 0;
	setAttr ".multiScatter" no;
	setAttr ".enableOverlappingVolumes" no;
	setAttr ".glowGain" 0;
	setAttr ".glowColor" -type "float3" 1 1 1 ;
	setAttr ".bumpNormal" -type "float3" 0 0 0 ;
	setAttr ".shadowColor" -type "float3" 0 0 0 ;
	setAttr ".shadowMode" 0;
	setAttr ".presence" 1;
	setAttr ".presenceCached" 1;
	setAttr ".mwStartable" no;
	setAttr ".roughnessMollificationClamp" 32;
	setAttr ".utilityPattern[0]"  0;
createNode shadingEngine -n "PxrSurface2SG";
	rename -uid "0CE919E2-6442-3A0D-E15C-6D81D97AC1FA";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
	rename -uid "10E8B86E-174E-0CFD-3160-ECB8B3FA39F1";
createNode PxrSurface -n "PxrSurface3";
	rename -uid "318BCB74-5B4E-32E8-60E1-958F39243814";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".inputMaterial" 0;
	setAttr ".diffuseGain" 1;
	setAttr ".diffuseColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".diffuseRoughness" 0;
	setAttr ".diffuseExponent" 1;
	setAttr ".diffuseBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".diffuseDoubleSided" no;
	setAttr ".diffuseBackUseDiffuseColor" yes;
	setAttr ".diffuseBackColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".diffuseTransmitGain" 0;
	setAttr ".diffuseTransmitColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".specularFresnelMode" 0;
	setAttr ".specularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".specularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".specularFresnelShape" 5;
	setAttr ".specularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".specularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".specularRoughness" 0.20000000298023224;
	setAttr ".specularModelType" 0;
	setAttr ".specularAnisotropy" 0;
	setAttr ".specularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".specularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".specularDoubleSided" no;
	setAttr ".roughSpecularFresnelMode" 0;
	setAttr ".roughSpecularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularFresnelShape" 5;
	setAttr ".roughSpecularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".roughSpecularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularRoughness" 0.60000002384185791;
	setAttr ".roughSpecularModelType" 0;
	setAttr ".roughSpecularAnisotropy" 0;
	setAttr ".roughSpecularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularDoubleSided" no;
	setAttr ".clearcoatFresnelMode" 0;
	setAttr ".clearcoatFaceColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatFresnelShape" 5;
	setAttr ".clearcoatIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".clearcoatExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".clearcoatThickness" 0;
	setAttr ".clearcoatAbsorptionTint" -type "float3" 0 0 0 ;
	setAttr ".clearcoatRoughness" 0;
	setAttr ".clearcoatModelType" 0;
	setAttr ".clearcoatAnisotropy" 0;
	setAttr ".clearcoatAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".clearcoatBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".clearcoatDoubleSided" no;
	setAttr ".specularEnergyCompensation" 0;
	setAttr ".clearcoatEnergyCompensation" 0;
	setAttr ".iridescenceFaceGain" 0;
	setAttr ".iridescenceEdgeGain" 0;
	setAttr ".iridescenceFresnelShape" 5;
	setAttr ".iridescenceMode" 0;
	setAttr ".iridescencePrimaryColor" -type "float3" 1 0 0 ;
	setAttr ".iridescenceSecondaryColor" -type "float3" 0 0 1 ;
	setAttr ".iridescenceRoughness" 0.20000000298023224;
	setAttr ".iridescenceAnisotropy" 0;
	setAttr ".iridescenceAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".iridescenceCurve" 1;
	setAttr ".iridescenceScale" 1;
	setAttr ".iridescenceFlip" no;
	setAttr ".iridescenceThickness" 800;
	setAttr ".iridescenceDoubleSided" no;
	setAttr ".fuzzGain" 0;
	setAttr ".fuzzColor" -type "float3" 1 1 1 ;
	setAttr ".fuzzConeAngle" 8;
	setAttr ".fuzzBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".fuzzDoubleSided" no;
	setAttr ".subsurfaceType" 0;
	setAttr ".subsurfaceGain" 0;
	setAttr ".subsurfaceColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".subsurfaceDmfp" 10;
	setAttr ".subsurfaceDmfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".shortSubsurfaceGain" 0;
	setAttr ".shortSubsurfaceColor" -type "float3" 0.89999998 0.89999998 0.89999998 ;
	setAttr ".shortSubsurfaceDmfp" 5;
	setAttr ".longSubsurfaceGain" 0;
	setAttr ".longSubsurfaceColor" -type "float3" 0.80000001 0 0 ;
	setAttr ".longSubsurfaceDmfp" 20;
	setAttr ".subsurfaceDirectionality" 0;
	setAttr ".subsurfaceBleed" 0;
	setAttr ".subsurfaceDiffuseBlend" 0;
	setAttr ".subsurfaceResolveSelfIntersections" no;
	setAttr ".subsurfaceIor" 1.3999999761581421;
	setAttr ".subsurfacePostTint" -type "float3" 1 1 1 ;
	setAttr ".subsurfaceDiffuseSwitch" 1;
	setAttr ".subsurfaceDoubleSided" no;
	setAttr ".subsurfaceTransmitGain" 0;
	setAttr ".considerBackside" yes;
	setAttr ".continuationRayMode" 0;
	setAttr ".maxContinuationHits" 2;
	setAttr ".followTopology" 0;
	setAttr ".subsurfaceSubset" -type "string" "";
	setAttr ".singlescatterGain" 0;
	setAttr ".singlescatterColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".singlescatterMfp" 10;
	setAttr ".singlescatterMfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".singlescatterDirectionality" 0;
	setAttr ".singlescatterIor" 1.2999999523162842;
	setAttr ".singlescatterBlur" 0;
	setAttr ".singlescatterDirectGain" 0;
	setAttr ".singlescatterDirectGainTint" -type "float3" 1 1 1 ;
	setAttr ".singlescatterDoubleSided" no;
	setAttr ".singlescatterConsiderBackside" yes;
	setAttr ".singlescatterContinuationRayMode" 0;
	setAttr ".singlescatterMaxContinuationHits" 2;
	setAttr ".singlescatterDirectGainMode" 0;
	setAttr ".singlescatterSubset" -type "string" "";
	setAttr ".irradianceTint" -type "float3" 1 1 1 ;
	setAttr ".irradianceRoughness" 0;
	setAttr ".unitLength" 0.10000000149011612;
	setAttr ".refractionGain" 0;
	setAttr ".reflectionGain" 0;
	setAttr ".refractionColor" -type "float3" 1 1 1 ;
	setAttr ".glassRoughness" 0.10000000149011612;
	setAttr ".glassAnisotropy" 0;
	setAttr ".glassAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".glassIor" 1.5;
	setAttr ".mwWalkable" no;
	setAttr ".mwIor" -1;
	setAttr ".thinGlass" no;
	setAttr ".ignoreFresnel" no;
	setAttr ".ignoreAccumOpacity" no;
	setAttr ".blocksVolumes" no;
	setAttr ".ssAlbedo" -type "float3" 0 0 0 ;
	setAttr ".extinction" -type "float3" 0 0 0 ;
	setAttr ".g" 0;
	setAttr ".multiScatter" no;
	setAttr ".enableOverlappingVolumes" no;
	setAttr ".glowGain" 0;
	setAttr ".glowColor" -type "float3" 1 1 1 ;
	setAttr ".bumpNormal" -type "float3" 0 0 0 ;
	setAttr ".shadowColor" -type "float3" 0 0 0 ;
	setAttr ".shadowMode" 0;
	setAttr ".presence" 1;
	setAttr ".presenceCached" 1;
	setAttr ".mwStartable" no;
	setAttr ".roughnessMollificationClamp" 32;
	setAttr ".utilityPattern[0]"  0;
createNode polyTorus -n "polyTorus3";
	rename -uid "51837BA3-D649-8C25-B2FC-F19ECFA6BB85";
	setAttr ".r" 0.35;
	setAttr ".sr" 0.05;
	setAttr ".tw" 45;
	setAttr ".sa" 60;
	setAttr ".sh" 4;
createNode objectSet -n "set3";
	rename -uid "F494F39E-8C40-B139-D413-09B396660B91";
	setAttr ".ihi" 0;
	setAttr -s 10 ".dsm";
	setAttr -s 8 ".gn";
createNode groupId -n "groupId37";
	rename -uid "B9060686-EE43-C83B-7B40-FEAC9797315D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "8BB12470-7D48-7C1A-61B5-A79ECE476C21";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[14:43]" "e[74:103]" "e[134:163]" "e[194:223]" "e[254:284]" "e[314:344]" "e[374:404]" "e[434:464]";
createNode deleteComponent -n "deleteComponent3";
	rename -uid "8DAD1742-B243-6866-07D1-EB88B49419A3";
	setAttr ".dc" -type "componentList" 4 "f[14:43]" "f[74:103]" "f[134:163]" "f[194:223]";
createNode objectSet -n "set4";
	rename -uid "136B2405-1444-8176-CEEC-988B4E6BB670";
	setAttr ".ihi" 0;
	setAttr -s 9 ".dsm";
	setAttr -s 8 ".gn";
createNode groupId -n "groupId38";
	rename -uid "3794C6C8-1540-E53B-A8C4-62B5FDABA63F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "D7C0D796-0F48-D72D-A6EF-949A44FE0E65";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 9 "e[0:13]" "e[29:43]" "e[59:73]" "e[89:103]" "e[119:134]" "e[150:165]" "e[181:196]" "e[212:227]" "e[243]";
createNode deleteComponent -n "deleteComponent4";
	rename -uid "88E44117-1249-0EBB-1088-1CA24D5BFE38";
	setAttr ".dc" -type "componentList" 5 "f[0:13]" "f[29:43]" "f[59:73]" "f[89:103]" "f[119]";
createNode polyCloseBorder -n "polyCloseBorder3";
	rename -uid "E5116CEE-6B44-E5EF-95A4-F4AB1C3E00B2";
	setAttr ".ics" -type "componentList" 1 "e[*]";
createNode polyTweak -n "polyTweak8";
	rename -uid "0A57E989-A746-81F9-68B7-7990A54F2C18";
	setAttr ".uopa" yes;
	setAttr -s 28 ".tk";
	setAttr ".tk[17]" -type "float3" 0 0 0.0021110449 ;
	setAttr ".tk[18]" -type "float3" 0 0 0.0084209712 ;
	setAttr ".tk[19]" -type "float3" 0 0 0.018860666 ;
	setAttr ".tk[20]" -type "float3" 0 0 0.033315774 ;
	setAttr ".tk[21]" -type "float3" 0 0 0.051627874 ;
	setAttr ".tk[22]" -type "float3" 0.047585391 2.0816682e-17 0.073596388 ;
	setAttr ".tk[23]" -type "float3" 0.12750223 2.7755576e-17 0.098980576 ;
	setAttr ".tk[24]" -type "float3" 0.098980449 2.0816682e-17 0.043269221 ;
	setAttr ".tk[25]" -type "float3" 0.073596291 1.3877788e-17 0.015894815 ;
	setAttr ".tk[26]" -type "float3" 0.051627807 1.3877788e-17 0 ;
	setAttr ".tk[27]" -type "float3" 0.033315711 6.9388939e-18 0 ;
	setAttr ".tk[28]" -type "float3" 0.018860603 6.9388939e-18 0 ;
	setAttr ".tk[29]" -type "float3" 0.0084209088 0 0 ;
	setAttr ".tk[30]" -type "float3" 0.0021110117 0 0.0017660903 ;
	setAttr ".tk[33]" -type "float3" 0 0 0.0021110449 ;
	setAttr ".tk[34]" -type "float3" 0 0 0.0084209712 ;
	setAttr ".tk[35]" -type "float3" 0 0 0.018860666 ;
	setAttr ".tk[36]" -type "float3" 0 0 0.033315774 ;
	setAttr ".tk[37]" -type "float3" 0 0 0.051627874 ;
	setAttr ".tk[38]" -type "float3" 0.047585391 2.0816682e-17 0.073596388 ;
	setAttr ".tk[39]" -type "float3" 0.12750223 2.7755576e-17 0.098980576 ;
	setAttr ".tk[40]" -type "float3" 0.098980449 2.0816682e-17 0.043269221 ;
	setAttr ".tk[41]" -type "float3" 0.073596291 1.3877788e-17 0.015894815 ;
	setAttr ".tk[42]" -type "float3" 0.051627807 1.3877788e-17 0 ;
	setAttr ".tk[43]" -type "float3" 0.033315711 6.9388939e-18 0 ;
	setAttr ".tk[44]" -type "float3" 0.018860603 6.9388939e-18 0 ;
	setAttr ".tk[45]" -type "float3" 0.0084209088 0 0 ;
	setAttr ".tk[46]" -type "float3" 0.0021110117 0 0.0017660903 ;
createNode objectSet -n "set5";
	rename -uid "F577FA09-D441-4BA0-B9D3-9FA172A09AE3";
	setAttr ".ihi" 0;
	setAttr -s 9 ".dsm";
	setAttr -s 8 ".gn";
createNode groupId -n "groupId39";
	rename -uid "B28D303E-3A4F-7AB7-6E3C-CEA0D3EEDE29";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "457ABA93-6C4D-E9D3-A193-9CA6326EFC6D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
createNode deleteComponent -n "deleteComponent5";
	rename -uid "43EB14D8-A64E-B0AD-6DB1-0383ED3B2BAA";
	setAttr ".dc" -type "componentList" 1 "f[61]";
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "30FB7285-2044-1759-2D7C-E0A123C4BB6C";
	setAttr ".ics" -type "componentList" 1 "f[60]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 2.7957363266636235 1.2059839130104333 0.92726954502800107 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.7957363 1.2059839 1.3345377 ;
	setAttr ".rs" 801951903;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.7603809859742028 1.2059839160281047 1.2419142095539868 ;
	setAttr ".cbx" -type "double3" 2.8310916710783345 1.2059839234038048 1.4271613625977337 ;
createNode polyTweak -n "polyTweak9";
	rename -uid "092DCC75-7E4E-FD92-4786-D99BA53A4755";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk";
	setAttr ".tk[0]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[16]" -type "float3" -2.9802322e-08 0 0.11453643 ;
	setAttr ".tk[17]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[18]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[19]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[20]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[21]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[22]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[23]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[32]" -type "float3" -2.9802322e-08 0 0.11453643 ;
	setAttr ".tk[33]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[34]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[35]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[36]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[37]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[38]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[39]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[48]" -type "float3" -2.9802322e-08 0 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "93B692F0-C546-845F-232A-75A8F0913F05";
	setAttr ".ics" -type "componentList" 1 "f[60]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 2.7957363266636235 1.2059839130104333 0.92726954502800107 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.7957363 0.6936186 1.3345377 ;
	setAttr ".rs" 2011245933;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.7603809971500737 0.69361857182390985 1.2419141797516644 ;
	setAttr ".cbx" -type "double3" 2.8310916561771733 0.69361857182390985 1.4271613625977337 ;
createNode polyTweak -n "polyTweak10";
	rename -uid "E7616B91-3E46-2987-02FC-DB9CBD9C6FCD";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[64:67]" -type "float3"  -0.51236534 -8.3266727e-17
		 0 -0.51236534 -8.3266727e-17 0 -0.51236534 -8.3266727e-17 0 -0.51236534 -8.3266727e-17
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "9A836E55-C348-A342-D728-24B508757164";
	setAttr ".ics" -type "componentList" 1 "f[66]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 2.7957363266636235 1.5421437383354455 0.92726954502800107 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.7957363 0.51488924 1.2419143 ;
	setAttr ".rs" 1611558153;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.7603809971500732 3.5828426447892525e-08 1.2419141797516644 ;
	setAttr ".cbx" -type "double3" 2.8310916561771733 1.0297783971489221 1.241914298960954 ;
createNode polyTweak -n "polyTweak11";
	rename -uid "8A7303F4-4A46-22FE-4402-E0B33E69D008";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[68:71]" -type "float3"  -1.029778361 -6.7307271e-16
		 0 -1.029778361 -6.7307271e-16 0 -1.029778361 -6.7307271e-16 0 -1.029778361 -6.7307271e-16
		 0;
createNode objectSet -n "set6";
	rename -uid "B55F7438-704B-B922-FC13-1888B279A6D1";
	setAttr ".ihi" 0;
	setAttr -s 9 ".dsm";
	setAttr -s 8 ".gn";
createNode groupId -n "groupId40";
	rename -uid "9FE20736-8A42-3BEE-5376-1A87798E56DC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts11";
	rename -uid "3E36C4F7-A040-7598-206F-A4B555B3768D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "e[142]" "e[144]" "e[146:147]";
createNode polyTweak -n "polyTweak12";
	rename -uid "22405D84-3A40-B3DE-847F-51BD4EF77548";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[72:75]" -type "float3"  0 0 -0.31464472 0 0 -0.31464472
		 0 0 -0.31464472 0 0 -0.31464472;
createNode deleteComponent -n "deleteComponent6";
	rename -uid "74E632DE-0642-808B-3EBB-67ABD2A5BB32";
	setAttr ".dc" -type "componentList" 1 "f[66]";
createNode polyMirror -n "polyMirror1";
	rename -uid "5A352607-8442-C11C-B361-5997022D8954";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 2.795736326663623 1.3578873562295586 1.0942815106624331 1;
	setAttr ".p" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
	setAttr ".a" 2;
	setAttr ".ma" 1;
	setAttr ".mtt" 1;
	setAttr ".cm" yes;
	setAttr ".fnf" 72;
	setAttr ".lnf" 143;
	setAttr ".pc" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
createNode polyTweak -n "polyTweak13";
	rename -uid "1936C8A8-E946-4609-2AB0-39AFE2E4F7B2";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk";
	setAttr ".tk[16]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[17]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[18]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[19]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[20]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[21]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[22]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[23]" -type "float3" 0.18425632 3.469447e-17 -0.12528354 ;
	setAttr ".tk[24]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[25]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[26]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[27]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[28]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[29]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[30]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[31]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[32]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[33]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[34]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[35]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[36]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[37]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[38]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[39]" -type "float3" 0.18425632 3.469447e-17 -0.12528354 ;
	setAttr ".tk[40]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[41]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[42]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[43]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[44]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[45]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[46]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[47]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[65]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[67]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[68]" -type "float3" 0.18425636 4.7878368e-16 0 ;
	setAttr ".tk[69]" -type "float3" 0.18425636 4.7878368e-16 -0.12528354 ;
	setAttr ".tk[70]" -type "float3" 0.18425636 4.7878368e-16 0 ;
	setAttr ".tk[71]" -type "float3" 0.18425636 4.7878368e-16 -0.12528354 ;
	setAttr ".tk[74]" -type "float3" 0.18425636 4.7878368e-16 0 ;
	setAttr ".tk[75]" -type "float3" 0.18425636 4.7878368e-16 0 ;
createNode groupId -n "groupId41";
	rename -uid "3FB9C73F-164E-406F-B012-73B10E4E199A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId42";
	rename -uid "3A800111-2E45-3453-4FCE-85BE95489F46";
	setAttr ".ihi" 0;
createNode groupId -n "groupId43";
	rename -uid "779998B5-F548-8E40-EA94-53BAA4BFE311";
	setAttr ".ihi" 0;
createNode groupId -n "groupId44";
	rename -uid "6A283094-6F44-14BF-CD75-6BABD1386E81";
	setAttr ".ihi" 0;
createNode groupId -n "groupId45";
	rename -uid "5CD7CE87-A149-1D5D-DB1A-CEABA270111B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId46";
	rename -uid "552E8282-7647-5544-8889-79A09BD9901C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId47";
	rename -uid "883CED9F-8C44-D39A-A4F3-BC8C48529EDA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId48";
	rename -uid "318DEDDF-A145-D7B9-033A-0D924E5C91B6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId49";
	rename -uid "4D6A390D-2B40-C447-0F83-DFBAF0C3FAC5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId50";
	rename -uid "54FE206D-8E46-358D-A80D-99B44721C5B1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId51";
	rename -uid "30F1B991-0A40-299C-3CF2-47B1D50EDBFE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId52";
	rename -uid "620CE80F-AC46-087C-99F7-E585AC21B318";
	setAttr ".ihi" 0;
createNode polyUnite -n "polyUnite1";
	rename -uid "DA648144-6C48-D55D-E602-90965F80F228";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId57";
	rename -uid "0D43BE3A-2E43-60B8-5E7A-E19FD2202C88";
	setAttr ".ihi" 0;
createNode groupId -n "groupId58";
	rename -uid "F7BF0501-174D-8955-C531-869FB3FF4D43";
	setAttr ".ihi" 0;
createNode groupId -n "groupId59";
	rename -uid "8884329C-6A4C-2D36-A27A-71A2B5E13255";
	setAttr ".ihi" 0;
createNode groupId -n "groupId60";
	rename -uid "348BFA20-E944-16E0-3A4A-5AA4CFE44737";
	setAttr ".ihi" 0;
createNode groupId -n "groupId61";
	rename -uid "95212C67-144B-01CD-F3D1-CA97A8013413";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "C3331297-644A-3616-0E2C-33BCDFF29992";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 16 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]" "e[348]" "e[364]" "e[380]" "e[396]" "e[437]" "e[481]" "e[511]" "e[541]";
createNode groupId -n "groupId62";
	rename -uid "0177AC27-1C40-2FB0-DF06-A1A1DA373F8A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts13";
	rename -uid "D4CC4910-AB4C-53A5-4B83-FD9008B4E8AF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[75]" "e[91]" "e[107]" "e[123]" "e[363]" "e[379]" "e[395]" "e[411]";
createNode groupId -n "groupId63";
	rename -uid "B8091BEA-FE47-5E7F-8956-2D9D5741CCB1";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts14";
	rename -uid "5307EB0A-1941-B531-6FB4-DFBE1E8C372D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[75]" "e[91]" "e[107]" "e[123]" "e[363]" "e[379]" "e[395]" "e[411]";
createNode groupId -n "groupId64";
	rename -uid "77AF1E7F-0F41-8017-8F84-06A01EAECF06";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts15";
	rename -uid "9A0A57A7-A14A-C431-AAE6-93A311202958";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "e[142]" "e[144]" "e[146:147]" "e[430]" "e[432]" "e[434:435]";
createNode groupId -n "groupId65";
	rename -uid "E3A9F1C5-E74B-5D96-FC6D-3BBAED337F66";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts16";
	rename -uid "C3B7E84A-7E45-C4C5-1C05-479F47B7713E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:287]";
createNode deleteComponent -n "deleteComponent7";
	rename -uid "418E9B81-FB4A-FFDF-09C1-58B5C2AF5007";
	setAttr ".dc" -type "componentList" 6 "f[15:21]" "f[64]" "f[67]" "f[231:237]" "f[280]" "f[283]";
createNode polyBridgeEdge -n "polyBridgeEdge1";
	rename -uid "748EDE8C-1341-E5D3-C92D-8CA1D3AC214D";
	setAttr ".ics" -type "componentList" 18 "e[15:21]" "e[30:36]" "e[76]" "e[118]" "e[122]" "e[125]" "e[129]" "e[430]" "e[433]" "e[436]" "e[439]" "e[442]" "e[445]" "e[448]" "e[473:480]" "e[547]" "e[550]" "e[553:554]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 71;
	setAttr ".sv2" 285;
	setAttr ".rev" yes;
	setAttr ".d" 1;
createNode polyBridgeEdge -n "polyBridgeEdge2";
	rename -uid "D2527D2C-624E-7804-8CE9-9CB859A8E166";
	setAttr ".ics" -type "componentList" 2 "e[131]" "e[544]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 69;
	setAttr ".sv2" 287;
	setAttr ".d" 1;
createNode polyUnite -n "polyUnite2";
	rename -uid "407FE17A-EA4E-39D3-39D1-F8A4B17EC888";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId66";
	rename -uid "2A1E3FD9-7548-9EE1-0A83-93AC638457D3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId67";
	rename -uid "BD137EEA-3442-9157-5730-BB83C7ADB8F9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId68";
	rename -uid "99B093EA-1046-70AE-ED19-3FAF68479629";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts17";
	rename -uid "66DD233E-B84E-4483-1F0C-5C9B0B4FE488";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 22 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]" "e[348]" "e[373]" "e[389]" "e[429]" "e[473]" "e[503]" "e[533]" "e[628]" "e[644]" "e[660]" "e[676]" "e[717]" "e[784]" "e[814]";
createNode groupId -n "groupId69";
	rename -uid "A46B80DC-5343-DEB5-2F2F-59BE73FF006C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts18";
	rename -uid "32A653AF-A849-AA66-FA74-979D4FD47677";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[75]" "e[91]" "e[107]" "e[123]" "e[363]" "e[372]" "e[388]" "e[404]" "e[643]" "e[659]" "e[675]" "e[691]";
createNode groupId -n "groupId70";
	rename -uid "B255EF1D-2C4C-28DF-F8E2-69B32F50D9C4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts19";
	rename -uid "057A549A-564D-9F3C-265E-73950D39BB48";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[75]" "e[91]" "e[107]" "e[123]" "e[363]" "e[372]" "e[388]" "e[404]" "e[643]" "e[659]" "e[675]" "e[691]";
createNode groupId -n "groupId71";
	rename -uid "C98A3FD9-F142-3F9F-37CB-2BB94F132BC5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts20";
	rename -uid "7E2F9E96-FF4A-EDEC-87F7-E2A3372A3B8A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 9 "e[142]" "e[144]" "e[146:147]" "e[422]" "e[424]" "e[426:427]" "e[710]" "e[712]" "e[714:715]";
createNode groupId -n "groupId72";
	rename -uid "78F3D9CA-A34E-BE7F-2225-FDAFD48C4C8B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts21";
	rename -uid "A6B0B33F-4349-B2CC-DADC-63860D36322F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:433]";
createNode deleteComponent -n "deleteComponent8";
	rename -uid "2C8A373C-9C4B-9DB7-8B79-17A4B54E58C7";
	setAttr ".dc" -type "componentList" 6 "f[87:93]" "f[136]" "f[139]" "f[294:300]" "f[343]" "f[346]";
createNode polyBridgeEdge -n "polyBridgeEdge3";
	rename -uid "D5D0521F-624B-7C89-609F-17AE72B4D08F";
	setAttr ".ics" -type "componentList" 18 "e[150]" "e[153]" "e[156]" "e[159]" "e[162]" "e[165]" "e[168]" "e[193:200]" "e[267]" "e[270]" "e[273:274]" "e[575:581]" "e[590:596]" "e[636]" "e[678]" "e[682]" "e[685]" "e[689]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 141;
	setAttr ".sv2" 359;
	setAttr ".rev" yes;
	setAttr ".d" 1;
createNode polyBridgeEdge -n "polyBridgeEdge4";
	rename -uid "7BA04A57-914E-C414-46CF-7183F2DF919F";
	setAttr ".ics" -type "componentList" 2 "e[264]" "e[691]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 143;
	setAttr ".sv2" 357;
	setAttr ".d" 1;
createNode polyUnite -n "polyUnite3";
	rename -uid "60085F76-1C43-92AC-FFC2-1AA35CBFC161";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId73";
	rename -uid "57656EEC-7743-ECFB-1BE5-94BBEC4EAAA2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts22";
	rename -uid "A0054933-1644-382B-BACC-5EA78CA35329";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:143]";
createNode groupId -n "groupId74";
	rename -uid "D93460D0-354F-61F0-8673-539DD82225BA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId75";
	rename -uid "0382DE87-A348-B74E-9FAB-DB8E12A1865C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts23";
	rename -uid "C052238D-9242-91E8-D813-20827F7F497A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 28 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]" "e[348]" "e[364]" "e[380]" "e[396]" "e[437]" "e[504]" "e[534]" "e[628]" "e[653]" "e[669]" "e[709]" "e[753]" "e[783]" "e[813]" "e[908]" "e[933]" "e[949]" "e[989]" "e[1056]" "e[1086]";
createNode groupId -n "groupId76";
	rename -uid "BD7304AE-454B-6C88-A618-4781F486A753";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts24";
	rename -uid "E5C3BD19-DE4E-F631-D02E-D4B908CFE4FE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 16 "e[75]" "e[91]" "e[107]" "e[123]" "e[363]" "e[379]" "e[395]" "e[411]" "e[643]" "e[652]" "e[668]" "e[684]" "e[923]" "e[932]" "e[948]" "e[964]";
createNode groupId -n "groupId77";
	rename -uid "19079929-2D44-55D5-A473-85944386FBC4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts25";
	rename -uid "BFD776D6-4F48-2ED7-8E96-879637EB71CF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 16 "e[75]" "e[91]" "e[107]" "e[123]" "e[363]" "e[379]" "e[395]" "e[411]" "e[643]" "e[652]" "e[668]" "e[684]" "e[923]" "e[932]" "e[948]" "e[964]";
createNode groupId -n "groupId78";
	rename -uid "FE37F942-EB40-7F73-508F-CF8603307762";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts26";
	rename -uid "08962B58-6F4D-8EE3-A230-5A80C64C352C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[142]" "e[144]" "e[146:147]" "e[430]" "e[432]" "e[434:435]" "e[702]" "e[704]" "e[706:707]" "e[982]" "e[984]" "e[986:987]";
createNode groupId -n "groupId79";
	rename -uid "4E0A6A17-3C40-69D7-9F5A-46B86E9A2053";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts27";
	rename -uid "615592BB-4B4D-645F-5137-399D207250C7";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:579]";
createNode deleteComponent -n "deleteComponent9";
	rename -uid "7A8D1E90-164A-42F3-015D-64B0997FEB19";
	setAttr ".dc" -type "componentList" 6 "f[87:93]" "f[136]" "f[139]" "f[159:165]" "f[208]" "f[211]";
createNode polyBridgeEdge -n "polyBridgeEdge5";
	rename -uid "AF46EB85-5D43-D250-0CEF-B3999F781C3B";
	setAttr ".ics" -type "componentList" 18 "e[150]" "e[153]" "e[156]" "e[159]" "e[162]" "e[165]" "e[168]" "e[193:200]" "e[267]" "e[270]" "e[273:274]" "e[295:301]" "e[310:316]" "e[356]" "e[398]" "e[402]" "e[405]" "e[409]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 141;
	setAttr ".sv2" 215;
	setAttr ".rev" yes;
	setAttr ".d" 1;
createNode polyBridgeEdge -n "polyBridgeEdge6";
	rename -uid "723421CE-CB48-4CDE-EFE9-E186F8491C7C";
	setAttr ".ics" -type "componentList" 2 "e[264]" "e[411]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 143;
	setAttr ".sv2" 213;
	setAttr ".d" 1;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "80E079AF-FD48-013B-6653-B9B0B5AF67CF";
	setAttr ".ics" -type "componentList" 3 "f[15:21]" "f[64]" "f[67]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -2.2204460492503131e-16 0 -1 0
		 -1 2.2204460492503131e-16 2.2204460492503131e-16 0 2.2341316122423018 1.3578873562295586 3.5858838520789247 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.8595233 0.96374947 3.5858839 ;
	setAttr ".rs" 388464941;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.8595233339677653 -3.1046137216605985e-08 3.5505285225653749 ;
	setAttr ".cbx" -type "double3" 1.8595233339677657 1.9274990248162895 3.6212391815924745 ;
createNode groupId -n "groupId80";
	rename -uid "D5267501-3141-DFC2-3E97-869C0C3BE6BE";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts28";
	rename -uid "114477FD-BB4A-5CC3-5C53-2A967970425B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]";
createNode groupId -n "groupId81";
	rename -uid "0B91BE33-3E46-C119-C4CA-C98140AD70EF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts29";
	rename -uid "9EEB835B-0B4A-01C8-A7E4-B5B2AD2EF766";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
createNode groupId -n "groupId82";
	rename -uid "38FB8D98-DB43-DD95-E624-0F9B7FA00420";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts30";
	rename -uid "AF2A445A-AA4E-EC6B-63A7-C9B2670CD74B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
createNode groupId -n "groupId83";
	rename -uid "50A1CA2E-7040-AF0C-7A1C-E292F6F236AD";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts31";
	rename -uid "9739C0F8-A24F-517F-7C1F-D9A716C5BCD1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "e[142]" "e[144]" "e[146:147]";
createNode polyChipOff -n "polyChipOff2";
	rename -uid "9C4409AF-7940-754E-2600-7D8116CBBFCB";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 2.0502456776410818 0 0 0 0 2.0502456776410818 0 0 0 0 7.1776411407192819 0
		 0 1.0251228388205409 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.91003388 1.0665517 0 ;
	setAttr ".rs" 1965329630;
	setAttr ".kft" no;
	setAttr ".dup" no;
createNode polyTweak -n "polyTweak14";
	rename -uid "553FC49A-094C-2802-92D6-379DEF4E477B";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[1]" -type "float3" 0.88773155 0 0 ;
	setAttr ".tk[2]" -type "float3" 0 0.040413585 0 ;
	setAttr ".tk[3]" -type "float3" 0.88773155 0.040413585 0 ;
	setAttr ".tk[4]" -type "float3" 0 0.040413585 0 ;
	setAttr ".tk[5]" -type "float3" 0.88773155 0.040413585 0 ;
	setAttr ".tk[7]" -type "float3" 0.88773155 0 0 ;
createNode polySeparate -n "polySeparate2";
	rename -uid "B8FA55D5-164C-6A9D-5A4B-2F9C61E0D108";
	setAttr ".ic" 2;
	setAttr -s 2 ".out";
createNode groupId -n "groupId84";
	rename -uid "DAFA3457-F845-26DD-C5E3-59B429330A0E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts32";
	rename -uid "9D394F73-4D45-B446-527B-21A1F1CD6C8F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:3]";
createNode groupId -n "groupId85";
	rename -uid "0A6AE5D3-8544-F684-E66E-7CB84BB6F269";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts33";
	rename -uid "35BB097A-B145-95CF-5BE9-B78EBDF0C674";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0]";
createNode groupId -n "groupId86";
	rename -uid "E2186B98-2F4A-A1FA-64E3-B88429F311CF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts34";
	rename -uid "F6BBBE9E-8946-CE15-5380-7FA855A9CABF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0]";
createNode objectSet -n "polySurfaceShape5HiddenFacesSet";
	rename -uid "DA96B3BB-D04C-8084-A961-30BCED0DCF81";
	setAttr ".ihi" 0;
createNode polyChipOff -n "polyChipOff3";
	rename -uid "5C2310C5-F34A-04EE-CC40-FAB0572F4A00";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 2.0502456776410818 0 0 0 0 2.0502456776410818 0 0 0 0 7.1776411407192819 0
		 0 1.0251228388205409 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.0251229 0 ;
	setAttr ".rs" 994369986;
	setAttr ".kft" no;
	setAttr ".dup" no;
createNode polySeparate -n "polySeparate3";
	rename -uid "59FA228D-E045-886E-E4B3-51B9D74FBA45";
	setAttr ".ic" 2;
	setAttr -s 2 ".out";
createNode groupId -n "groupId87";
	rename -uid "12C40570-844D-2612-A82F-5499B533C125";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts35";
	rename -uid "072E534C-4549-8124-4D55-B8A291E02C49";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:2]";
createNode groupId -n "groupId88";
	rename -uid "EA0F5168-6D40-898B-D65E-D8AEC380515A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts36";
	rename -uid "6E64FD83-DE43-1D95-B699-7196B4BA66CE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0]";
createNode polyCube -n "polyCube3";
	rename -uid "1438A646-8C4E-0899-A7A2-179979F91978";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	rename -uid "097D8D56-F14F-24F7-2231-CDA1FF883044";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.2049841028367552 -5.1960120375416112 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.50140822 1.9084951 0.57622296 ;
	setAttr ".rs" 1039667726;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.50140872566456052 1.8450141312356436 0.51490605092712283 ;
	setAttr ".cbx" -type "double3" -0.50140770353418329 1.9719759867412758 0.637539839102784 ;
createNode polyExtrudeFace -n "polyExtrudeFace10";
	rename -uid "8FDA3B1B-1544-4760-8496-F2A6941C7BA3";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 2.0502456776410818 0 0 0 0 2.0502456776410818 0 0 0 0 7.1776411407192819 0
		 0 1.0251228388205409 -1.8778957074221612 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.91003388 1.0665517 -5.4667158 ;
	setAttr ".rs" 1679198892;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.0251228388205409 0 -5.4667158499610515 ;
	setAttr ".cbx" -type "double3" 2.8451906164684289 2.1331032790350362 -5.4667158499610515 ;
createNode polyCube -n "polyCube4";
	rename -uid "3604BE65-0349-E599-D9CD-D8836E5F769E";
	setAttr ".cuv" 4;
createNode polyCBoolOp -n "polyCBoolOp1";
	rename -uid "C615C87D-7045-36AB-08C3-02B5E1292310";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
	setAttr ".op" 2;
	setAttr ".cls" 1;
	setAttr ".mg" -type "Int32Array" 2 54 -59 ;
createNode groupId -n "groupId89";
	rename -uid "0927D6D3-A64E-2937-FA3C-1DB673BBB31C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts37";
	rename -uid "896434FA-4C4B-C758-BE69-42812503548F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId90";
	rename -uid "49274379-1B46-67BC-DC02-5FAF8F5EE71A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId91";
	rename -uid "E935BC09-0F46-B9B0-9F17-F783C680CD94";
	setAttr ".ihi" 0;
createNode polyCube -n "polyCube5";
	rename -uid "CD8FFEE8-464C-DF48-E84A-F3BAEDCCA9A3";
	setAttr ".cuv" 4;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "3676402E-4548-4FE5-FA2B-D2BF0A60BB00";
	setAttr ".sa" 50;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode objectSet -n "set7";
	rename -uid "B0E73A7D-EC4C-C33F-954D-62BADB40AD55";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
createNode groupId -n "groupId92";
	rename -uid "B4096F6A-7E42-2DA5-FFE9-3AB6A7A27A0C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts38";
	rename -uid "FD079673-864C-941C-4033-3B940DA57151";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 5 "e[24:48]" "e[74:98]" "e[124:149]" "e[174:199]" "e[224:249]";
createNode deleteComponent -n "deleteComponent10";
	rename -uid "382B92DB-244A-28BA-01BD-FD902677C508";
	setAttr ".dc" -type "componentList" 3 "f[24:48]" "f[74:98]" "f[124:148]";
createNode polyCloseBorder -n "polyCloseBorder4";
	rename -uid "FB76AEB7-A746-EA3D-8602-9E9CC81492A9";
	setAttr ".ics" -type "componentList" 3 "e[74:75]" "e[100:101]" "e[126:127]";
createNode polyCloseBorder -n "polyCloseBorder5";
	rename -uid "F32E6F94-D74D-1312-912A-7F9F9A772178";
	setAttr ".ics" -type "componentList" 1 "e[*]";
createNode polyExtrudeFace -n "polyExtrudeFace11";
	rename -uid "C3D68034-5942-447D-3D3F-DB864482E2C6";
	setAttr ".ics" -type "componentList" 1 "f[75]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 3.3504643470199065e-17 0.15089149984756983 0
		 0 -1 2.2204460492503131e-16 0 0 0 7.6384943608136915 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -2.9802322e-07 -4.8428774e-08 7.6384945 ;
	setAttr ".rs" 284622793;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.0000005960464478 -9.6857547793514405e-08 7.4876028609661214 ;
	setAttr ".cbx" -type "double3" 1 3.3504643470199065e-17 7.7893858606612616 ;
createNode polyCBoolOp -n "polyCBoolOp2";
	rename -uid "DA4CB86F-A24D-7943-7D0F-798F31704119";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
	setAttr ".op" 2;
	setAttr ".cls" 1;
	setAttr ".mg" -type "Int32Array" 7 64 -66 -46 -47 -48 -49
		 -63 ;
createNode groupId -n "groupId93";
	rename -uid "EB917434-8C45-81F9-DD08-0E9658BC32F6";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts39";
	rename -uid "80D72324-004B-971E-6FB9-8885B8F6AFAB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:179]";
createNode groupId -n "groupId94";
	rename -uid "3104452A-114F-E2A2-DABF-06931B299551";
	setAttr ".ihi" 0;
createNode groupId -n "groupId95";
	rename -uid "4AA4F03C-EC47-594E-F099-82BA8480D553";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts40";
	rename -uid "6E36C5AF-064A-46B7-9270-9B9FB54094A4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:81]";
createNode groupId -n "groupId96";
	rename -uid "97A45BEF-394D-6FAA-2BF0-E8B710E5729A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId97";
	rename -uid "036D08F9-6E4F-B3B5-E309-5181740B60DB";
	setAttr ".ihi" 0;
createNode polyAutoProj -n "polyAutoProj1";
	rename -uid "6C0010E6-F643-AE40-C7DF-889757BF1296";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:581]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.031323216042642876 0 0 1;
	setAttr ".s" -type "double3" 7.177640438079834 7.177640438079834 7.177640438079834 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweak -n "polyTweak15";
	rename -uid "4C550171-F641-FD45-D9BE-2694778CA3FA";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk";
	setAttr ".tk[16]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[17]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[18]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[19]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[20]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[21]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[22]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[23]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[32]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[33]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[34]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[35]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[36]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[37]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[38]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[39]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[65]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[67]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[69]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[71]" -type "float3" 0 0 0.6151793 ;
	setAttr ".tk[379]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[380]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[381]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[382]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[383]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[384]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[385]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[386]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[394]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[395]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[396]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[397]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[398]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[399]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[400]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[401]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[425]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[427]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[429]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".tk[431]" -type "float3" 0 0 -1.3691998 ;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "3DE06FA8-EB4E-0460-FB42-92B19493E89C";
	setAttr ".uopa" yes;
	setAttr -s 1084 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 0.69136119 0.093670778 0.69616008 0.089426555
		 0.78819478 0.18311374 0.78260517 0.18860476 0.76725918 0.019581452 0.85929394 0.11326864
		 0.68614495 0.097390153 0.77707684 0.19403557 0.88458931 -0.095679231 0.97662407 -0.0019920468
		 0.68056846 0.10054392 0.77167034 0.19934678 0.72436714 -0.024080701 0.84169728 -0.13934138
		 0.67469281 0.10309751 0.7664448 0.20448011 0.75109243 0.026964493 0.71244901 -0.012372762
		 0.79880524 -0.18300353 0.6814751 -0.067742847 0.66858244 0.10502297 0.76145762 0.20937936
		 0.67380553 -0.051710039 0.67330086 -0.076063819 0.790631 -0.1913245 0.6623044 0.10629911
		 0.7501601 0.2204776 0.57349753 -0.17765909 0.69082767 -0.29291978 0.6022017 -0.0062187165
		 0.610376 0.0021022558 0.65592736 0.10691206 0.7091518 0.2607626 0.50239837 -0.107814
		 0.56532335 -0.18597999 0.68265349 -0.30124068 0.60604703 0.0068248659 0.5966121 -0.00072769821
		 0.64952111 0.10685504 0.61527365 0.16519894 0.4968088 -0.10232298 0.49422422 -0.1161349
		 0.52243125 -0.22964218 0.63976139 -0.34490287 0.60223544 0.011974096 0.59108377 0.0047031194
		 0.64315593 0.10612867 0.60726881 0.15705037 0.49128047 -0.096892163 0.48942536 -0.11189067
		 0.54915661 -0.17859697 0.51051313 -0.21793425 0.59686941 -0.388565 0.47953928 -0.27330431
		 0.59898293 0.017493576 0.58567727 0.010014325 0.6369015 0.10474106 0.60049063 0.15015048
		 0.48587391 -0.091580957 0.48420912 -0.10817133 0.47186977 -0.25727147 0.47136509
		 -0.28162521 0.58869523 -0.3968859 0.59632516 0.023322806 0.58045173 0.015147656 0.63082641
		 0.10270721 0.59559137 0.14516327 0.4806484 -0.086447626 0.47863263 -0.10501753 0.37156177
		 -0.38322049 0.4888919 -0.49848118 0.40026596 -0.21178012 0.40844014 -0.20345922 0.59429121
		 0.02939789 0.57546455 0.020046905 0.6249972 0.10004948 0.59045804 0.13993776 0.47566122
		 -0.081548378 0.47275698 -0.10246396 0.30046263 -0.31337541 0.36338755 -0.39154145
		 0.48071769 -0.50680214 0.40411121 -0.19873661 0.39467636 -0.2062891 0.59290361 0.035652265
		 0.56416702 0.031145141 0.61947781 0.09679696 0.58514684 0.13453121 0.46436369 -0.070450142
		 0.4666467 -0.10053848 0.29487303 -0.3078844 0.29228842 -0.32169634 0.32049555 -0.43520358
		 0.43782568 -0.55046427 0.40029955 -0.19358741 0.38914803 -0.20085828 0.59217733 0.042017445
		 0.52315873 0.071430147 0.6143285 0.092985392 0.57995677 0.12924796 0.4233554 -0.030165136
		 0.46036857 -0.099262372 0.2893447 -0.30245358 0.28748953 -0.31745213 0.34722078 -0.38415843
		 0.30857742 -0.42349565 0.39493358 -0.59412646 0.27760345 -0.47886577 0.3970471 -0.18806787
		 0.38374147 -0.19554707 0.59212035 0.048423678 0.5331763 0.081627563 0.60960591 0.088656411
		 0.57422501 0.12341326 0.45399147 -0.098649427 0.28393814 -0.29714236 0.28227335 -0.31373274
		 0.26993394 -0.46283293 0.2694293 -0.4871867 0.38675943 -0.60244739 0.39438939 -0.18223864
		 0.37851596 -0.19041374 0.59273332 0.054800734 0.54118115 0.089776129 0.56849319 0.11757857
		 0.60536158 0.083857507 0.44758534 -0.098706409 0.41333789 -0.040362507 0.27871263
		 -0.29200903 0.2766968 -0.310579 0.16962597 -0.58878195 0.2869561 -0.70404261 0.19833016
		 -0.41734159 0.20650432 -0.40902066 0.39235541 -0.1761636 0.37352878 -0.18551449 0.59400946
		 0.061078906 0.54795933 0.096676037 0.56330311 0.11229528 0.60164225 0.078641206 0.4412201
		 -0.099432826 0.40533298 -0.048511088 0.27372545 -0.28710979 0.27082121 -0.30802542
		 0.16145182 -0.59710288 0.27878195 -0.71236354 0.098526835 -0.51893681 0.20217545
		 -0.40429801 0.19274056 -0.41185057 0.39096785 -0.16990919 0.36223125 -0.17441626
		 0.59593487 0.067189187 0.55285859 0.10166325 0.55799192 0.10688874 0.59848845 0.073064789
		 0.43496567 -0.10082042 0.3985548 -0.055410981 0.26242793 -0.27601153 0.2647109 -0.30609989
		 0.090352684 -0.52725774 0.11855974 -0.64076507 0.23588987 -0.75602573 0.092937231
		 -0.51344579 0.1983638 -0.39914882 0.18721223 -0.40641975 0.3902415 -0.16354404 0.32122296
		 -0.13413125 0.42889059 -0.10285427 0.39365554 -0.060398221 0.22141963 -0.23572654
		 0.25843281 -0.30482382 0.08555375 -0.52301359 0.145285 -0.58971989 0.10664159 -0.62905711
		 0.19299778 -0.79968792 0.075667657 -0.68442726 0.0874089 -0.50801498 0.19511136 -0.39362925
		 0.18180567 -0.40110856 0.39018452 -0.1571378 0.33124053 -0.12393385 0.42306137 -0.10551198
		 0.38852221 -0.06562373 0.25205576 -0.30421084 0.080337539 -0.5192942 0.067998141
		 -0.66839439 -0.11915445 -0.88274729 -0.001824319 -0.99800795 0.082002342 -0.50270379
		 0.19245358 -0.3878001 0.17658018 -0.39597523 0.3907975 -0.15076071 0.33924532 -0.11578536
		 0.41754198 -0.10876448 0.38321108 -0.071030229 0.24564949 -0.30426788 0.21140212
		 -0.24592391 0.074761003 -0.51614046 -0.19025359 -0.81290215 0.0045685172 -0.61458212
		 0.076776847 -0.49757046 0.19041964 -0.38172501 0.17159297 -0.39107597 0.39207369
		 -0.14448255 0.34602356 -0.10888541 0.41239271 -0.11257605 0.37802088 -0.076313555
		 0.23928437 -0.3049942 0.20339721 -0.25407252 0.068885401 -0.51358688 0.00023964047
		 -0.60985947 -0.19584319 -0.80741113 0.071789637 -0.49267119 0.18903203 -0.37547064
		 0.16029546 -0.37997776 0.3939991 -0.13837226 0.35092276 -0.10389823 0.40767002 -0.11690508
		 0.37228912 -0.082148224 0.23302993 -0.30638182 0.19661903 -0.26097241 0.062775105
		 -0.51166135 -0.0035720021 -0.60471028 -0.20137152 -0.80198032 0.060492128 -0.48157299
		 0.18830572 -0.36910546 0.11928716 -0.33969274 0.39655262 -0.13249673 0.35605609 -0.098672718
		 0.36655742 -0.087982893 0.40342584 -0.12170394 0.2269548 -0.30841571 0.19171974 -0.26595965
		 0.056497052 -0.51038522 -0.0068244934 -0.59919077 -0.20677808 -0.79666913 0.019483835
		 -0.44128796 0.18824881 -0.36269915 0.12930471 -0.32949534 0.39970642 -0.12692027
		 0.36136723 -0.093266219 0.22112565 -0.31107342 0.18658644 -0.27118513 0.050119981
		 -0.50977224 -0.0094822198 -0.59336156 -0.21200357 -0.79153579 0.18886173 -0.35632214
		 0.13730952 -0.32134679 0.21560623 -0.31432587 0.18127531 -0.27659163;
	setAttr ".uvtk[250:499]" 0.043713689 -0.50982934 0.0094662607 -0.45148543 -0.011516094
		 -0.58728641 -0.21699078 -0.78663653 0.19013786 -0.35004401 0.14408779 -0.31444684
		 0.21045691 -0.31813753 0.17608514 -0.28187498 0.037348568 -0.51055562 0.0014614165
		 -0.45963398 -0.012903765 -0.5810321 -0.22828829 -0.77553833 0.19206335 -0.34393364
		 0.14898705 -0.3094596 0.2057343 -0.32246649 0.17035341 -0.28770962 0.031094134 -0.51194328
		 -0.0053167641 -0.46653387 -0.013630033 -0.57466686 -0.26929659 -0.73525333 0.19461684
		 -0.33805817 0.15412036 -0.30423412 0.16462162 -0.29354432 0.20148998 -0.32726544
		 0.025019005 -0.51397717 -0.010215998 -0.47152105 -0.013686985 -0.56826061 -0.072631106
		 -0.53505683 0.19777064 -0.33248168 0.15943152 -0.29882762 0.019189879 -0.51663482
		 -0.015349358 -0.47674659 -0.01307407 -0.56188357 -0.064626217 -0.52690816 0.01367037
		 -0.51988739 -0.020660564 -0.48215315 -0.01179795 -0.55560547 -0.057847977 -0.52000821
		 0.0085210949 -0.52369899 -0.025850609 -0.48743638 -0.0098725259 -0.54949516 -0.052948758
		 -0.51502109 0.0037984848 -0.52802795 -0.0315824 -0.49327108 -0.0073189139 -0.54361957
		 -0.047815442 -0.50979555 -0.037314191 -0.49910578 -0.00044582784 -0.5328269 -0.0041651577
		 -0.53804314 -0.042504296 -0.50438905 -0.48584616 -0.5474813 -0.48025653 -0.55289102
		 -0.39027581 -0.45016029 -0.39417911 -0.44596529 -0.40915745 -0.62170076 -0.32621923
		 -0.51215434 -0.49137455 -0.54213095 -0.39762253 -0.44138539 -0.29182726 -0.73525321
		 -0.20115663 -0.64156604 -0.49678102 -0.53689855 -0.40056843 -0.43647051 -0.1589004
		 -0.59790397 -0.28814864 -0.47281721 -0.50200647 -0.53184134 -0.4029842 -0.43127474
		 -0.25007784 -0.43347982 -0.11664404 -0.55424178 -0.50699377 -0.52701461 -0.40484408
		 -0.42585477 -0.10859092 -0.54592073 -0.22592111 -0.43236831 -0.51829123 -0.51608092
		 -0.40612686 -0.42027026 -0.01026687 -0.44432548 -0.12759706 -0.33077309 -0.31413442
		 -0.37148574 -0.2970202 -0.3635585 -0.55929959 -0.47639298 -0.4068189 -0.41458216
		 -0.12727645 -0.30659291 -0.0022138655 -0.43600455 -0.19869614 -0.26196328 -0.3026098
		 -0.35814884 -0.31845495 -0.36772186 -0.46681291 -0.38082933 -0.40691265 -0.40885291
		 -0.19133303 -0.24459884 0.04004249 -0.39234236 -0.089205772 -0.26725563 -0.20428577
		 -0.25655362 -0.30813819 -0.35279846 -0.32314503 -0.36443007 -0.45892674 -0.37268078
		 -0.40640706 -0.40314519 -0.19523638 -0.24040389 -0.051135063 -0.22791836 0.082298845
		 -0.34868017 -0.20981416 -0.25120324 -0.31354469 -0.34756607 -0.32815376 -0.36164665
		 -0.45224905 -0.36578095 -0.4053075 -0.39752164 -0.19867975 -0.23582393 0.09035179
		 -0.3403593 -0.026978403 -0.22680691 -0.21522063 -0.24597085 -0.31877011 -0.34250885
		 -0.33342564 -0.35940203 -0.44742239 -0.36079371 -0.40362608 -0.39204377 -0.20162565
		 -0.23090905 0.18867591 -0.23876402 0.071345717 -0.12521163 -0.11519164 -0.16592428
		 -0.098077476 -0.1579971 -0.22044608 -0.24091363 -0.32375741 -0.33768213 -0.33890367
		 -0.35772058 -0.44236517 -0.35556823 -0.40138149 -0.38677165 -0.20404139 -0.22571328
		 0.07166627 -0.10103151 0.19672886 -0.23044315 0.00024664402 -0.056401819 -0.10366711
		 -0.15258744 -0.11951211 -0.16216034 -0.22543338 -0.23608691 -0.33505487 -0.32674843
		 -0.34452718 -0.35662121 -0.43713269 -0.35016167 -0.39859813 -0.3817631 -0.20590129
		 -0.22029331 0.0076096952 -0.039037436 0.23898524 -0.18678096 0.10973698 -0.061694235
		 -0.0053429902 -0.050992161 -0.1091955 -0.14723706 -0.12420222 -0.15886861 -0.23673084
		 -0.22515321 -0.37606323 -0.2870605 -0.35023487 -0.35611561 -0.43201944 -0.34487832
		 -0.39530623 -0.37707293 -0.20718396 -0.21470869 0.0037063658 -0.034842461 0.14780766
		 -0.022356957 0.2812416 -0.14311877 -0.010871381 -0.04564178 -0.11460197 -0.14200467
		 -0.12921098 -0.15608519 -0.2777392 -0.18546528 -0.38593242 -0.29725802 -0.35596406
		 -0.35620931 -0.42637271 -0.33904374 -0.39154249 -0.37275246 -0.20787612 -0.2090207
		 0.0002630055 -0.0302625 0.28929454 -0.13479787 0.17196435 -0.02124548 -0.01627785
		 -0.040409386 -0.11982742 -0.13694745 -0.13448286 -0.15384057 -0.39381856 -0.30540651
		 -0.36165223 -0.35690141 -0.38734737 -0.36884928 -0.42072582 -0.33320898 -0.26787013
		 -0.17526788 -0.20796981 -0.20329139 -0.0026828349 -0.025347561 0.38761866 -0.033202589
		 0.27028847 0.080349803 0.083751082 0.039637119 0.10086527 0.047564328 -0.021503299
		 -0.03535217 -0.12481472 -0.13212073 -0.13996089 -0.15215912 -0.40049627 -0.3123064
		 -0.36723679 -0.35818437 -0.38276738 -0.36540568 -0.41561264 -0.32792568 -0.2599839
		 -0.16711926 -0.20746416 -0.19758362 -0.0050987005 -0.020151913 0.27060902 0.10452992
		 0.39567178 -0.024881572 0.19918939 0.14915961 0.09527564 0.052973986 0.07943064 0.043401092
		 -0.026490599 -0.030525446 -0.13611218 -0.12118703 -0.14558437 -0.15105975 -0.40532294
		 -0.31729364 -0.3726567 -0.36004406 -0.37785262 -0.36245999 -0.41038018 -0.32251912
		 -0.25330615 -0.16021937 -0.20636472 -0.19196019 -0.006958425 -0.014731765 0.20655245
		 0.16652399 0.43792796 0.01878047 0.3086797 0.14386719 0.19359976 0.15456927 0.08974725
		 0.058324367 0.074740469 0.046692789 -0.037788063 -0.019591749 -0.17712054 -0.0814991
		 -0.15129209 -0.15055415 -0.24847955 -0.15523219 -0.20468333 -0.18648231 -0.0082412362
		 -0.0091472864 0.20264912 0.17071897 0.34675038 0.18320447 0.48018432 0.062442645
		 0.18807137 0.15991965 0.084340781 0.063556761 0.069731861 0.049476355 -0.078796417
		 0.020096183 -0.18698952 -0.091696441 -0.15702134 -0.15064791 -0.24342227 -0.15000665
		 -0.20243871 -0.18121019 -0.0089333057 -0.0034591854 0.19920576 0.17529893 0.67211872
		 0.2607626 0.55478853 0.37431499 0.1826649 0.16515204 0.079115331 0.068613976 0.06445992
		 0.051720858 -0.19487584 -0.099845111 -0.16270944 -0.15133995 -0.23818979 -0.14460009
		 -0.19965526 -0.17620152 -0.068927377 0.030293554 -0.0090270638 0.0022700429 0.19625995
		 0.1802139 0.2826938 0.24519855 0.48368946 0.4431248 0.17743945 0.17020926 0.074128032
		 0.073440701 0.058981836 0.053402275 -0.20155355 -0.106745 -0.16829401 -0.15262291
		 -0.23307672 -0.13931692 -0.19636345 -0.17151147 -0.061041147 0.038442165 -0.0085214972
		 0.0079777539 0.19384408 0.18540955 0.47809982 0.44853446 0.27837336 0.24896252;
	setAttr ".uvtk[500:749]" 0.17245215 0.17503598 0.062830567 0.084374398 0.053358376
		 0.054501683 -0.20638016 -0.11173218 -0.17371383 -0.15448248 -0.22742984 -0.13348216
		 -0.19259962 -0.16719088 -0.05436343 0.045342058 -0.0074218512 0.013601333 0.1919843
		 0.19082966 0.47257143 0.45388484 0.27368319 0.25225419 0.16115469 0.18596968 0.021822214
		 0.12406233 0.047650754 0.055007368 -0.2114374 -0.11695766 -0.17890972 -0.15689841
		 -0.18840447 -0.1632877 -0.2217831 -0.12764758 -0.049536824 0.050329238 -0.0057405829
		 0.019079089 0.19070154 0.19641414 0.46716496 0.45911723 0.26867464 0.25503778 0.12014633
		 0.22565761 0.011953175 0.11386496 0.041921377 0.054913521 -0.21666986 -0.12236422
		 -0.18382463 -0.15984422 -0.044479549 0.055554748 -0.0034959912 0.024351209 0.19000947
		 0.20210224 0.46193951 0.46417445 0.26340264 0.25728226 0.0040669441 0.10571632 0.036233306
		 0.054221481 -0.039247096 0.060961306 -0.00071251392 0.029359877 0.13001537 0.23585498
		 0.18991566 0.20783147 0.45695221 0.46900117 0.25792468 0.25896382 -0.0026108027 0.098816425
		 0.030648828 0.05293861 -0.034133971 0.066244513 0.0025792718 0.034049928 0.1379016
		 0.24400359 0.19042128 0.21353921 0.44565475 0.47993487 0.2523011 0.26006311 -0.007437408
		 0.093829244 0.025228918 0.051078916 -0.028487086 0.072079241 0.0063431263 0.03837052
		 0.14457935 0.25090349 0.19152087 0.21916276 0.4046464 0.5196228 0.24659348 0.2605688
		 -0.012494683 0.088603735 0.020033002 0.04866299 0.01053822 0.0422737 -0.022840261
		 0.07791397 0.14940596 0.2558907 0.19320214 0.22464052 0.2108959 0.31942636 0.24086416
		 0.26047495 -0.017727137 0.083197176 0.015118122 0.04571718 0.15446323 0.26111618
		 0.19544679 0.22991264 0.20300967 0.31127775 0.23517603 0.25978291 0.15969568 0.26652277
		 0.19823021 0.23492131 0.19633192 0.30437785 0.22959155 0.25850004 0.16480881 0.27180594
		 0.20152199 0.23961136 0.19150531 0.29939067 0.22417164 0.25664037 0.17045563 0.27764067
		 0.20528585 0.24393195 0.1864481 0.29416516 0.21897578 0.25422442 0.209481 0.24783513
		 0.17610252 0.2834754 0.18121558 0.28875858 0.21406084 0.25127861 0.71514416 -0.0056800768
		 0.71514416 -0.0019920319 0.71476543 -0.0019920319 0.71476543 -0.0056800768 0.71514416
		 -0.0059945509 0.71476543 -0.0059945509 0.71514416 -0.0062608346 0.71476543 -0.0062608346
		 0.71514416 -0.0064533055 0.71476543 -0.0064533055 0.71514416 -0.0066549703 0.71476543
		 -0.0066549703 0.71514416 -0.0068636239 0.71476543 -0.0068636239 0.71514416 -0.0070675164
		 0.71476543 -0.0070675164 0.71514416 -0.0072926879 0.71476543 -0.0072926879 0.71476543
		 -0.0075178742 0.71514416 -0.0075178742 0.71476543 -0.0077217668 0.71514416 -0.0077217668
		 0.71476543 -0.0079304129 0.71514416 -0.0079304129 0.71476543 -0.0081320852 0.71514416
		 -0.0081320852 0.71476543 -0.0083245486 0.71514416 -0.0083245486 0.71476543 -0.0085908324
		 0.71514416 -0.0085908324 0.71476543 -0.0089053065 0.71514416 -0.0089053065 0.71476543
		 -0.009298861 0.71514416 -0.009298861 0.71476543 -0.013219684 0.71514416 -0.013219684
		 0.71514416 -0.013613224 0.71476543 -0.013613224 0.71514416 -0.013927698 0.71476543
		 -0.013927698 0.71514416 -0.014193982 0.71476543 -0.014193982 0.71514416 -0.014386445
		 0.71476543 -0.014386445 0.71514416 -0.014588118 0.71476543 -0.014588118 0.71514416
		 -0.014796764 0.71476543 -0.014796764 0.71514416 -0.015000671 0.71476543 -0.015000671
		 0.71514416 -0.015225828 0.71476543 -0.015225828 0.71476543 -0.015451014 0.71514416
		 -0.015451014 0.71476543 -0.015654892 0.71514416 -0.015654892 0.71476543 -0.015863568
		 0.71514416 -0.015863568 0.71476543 -0.01606521 0.71514416 -0.01606521 0.71476543
		 -0.016257703 0.71514416 -0.016257703 0.71476543 -0.016523987 0.71514416 -0.016523987
		 0.71476543 -0.016838461 0.71514416 -0.016838461 0.71476543 -0.017232001 0.71514416
		 -0.017232001 0.71476543 -0.021152824 0.71514416 -0.021152824 0.71514416 -0.021546364
		 0.71476543 -0.021546364 0.71514416 -0.021860838 0.71476543 -0.021860838 0.71514416
		 -0.022127092 0.71476543 -0.022127092 0.71514416 -0.022319615 0.71476543 -0.022319615
		 0.71514416 -0.022521257 0.71476543 -0.022521257 0.71514416 -0.022729933 0.71476543
		 -0.022729933 0.71514416 -0.022933781 0.71476543 -0.022933781 0.71514416 -0.023158967
		 0.71476543 -0.023158967 0.71476543 -0.023384154 0.71514416 -0.023384154 0.71476543
		 -0.023588061 0.71514416 -0.023588061 0.71476543 -0.023796678 0.71514416 -0.023796678
		 0.71476543 -0.02399838 0.71514416 -0.02399838 0.71476543 -0.024190843 0.71514416
		 -0.024190843 0.71476543 -0.024457097 0.71514416 -0.024457097 0.71476543 -0.024771571
		 0.71514416 -0.024771571 0.71476543 -0.025165141 0.71514416 -0.025165141 0.71476543
		 -0.029085934 0.71514416 -0.029085934 0.71514416 -0.029479504 0.71476543 -0.029479504
		 0.71514416 -0.029793978 0.71476543 -0.029793978 0.71514416 -0.030060232 0.71476543
		 -0.030060232 0.71514416 -0.030252695 0.71476543 -0.030252695 0.71514416 -0.030454397
		 0.71476543 -0.030454397 0.71514416 -0.030663073 0.71476543 -0.030663073 0.71514416
		 -0.030866921 0.71476543 -0.030866921 0.71514416 -0.031092107 0.71476543 -0.031092107
		 0.71476543 -0.031317294 0.71514416 -0.031317294 0.71476543 -0.031521201 0.71514416
		 -0.031521201 0.71476543 -0.031729817 0.71514416 -0.031729817 0.71476543 -0.03193152
		 0.71514416 -0.03193152 0.71476543 -0.032123983 0.71514416 -0.032123983 0.71476543
		 -0.032390237 0.71514416 -0.032390237 0.71476543 -0.032704711 0.71514416 -0.032704711
		 0.71476543 -0.040430844 0.71514416 -0.040430844 -0.47446212 0.48571026 -0.47834149
		 0.48204151 -0.46180877 0.45997915 -0.4575029 0.46405125 -0.48199302 0.47858828 -0.46586171
		 0.4561463 -0.47039738 0.48955429 -0.45299125 0.4683179 -0.4853766 0.47538841 -0.46961737
		 0.45259458 -0.46619186 0.49353147 -0.44832331 0.47273239 -0.48845533 0.47247687 -0.47303453
		 0.44936293 -0.46189153 0.49759826 -0.44355023 0.47724631 -0.49119532 0.46988565 -0.47607577
		 0.44648683 -0.45754358 0.50171018 -0.43872425 0.48181024 -0.4935666 0.46764308 -0.47870779
		 0.44399771 -0.43389827 0.4863742 -0.45319563 0.505822 -0.42912513 0.49088815 -0.44889531
		 0.50988883;
	setAttr ".uvtk[750:999]" -0.42445722 0.49530259 -0.44468975 0.51386601 -0.41994557
		 0.49956927 -0.44062498 0.51771009 -0.41563973 0.50364131 -0.43674564 0.52137882 -0.41158673
		 0.50747424 -0.43309411 0.52483201 -0.4078311 0.51102591 -0.42971051 0.52803195 -0.40441394
		 0.51425755 -0.42663181 0.53094345 -0.40137267 0.51713371 -0.42389181 0.53353465 -0.39874065
		 0.5196228 -0.4215205 0.53577721 0.41561216 -0.040432274 0.41561216 -0.036816537 0.41523343
		 -0.036816537 0.41523343 -0.040432274 0.41561216 -0.035131395 0.41523343 -0.035131395
		 0.41523343 -0.033446312 0.41561216 -0.033446312 0.41561216 -0.033125162 0.41523343
		 -0.033125162 0.41561216 -0.02920419 0.41523343 -0.02920419 0.41561216 -0.02888304
		 0.41523343 -0.02888304 0.41561216 -0.027197957 0.41523343 -0.027197957 0.41523343
		 -0.025512874 0.41561216 -0.025512874 0.41561216 -0.025191724 0.41523343 -0.025191724
		 0.41561216 -0.021270752 0.41523343 -0.021270752 0.41561216 -0.020949632 0.41523343
		 -0.020949632 0.41561216 -0.019264519 0.41523343 -0.019264519 0.41523343 -0.017579436
		 0.41561216 -0.017579436 0.41561216 -0.017258286 0.41523343 -0.017258286 0.41561216
		 -0.013337314 0.41523343 -0.013337314 0.41561216 -0.013016194 0.41523343 -0.013016194
		 0.41561216 -0.011331096 0.41523343 -0.011331096 0.41523343 -0.0096459985 0.41561216
		 -0.0096459985 0.41561216 -0.0019920319 0.41523343 -0.0019920319 -0.41905227 0.48571026
		 -0.42293161 0.48204157 -0.40639889 0.45997921 -0.40209302 0.46405125 -0.42658314
		 0.47858831 -0.41045183 0.45614633 -0.41498753 0.48955429 -0.3975814 0.46831793 -0.42996675
		 0.47538841 -0.41420752 0.45259458 -0.41078198 0.49353147 -0.39291346 0.47273239 -0.43304542
		 0.4724769 -0.41762465 0.44936299 -0.40648165 0.49759832 -0.38814038 0.47724631 -0.43578547
		 0.46988565 -0.42066589 0.44648689 -0.40213367 0.50171018 -0.38331434 0.4818103 -0.43815675
		 0.46764311 -0.42329794 0.44399774 -0.37848839 0.4863742 -0.39778578 0.505822 -0.37371528
		 0.49088815 -0.39348543 0.50988889 -0.36904734 0.49530265 -0.3892799 0.51386607 -0.36453572
		 0.49956927 -0.38521513 0.51771009 -0.36022985 0.50364137 -0.38133577 0.52137882 -0.35617688
		 0.50747424 -0.37768427 0.52483207 -0.35242125 0.51102597 -0.37430066 0.52803195 -0.34900406
		 0.51425761 -0.37122196 0.53094351 -0.34596282 0.51713371 -0.36848193 0.53353471 -0.3433308
		 0.5196228 -0.36611065 0.53577727 -0.60642838 0.48571023 -0.61030769 0.4820416 -0.59377503
		 0.45997918 -0.58946913 0.46405128 -0.61395925 0.47858831 -0.59782797 0.4561463 -0.60236371
		 0.48955426 -0.5849576 0.4683179 -0.61734283 0.47538841 -0.6015836 0.45259461 -0.59815806
		 0.49353153 -0.58028954 0.47273242 -0.62042153 0.47247693 -0.60500079 0.44936296 -0.59385777
		 0.49759829 -0.57551646 0.47724634 -0.62316155 0.46988568 -0.608042 0.44648686 -0.58950984
		 0.50171018 -0.57069051 0.48181027 -0.62553287 0.46764314 -0.61067402 0.44399777 -0.56586444
		 0.48637423 -0.58516186 0.50582206 -0.56109142 0.49088815 -0.58086157 0.50988883 -0.55642343
		 0.49530265 -0.57665598 0.51386607 -0.55191183 0.4995693 -0.57259125 0.51771009 -0.54760599
		 0.50364137 -0.56871188 0.52137882 -0.54355299 0.50747424 -0.56506038 0.52483201 -0.53979743
		 0.51102591 -0.5616768 0.52803189 -0.53638017 0.51425755 -0.55859804 0.53094351 -0.5333389
		 0.51713371 -0.55585814 0.53353465 -0.53070688 0.5196228 -0.55348676 0.53577727 -0.68335593
		 0.13954419 -0.68671554 0.13781515 -0.67982644 0.11956224 -0.67609739 0.12148139 -0.68987787
		 0.13618769 -0.68333638 0.11775582 -0.6798358 0.14135584 -0.67219025 0.1234922 -0.69280815
		 0.1346796 -0.68658882 0.11608194 -0.67619359 0.14323029 -0.66814768 0.12557271 -0.69547439
		 0.13330743 -0.68954819 0.11455888 -0.6724695 0.14514692 -0.66401398 0.12770012 -0.69784725
		 0.1320862 -0.692182 0.1132034 -0.66870403 0.14708483 -0.65983462 0.12985104 -0.69990087
		 0.13102931 -0.69446146 0.11203029 -0.65565515 0.13200201 -0.66493857 0.14902274 -0.65152156
		 0.13412939 -0.66121441 0.15093938 -0.647479 0.13620991 -0.65757227 0.15281381 -0.64357185
		 0.13822071 -0.65405214 0.15462546 -0.63984281 0.14013986 -0.65069246 0.15635452 -0.63633287
		 0.14194627 -0.6475302 0.15798198 -0.63308042 0.14362016 -0.64459991 0.15949006 -0.63012099
		 0.14514321 -0.64193368 0.16086225 -0.62748724 0.1464987 -0.6395607 0.16208348 -0.62520778
		 0.1476718 -0.63750714 0.16314037 -0.51102924 0.32560396 -0.50565535 0.33119357 -0.51546764
		 0.34062719 -0.52084154 0.33503759 -0.43730021 0.40229267 -0.4471125 0.4117263 -0.51634419
		 0.32007563 -0.52615649 0.32950926 -0.32449833 0.5196228 -0.33431062 0.52905643 -0.52154207
		 0.31466907 -0.53135443 0.3241027 -0.52656591 0.30944356 -0.5363782 0.31887719 -0.53136063
		 0.30445635 -0.54117298 0.31388998 -0.54222214 0.29315883 -0.55203444 0.30259246 -0.58164775
		 0.25215057 -0.59146005 0.26158419 0.070314527 0.42890698 0.072324812 0.42131302 0.10946345
		 0.42150965 0.10723221 0.42993814 0.10364109 0.3030152 0.14422274 0.29020569 0.068326414
		 0.43641719 0.10502535 0.43827456 0.066381931 0.44376248 0.10286725 0.44642681 0.064502716
		 0.4508613 0.10078126 0.45430669 0.062709153 0.45763648 0.098790586 0.46182653 -0.33552653
		 0.38277531 -0.33048034 0.37425259 -0.32824913 0.3749519 -0.3335163 0.38340533 -0.33751473
		 0.38215214 -0.33268714 0.37356094 -0.29348981 0.3858462 -0.30220005 0.39322048 -0.33945912
		 0.38154274 -0.3348453 0.37288451 -0.3413384 0.38095373 -0.3369312 0.37223077 -0.34313202
		 0.3803916 -0.33892199 0.37160683 -0.26672283 0.18772481 -0.26672283 0.17959221 -0.2295025
		 0.16994964 -0.2295025 0.178976 -0.26672283 0.052903175 -0.2295025 0.02933196 -0.26672283
		 0.1957677 -0.2295025 0.18790375 -0.26672283 0.20363402 -0.2295025 0.19663423 -0.26672283
		 0.21123639 -0.2295025 0.20507306 -0.26672283 0.21849215 -0.2295025 0.21312629 -0.45375288
		 0.052494712 -0.42947873 0.075016424 -0.43334332 0.081811555 -0.45723468 0.05861675
		 -0.45030919 0.046439663 -0.42565644 0.06829571;
	setAttr ".uvtk[1000:1083]" -0.49354821 0.18767001 -0.51147604 0.15398955 -0.44694144
		 0.040518098 -0.42191839 0.061723046 -0.4436864 0.034794785 -0.41830549 0.055370495
		 -0.44057977 0.029332407 -0.41485733 0.049307585 -0.11705744 0.18772481 -0.11705744
		 0.17959221 -0.079837084 0.16994964 -0.079837084 0.178976 -0.11705744 0.052903175
		 -0.079837084 0.02933196 -0.11705744 0.1957677 -0.079837084 0.18790375 -0.11705744
		 0.20363402 -0.079837084 0.19663423 -0.11705744 0.21123639 -0.079837084 0.20507306
		 -0.11705744 0.21849215 -0.079837084 0.21312629 0.36661655 0.25792772 0.36623788 0.25792772
		 0.36623788 0.25771201 0.36661655 0.25771201 0.36661655 0.25814107 0.36623788 0.25814107
		 0.36623788 0.25496799 0.36661655 0.25496799 0.36661655 0.25834975 0.36623788 0.25834975
		 0.36623788 0.25043973 0.36661655 0.25043973 0.36661655 0.25855142 0.36623788 0.25855142
		 0.36661655 0.25874391 0.36623788 0.25874391 0.36661655 0.25917992 0.36623788 0.25917992
		 0.36661655 0.26076263 0.36623788 0.26076263 -0.63240707 0.38406098 -0.6319887 0.38415933
		 -0.6319887 0.38426077 -0.63240707 0.38415241 -0.63240707 0.38397056 -0.6319887 0.38405898
		 -0.6319887 0.38584131 -0.63240707 0.38557637 -0.63240707 0.38388216 -0.6319887 0.38396084
		 -0.63240707 0.38379669 -0.6319887 0.38386598 -0.63240707 0.38371515 -0.6319887 0.38377547
		 -0.25778002 0.44729757 -0.2597903 0.43970361 -0.22741878 0.42149925 -0.22518754 0.42992774
		 -0.29110655 0.32140577 -0.26217806 0.29019529 -0.25579184 0.45480776 -0.22298068
		 0.43826416 -0.25384736 0.46215305 -0.22082263 0.44641638 -0.25196815 0.46925187 -0.21873659
		 0.45429626 -0.25017458 0.47602707 -0.21674591 0.46181613 0.045992136 0.1035041 0.046713233
		 0.1133824 0.044482052 0.11408172 0.043981969 0.10413415 0.047980368 0.10288094 0.048920035
		 0.11269074 0.0097227693 0.12497607 0.012665749 0.11394938 0.049924731 0.10227153
		 0.0510782 0.11201432 0.051804006 0.10168252 0.053164124 0.11136055 0.053597629 0.10112036
		 0.05515492 0.11073658;
createNode psdFileTex -n "PSD_lambert1_color";
	rename -uid "F8A21DCA-1D45-F3F9-F78A-34B165053F86";
	setAttr ".ftn" -type "string" "/Users/rayne/cnm190/models//sourceimages/bottega-windows-wall.psd";
	setAttr ".cs" -type "string" "sRGB";
	setAttr ".lsn" -type "string" "lambert1.color";
createNode place2dTexture -n "place2dTexture1";
	rename -uid "4D1AA5F5-B144-137C-BD74-7AB82D88DD90";
createNode polyAutoProj -n "polyAutoProj2";
	rename -uid "FABD0D5C-D549-9298-8B43-4D83DE9D9B6E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:255]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -2.1230010641746091 1;
	setAttr ".s" -type "double3" 3.8703138828277588 3.8703138828277588 3.8703138828277588 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyNormal -n "polyNormal3";
	rename -uid "1FCC815B-0E4E-EA40-DADC-FC821EC1534A";
	setAttr ".ics" -type "componentList" 1 "f[0:261]";
	setAttr ".nm" 2;
createNode polySplitVert -n "polySplitVert1";
	rename -uid "74905A7A-D54D-3FC1-1700-C5978BFF6751";
	setAttr ".ics" -type "componentList" 3 "vtx[82:83]" "vtx[86]" "vtx[89]";
createNode polyChipOff -n "polyChipOff4";
	rename -uid "F7B32E25-6547-8B12-A302-E29911B58931";
	setAttr ".ics" -type "componentList" 1 "f[51:53]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -2.1230010641746091 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 0 -2.1230011 ;
	setAttr ".rs" 1908602832;
	setAttr ".kft" no;
	setAttr ".dup" no;
createNode polySplitVert -n "polySplitVert2";
	rename -uid "453AC38B-B54D-AC72-A1AE-FBACA7630D0B";
	setAttr ".ics" -type "componentList" 3 "vtx[306]" "vtx[309]" "vtx[313]";
createNode polyChipOff -n "polyChipOff5";
	rename -uid "40FFA8CF-DD4F-D626-6FF5-09A04BAF36C6";
	setAttr ".ics" -type "componentList" 1 "f[51:53]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -2.1230010641746091 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 0 -2.1230011 ;
	setAttr ".rs" 345172106;
	setAttr ".kft" no;
	setAttr ".dup" no;
createNode polyAutoProj -n "polyAutoProj3";
	rename -uid "DCEBF61B-9647-3D7F-422F-F9BB4F3CE8ED";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:11]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 1.1073583521137715 1;
	setAttr ".s" -type "double3" 3.8703136444091797 3.8703136444091797 3.8703136444091797 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "42074FA1-E849-26A2-E067-D18072C7D74D";
	setAttr ".uopa" yes;
	setAttr -s 48 ".uvtk[0:47]" -type "float2" 0.29956236 -0.078763902 0.44265488
		 -0.13119167 0.46390772 -0.073185682 0.32081521 -0.020757914 0.44904763 0.39120144
		 0.3194766 -0.10511911 0.50516605 -0.15359581 0.63473707 0.3427248 0.25626543 -0.131001
		 0.51540738 0.14089602 0.41368198 0.2378493 0.15454002 -0.034047723 -0.87628597 -0.30767637
		 -0.55957365 0.024624646 -0.68129569 0.1406365 -0.99800801 -0.19166452 0.73930061
		 0.84572363 0.451776 -0.25521234 0.67403811 -0.31325918 0.96156275 0.78767675 -0.5785985
		 0.23758984 -0.87015069 -0.068312585 -0.76842529 -0.16526592 -0.47687313 0.1406365
		 -0.064891636 -0.26195961 0.22666046 0.04394269 0.12493506 0.14089608 -0.16661704
		 -0.16500622 0.37087792 -0.030162632 0.37087792 -0.0018784424 0.36516774 -0.0018784424
		 0.36516774 -0.030162632 0.072342157 -0.6231488 0.085097134 -0.6231488 0.085097134
		 -0.60879856 0.072342157 -0.60879856 -0.13081777 -0.28809631 0.41179702 0.34535944
		 0.11152562 0.63154471 -0.46311882 0.028616071 -0.4394851 0.13751723 -0.16758814 -0.1216247
		 -0.4591403 -0.42752707 -0.59856641 -0.60480708 -0.023921967 -0.0018784963 -0.3241933
		 0.28430673 -0.93086743 -0.2880947 -0.73103726 -0.16838515;
createNode polyAutoProj -n "polyAutoProj4";
	rename -uid "D6210F81-0A43-77EF-A10F-63AE96324062";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:2]";
	setAttr ".ix" -type "matrix" 2.0502456776410818 0 0 0 0 2.0502456776410818 0 0 0 0 7.1776411407192819 0
		 0 1.0251228388205409 0 1;
	setAttr ".s" -type "double3" 7.1776402850777812 7.1776402850777812 7.1776402850777812 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "409F500C-0243-5C0A-C774-D4A65083D082";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk[0:11]" -type "float2" 0.072003305 -0.64542741 0.072003305
		 -0.63901973 0.065844536 -0.63901973 0.065844536 -0.64542741 -0.34073734 -0.29642886
		 -0.0018522888 -0.64171594 0.64995694 -0.0019920322 0.31107187 0.34329504 -0.68705475
		 0.048615038 -0.34816968 -0.29642886 0.30318046 0.34329504 -0.035704613 0.68833894;
createNode polyAutoProj -n "polyAutoProj5";
	rename -uid "1114321D-794E-39F8-A84D-F7809FA92787";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:121]";
	setAttr ".ix" -type "matrix" 2.3209442420421267 0 0 0 0 5.1535314727727026e-16 2.3209442420421267 0
		 0 -2.3209442420421267 5.1535314727727026e-16 0 0.22144663179391605 0.90012333193416383 1.8586220401509119 1;
	setAttr ".s" -type "double3" 3.9684083022793013 3.9684083022793013 3.9684083022793013 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweak -n "polyTweak16";
	rename -uid "785AAA88-3C49-4BB0-D25C-DD9B6C237D7C";
	setAttr ".uopa" yes;
	setAttr -s 66 ".tk";
	setAttr ".tk[13]" -type "float3" 0 0 0.0021680172 ;
	setAttr ".tk[15]" -type "float3" 0 0 0.0021680172 ;
	setAttr ".tk[29]" -type "float3" 5.9604645e-08 -6.9388939e-17 0.38782632 ;
	setAttr ".tk[30]" -type "float3" 5.9604645e-08 -6.9388939e-17 0.38782632 ;
	setAttr ".tk[31]" -type "float3" 0.0031258948 0 0 ;
	setAttr ".tk[32]" -type "float3" 0.01247095 0 0 ;
	setAttr ".tk[33]" -type "float3" 0.027932156 0 0 ;
	setAttr ".tk[34]" -type "float3" 0.049340278 0 0 ;
	setAttr ".tk[35]" -type "float3" 0.076460361 0 3.7252903e-09 ;
	setAttr ".tk[36]" -type "float3" 0.10899578 7.4505806e-09 3.7252903e-09 ;
	setAttr ".tk[37]" -type "float3" 0.14658974 0 0 ;
	setAttr ".tk[38]" -type "float3" 0.18883049 0 0 ;
	setAttr ".tk[39]" -type "float3" 0.23525521 0 0 ;
	setAttr ".tk[40]" -type "float3" 0.28535521 0 0 ;
	setAttr ".tk[41]" -type "float3" 0.33858174 0 0 ;
	setAttr ".tk[42]" -type "float3" 0.39435133 0 -0.015461199 ;
	setAttr ".tk[43]" -type "float3" 0 0 -0.0093449727 ;
	setAttr ".tk[46]" -type "float3" 0 0 -0.0067744306 ;
	setAttr ".tk[47]" -type "float3" 0 0 -0.016119344 ;
	setAttr ".tk[48]" -type "float3" 0 0 -0.031580552 ;
	setAttr ".tk[49]" -type "float3" -0.30496681 -2.7755576e-17 -0.0098703057 ;
	setAttr ".tk[50]" -type "float3" -0.25174037 0 0 ;
	setAttr ".tk[51]" -type "float3" -0.20164035 0 0 ;
	setAttr ".tk[52]" -type "float3" -0.15521568 0 0 ;
	setAttr ".tk[53]" -type "float3" -0.11297499 0 0 ;
	setAttr ".tk[54]" -type "float3" -0.075381055 0 0 ;
	setAttr ".tk[55]" -type "float3" -0.042845741 0 0 ;
	setAttr ".tk[56]" -type "float3" -0.015725538 0 0 ;
	setAttr ".tk[57]" -type "float3" 0.0056823939 0 0 ;
	setAttr ".tk[58]" -type "float3" 0.021143571 0 0 ;
	setAttr ".tk[59]" -type "float3" 0.030488566 0 0 ;
	setAttr ".tk[60]" -type "float3" 0.033614948 -6.9388939e-17 0.38782632 ;
	setAttr ".tk[61]" -type "float3" 5.9604645e-08 -6.9388939e-17 0.38782632 ;
	setAttr ".tk[62]" -type "float3" 0.0031258948 0 0 ;
	setAttr ".tk[63]" -type "float3" 0.01247095 0 0 ;
	setAttr ".tk[64]" -type "float3" 0.027932156 0 0 ;
	setAttr ".tk[65]" -type "float3" 0.049340278 0 0 ;
	setAttr ".tk[66]" -type "float3" 0.076460361 0 3.7252903e-09 ;
	setAttr ".tk[67]" -type "float3" 0.10899578 7.4505806e-09 3.7252903e-09 ;
	setAttr ".tk[68]" -type "float3" 0.14658974 0 0 ;
	setAttr ".tk[69]" -type "float3" 0.18883049 0 0 ;
	setAttr ".tk[70]" -type "float3" 0.23525521 0 0 ;
	setAttr ".tk[71]" -type "float3" 0.28535521 0 0 ;
	setAttr ".tk[72]" -type "float3" 0.33858174 0 0 ;
	setAttr ".tk[73]" -type "float3" 0.39435133 0 -0.015461199 ;
	setAttr ".tk[74]" -type "float3" 0 0 -0.0093449727 ;
	setAttr ".tk[77]" -type "float3" 0 0 -0.0067744306 ;
	setAttr ".tk[78]" -type "float3" 0 0 -0.016119344 ;
	setAttr ".tk[79]" -type "float3" 0 0 -0.031580552 ;
	setAttr ".tk[80]" -type "float3" -0.30496678 -5.9604645e-08 -0.0098702908 ;
	setAttr ".tk[81]" -type "float3" -0.25174037 0 0 ;
	setAttr ".tk[82]" -type "float3" -0.20164035 0 0 ;
	setAttr ".tk[83]" -type "float3" -0.15521568 0 0 ;
	setAttr ".tk[84]" -type "float3" -0.11297499 0 0 ;
	setAttr ".tk[85]" -type "float3" -0.075381055 0 0 ;
	setAttr ".tk[86]" -type "float3" -0.042845741 0 0 ;
	setAttr ".tk[87]" -type "float3" -0.015725538 0 0 ;
	setAttr ".tk[88]" -type "float3" 0.0056823939 0 0 ;
	setAttr ".tk[89]" -type "float3" 0.021143571 0 0 ;
	setAttr ".tk[90]" -type "float3" 0.030488566 0 0 ;
	setAttr ".tk[91]" -type "float3" 0.033614948 -6.9388939e-17 0.38782632 ;
	setAttr ".tk[92]" -type "float3" 5.9604645e-08 -6.9388939e-17 0.38782632 ;
	setAttr ".tk[106]" -type "float3" 0 0 0.0021680172 ;
	setAttr ".tk[108]" -type "float3" 0 0 0.0021680172 ;
	setAttr ".tk[122]" -type "float3" 5.9604645e-08 -6.9388939e-17 0.38782632 ;
	setAttr ".tk[123]" -type "float3" 5.9604645e-08 -6.9388939e-17 0.38782632 ;
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "0A9F2CAC-C44B-4A4D-BC1C-29B858CB3C1E";
	setAttr ".uopa" yes;
	setAttr -s 264 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" -0.7333461 -0.067946821 -0.75631684
		 -0.091649383 -0.69950408 -0.14670794 -0.67653334 -0.12300538 -0.93053102 -0.27141416
		 -0.87371826 -0.32647273 -0.71088153 -0.044766501 -0.65406877 -0.099825054 -0.68916917
		 -0.022362322 -0.63235641 -0.077420875 -0.66844702 -0.00098001212 -0.61163425 -0.05603857
		 -0.64894199 0.019146487 -0.59212923 -0.03591207 -0.63086796 0.037796333 -0.57405519
		 -0.017262224 -0.61442268 0.054765616 -0.55760992 -0.00029294193 -0.59978652 0.069868088
		 -0.54297376 0.01480953 -0.58711994 0.082938224 -0.53030717 0.027879663 -0.57656145
		 0.093833104 -0.51974869 0.038774543 -0.56220746 0.10864443 -0.5053947 0.053585868
		 -0.68472683 -0.50813806 -0.69719863 -0.5157975 -0.66791523 -0.56347954 -0.65544349
		 -0.55582011 -0.67375708 -0.50140119 -0.64447379 -0.54908323 -0.66108179 -0.49361676
		 -0.63179845 -0.54129881 -0.64683986 -0.48487025 -0.61755651 -0.5325523 -0.63118714
		 -0.47525734 -0.6019038 -0.52293932 -0.61429524 -0.46488339 -0.5850119 -0.51256543
		 -0.59634936 -0.45386213 -0.56706601 -0.50154412 -0.57754582 -0.44231415 -0.54826248
		 -0.48999619 -0.55809093 -0.43036616 -0.52880752 -0.47804818 -0.53819776 -0.41814899
		 -0.50891435 -0.46583101 -0.38732356 -0.3254914 -0.35804015 -0.37317345 -0.36910594
		 -0.42466554 -0.38481459 -0.3977671 -0.47052699 -0.44782296 -0.45481837 -0.4747214
		 -0.53796756 -0.1355179 -0.62368 -0.18557374 -0.35374331 -0.45097154 -0.43945572 -0.50102741
		 -0.33889514 -0.47639659 -0.42460757 -0.52645242 -0.3247242 -0.50066203 -0.41043657
		 -0.55071789 -0.31138569 -0.52350199 -0.39709806 -0.57355785 -0.29902554 -0.54466665
		 -0.38473797 -0.59472251 -0.28777933 -0.56392407 -0.3734917 -0.61397988 -0.27777028
		 -0.58106285 -0.36348265 -0.63111871 -0.26910818 -0.59589529 -0.35482055 -0.64595115
		 -0.14664137 -0.62161124 -0.14664137 -0.62189066 -0.14565456 -0.62189066 -0.14565456
		 -0.62161124 -0.14664137 -0.62215358 -0.14565456 -0.62215358 -0.14664137 -0.62131852
		 -0.14565456 -0.62131852 -0.14664137 -0.6223973 -0.14565456 -0.6223973 -0.14664137
		 -0.62101567 -0.14565456 -0.62101567 -0.14664137 -0.62261897 -0.14565456 -0.62261897
		 -0.14664137 -0.62070596 -0.14565456 -0.62070596 -0.14664137 -0.62281632 -0.14565456
		 -0.62281632 -0.14664137 -0.61768663 -0.14565456 -0.61768663 -0.14664137 -0.62298709
		 -0.14565456 -0.62298709 0.41331771 0.12316501 0.43388361 0.1087105 0.48391894 0.17222272
		 0.46021634 0.19519347 0.60862523 -0.058804382 0.66368371 -0.0019918478 0.39135385
		 0.13539101 0.43703604 0.2176581 0.36823207 0.14525399 0.41463187 0.23937054 0.34420609
		 0.15264615 0.39324954 0.26009268 0.31953892 0.15748629 0.37312305 0.27959776 0.29450125
		 0.15972175 0.3544732 0.29767179 0.26936659 0.15932766 0.33750394 0.31411713 0.24441126
		 0.15630834 0.32240146 0.32875335 0.219908 0.1506972 0.30933133 0.34141994 0.19612563
		 0.14255542 0.29843643 0.35197842 0.17332441 0.13197242 0.28362513 0.36633247 0.15175465
		 0.11906382 0.10387671 0.18836948 0.132523 0.1031272 0.080905974 0.16466694 -0.0054176748
		 -0.032642275 -0.062230229 0.022416174 -0.13679601 -0.17477196 -0.20264503 -0.12247205
		 -0.15212168 -0.19449359 -0.22561575 -0.14617458 -0.16434753 -0.21645769 -0.24808043
		 -0.16935495 -0.17421067 -0.23957926 -0.37120113 -0.3310588 -0.18160281 -0.26360518
		 -0.35634115 -0.34545997 -0.18644315 -0.28827253 -0.34327084 -0.35812673 -0.18867826
		 -0.3133103 -0.32816836 -0.37276292 -0.18828434 -0.33844471 -0.31119928 -0.38920808
		 -0.185265 -0.36340004 -0.29254922 -0.40728232 -0.17965391 -0.38790336 -0.27242294
		 -0.4267872 -0.17151213 -0.41168573 -0.25104043 -0.44750953 -0.16092888 -0.43448672
		 -0.22863644 -0.46922177 -0.14802052 -0.45605671 -0.20545612 -0.4916864 -0.13292812
		 -0.4761591 -0.18175355 -0.51465714 0.039982792 -0.64556342 -0.0019885574 -0.68887192
		 -0.11439878 0.67308992 -0.15799457 0.66160452 -0.14093047 0.55249518 -0.1067692 0.55594039
		 -0.48863372 0.57449722 -0.46110451 0.47000271 -0.071763575 0.68432224 -0.072434962
		 0.55579633 -0.030555904 0.69517845 -0.038303912 0.55206376 0.0087724328 0.70553952
		 -0.0047499537 0.54478425 0.045790732 0.71529204 0.027859032 0.53403717 0.080093324
		 0.72432911 0.059166193 0.5199405 0.11130458 0.73255175 0.088828325 0.50264847 0.13908219
		 0.73986983 0.11652058 0.48235047 0.16312227 0.74620318 0.14193934 0.45926934 0.1831609
		 0.75148243 0.16480613 0.43365759 0.21040317 0.75865942 0.1848706 0.40579581 0.30530521
		 0.42646185 0.20191288 0.37598935 0.31679061 0.38286608 0.21414411 0.34414274 0.39200348
		 0.11769387 0.28750873 0.090164602 0.46221089 -0.14879712 0.34880364 -0.16699365 0.47369623
		 -0.19239289 0.35385102 -0.20073274 0.48492855 -0.23502821 0.35370672 -0.23506695
		 0.52329028 -0.50996554 0.34997433 -0.26919809 0.49595833 -0.51716614 0.34269482 -0.3027519
		 0.47191846 -0.52349949 0.33194762 -0.33536118 0.44414073 -0.53081757 0.31785095 -0.36666822
		 0.41292948 -0.53904021 0.30055892 -0.39633036 0.37862688 -0.54807723 0.2802611 -0.42402256
		 0.34160846 -0.5578298 0.2571798 -0.44944134 0.30228013 -0.56819093 0.23156804 -0.4723081
		 0.26107258 -0.57904708 0.2037062 -0.49237254 0.21843737 -0.5902794 0.17389995 -0.50941473
		 0.17484146 -0.6017648 0.14247543 -0.52324784 -0.15579748 -0.68887204 -0.1767832 -0.6092152
		 0.094883114 0.1910069 0.27101728 0.37260792 0.21420455 0.42771038 0.038070381 0.24610937
		 0.07189393 0.16730416 0.015081197 0.22240663 -0.068632215 0.022416353 -0.12544495
		 0.077518821 -0.20915912 -0.12247223 -0.26597184 -0.067369759 -0.23214795 -0.14617458
		 -0.2889607 -0.091072112 -0.25463027 -0.16935471 -0.31144297 -0.11425224 -0.39518479
		 -0.31427178 -0.45199752 -0.25916931 -0.41995099 -0.018075895 -0.40421671 -0.0019920319
		 -0.46102935 0.053585868 -0.47676364 0.037502006 -0.43643713 -0.034928299 -0.49324977
		 0.020649601 -0.45349452 -0.052364614 -0.51030719 0.0032132864 -0.47093615 -0.070193753
		 -0.52774882 -0.014615852 -0.60686582 -0.20914331 -0.66367847 -0.15356541 -0.74279559
		 -0.34809297 -0.79960823 -0.29251507 -0.76023722 -0.36592203 -0.81704986 -0.31034413;
	setAttr ".uvtk[250:263]" -0.77729452 -0.38335833 -0.83410716 -0.32778043 -0.79378068
		 -0.40021077 -0.85059333 -0.34463286 -0.809515 -0.41629463 -0.86632764 -0.36071673
		 -0.31396335 -0.74966908 -0.25890487 -0.69285649 -0.30221343 -0.65088511 -0.35727191
		 -0.70769769 0.028012455 -0.69461066 0.084824979 -0.74966908 0.13988346 -0.69285649
		 0.083070934 -0.63779807;
createNode polyAutoProj -n "polyAutoProj6";
	rename -uid "0C699FDB-4047-5404-F57C-72ABD5973DBD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.376360790259008 -2.6812261722653372 1;
	setAttr ".s" -type "double3" 3.8703140494769768 3.8703140494769768 3.8703140494769768 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "6DE7035C-FA4F-1ECD-36C9-52AA3D5B418C";
	setAttr ".uopa" yes;
	setAttr -s 20 ".uvtk[0:19]" -type "float2" -0.26333782 0.78936213 -0.45525366
		 0.51796508 -0.1770587 0.32124218 0.014857143 0.59263921 0.4337486 0.28667641 0.24183276
		 0.015279353 0.0082296729 -0.37779844 0.087710857 -0.37779844 0.087710857 -0.2963264
		 0.0082296729 -0.2963264 0.01013279 -0.5018205 0.089613974 -0.5018205 0.2390649 -0.0024133027
		 0.46462119 0.12625751 0.33707392 0.15045291 0.11151767 0.0217821 -0.46601132 0.090506136
		 -0.21947581 -0.3014566 0.001442492 -0.37063226 -0.245093 0.021330476;
createNode polyAutoProj -n "polyAutoProj7";
	rename -uid "DAC6C8AA-8D40-1CC0-4F9B-4F8A5AFBD9E2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.376360790259008 -3.239483148583139 1;
	setAttr ".s" -type "double3" 3.8703140494769768 3.8703140494769768 3.8703140494769768 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "CEDA2E2C-1D43-2FAB-DDFC-9BB334919A32";
	setAttr ".uopa" yes;
	setAttr -s 20 ".uvtk[0:19]" -type "float2" -0.26333782 0.78936213 -0.45525366
		 0.51796508 -0.1770587 0.32124218 0.014857143 0.59263921 0.4337486 0.28667641 0.24183276
		 0.015279353 0.0082296729 -0.37779844 0.087710857 -0.37779844 0.087710857 -0.2963264
		 0.0082296729 -0.2963264 0.01013279 -0.5018205 0.089613974 -0.5018205 0.2390649 -0.0024133027
		 0.46462119 0.12625751 0.33707392 0.15045291 0.11151767 0.0217821 -0.46601132 0.090506136
		 -0.21947581 -0.3014566 0.001442492 -0.37063226 -0.245093 0.021330476;
createNode polyAutoProj -n "polyAutoProj8";
	rename -uid "BF4E0E4D-BE49-09CB-990C-5C99701C50E6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.376360790259008 -4.6232519705704833 1;
	setAttr ".s" -type "double3" 3.8703140494769768 3.8703140494769768 3.8703140494769768 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "FFE59B65-8649-608C-D92B-F7B3C622BF85";
	setAttr ".uopa" yes;
	setAttr -s 20 ".uvtk[0:19]" -type "float2" -0.26333782 0.78936213 -0.45525366
		 0.51796508 -0.1770587 0.32124218 0.014857143 0.59263921 0.4337486 0.28667641 0.24183276
		 0.015279353 0.0082296729 -0.37779844 0.087710857 -0.37779844 0.087710857 -0.2963264
		 0.0082296729 -0.2963264 0.01013279 -0.5018205 0.089613974 -0.5018205 0.2390649 -0.0024133027
		 0.46462119 0.12625751 0.33707392 0.15045291 0.11151767 0.0217821 -0.46601132 0.090506136
		 -0.21947581 -0.3014566 0.001442492 -0.37063226 -0.245093 0.021330476;
createNode polyAutoProj -n "polyAutoProj9";
	rename -uid "E5B8B261-CA4D-2A91-4D62-24A1CB40C604";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.3763607902590078 -5.890884280344225 1;
	setAttr ".s" -type "double3" 3.8703140494769768 3.8703140494769768 3.8703140494769768 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "E3E5FAD7-E849-25AF-2C5C-788B093E023C";
	setAttr ".uopa" yes;
	setAttr -s 20 ".uvtk[0:19]" -type "float2" -0.26333782 0.78936213 -0.45525366
		 0.51796508 -0.1770587 0.32124218 0.014857143 0.59263921 0.4337486 0.28667641 0.24183276
		 0.015279353 0.0082296729 -0.37779844 0.087710857 -0.37779844 0.087710857 -0.2963264
		 0.0082296729 -0.2963264 0.01013279 -0.5018205 0.089613974 -0.5018205 0.2390649 -0.0024133027
		 0.46462119 0.12625751 0.33707392 0.15045291 0.11151767 0.0217821 -0.46601132 0.090506136
		 -0.21947581 -0.3014566 0.001442492 -0.37063226 -0.245093 0.021330476;
createNode polyAutoProj -n "polyAutoProj10";
	rename -uid "10D71799-3A4F-CFC1-D194-12A40F9C6B70";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.3763607902590078 -7.2814177312961093 1;
	setAttr ".s" -type "double3" 3.8703140494769768 3.8703140494769768 3.8703140494769768 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "2C0E0E8B-DF42-CDCF-A56C-EFAC93A19BDE";
	setAttr ".uopa" yes;
	setAttr -s 20 ".uvtk[0:19]" -type "float2" -0.26333782 0.78936213 -0.45525366
		 0.51796508 -0.1770587 0.32124218 0.014857143 0.59263921 0.4337486 0.28667641 0.24183276
		 0.015279353 0.0082296729 -0.37779844 0.087710857 -0.37779844 0.087710857 -0.2963264
		 0.0082296729 -0.2963264 0.01013279 -0.5018205 0.089613974 -0.5018205 0.2390649 -0.0024133027
		 0.46462119 0.12625751 0.33707392 0.15045291 0.11151767 0.0217821 -0.46601132 0.090506136
		 -0.21947581 -0.3014566 0.001442492 -0.37063226 -0.245093 0.021330476;
createNode polyAutoProj -n "polyAutoProj11";
	rename -uid "E2B40808-E74F-AB02-D231-E09DE8B2D5C9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.376360790259008 -8.671951182247998 1;
	setAttr ".s" -type "double3" 3.8703140494769768 3.8703140494769768 3.8703140494769768 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "909F8480-1C49-6A75-0BB0-A8A6C789AE6D";
	setAttr ".uopa" yes;
	setAttr -s 20 ".uvtk[0:19]" -type "float2" -0.26333782 0.78936213 -0.45525366
		 0.51796508 -0.1770587 0.32124218 0.014857143 0.59263921 0.4337486 0.28667641 0.24183276
		 0.015279353 0.0082296729 -0.37779844 0.087710857 -0.37779844 0.087710857 -0.2963264
		 0.0082296729 -0.2963264 0.01013279 -0.5018205 0.089613974 -0.5018205 0.2390649 -0.0024133027
		 0.46462119 0.12625751 0.33707392 0.15045291 0.11151767 0.0217821 -0.46601132 0.090506136
		 -0.21947581 -0.3014566 0.001442492 -0.37063226 -0.245093 0.021330476;
createNode animCurveTU -n "pCube4_visibility";
	rename -uid "BD6FB59E-6747-B7A8-62CA-13B6C298FED6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCube4_translateX";
	rename -uid "961694D1-A740-4155-A74B-1198BA99B61A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.5117128267902782;
createNode animCurveTL -n "pCube4_translateY";
	rename -uid "EC7A2D0F-9E43-824C-BA82-C1873D2B0368";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.2049841028367543;
createNode animCurveTL -n "pCube4_translateZ";
	rename -uid "D61DA74F-AE49-7F95-9347-B2AA062F2C37";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -5.1960120375416077;
createNode animCurveTA -n "pCube4_rotateX";
	rename -uid "9DBD131F-1947-EAAB-0C1D-8E9C28EEFB25";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pCube4_rotateY";
	rename -uid "1BBF6E22-164B-CDBA-9A1D-D58790A32091";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pCube4_rotateZ";
	rename -uid "375967B3-714A-114E-A39B-CEA3B7EB2148";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "pCube4_scaleX";
	rename -uid "717B0A70-5745-8965-9EA1-989E62E95EB4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 4.2871255296034541;
createNode animCurveTU -n "pCube4_scaleY";
	rename -uid "435A13C8-F748-E512-B13B-44AE00C65BF4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.19413119373823251;
createNode animCurveTU -n "pCube4_scaleZ";
	rename -uid "7EAC2FDE-494D-0F57-7094-38AA9E3E4EEE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.18067922107111714;
createNode polyAutoProj -n "polyAutoProj12";
	rename -uid "71ECDBDC-2144-13FE-BACA-8FB05AC333C5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.3763607902590078 -5.1960120375416112 1;
	setAttr ".s" -type "double3" 3.8703140494769768 3.8703140494769768 3.8703140494769768 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweak -n "polyTweak17";
	rename -uid "712E2051-B143-C53C-9AD7-9E90146C20F3";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  -0.091733545 3.70235968 31.77053642
		 -0.18895757 2.64326072 31.78678894 -0.091733545 3.39809823 31.77053642 -0.18895757
		 2.33899927 31.78678894 -0.091733545 3.39809823 32.09180069 -0.18895757 2.33899927
		 32.1080513 -0.091733545 3.70235968 32.09180069 -0.18895757 2.64326072 32.1080513;
createNode polyTweakUV -n "polyTweakUV11";
	rename -uid "61A94D25-A34E-D3F1-B82C-619877116074";
	setAttr ".uopa" yes;
	setAttr -s 20 ".uvtk[0:19]" -type "float2" -0.26333782 0.78936213 -0.45525366
		 0.51796508 -0.1770587 0.32124218 0.014857143 0.59263921 0.4337486 0.28667641 0.24183276
		 0.015279353 0.0082296729 -0.37779844 0.087710857 -0.37779844 0.087710857 -0.2963264
		 0.0082296729 -0.2963264 0.01013279 -0.5018205 0.089613974 -0.5018205 0.2390649 -0.0024133027
		 0.46462119 0.12625751 0.33707392 0.15045291 0.11151767 0.0217821 -0.46601132 0.090506136
		 -0.21947581 -0.3014566 0.001442492 -0.37063226 -0.245093 0.021330476;
createNode polyAutoProj -n "polyAutoProj13";
	rename -uid "64631064-454C-BCD6-BEF6-A0BCB3ACE1E0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:9]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.2049841028367552 -5.1960120375416112 1;
	setAttr ".s" -type "double3" 0.74825756977420421 0.74825756977420421 0.74825756977420421 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweak -n "polyTweak18";
	rename -uid "15B620BB-2A4B-D0E3-2263-54A95A1AC181";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  0.052375901 0.49401012 0 0.052375901
		 0.49401012 0 0.052375901 -0.046530426 0 0.052375901 -0.046530426 0;
createNode polyAutoProj -n "polyAutoProj14";
	rename -uid "52CC497B-8843-2754-B635-66A3AEFBBA0C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 0.13512841007918389 0 0 0 0 3.1171321414407878 0 0 0 0 0.18067922107111714 0
		 -1.0461145939227876 -6.7017644640501715 -5.1960120375416112 1;
	setAttr ".s" -type "double3" 2.1687119814444644 2.1687119814444644 2.1687119814444644 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyAutoProj -n "polyAutoProj15";
	rename -uid "E9F1AD24-5446-B4A2-FD7B-77BBFA805653";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.3763607902590078 -6.5818370584803727 1;
	setAttr ".s" -type "double3" 3.8703140494769768 3.8703140494769768 3.8703140494769768 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV12";
	rename -uid "D6050616-A44F-EAEC-6E21-099E4CE2B634";
	setAttr ".uopa" yes;
	setAttr -s 20 ".uvtk[0:19]" -type "float2" -0.26333782 0.78936213 -0.45525366
		 0.51796508 -0.1770587 0.32124218 0.014857143 0.59263921 0.4337486 0.28667641 0.24183276
		 0.015279353 0.0082296729 -0.37779844 0.087710857 -0.37779844 0.087710857 -0.2963264
		 0.0082296729 -0.2963264 0.01013279 -0.5018205 0.089613974 -0.5018205 0.2390649 -0.0024133027
		 0.46462119 0.12625751 0.33707392 0.15045291 0.11151767 0.0217821 -0.46601132 0.090506136
		 -0.21947581 -0.3014566 0.001442492 -0.37063226 -0.245093 0.021330476;
createNode polyAutoProj -n "polyAutoProj16";
	rename -uid "CD60FDDB-604D-BECC-490A-2B9AAE9E9DEA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:9]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.2049841028367552 -6.5818370584803727 1;
	setAttr ".s" -type "double3" 0.74825756977420421 0.74825756977420421 0.74825756977420421 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyAutoProj -n "polyAutoProj17";
	rename -uid "756B9FD2-6D4C-DFCD-3218-8C8C411A9FE3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 0.13512841007918389 0 0 0 0 3.1171321414407878 0 0 0 0 0.18067922107111714 0
		 -1.0461145939227876 -6.7017644640501715 -6.5818370584803727 1;
	setAttr ".s" -type "double3" 2.1687119814444644 2.1687119814444644 2.1687119814444644 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyAutoProj -n "polyAutoProj18";
	rename -uid "65C757E5-8142-945F-68E3-F896CAA9A1F8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.3763607902590078 -7.9676620794191351 1;
	setAttr ".s" -type "double3" 3.8703140494769768 3.8703140494769768 3.8703140494769768 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV13";
	rename -uid "70FFC3F0-0149-907D-565C-74A6D8CAC1FC";
	setAttr ".uopa" yes;
	setAttr -s 20 ".uvtk[0:19]" -type "float2" -0.26333782 0.78936213 -0.45525366
		 0.51796508 -0.1770587 0.32124218 0.014857143 0.59263921 0.4337486 0.28667641 0.24183276
		 0.015279353 0.0082296729 -0.37779844 0.087710857 -0.37779844 0.087710857 -0.2963264
		 0.0082296729 -0.2963264 0.01013279 -0.5018205 0.089613974 -0.5018205 0.2390649 -0.0024133027
		 0.46462119 0.12625751 0.33707392 0.15045291 0.11151767 0.0217821 -0.46601132 0.090506136
		 -0.21947581 -0.3014566 0.001442492 -0.37063226 -0.245093 0.021330476;
createNode polyAutoProj -n "polyAutoProj19";
	rename -uid "064F8709-F845-179E-C0DA-9ABD91CAC9DC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:9]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.2049841028367552 -7.9676620794191351 1;
	setAttr ".s" -type "double3" 0.74825756977420421 0.74825756977420421 0.74825756977420421 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyAutoProj -n "polyAutoProj20";
	rename -uid "2D0521E4-4E41-B6AD-7914-A694C26DB1CB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 0.13512841007918389 0 0 0 0 3.1171321414407878 0 0 0 0 0.18067922107111714 0
		 -1.0461145939227876 -6.7017644640501715 -7.9676620794191351 1;
	setAttr ".s" -type "double3" 2.1687119814444644 2.1687119814444644 2.1687119814444644 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".aoon" yes;
	setAttr ".msaa" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 5 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 7 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
	setAttr -s 5 ".r";
select -ne :lightList1;
	setAttr -s 2 ".l";
select -ne :defaultTextureList1;
select -ne :lambert1;
select -ne :initialShadingGroup;
	setAttr -s 48 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 25 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :initialMaterialInfo;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "renderman";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultLightSet;
	setAttr -s 2 ".dsm";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHideFaceDataSet;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "polyNormal1.out" "polySurfaceShape1.i";
connectAttr "groupId3.id" "polySurfaceShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape1.iog.og[0].gco";
connectAttr "polyTweakUV3.out" "polySurfaceShape6.i";
connectAttr "groupId87.id" "polySurfaceShape6.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape6.iog.og[0].gco";
connectAttr "polyTweakUV3.uvtk[0]" "polySurfaceShape6.uvst[0].uvtw";
connectAttr "polyExtrudeFace10.out" "polySurfaceShape7.i";
connectAttr "groupId88.id" "polySurfaceShape7.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape7.iog.og[0].gco";
connectAttr "polyChipOff3.out" "polySurfaceShape4.i";
connectAttr "groupId84.id" "polySurfaceShape4.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape4.iog.og[0].gco";
connectAttr "groupParts34.og" "polySurfaceShape5.i";
connectAttr "groupId85.id" "polySurfaceShape5.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape5.iog.og[0].gco";
connectAttr "groupId86.id" "polySurfaceShape5.iog.og[1].gid";
connectAttr "polyChipOff2.out" "polySurfaceShape2.i";
connectAttr "groupId4.id" "polySurfaceShape2.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape2.iog.og[0].gco";
connectAttr "groupId1.id" "pCubeShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "groupParts1.og" "pCubeShape1.i";
connectAttr "groupId2.id" "pCubeShape1.ciog.cog[0].cgid";
connectAttr "groupId5.id" "pTorusShape1.iog.og[0].gid";
connectAttr "set1.mwc" "pTorusShape1.iog.og[0].gco";
connectAttr "polyExtrudeFace1.out" "pTorusShape1.i";
connectAttr "groupId6.id" "pTorusShape2.iog.og[0].gid";
connectAttr "set2.mwc" "pTorusShape2.iog.og[0].gco";
connectAttr "polyTweakUV4.out" "pTorusShape2.i";
connectAttr "polyTweakUV4.uvtk[0]" "pTorusShape2.uvst[0].uvtw";
connectAttr "groupId37.id" "pTorusShape3.iog.og[4].gid";
connectAttr "set3.mwc" "pTorusShape3.iog.og[4].gco";
connectAttr "groupId38.id" "pTorusShape3.iog.og[5].gid";
connectAttr "set4.mwc" "pTorusShape3.iog.og[5].gco";
connectAttr "groupId39.id" "pTorusShape3.iog.og[6].gid";
connectAttr "set5.mwc" "pTorusShape3.iog.og[6].gco";
connectAttr "groupId40.id" "pTorusShape3.iog.og[7].gid";
connectAttr "set6.mwc" "pTorusShape3.iog.og[7].gco";
connectAttr "groupId73.id" "pTorusShape3.iog.og[8].gid";
connectAttr ":initialShadingGroup.mwc" "pTorusShape3.iog.og[8].gco";
connectAttr "groupParts22.og" "pTorusShape3.i";
connectAttr "groupId74.id" "pTorusShape3.ciog.cog[0].cgid";
connectAttr "groupId41.id" "pTorusShape4.iog.og[4].gid";
connectAttr "set3.mwc" "pTorusShape4.iog.og[4].gco";
connectAttr "groupId42.id" "pTorusShape4.iog.og[5].gid";
connectAttr "set4.mwc" "pTorusShape4.iog.og[5].gco";
connectAttr "groupId43.id" "pTorusShape4.iog.og[6].gid";
connectAttr "set5.mwc" "pTorusShape4.iog.og[6].gco";
connectAttr "groupId44.id" "pTorusShape4.iog.og[7].gid";
connectAttr "set6.mwc" "pTorusShape4.iog.og[7].gco";
connectAttr "groupId66.id" "pTorusShape4.iog.og[10].gid";
connectAttr ":initialShadingGroup.mwc" "pTorusShape4.iog.og[10].gco";
connectAttr "groupId67.id" "pTorusShape4.ciog.cog[2].cgid";
connectAttr "groupId45.id" "pTorusShape5.iog.og[4].gid";
connectAttr "set3.mwc" "pTorusShape5.iog.og[4].gco";
connectAttr "groupId46.id" "pTorusShape5.iog.og[5].gid";
connectAttr "set4.mwc" "pTorusShape5.iog.og[5].gco";
connectAttr "groupId47.id" "pTorusShape5.iog.og[6].gid";
connectAttr "set5.mwc" "pTorusShape5.iog.og[6].gco";
connectAttr "groupId48.id" "pTorusShape5.iog.og[7].gid";
connectAttr "set6.mwc" "pTorusShape5.iog.og[7].gco";
connectAttr "groupId59.id" "pTorusShape5.iog.og[8].gid";
connectAttr ":initialShadingGroup.mwc" "pTorusShape5.iog.og[8].gco";
connectAttr "groupId60.id" "pTorusShape5.ciog.cog[0].cgid";
connectAttr "groupId49.id" "pTorusShape6.iog.og[4].gid";
connectAttr "set3.mwc" "pTorusShape6.iog.og[4].gco";
connectAttr "groupId50.id" "pTorusShape6.iog.og[5].gid";
connectAttr "set4.mwc" "pTorusShape6.iog.og[5].gco";
connectAttr "groupId51.id" "pTorusShape6.iog.og[6].gid";
connectAttr "set5.mwc" "pTorusShape6.iog.og[6].gco";
connectAttr "groupId52.id" "pTorusShape6.iog.og[7].gid";
connectAttr "set6.mwc" "pTorusShape6.iog.og[7].gco";
connectAttr "groupId57.id" "pTorusShape6.iog.og[8].gid";
connectAttr ":initialShadingGroup.mwc" "pTorusShape6.iog.og[8].gco";
connectAttr "groupId58.id" "pTorusShape6.ciog.cog[0].cgid";
connectAttr "groupId80.id" "pTorusShape7.iog.og[4].gid";
connectAttr "set3.mwc" "pTorusShape7.iog.og[4].gco";
connectAttr "groupId81.id" "pTorusShape7.iog.og[5].gid";
connectAttr "set4.mwc" "pTorusShape7.iog.og[5].gco";
connectAttr "groupId82.id" "pTorusShape7.iog.og[6].gid";
connectAttr "set5.mwc" "pTorusShape7.iog.og[6].gco";
connectAttr "groupId83.id" "pTorusShape7.iog.og[7].gid";
connectAttr "set6.mwc" "pTorusShape7.iog.og[7].gco";
connectAttr "groupId93.id" "pTorusShape7.iog.og[8].gid";
connectAttr ":initialShadingGroup.mwc" "pTorusShape7.iog.og[8].gco";
connectAttr "groupParts39.og" "pTorusShape7.i";
connectAttr "groupId94.id" "pTorusShape7.ciog.cog[0].cgid";
connectAttr "groupId61.id" "pTorus8Shape.iog.og[0].gid";
connectAttr "set3.mwc" "pTorus8Shape.iog.og[0].gco";
connectAttr "groupId62.id" "pTorus8Shape.iog.og[1].gid";
connectAttr "set4.mwc" "pTorus8Shape.iog.og[1].gco";
connectAttr "groupId63.id" "pTorus8Shape.iog.og[2].gid";
connectAttr "set5.mwc" "pTorus8Shape.iog.og[2].gco";
connectAttr "groupId64.id" "pTorus8Shape.iog.og[3].gid";
connectAttr "set6.mwc" "pTorus8Shape.iog.og[3].gco";
connectAttr "groupId65.id" "pTorus8Shape.iog.og[4].gid";
connectAttr ":initialShadingGroup.mwc" "pTorus8Shape.iog.og[4].gco";
connectAttr "polyBridgeEdge2.out" "pTorus8Shape.i";
connectAttr "polyBridgeEdge4.out" "pTorus9Shape.i";
connectAttr "groupId68.id" "pTorus9Shape.iog.og[0].gid";
connectAttr "set3.mwc" "pTorus9Shape.iog.og[0].gco";
connectAttr "groupId69.id" "pTorus9Shape.iog.og[1].gid";
connectAttr "set4.mwc" "pTorus9Shape.iog.og[1].gco";
connectAttr "groupId70.id" "pTorus9Shape.iog.og[2].gid";
connectAttr "set5.mwc" "pTorus9Shape.iog.og[2].gco";
connectAttr "groupId71.id" "pTorus9Shape.iog.og[3].gid";
connectAttr "set6.mwc" "pTorus9Shape.iog.og[3].gco";
connectAttr "groupId72.id" "pTorus9Shape.iog.og[4].gid";
connectAttr ":initialShadingGroup.mwc" "pTorus9Shape.iog.og[4].gco";
connectAttr "polyTweakUV1.out" "pTorus10Shape.i";
connectAttr "groupId75.id" "pTorus10Shape.iog.og[0].gid";
connectAttr "set3.mwc" "pTorus10Shape.iog.og[0].gco";
connectAttr "groupId76.id" "pTorus10Shape.iog.og[1].gid";
connectAttr "set4.mwc" "pTorus10Shape.iog.og[1].gco";
connectAttr "groupId77.id" "pTorus10Shape.iog.og[2].gid";
connectAttr "set5.mwc" "pTorus10Shape.iog.og[2].gco";
connectAttr "groupId78.id" "pTorus10Shape.iog.og[3].gid";
connectAttr "set6.mwc" "pTorus10Shape.iog.og[3].gco";
connectAttr "groupId79.id" "pTorus10Shape.iog.og[4].gid";
connectAttr ":initialShadingGroup.mwc" "pTorus10Shape.iog.og[4].gco";
connectAttr "polyTweakUV1.uvtk[0]" "pTorus10Shape.uvst[0].uvtw";
connectAttr "polyTweakUV8.out" "beam_Shape4.i";
connectAttr "polyTweakUV8.uvtk[0]" "beam_Shape4.uvst[0].uvtw";
connectAttr "polyTweakUV11.out" "beam_7Shape.i";
connectAttr "polyTweakUV11.uvtk[0]" "beam_7Shape.uvst[0].uvtw";
connectAttr "polyAutoProj13.out" "support_Shape1.i";
connectAttr "polyAutoProj14.out" "vertical_Shape1.i";
connectAttr "polyTweakUV12.out" "beam_Shape8.i";
connectAttr "polyTweakUV12.uvtk[0]" "beam_Shape8.uvst[0].uvtw";
connectAttr "polyAutoProj16.out" "support_Shape2.i";
connectAttr "polyAutoProj17.out" "vertical_Shape2.i";
connectAttr "polyTweakUV13.out" "beam_Shape9.i";
connectAttr "polyTweakUV13.uvtk[0]" "beam_Shape9.uvst[0].uvtw";
connectAttr "pCube4_visibility.o" "support_3.v";
connectAttr "pCube4_translateX.o" "support_3.tx";
connectAttr "pCube4_translateY.o" "support_3.ty";
connectAttr "pCube4_translateZ.o" "support_3.tz";
connectAttr "pCube4_rotateX.o" "support_3.rx";
connectAttr "pCube4_rotateY.o" "support_3.ry";
connectAttr "pCube4_rotateZ.o" "support_3.rz";
connectAttr "pCube4_scaleX.o" "support_3.sx";
connectAttr "pCube4_scaleY.o" "support_3.sy";
connectAttr "pCube4_scaleZ.o" "support_3.sz";
connectAttr "polyAutoProj19.out" "support_Shape3.i";
connectAttr "polyAutoProj20.out" "vertical_Shape3.i";
connectAttr "polyTweakUV9.out" "beam_Shape5.i";
connectAttr "polyTweakUV9.uvtk[0]" "beam_Shape5.uvst[0].uvtw";
connectAttr "polyTweakUV10.out" "beam_Shape6.i";
connectAttr "polyTweakUV10.uvtk[0]" "beam_Shape6.uvst[0].uvtw";
connectAttr "polyTweakUV7.out" "beam_Shape3.i";
connectAttr "polyTweakUV7.uvtk[0]" "beam_Shape3.uvst[0].uvtw";
connectAttr "polyTweakUV6.out" "beam_Shape2.i";
connectAttr "polyTweakUV6.uvtk[0]" "beam_Shape2.uvst[0].uvtw";
connectAttr "polyTweakUV5.out" "beam_Shape1.i";
connectAttr "polyTweakUV5.uvtk[0]" "beam_Shape1.uvst[0].uvtw";
connectAttr "groupId89.id" "pCubeShape12.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape12.iog.og[1].gco";
connectAttr "groupParts37.og" "pCubeShape12.i";
connectAttr "groupId90.id" "pCubeShape12.ciog.cog[1].cgid";
connectAttr "polyTweakUV2.out" "polySurface6Shape.i";
connectAttr "groupId91.id" "polySurface6Shape.ciog.cog[0].cgid";
connectAttr "polyTweakUV2.uvtk[0]" "polySurface6Shape.uvst[0].uvtw";
connectAttr "polyCube5.out" "pCubeShape13.i";
connectAttr "groupId92.id" "pCylinderShape1.iog.og[1].gid";
connectAttr "set7.mwc" "pCylinderShape1.iog.og[1].gco";
connectAttr "groupId95.id" "pCylinderShape1.iog.og[2].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape1.iog.og[2].gco";
connectAttr "groupParts40.og" "pCylinderShape1.i";
connectAttr "groupId96.id" "pCylinderShape1.ciog.cog[0].cgid";
connectAttr "polyChipOff5.out" "pTorus11Shape.i";
connectAttr "groupId93.id" "pTorus11Shape.iog.og[0].gid";
connectAttr "groupId80.id" "pTorus11Shape.iog.og[1].gid";
connectAttr "groupId81.id" "pTorus11Shape.iog.og[2].gid";
connectAttr "groupId82.id" "pTorus11Shape.iog.og[3].gid";
connectAttr "groupId83.id" "pTorus11Shape.iog.og[4].gid";
connectAttr "groupId92.id" "pTorus11Shape.iog.og[5].gid";
connectAttr "groupId97.id" "pTorus11Shape.ciog.cog[0].cgid";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "svgBlinn1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "PxrSurface1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "PxrSurface2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "svgBlinn1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "PxrSurface1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "PxrSurface2SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyTweak1.out" "polyChipOff1.ip";
connectAttr "pCubeShape1.wm" "polyChipOff1.mp";
connectAttr "polyCube1.out" "polyTweak1.ip";
connectAttr "pCubeShape1.o" "polySeparate1.ip";
connectAttr "polyChipOff1.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polySeparate1.out[0]" "groupParts2.ig";
connectAttr "groupId3.id" "groupParts2.gi";
connectAttr "polySeparate1.out[1]" "groupParts3.ig";
connectAttr "groupId4.id" "groupParts3.gi";
connectAttr "groupParts2.og" "polyNormal1.ip";
connectAttr "groupParts3.og" "polyNormal2.ip";
connectAttr "groupId5.msg" "set1.gn" -na;
connectAttr "pTorusShape1.iog.og[0]" "set1.dsm" -na;
connectAttr "polyTorus1.out" "groupParts4.ig";
connectAttr "groupId5.id" "groupParts4.gi";
connectAttr "groupParts4.og" "deleteComponent1.ig";
connectAttr "polyTweak2.out" "polyCloseBorder1.ip";
connectAttr "deleteComponent1.og" "polyTweak2.ip";
connectAttr "polyCloseBorder1.out" "polyExtrudeFace1.ip";
connectAttr "pTorusShape1.wm" "polyExtrudeFace1.mp";
connectAttr "groupId6.msg" "set2.gn" -na;
connectAttr "pTorusShape2.iog.og[0]" "set2.dsm" -na;
connectAttr "polyTorus2.out" "groupParts5.ig";
connectAttr "groupId6.id" "groupParts5.gi";
connectAttr "groupParts5.og" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polySplitEdge1.ip";
connectAttr "polyTweak3.out" "polyMergeVert1.ip";
connectAttr "pTorusShape2.wm" "polyMergeVert1.mp";
connectAttr "polySplitEdge1.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polyMergeVert2.ip";
connectAttr "pTorusShape2.wm" "polyMergeVert2.mp";
connectAttr "polyMergeVert1.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polyMergeVert3.ip";
connectAttr "pTorusShape2.wm" "polyMergeVert3.mp";
connectAttr "polyMergeVert2.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polyMergeVert4.ip";
connectAttr "pTorusShape2.wm" "polyMergeVert4.mp";
connectAttr "polyMergeVert3.out" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polyCloseBorder2.ip";
connectAttr "polyMergeVert4.out" "polyTweak7.ip";
connectAttr ":rmanDefaultDisplay.msg" ":rmanGlobals.displays[0]";
connectAttr ":PxrPathTracer.msg" ":rmanGlobals.ri_integrator";
connectAttr "d_openexr.msg" ":rmanDefaultDisplay.displayType";
connectAttr "Ci.msg" ":rmanDefaultDisplay.displayChannels[0]";
connectAttr "a.msg" ":rmanDefaultDisplay.displayChannels[1]";
connectAttr "svgBlinn1.oc" "svgBlinn1SG.ss";
connectAttr "svgBlinn1SG.msg" "materialInfo1.sg";
connectAttr "svgBlinn1.msg" "materialInfo1.m";
connectAttr "PxrSurface1.oc" "PxrSurface1SG.rman__surface";
connectAttr "lambert2.oc" "PxrSurface1SG.ss";
connectAttr "PxrSurface1SG.msg" "materialInfo2.sg";
connectAttr "lambert2.msg" "materialInfo2.m";
connectAttr "PxrSurface2.oc" "PxrSurface2SG.rman__surface";
connectAttr "PxrSurface3.oc" "PxrSurface2SG.ss";
connectAttr "PxrSurface2SG.msg" "materialInfo3.sg";
connectAttr "PxrSurface3.msg" "materialInfo3.m";
connectAttr "PxrSurface3.msg" "materialInfo3.t" -na;
connectAttr "groupId37.msg" "set3.gn" -na;
connectAttr "groupId41.msg" "set3.gn" -na;
connectAttr "groupId45.msg" "set3.gn" -na;
connectAttr "groupId49.msg" "set3.gn" -na;
connectAttr "groupId61.msg" "set3.gn" -na;
connectAttr "groupId68.msg" "set3.gn" -na;
connectAttr "groupId75.msg" "set3.gn" -na;
connectAttr "groupId80.msg" "set3.gn" -na;
connectAttr "pTorusShape3.iog.og[4]" "set3.dsm" -na;
connectAttr "pTorusShape4.iog.og[4]" "set3.dsm" -na;
connectAttr "pTorusShape5.iog.og[4]" "set3.dsm" -na;
connectAttr "pTorusShape6.iog.og[4]" "set3.dsm" -na;
connectAttr "pTorus8Shape.iog.og[0]" "set3.dsm" -na;
connectAttr "pTorus9Shape.iog.og[0]" "set3.dsm" -na;
connectAttr "pTorus10Shape.iog.og[0]" "set3.dsm" -na;
connectAttr "pTorusShape7.iog.og[4]" "set3.dsm" -na;
connectAttr "pTorus11Shape.iog.og[1]" "set3.dsm" -na;
connectAttr "pTorus11Shape.ciog.cog[0]" "set3.dsm" -na;
connectAttr "polyTorus3.out" "groupParts8.ig";
connectAttr "groupId37.id" "groupParts8.gi";
connectAttr "groupParts8.og" "deleteComponent3.ig";
connectAttr "groupId38.msg" "set4.gn" -na;
connectAttr "groupId42.msg" "set4.gn" -na;
connectAttr "groupId46.msg" "set4.gn" -na;
connectAttr "groupId50.msg" "set4.gn" -na;
connectAttr "groupId62.msg" "set4.gn" -na;
connectAttr "groupId69.msg" "set4.gn" -na;
connectAttr "groupId76.msg" "set4.gn" -na;
connectAttr "groupId81.msg" "set4.gn" -na;
connectAttr "pTorusShape3.iog.og[5]" "set4.dsm" -na;
connectAttr "pTorusShape4.iog.og[5]" "set4.dsm" -na;
connectAttr "pTorusShape5.iog.og[5]" "set4.dsm" -na;
connectAttr "pTorusShape6.iog.og[5]" "set4.dsm" -na;
connectAttr "pTorus8Shape.iog.og[1]" "set4.dsm" -na;
connectAttr "pTorus9Shape.iog.og[1]" "set4.dsm" -na;
connectAttr "pTorus10Shape.iog.og[1]" "set4.dsm" -na;
connectAttr "pTorusShape7.iog.og[5]" "set4.dsm" -na;
connectAttr "pTorus11Shape.iog.og[2]" "set4.dsm" -na;
connectAttr "deleteComponent3.og" "groupParts9.ig";
connectAttr "groupId38.id" "groupParts9.gi";
connectAttr "groupParts9.og" "deleteComponent4.ig";
connectAttr "polyTweak8.out" "polyCloseBorder3.ip";
connectAttr "deleteComponent4.og" "polyTweak8.ip";
connectAttr "groupId39.msg" "set5.gn" -na;
connectAttr "groupId43.msg" "set5.gn" -na;
connectAttr "groupId47.msg" "set5.gn" -na;
connectAttr "groupId51.msg" "set5.gn" -na;
connectAttr "groupId63.msg" "set5.gn" -na;
connectAttr "groupId70.msg" "set5.gn" -na;
connectAttr "groupId77.msg" "set5.gn" -na;
connectAttr "groupId82.msg" "set5.gn" -na;
connectAttr "pTorusShape3.iog.og[6]" "set5.dsm" -na;
connectAttr "pTorusShape4.iog.og[6]" "set5.dsm" -na;
connectAttr "pTorusShape5.iog.og[6]" "set5.dsm" -na;
connectAttr "pTorusShape6.iog.og[6]" "set5.dsm" -na;
connectAttr "pTorus8Shape.iog.og[2]" "set5.dsm" -na;
connectAttr "pTorus9Shape.iog.og[2]" "set5.dsm" -na;
connectAttr "pTorus10Shape.iog.og[2]" "set5.dsm" -na;
connectAttr "pTorusShape7.iog.og[6]" "set5.dsm" -na;
connectAttr "pTorus11Shape.iog.og[3]" "set5.dsm" -na;
connectAttr "polyCloseBorder3.out" "groupParts10.ig";
connectAttr "groupId39.id" "groupParts10.gi";
connectAttr "groupParts10.og" "deleteComponent5.ig";
connectAttr "polyTweak9.out" "polyExtrudeFace2.ip";
connectAttr "pTorusShape3.wm" "polyExtrudeFace2.mp";
connectAttr "deleteComponent5.og" "polyTweak9.ip";
connectAttr "polyTweak10.out" "polyExtrudeFace3.ip";
connectAttr "pTorusShape3.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polyExtrudeFace4.ip";
connectAttr "pTorusShape3.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak11.ip";
connectAttr "groupId40.msg" "set6.gn" -na;
connectAttr "groupId44.msg" "set6.gn" -na;
connectAttr "groupId48.msg" "set6.gn" -na;
connectAttr "groupId52.msg" "set6.gn" -na;
connectAttr "groupId64.msg" "set6.gn" -na;
connectAttr "groupId71.msg" "set6.gn" -na;
connectAttr "groupId78.msg" "set6.gn" -na;
connectAttr "groupId83.msg" "set6.gn" -na;
connectAttr "pTorusShape3.iog.og[7]" "set6.dsm" -na;
connectAttr "pTorusShape4.iog.og[7]" "set6.dsm" -na;
connectAttr "pTorusShape5.iog.og[7]" "set6.dsm" -na;
connectAttr "pTorusShape6.iog.og[7]" "set6.dsm" -na;
connectAttr "pTorus8Shape.iog.og[3]" "set6.dsm" -na;
connectAttr "pTorus9Shape.iog.og[3]" "set6.dsm" -na;
connectAttr "pTorus10Shape.iog.og[3]" "set6.dsm" -na;
connectAttr "pTorusShape7.iog.og[7]" "set6.dsm" -na;
connectAttr "pTorus11Shape.iog.og[4]" "set6.dsm" -na;
connectAttr "polyExtrudeFace4.out" "groupParts11.ig";
connectAttr "groupId40.id" "groupParts11.gi";
connectAttr "groupParts11.og" "polyTweak12.ip";
connectAttr "polyTweak12.out" "deleteComponent6.ig";
connectAttr "polyTweak13.out" "polyMirror1.ip";
connectAttr "pTorus3.sp" "polyMirror1.sp";
connectAttr "pTorusShape3.wm" "polyMirror1.mp";
connectAttr "deleteComponent6.og" "polyTweak13.ip";
connectAttr "pTorusShape6.o" "polyUnite1.ip[0]";
connectAttr "pTorusShape5.o" "polyUnite1.ip[1]";
connectAttr "pTorusShape6.wm" "polyUnite1.im[0]";
connectAttr "pTorusShape5.wm" "polyUnite1.im[1]";
connectAttr "polyUnite1.out" "groupParts12.ig";
connectAttr "groupId61.id" "groupParts12.gi";
connectAttr "groupParts12.og" "groupParts13.ig";
connectAttr "groupId62.id" "groupParts13.gi";
connectAttr "groupParts13.og" "groupParts14.ig";
connectAttr "groupId63.id" "groupParts14.gi";
connectAttr "groupParts14.og" "groupParts15.ig";
connectAttr "groupId64.id" "groupParts15.gi";
connectAttr "groupParts15.og" "groupParts16.ig";
connectAttr "groupId65.id" "groupParts16.gi";
connectAttr "groupParts16.og" "deleteComponent7.ig";
connectAttr "deleteComponent7.og" "polyBridgeEdge1.ip";
connectAttr "pTorus8Shape.wm" "polyBridgeEdge1.mp";
connectAttr "polyBridgeEdge1.out" "polyBridgeEdge2.ip";
connectAttr "pTorus8Shape.wm" "polyBridgeEdge2.mp";
connectAttr "pTorusShape4.o" "polyUnite2.ip[0]";
connectAttr "pTorus8Shape.o" "polyUnite2.ip[1]";
connectAttr "pTorusShape4.wm" "polyUnite2.im[0]";
connectAttr "pTorus8Shape.wm" "polyUnite2.im[1]";
connectAttr "polyUnite2.out" "groupParts17.ig";
connectAttr "groupId68.id" "groupParts17.gi";
connectAttr "groupParts17.og" "groupParts18.ig";
connectAttr "groupId69.id" "groupParts18.gi";
connectAttr "groupParts18.og" "groupParts19.ig";
connectAttr "groupId70.id" "groupParts19.gi";
connectAttr "groupParts19.og" "groupParts20.ig";
connectAttr "groupId71.id" "groupParts20.gi";
connectAttr "groupParts20.og" "groupParts21.ig";
connectAttr "groupId72.id" "groupParts21.gi";
connectAttr "groupParts21.og" "deleteComponent8.ig";
connectAttr "deleteComponent8.og" "polyBridgeEdge3.ip";
connectAttr "pTorus9Shape.wm" "polyBridgeEdge3.mp";
connectAttr "polyBridgeEdge3.out" "polyBridgeEdge4.ip";
connectAttr "pTorus9Shape.wm" "polyBridgeEdge4.mp";
connectAttr "pTorusShape3.o" "polyUnite3.ip[0]";
connectAttr "pTorus9Shape.o" "polyUnite3.ip[1]";
connectAttr "pTorusShape3.wm" "polyUnite3.im[0]";
connectAttr "pTorus9Shape.wm" "polyUnite3.im[1]";
connectAttr "polyMirror1.out" "groupParts22.ig";
connectAttr "groupId73.id" "groupParts22.gi";
connectAttr "polyUnite3.out" "groupParts23.ig";
connectAttr "groupId75.id" "groupParts23.gi";
connectAttr "groupParts23.og" "groupParts24.ig";
connectAttr "groupId76.id" "groupParts24.gi";
connectAttr "groupParts24.og" "groupParts25.ig";
connectAttr "groupId77.id" "groupParts25.gi";
connectAttr "groupParts25.og" "groupParts26.ig";
connectAttr "groupId78.id" "groupParts26.gi";
connectAttr "groupParts26.og" "groupParts27.ig";
connectAttr "groupId79.id" "groupParts27.gi";
connectAttr "groupParts27.og" "deleteComponent9.ig";
connectAttr "deleteComponent9.og" "polyBridgeEdge5.ip";
connectAttr "pTorus10Shape.wm" "polyBridgeEdge5.mp";
connectAttr "polyBridgeEdge5.out" "polyBridgeEdge6.ip";
connectAttr "pTorus10Shape.wm" "polyBridgeEdge6.mp";
connectAttr "groupParts31.og" "polyExtrudeFace5.ip";
connectAttr "pTorusShape7.wm" "polyExtrudeFace5.mp";
connectAttr "polySurfaceShape3.o" "groupParts28.ig";
connectAttr "groupId80.id" "groupParts28.gi";
connectAttr "groupParts28.og" "groupParts29.ig";
connectAttr "groupId81.id" "groupParts29.gi";
connectAttr "groupParts29.og" "groupParts30.ig";
connectAttr "groupId82.id" "groupParts30.gi";
connectAttr "groupParts30.og" "groupParts31.ig";
connectAttr "groupId83.id" "groupParts31.gi";
connectAttr "polyTweak14.out" "polyChipOff2.ip";
connectAttr "polySurfaceShape2.wm" "polyChipOff2.mp";
connectAttr "polyNormal2.out" "polyTweak14.ip";
connectAttr "polySurfaceShape2.o" "polySeparate2.ip";
connectAttr "polySeparate2.out[0]" "groupParts32.ig";
connectAttr "groupId84.id" "groupParts32.gi";
connectAttr "polySeparate2.out[1]" "groupParts33.ig";
connectAttr "groupId85.id" "groupParts33.gi";
connectAttr "groupParts33.og" "groupParts34.ig";
connectAttr "groupId86.id" "groupParts34.gi";
connectAttr "groupParts32.og" "polyChipOff3.ip";
connectAttr "polySurfaceShape4.wm" "polyChipOff3.mp";
connectAttr "polySurfaceShape4.o" "polySeparate3.ip";
connectAttr "polySeparate3.out[0]" "groupParts35.ig";
connectAttr "groupId87.id" "groupParts35.gi";
connectAttr "polySeparate3.out[1]" "groupParts36.ig";
connectAttr "groupId88.id" "groupParts36.gi";
connectAttr "|crossbeam_1|support_1|polySurfaceShape8.o" "polyExtrudeFace9.ip";
connectAttr "support_Shape1.wm" "polyExtrudeFace9.mp";
connectAttr "groupParts36.og" "polyExtrudeFace10.ip";
connectAttr "polySurfaceShape7.wm" "polyExtrudeFace10.mp";
connectAttr "polySurfaceShape7.o" "polyCBoolOp1.ip[0]";
connectAttr "pCubeShape12.o" "polyCBoolOp1.ip[1]";
connectAttr "polySurfaceShape7.wm" "polyCBoolOp1.im[0]";
connectAttr "pCubeShape12.wm" "polyCBoolOp1.im[1]";
connectAttr "polyCube4.out" "groupParts37.ig";
connectAttr "groupId89.id" "groupParts37.gi";
connectAttr "groupId92.msg" "set7.gn" -na;
connectAttr "pCylinderShape1.iog.og[1]" "set7.dsm" -na;
connectAttr "pTorus11Shape.iog.og[5]" "set7.dsm" -na;
connectAttr "polyCylinder1.out" "groupParts38.ig";
connectAttr "groupId92.id" "groupParts38.gi";
connectAttr "groupParts38.og" "deleteComponent10.ig";
connectAttr "deleteComponent10.og" "polyCloseBorder4.ip";
connectAttr "polyCloseBorder4.out" "polyCloseBorder5.ip";
connectAttr "polyCloseBorder5.out" "polyExtrudeFace11.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace11.mp";
connectAttr "pTorusShape7.o" "polyCBoolOp2.ip[0]";
connectAttr "pCylinderShape1.o" "polyCBoolOp2.ip[1]";
connectAttr "pTorusShape7.wm" "polyCBoolOp2.im[0]";
connectAttr "pCylinderShape1.wm" "polyCBoolOp2.im[1]";
connectAttr "polyExtrudeFace5.out" "groupParts39.ig";
connectAttr "groupId93.id" "groupParts39.gi";
connectAttr "polyExtrudeFace11.out" "groupParts40.ig";
connectAttr "groupId95.id" "groupParts40.gi";
connectAttr "polyTweak15.out" "polyAutoProj1.ip";
connectAttr "pTorus10Shape.wm" "polyAutoProj1.mp";
connectAttr "polyBridgeEdge6.out" "polyTweak15.ip";
connectAttr "polyAutoProj1.out" "polyTweakUV1.ip";
connectAttr ":defaultColorMgtGlobals.cme" "PSD_lambert1_color.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "PSD_lambert1_color.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "PSD_lambert1_color.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "PSD_lambert1_color.ws";
connectAttr "place2dTexture1.c" "PSD_lambert1_color.c";
connectAttr "place2dTexture1.tf" "PSD_lambert1_color.tf";
connectAttr "place2dTexture1.rf" "PSD_lambert1_color.rf";
connectAttr "place2dTexture1.mu" "PSD_lambert1_color.mu";
connectAttr "place2dTexture1.mv" "PSD_lambert1_color.mv";
connectAttr "place2dTexture1.s" "PSD_lambert1_color.s";
connectAttr "place2dTexture1.wu" "PSD_lambert1_color.wu";
connectAttr "place2dTexture1.wv" "PSD_lambert1_color.wv";
connectAttr "place2dTexture1.re" "PSD_lambert1_color.re";
connectAttr "place2dTexture1.of" "PSD_lambert1_color.of";
connectAttr "place2dTexture1.r" "PSD_lambert1_color.ro";
connectAttr "place2dTexture1.n" "PSD_lambert1_color.n";
connectAttr "place2dTexture1.vt1" "PSD_lambert1_color.vt1";
connectAttr "place2dTexture1.vt2" "PSD_lambert1_color.vt2";
connectAttr "place2dTexture1.vt3" "PSD_lambert1_color.vt3";
connectAttr "place2dTexture1.vc1" "PSD_lambert1_color.vc1";
connectAttr "place2dTexture1.o" "PSD_lambert1_color.uv";
connectAttr "place2dTexture1.ofs" "PSD_lambert1_color.fs";
connectAttr "polyCBoolOp2.out" "polyAutoProj2.ip";
connectAttr "pTorus11Shape.wm" "polyAutoProj2.mp";
connectAttr "polyAutoProj2.out" "polyNormal3.ip";
connectAttr "polyNormal3.out" "polySplitVert1.ip";
connectAttr "polySplitVert1.out" "polyChipOff4.ip";
connectAttr "pTorus11Shape.wm" "polyChipOff4.mp";
connectAttr "polyChipOff4.out" "polySplitVert2.ip";
connectAttr "polySplitVert2.out" "polyChipOff5.ip";
connectAttr "pTorus11Shape.wm" "polyChipOff5.mp";
connectAttr "polyCBoolOp1.out" "polyAutoProj3.ip";
connectAttr "polySurface6Shape.wm" "polyAutoProj3.mp";
connectAttr "polyAutoProj3.out" "polyTweakUV2.ip";
connectAttr "groupParts35.og" "polyAutoProj4.ip";
connectAttr "polySurfaceShape6.wm" "polyAutoProj4.mp";
connectAttr "polyAutoProj4.out" "polyTweakUV3.ip";
connectAttr "polyTweak16.out" "polyAutoProj5.ip";
connectAttr "pTorusShape2.wm" "polyAutoProj5.mp";
connectAttr "polyCloseBorder2.out" "polyTweak16.ip";
connectAttr "polyAutoProj5.out" "polyTweakUV4.ip";
connectAttr "polySurfaceShape9.o" "polyAutoProj6.ip";
connectAttr "beam_Shape1.wm" "polyAutoProj6.mp";
connectAttr "polyAutoProj6.out" "polyTweakUV5.ip";
connectAttr "polySurfaceShape10.o" "polyAutoProj7.ip";
connectAttr "beam_Shape2.wm" "polyAutoProj7.mp";
connectAttr "polyAutoProj7.out" "polyTweakUV6.ip";
connectAttr "polySurfaceShape11.o" "polyAutoProj8.ip";
connectAttr "beam_Shape3.wm" "polyAutoProj8.mp";
connectAttr "polyAutoProj8.out" "polyTweakUV7.ip";
connectAttr "polySurfaceShape12.o" "polyAutoProj9.ip";
connectAttr "beam_Shape4.wm" "polyAutoProj9.mp";
connectAttr "polyAutoProj9.out" "polyTweakUV8.ip";
connectAttr "polySurfaceShape13.o" "polyAutoProj10.ip";
connectAttr "beam_Shape5.wm" "polyAutoProj10.mp";
connectAttr "polyAutoProj10.out" "polyTweakUV9.ip";
connectAttr "polySurfaceShape14.o" "polyAutoProj11.ip";
connectAttr "beam_Shape6.wm" "polyAutoProj11.mp";
connectAttr "polyAutoProj11.out" "polyTweakUV10.ip";
connectAttr "polyTweak17.out" "polyAutoProj12.ip";
connectAttr "beam_7Shape.wm" "polyAutoProj12.mp";
connectAttr "polyCube3.out" "polyTweak17.ip";
connectAttr "polyAutoProj12.out" "polyTweakUV11.ip";
connectAttr "polyTweak18.out" "polyAutoProj13.ip";
connectAttr "support_Shape1.wm" "polyAutoProj13.mp";
connectAttr "polyExtrudeFace9.out" "polyTweak18.ip";
connectAttr "polySurfaceShape15.o" "polyAutoProj14.ip";
connectAttr "vertical_Shape1.wm" "polyAutoProj14.mp";
connectAttr "polySurfaceShape16.o" "polyAutoProj15.ip";
connectAttr "beam_Shape8.wm" "polyAutoProj15.mp";
connectAttr "polyAutoProj15.out" "polyTweakUV12.ip";
connectAttr "polySurfaceShape17.o" "polyAutoProj16.ip";
connectAttr "support_Shape2.wm" "polyAutoProj16.mp";
connectAttr "polySurfaceShape18.o" "polyAutoProj17.ip";
connectAttr "vertical_Shape2.wm" "polyAutoProj17.mp";
connectAttr "polySurfaceShape19.o" "polyAutoProj18.ip";
connectAttr "beam_Shape9.wm" "polyAutoProj18.mp";
connectAttr "polyAutoProj18.out" "polyTweakUV13.ip";
connectAttr "polySurfaceShape20.o" "polyAutoProj19.ip";
connectAttr "support_Shape3.wm" "polyAutoProj19.mp";
connectAttr "polySurfaceShape21.o" "polyAutoProj20.ip";
connectAttr "vertical_Shape3.wm" "polyAutoProj20.mp";
connectAttr "svgBlinn1SG.pa" ":renderPartition.st" -na;
connectAttr "PxrSurface1SG.pa" ":renderPartition.st" -na;
connectAttr "PxrSurface2SG.pa" ":renderPartition.st" -na;
connectAttr "svgBlinn1.msg" ":defaultShaderList1.s" -na;
connectAttr "PxrSurface1.msg" ":defaultShaderList1.s" -na;
connectAttr "PxrSurface2.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr ":rmanGlobals.msg" ":defaultRenderingList1.r" -na;
connectAttr ":rmanDefaultDisplay.msg" ":defaultRenderingList1.r" -na;
connectAttr "d_openexr.msg" ":defaultRenderingList1.r" -na;
connectAttr ":PxrPathTracer.msg" ":defaultRenderingList1.r" -na;
connectAttr "PxrSphereLightShape.msg" ":lightList1.l" -na;
connectAttr "PxrSphereLight1Shape.msg" ":lightList1.l" -na;
connectAttr "PSD_lambert1_color.msg" ":defaultTextureList1.tx" -na;
connectAttr "PSD_lambert1_color.oc" ":lambert1.c";
connectAttr "pCubeShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape2.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape6.iog.og[8]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape6.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape5.iog.og[8]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape5.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorus8Shape.iog.og[4]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape4.iog.og[10]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape4.ciog.cog[2]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorus9Shape.iog.og[4]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape3.iog.og[8]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape3.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorus10Shape.iog.og[4]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape4.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape5.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape6.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape7.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "beam_7Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "support_Shape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "vertical_Shape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "beam_Shape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "beam_Shape8.iog" ":initialShadingGroup.dsm" -na;
connectAttr "support_Shape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "vertical_Shape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "beam_Shape9.iog" ":initialShadingGroup.dsm" -na;
connectAttr "support_Shape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "vertical_Shape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "beam_Shape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "beam_Shape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "beam_Shape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "beam_Shape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "beam_Shape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape12.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape12.ciog.cog[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurface6Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "polySurface6Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape13.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape14.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape15.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape7.iog.og[8]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape7.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.iog.og[2]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorus11Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId57.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId58.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId59.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId60.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId65.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId66.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId67.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId72.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId73.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId74.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId79.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId84.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId85.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId87.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId88.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId89.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId90.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId93.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId94.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId95.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId96.msg" ":initialShadingGroup.gn" -na;
connectAttr "PSD_lambert1_color.msg" ":initialMaterialInfo.t" -na;
connectAttr "PxrSphereLight.iog" ":defaultLightSet.dsm" -na;
connectAttr "PxrSphereLight1.iog" ":defaultLightSet.dsm" -na;
connectAttr "polySurfaceShape5HiddenFacesSet.msg" ":defaultHideFaceDataSet.dnsm"
		 -na;
connectAttr "groupId86.msg" ":defaultLastHiddenSet.gn" -na;
connectAttr "polySurfaceShape5.iog.og[1]" ":defaultLastHiddenSet.dsm" -na;
// End of bottega-uv.ma
