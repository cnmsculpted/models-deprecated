//Maya ASCII 2018 scene
//Name: bottega-3.ma
//Last modified: Fri, Nov 09, 2018 12:09:43 PM
//Codeset: UTF-8
requires maya "2018";
requires -nodeType "displayPoints" "Type" "2.0a";
requires -nodeType "PxrSurface" -nodeType "rmanDisplayChannel" -nodeType "d_openexr"
		 -nodeType "rmanGlobals" -nodeType "PxrPathTracer" -nodeType "PxrSphereLight" -nodeType "rmanDisplay"
		 "RenderMan_for_Maya.py" "1.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Mac OS X 10.14.1";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "C270CC9B-2445-5912-2C50-5C820F1137A4";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.0809740417471179 0.69445003810303174 4.6074486246513855 ;
	setAttr ".r" -type "double3" 359.06164727010406 -715.79999999993902 -6.2287483076773022e-18 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "B3B9821A-7E4C-248B-CB5B-728BFA7F55C8";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 7.2520604246047098;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.68232016184261579 0.78547543513318652 -3.5392022858237766 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "A4ECAAE5-004D-221E-ECCB-45A84559B1D0";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.98407330529478743 1000.1 -3.6398949071614641 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "2AF13C9B-3945-5663-DA6B-F6B8D3BC8E75";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 2.2516097179091741;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "DB9A5EF0-AF49-4BA8-8E85-759A94C84A56";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.3950517109989264 1.3163585772463842 1000.101046419402 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "093CCAB8-3940-ECAC-0323-C69706BC0946";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 996.51516256732305;
	setAttr ".ow" 3.9942078399340795;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 2.8451905626664962 1.3578873562295586 3.5858838520789247 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "5232A728-8D47-3C38-9B57-DDAD144F57BB";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1000614681579 1.3809535516644569 1.9871506626828026 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "E744E83C-4042-3668-4D1F-B9BA64604ECB";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 997.30432513963149;
	setAttr ".ow" 3.0575881592811234;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 2.7957363285262686 1.3986615911969729 0.44064854350950267 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCube1";
	rename -uid "9EFED24B-2247-7983-A205-5A8ECAB6647B";
	setAttr ".t" -type "double3" 0 0.5 0 ;
	setAttr ".s" -type "double3" 2.0502456776410818 2.0502456776410818 5.8539501523725681 ;
	setAttr ".rp" -type "double3" 0 -0.5 0 ;
	setAttr ".sp" -type "double3" 0 -0.5 0 ;
createNode transform -n "polySurface1" -p "pCube1";
	rename -uid "E80D4293-5749-E594-FCA2-70981D1DDFB7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 0.16036265464520669 ;
	setAttr ".rp" -type "double3" 0 0 0.72023093700408936 ;
	setAttr ".sp" -type "double3" 0 0 0.72023093700408936 ;
createNode mesh -n "polySurfaceShape1" -p "polySurface1";
	rename -uid "1EF9C3B8-B942-C291-19A9-068B8FEE95CE";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -2.0198634e-09 -2.9802322e-08 
		-0.1071713 0.88773161 1.4901161e-08 -0.10717133 0.88773155 0.040413637 -0.10717133 
		-2.2561366e-08 0.040413573 -0.10717133;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface2" -p "pCube1";
	rename -uid "5BF0F182-5F41-4A6A-2CBE-1EBA21BB1828";
	setAttr ".s" -type "double3" 1 1 1.226119279100836 ;
createNode transform -n "polySurface3" -p "polySurface2";
	rename -uid "7B2C1CFA-2444-95C3-ED27-9CBE0F3D7A75";
createNode transform -n "polySurface5" -p "polySurface3";
	rename -uid "94EAF9B0-9241-5343-200F-91A52250B935";
createNode mesh -n "polySurfaceShape6" -p "polySurface5";
	rename -uid "988DDE79-2742-5894-D526-96A7FF2F2CDD";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface6" -p "polySurface3";
	rename -uid "C21670CB-954C-E47B-A02E-56A5E73DE76D";
	setAttr ".t" -type "double3" 0 0 -0.26163131739322015 ;
createNode mesh -n "polySurfaceShape7" -p "polySurface6";
	rename -uid "FB87EE86-C544-715B-D259-759C747850EB";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform10" -p "polySurface3";
	rename -uid "BDB5D36E-2642-DCC3-8546-0ABE49EC0A12";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape4" -p "transform10";
	rename -uid "C1ED3E7F-1541-03B7-6E11-9E9FC0DA1395";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface4" -p "polySurface2";
	rename -uid "64F52286-0945-6434-1539-81AE30449943";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.20253147292925699 0 0 ;
createNode mesh -n "polySurfaceShape5" -p "polySurface4";
	rename -uid "9F7F490E-E643-E203-D710-458122A26F20";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0.37705776 0 0 0.37705776 
		0 0 0.37705776 0 0 0.37705776 0 0;
createNode transform -n "transform9" -p "polySurface2";
	rename -uid "88164B92-404D-96C3-554C-08B0C352D8C6";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape2" -p "transform9";
	rename -uid "196DF428-2A41-C0EE-920B-9985C387CD17";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "transform1" -p "pCube1";
	rename -uid "8C9F10F5-3348-ED45-0765-98BD2097EA38";
	setAttr ".v" no;
createNode mesh -n "pCubeShape1" -p "transform1";
	rename -uid "BB9A2BC3-874D-C271-9807-059C83500952";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pTorus1";
	rename -uid "7F714773-D246-2CE8-AA09-E08254CA678C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.3248204499287206 1.1142577440074855 1.2834600340007283 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 1.8722901894521309 1.5152858838336571 0.84913650370180194 ;
	setAttr ".rp" -type "double3" 1.6224328189310211 0 0 ;
	setAttr ".sp" -type "double3" 1.0707107063033443 0 0 ;
	setAttr ".spt" -type "double3" 0.55172211262767323 0 0 ;
createNode mesh -n "pTorusShape1" -p "pTorus1";
	rename -uid "BC063B8B-0B43-2554-4E52-4CBC3EF88BC3";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999979138374329 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 94 ".pt";
	setAttr ".pt[0]" -type "float3" -2.7474016e-08 0 -4.6566129e-09 ;
	setAttr ".pt[1]" -type "float3" 5.2154064e-08 0 1.7695129e-08 ;
	setAttr ".pt[2]" -type "float3" 1.3783574e-07 0 -2.2351742e-08 ;
	setAttr ".pt[3]" -type "float3" 5.9604645e-08 0 -6.3329935e-08 ;
	setAttr ".pt[4]" -type "float3" 1.4156103e-07 0 -4.0978193e-08 ;
	setAttr ".pt[5]" -type "float3" -2.2351742e-08 0 1.4901161e-08 ;
	setAttr ".pt[6]" -type "float3" -2.9057264e-07 0 0 ;
	setAttr ".pt[7]" -type "float3" 1.8626451e-08 0 -1.8626451e-08 ;
	setAttr ".pt[8]" -type "float3" -1.7136335e-07 0 -1.7881393e-07 ;
	setAttr ".pt[9]" -type "float3" -2.6728958e-07 0 4.0978193e-08 ;
	setAttr ".pt[10]" -type "float3" 7.7998266e-09 0 1.3469253e-07 ;
	setAttr ".pt[11]" -type "float3" -5.1688403e-08 0 -1.4901161e-08 ;
	setAttr ".pt[12]" -type "float3" 4.1909516e-09 0 4.4703484e-08 ;
	setAttr ".pt[13]" -type "float3" 2.9773219e-08 0 1.5308615e-07 ;
	setAttr ".pt[14]" -type "float3" 2.9150397e-07 0 2.6077032e-08 ;
	setAttr ".pt[15]" -type "float3" 2.4214387e-07 0 -1.7974526e-07 ;
	setAttr ".pt[16]" -type "float3" -8.9406967e-08 0 -1.8626451e-08 ;
	setAttr ".pt[17]" -type "float3" 3.054738e-07 0 0 ;
	setAttr ".pt[18]" -type "float3" -9.6857548e-08 0 -1.4901161e-08 ;
	setAttr ".pt[19]" -type "float3" 0 0 7.4505806e-08 ;
	setAttr ".pt[20]" -type "float3" -1.5646219e-07 0 2.2351742e-08 ;
	setAttr ".pt[21]" -type "float3" 2.9802322e-08 0 -2.9802322e-08 ;
	setAttr ".pt[22]" -type "float3" -5.5879354e-09 0 4.6566129e-09 ;
	setAttr ".pt[23]" -type "float3" 2.0489097e-08 0 0 ;
	setAttr ".pt[24]" -type "float3" -2.3283064e-10 0 -3.8999133e-09 ;
	setAttr ".pt[25]" -type "float3" -1.4668331e-08 0 -3.6670826e-09 ;
	setAttr ".pt[27]" -type "float3" -1.8626451e-09 0 -9.3132257e-10 ;
	setAttr ".pt[29]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".pt[30]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".pt[31]" -type "float3" -4.6566129e-10 0 0 ;
	setAttr ".pt[32]" -type "float3" 1.8067658e-07 0 -1.9324943e-08 ;
	setAttr ".pt[33]" -type "float3" 3.981404e-08 0 -6.2864274e-09 ;
	setAttr ".pt[34]" -type "float3" 1.4901161e-08 0 5.9604645e-08 ;
	setAttr ".pt[35]" -type "float3" -3.6414713e-07 0 -9.5926225e-08 ;
	setAttr ".pt[36]" -type "float3" 7.9162419e-08 0 -1.4901161e-08 ;
	setAttr ".pt[37]" -type "float3" 1.1920929e-07 0 -4.4703484e-08 ;
	setAttr ".pt[38]" -type "float3" -1.8253922e-07 0 -1.4901161e-08 ;
	setAttr ".pt[39]" -type "float3" -8.2887709e-08 0 -2.9802322e-08 ;
	setAttr ".pt[40]" -type "float3" 2.7194619e-07 0 9.406358e-08 ;
	setAttr ".pt[41]" -type "float3" -6.519258e-08 0 7.8231096e-08 ;
	setAttr ".pt[42]" -type "float3" 1.3504177e-08 0 -2.910383e-09 ;
	setAttr ".pt[51]" -type "float3" 4.6566129e-10 0 9.3132257e-10 ;
	setAttr ".pt[52]" -type "float3" 9.3132257e-10 0 -1.3969839e-09 ;
	setAttr ".pt[53]" -type "float3" 0 0 9.3132257e-10 ;
	setAttr ".pt[54]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".pt[55]" -type "float3" 0 0 -1.8626451e-09 ;
	setAttr ".pt[56]" -type "float3" -1.8626451e-09 0 9.3132257e-10 ;
	setAttr ".pt[57]" -type "float3" -3.4924597e-10 0 1.1641532e-10 ;
	setAttr ".pt[58]" -type "float3" 1.7695129e-07 0 -1.5599653e-08 ;
	setAttr ".pt[59]" -type "float3" 4.353933e-08 0 1.1641532e-09 ;
	setAttr ".pt[60]" -type "float3" 4.4703484e-08 0 5.9604645e-08 ;
	setAttr ".pt[61]" -type "float3" -3.6414713e-07 0 -9.5926225e-08 ;
	setAttr ".pt[62]" -type "float3" 7.9162419e-08 0 -1.4901161e-08 ;
	setAttr ".pt[63]" -type "float3" 1.1920929e-07 0 -4.4703484e-08 ;
	setAttr ".pt[64]" -type "float3" -1.8253922e-07 0 -1.4901161e-08 ;
	setAttr ".pt[65]" -type "float3" -1.1269003e-07 0 -2.9802322e-08 ;
	setAttr ".pt[66]" -type "float3" 2.6449561e-07 0 9.406358e-08 ;
	setAttr ".pt[67]" -type "float3" -9.8720193e-08 0 7.8231096e-08 ;
	setAttr ".pt[68]" -type "float3" -2.3748726e-08 0 4.5401976e-09 ;
	setAttr ".pt[77]" -type "float3" 4.6566129e-10 0 0 ;
	setAttr ".pt[78]" -type "float3" -1.3969839e-09 0 2.7939677e-09 ;
	setAttr ".pt[79]" -type "float3" 2.6077032e-08 0 1.0244548e-08 ;
	setAttr ".pt[80]" -type "float3" 1.0803342e-07 0 -1.4901161e-08 ;
	setAttr ".pt[81]" -type "float3" 4.4703484e-08 0 -5.5879354e-08 ;
	setAttr ".pt[82]" -type "float3" 9.6857548e-08 0 -2.6077032e-08 ;
	setAttr ".pt[83]" -type "float3" -3.7252903e-08 0 -1.4901161e-08 ;
	setAttr ".pt[84]" -type "float3" -3.5017729e-07 0 1.4901161e-08 ;
	setAttr ".pt[85]" -type "float3" -1.1175871e-08 0 -1.8626451e-08 ;
	setAttr ".pt[86]" -type "float3" -8.1956387e-08 0 -1.7881393e-07 ;
	setAttr ".pt[87]" -type "float3" -1.7788261e-07 0 4.0978193e-08 ;
	setAttr ".pt[88]" -type "float3" 7.7998266e-09 0 1.3469253e-07 ;
	setAttr ".pt[89]" -type "float3" -5.1688403e-08 0 -1.4901161e-08 ;
	setAttr ".pt[90]" -type "float3" 4.1909516e-09 0 4.4703484e-08 ;
	setAttr ".pt[91]" -type "float3" 5.9575541e-08 0 1.5308615e-07 ;
	setAttr ".pt[92]" -type "float3" 1.7229468e-07 0 2.6077032e-08 ;
	setAttr ".pt[93]" -type "float3" 1.2293458e-07 0 -1.4994293e-07 ;
	setAttr ".pt[94]" -type "float3" -4.4703484e-08 0 -1.8626451e-08 ;
	setAttr ".pt[95]" -type "float3" 3.3527613e-07 0 1.4901161e-08 ;
	setAttr ".pt[96]" -type "float3" -8.1956387e-08 0 -4.4703484e-08 ;
	setAttr ".pt[97]" -type "float3" 7.4505806e-08 0 8.9406967e-08 ;
	setAttr ".pt[98]" -type "float3" -4.4703484e-08 0 0 ;
	setAttr ".pt[99]" -type "float3" 5.2154064e-08 0 -3.7252903e-08 ;
	setAttr ".pt[100]" -type "float3" 1.8626451e-09 0 8.3819032e-09 ;
	setAttr ".pt[101]" -type "float3" 2.514571e-08 0 3.7252903e-09 ;
	setAttr ".pt[102]" -type "float3" 3.608875e-09 0 -3.8999133e-09 ;
	setAttr ".pt[103]" -type "float3" 2.3283064e-10 0 -7.3923729e-09 ;
	setAttr ".pt[104]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[105]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[106]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[107]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[108]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[109]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[110]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".pt[111]" -type "float3" 0 -1.8041124e-16 1.3146042 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pTorus2";
	rename -uid "54D20A4F-6343-DB06-6B5E-8FBEAC3F83C1";
	setAttr ".t" -type "double3" 0.22144663179391605 0.90012333193416383 1.8586220401509119 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 2.3209442420421267 2.3209442420421267 2.3209442420421267 ;
createNode mesh -n "pTorusShape2" -p "pTorus2";
	rename -uid "022FB0AB-074A-D95C-7536-618A001F88AA";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.23333333432674408 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 66 ".pt";
	setAttr ".pt[13]" -type "float3" 0 0 0.0021680172 ;
	setAttr ".pt[15]" -type "float3" 0 0 0.0021680172 ;
	setAttr ".pt[29]" -type "float3" 5.9604645e-08 -6.9388939e-17 0.38782632 ;
	setAttr ".pt[30]" -type "float3" 5.9604645e-08 -6.9388939e-17 0.38782632 ;
	setAttr ".pt[31]" -type "float3" 0.0031258948 0 0 ;
	setAttr ".pt[32]" -type "float3" 0.01247095 0 0 ;
	setAttr ".pt[33]" -type "float3" 0.027932156 0 0 ;
	setAttr ".pt[34]" -type "float3" 0.049340278 0 0 ;
	setAttr ".pt[35]" -type "float3" 0.076460361 0 3.7252903e-09 ;
	setAttr ".pt[36]" -type "float3" 0.10899578 7.4505806e-09 3.7252903e-09 ;
	setAttr ".pt[37]" -type "float3" 0.14658974 0 0 ;
	setAttr ".pt[38]" -type "float3" 0.18883049 0 0 ;
	setAttr ".pt[39]" -type "float3" 0.23525521 0 0 ;
	setAttr ".pt[40]" -type "float3" 0.28535521 0 0 ;
	setAttr ".pt[41]" -type "float3" 0.33858174 0 0 ;
	setAttr ".pt[42]" -type "float3" 0.39435133 0 -0.015461199 ;
	setAttr ".pt[43]" -type "float3" 0 0 -0.0093449727 ;
	setAttr ".pt[46]" -type "float3" 0 0 -0.0067744306 ;
	setAttr ".pt[47]" -type "float3" 0 0 -0.016119344 ;
	setAttr ".pt[48]" -type "float3" 0 0 -0.031580552 ;
	setAttr ".pt[49]" -type "float3" -0.30496681 -2.7755576e-17 -0.0098703057 ;
	setAttr ".pt[50]" -type "float3" -0.25174037 0 0 ;
	setAttr ".pt[51]" -type "float3" -0.20164035 0 0 ;
	setAttr ".pt[52]" -type "float3" -0.15521568 0 0 ;
	setAttr ".pt[53]" -type "float3" -0.11297499 0 0 ;
	setAttr ".pt[54]" -type "float3" -0.075381055 0 0 ;
	setAttr ".pt[55]" -type "float3" -0.042845741 0 0 ;
	setAttr ".pt[56]" -type "float3" -0.015725538 0 0 ;
	setAttr ".pt[57]" -type "float3" 0.0056823939 0 0 ;
	setAttr ".pt[58]" -type "float3" 0.021143571 0 0 ;
	setAttr ".pt[59]" -type "float3" 0.030488566 0 0 ;
	setAttr ".pt[60]" -type "float3" 0.033614948 -6.9388939e-17 0.38782632 ;
	setAttr ".pt[61]" -type "float3" 5.9604645e-08 -6.9388939e-17 0.38782632 ;
	setAttr ".pt[62]" -type "float3" 0.0031258948 0 0 ;
	setAttr ".pt[63]" -type "float3" 0.01247095 0 0 ;
	setAttr ".pt[64]" -type "float3" 0.027932156 0 0 ;
	setAttr ".pt[65]" -type "float3" 0.049340278 0 0 ;
	setAttr ".pt[66]" -type "float3" 0.076460361 0 3.7252903e-09 ;
	setAttr ".pt[67]" -type "float3" 0.10899578 7.4505806e-09 3.7252903e-09 ;
	setAttr ".pt[68]" -type "float3" 0.14658974 0 0 ;
	setAttr ".pt[69]" -type "float3" 0.18883049 0 0 ;
	setAttr ".pt[70]" -type "float3" 0.23525521 0 0 ;
	setAttr ".pt[71]" -type "float3" 0.28535521 0 0 ;
	setAttr ".pt[72]" -type "float3" 0.33858174 0 0 ;
	setAttr ".pt[73]" -type "float3" 0.39435133 0 -0.015461199 ;
	setAttr ".pt[74]" -type "float3" 0 0 -0.0093449727 ;
	setAttr ".pt[77]" -type "float3" 0 0 -0.0067744306 ;
	setAttr ".pt[78]" -type "float3" 0 0 -0.016119344 ;
	setAttr ".pt[79]" -type "float3" 0 0 -0.031580552 ;
	setAttr ".pt[80]" -type "float3" -0.30496678 -5.9604645e-08 -0.0098702908 ;
	setAttr ".pt[81]" -type "float3" -0.25174037 0 0 ;
	setAttr ".pt[82]" -type "float3" -0.20164035 0 0 ;
	setAttr ".pt[83]" -type "float3" -0.15521568 0 0 ;
	setAttr ".pt[84]" -type "float3" -0.11297499 0 0 ;
	setAttr ".pt[85]" -type "float3" -0.075381055 0 0 ;
	setAttr ".pt[86]" -type "float3" -0.042845741 0 0 ;
	setAttr ".pt[87]" -type "float3" -0.015725538 0 0 ;
	setAttr ".pt[88]" -type "float3" 0.0056823939 0 0 ;
	setAttr ".pt[89]" -type "float3" 0.021143571 0 0 ;
	setAttr ".pt[90]" -type "float3" 0.030488566 0 0 ;
	setAttr ".pt[91]" -type "float3" 0.033614948 -6.9388939e-17 0.38782632 ;
	setAttr ".pt[92]" -type "float3" 5.9604645e-08 -6.9388939e-17 0.38782632 ;
	setAttr ".pt[106]" -type "float3" 0 0 0.0021680172 ;
	setAttr ".pt[108]" -type "float3" 0 0 0.0021680172 ;
	setAttr ".pt[122]" -type "float3" 5.9604645e-08 -6.9388939e-17 0.38782632 ;
	setAttr ".pt[123]" -type "float3" 5.9604645e-08 -6.9388939e-17 0.38782632 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "PxrSphereLight";
	rename -uid "1C319F51-3743-E680-3282-BE9BECC96CE6";
	setAttr ".t" -type "double3" 0.87355979062890599 0.94393635246008467 -0.093621399966719032 ;
createNode PxrSphereLight -n "PxrSphereLightShape" -p "PxrSphereLight";
	rename -uid "FDD91B64-AB4E-655E-2DA4-FAA8A3909284";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".isc" no;
	setAttr ".bbx" no;
	setAttr ".icn" -type "string" "";
	setAttr ".vwm" 2;
	setAttr ".tpv" 0;
	setAttr ".uit" 0;
	setAttr -k off ".v" yes;
	setAttr ".io" no;
	setAttr ".tmp" no;
	setAttr ".gh" no;
	setAttr ".obcc" -type "float3" 0 0 0 ;
	setAttr ".wfcc" -type "float3" 0 0 0 ;
	setAttr ".uoc" 0;
	setAttr ".oc" 0;
	setAttr ".ovdt" 0;
	setAttr ".ovlod" 0;
	setAttr ".ovs" no;
	setAttr ".ovt" yes;
	setAttr ".ovp" yes;
	setAttr ".ove" yes;
	setAttr ".ovv" yes;
	setAttr ".hpb" no;
	setAttr ".ovrgbf" no;
	setAttr ".ovc" 0;
	setAttr ".ovrgb" -type "float3" 0 0 0 ;
	setAttr ".lodv" yes;
	setAttr ".sech" yes;
	setAttr ".rlid" 0;
	setAttr ".rndr" yes;
	setAttr ".lovc" 0;
	setAttr ".gc" 0;
	setAttr ".gpr" 3;
	setAttr ".gps" 3;
	setAttr ".gss" 1;
	setAttr ".gap" 1;
	setAttr ".gcp" -type "float3" 0.447 1 1 ;
	setAttr ".gla" 1;
	setAttr ".gac" -type "float3" 0.87800002 0.67799997 0.66299999 ;
	setAttr ".grs" 0;
	setAttr ".gre" 100;
	setAttr ".rt" 0;
	setAttr ".rv" no;
	setAttr ".vf" 1;
	setAttr ".hfm" 1;
	setAttr ".mb" yes;
	setAttr ".vir" no;
	setAttr ".vif" no;
	setAttr ".csh" yes;
	setAttr ".rcsh" yes;
	setAttr ".asbg" no;
	setAttr ".vbo" no;
	setAttr ".mvs" 1;
	setAttr ".gao" no;
	setAttr ".gal" 1;
	setAttr ".sso" no;
	setAttr ".ssa" 1;
	setAttr ".msa" 1;
	setAttr ".vso" no;
	setAttr ".vss" 1;
	setAttr ".dej" no;
	setAttr ".iss" no;
	setAttr ".vis" no;
	setAttr ".tw" no;
	setAttr ".rtw" yes;
	setAttr ".pv" -type "double2" 0 0 ;
	setAttr ".di" no;
	setAttr ".dcol" no;
	setAttr ".dcc" -type "string" "color";
	setAttr ".ih" no;
	setAttr ".ds" yes;
	setAttr ".op" no;
	setAttr ".hot" no;
	setAttr ".smo" yes;
	setAttr ".bbs" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".fbda" yes;
	setAttr ".dsr" 6;
	setAttr ".xsr" 5;
	setAttr ".fth" 0;
	setAttr ".nat" 30;
	setAttr ".dhe" no;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".intensity" 5;
	setAttr ".exposure" 0;
	setAttr ".lightColor" -type "float3" 1 1 1 ;
	setAttr ".enableTemperature" no;
	setAttr ".temperature" 6500;
	setAttr ".emissionFocus" 0;
	setAttr ".emissionFocusTint" -type "float3" 0 0 0 ;
	setAttr ".specular" 1;
	setAttr ".diffuse" 1;
	setAttr ".intensityNearDist" 0;
	setAttr ".coneAngle" 90;
	setAttr ".coneSoftness" 0;
	setAttr ".iesProfile" -type "string" "";
	setAttr ".iesProfileScale" 0;
	setAttr ".iesProfileNormalize" no;
	setAttr ".enableShadows" yes;
	setAttr ".shadowColor" -type "float3" 0 0 0 ;
	setAttr ".shadowDistance" -1;
	setAttr ".shadowFalloff" -1;
	setAttr ".shadowFalloffGamma" 1;
	setAttr ".shadowSubset" -type "string" "";
	setAttr ".shadowExcludeSubset" -type "string" "";
	setAttr ".areaNormalize" no;
	setAttr ".traceLightPaths" no;
	setAttr ".thinShadow" yes;
	setAttr ".visibleInRefractionPath" no;
	setAttr ".cheapCaustics" no;
	setAttr ".cheapCausticsExcludeGroup" -type "string" "";
	setAttr ".fixedSampleCount" 0;
	setAttr ".lightGroup" -type "string" "";
	setAttr ".importanceMultiplier" 1;
	setAttr ".rman__lightfilters[0]" -type "float3"  0 0 0;
	setAttr ".cl" -type "float3" 1 1 1 ;
	setAttr ".ed" yes;
	setAttr ".sn" yes;
	setAttr ".lls" 1;
	setAttr ".de" 2;
	setAttr ".urs" yes;
	setAttr ".col" 5;
	setAttr ".hio" no;
	setAttr ".uocol" no;
	setAttr ".oclr" -type "float3" 0 0 0 ;
	setAttr ".rman_coneAngleDepth" 10;
	setAttr ".rman_coneAngleOpacity" 0.5;
createNode transform -n "PxrSphereLight1";
	rename -uid "4CDE5F1B-3646-C86A-2B00-A59E0219B53D";
	setAttr ".t" -type "double3" 0 0.66763259666593378 2.1298540206304231 ;
createNode PxrSphereLight -n "PxrSphereLight1Shape" -p "PxrSphereLight1";
	rename -uid "CAA4D469-F543-C92C-9167-3E95309C3B56";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".isc" no;
	setAttr ".bbx" no;
	setAttr ".icn" -type "string" "";
	setAttr ".vwm" 2;
	setAttr ".tpv" 0;
	setAttr ".uit" 0;
	setAttr -k off ".v" yes;
	setAttr ".io" no;
	setAttr ".tmp" no;
	setAttr ".gh" no;
	setAttr ".obcc" -type "float3" 0 0 0 ;
	setAttr ".wfcc" -type "float3" 0 0 0 ;
	setAttr ".uoc" 0;
	setAttr ".oc" 0;
	setAttr ".ovdt" 0;
	setAttr ".ovlod" 0;
	setAttr ".ovs" no;
	setAttr ".ovt" yes;
	setAttr ".ovp" yes;
	setAttr ".ove" yes;
	setAttr ".ovv" yes;
	setAttr ".hpb" no;
	setAttr ".ovrgbf" no;
	setAttr ".ovc" 0;
	setAttr ".ovrgb" -type "float3" 0 0 0 ;
	setAttr ".lodv" yes;
	setAttr ".sech" yes;
	setAttr ".rlid" 0;
	setAttr ".rndr" yes;
	setAttr ".lovc" 0;
	setAttr ".gc" 0;
	setAttr ".gpr" 3;
	setAttr ".gps" 3;
	setAttr ".gss" 1;
	setAttr ".gap" 1;
	setAttr ".gcp" -type "float3" 0.447 1 1 ;
	setAttr ".gla" 1;
	setAttr ".gac" -type "float3" 0.87800002 0.67799997 0.66299999 ;
	setAttr ".grs" 0;
	setAttr ".gre" 100;
	setAttr ".rt" 0;
	setAttr ".rv" no;
	setAttr ".vf" 1;
	setAttr ".hfm" 1;
	setAttr ".mb" yes;
	setAttr ".vir" no;
	setAttr ".vif" no;
	setAttr ".csh" yes;
	setAttr ".rcsh" yes;
	setAttr ".asbg" no;
	setAttr ".vbo" no;
	setAttr ".mvs" 1;
	setAttr ".gao" no;
	setAttr ".gal" 1;
	setAttr ".sso" no;
	setAttr ".ssa" 1;
	setAttr ".msa" 1;
	setAttr ".vso" no;
	setAttr ".vss" 1;
	setAttr ".dej" no;
	setAttr ".iss" no;
	setAttr ".vis" no;
	setAttr ".tw" no;
	setAttr ".rtw" yes;
	setAttr ".pv" -type "double2" 0 0 ;
	setAttr ".di" no;
	setAttr ".dcol" no;
	setAttr ".dcc" -type "string" "color";
	setAttr ".ih" no;
	setAttr ".ds" yes;
	setAttr ".op" no;
	setAttr ".hot" no;
	setAttr ".smo" yes;
	setAttr ".bbs" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".fbda" yes;
	setAttr ".dsr" 6;
	setAttr ".xsr" 5;
	setAttr ".fth" 0;
	setAttr ".nat" 30;
	setAttr ".dhe" no;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".intensity" 2;
	setAttr ".exposure" 0;
	setAttr ".lightColor" -type "float3" 1 1 1 ;
	setAttr ".enableTemperature" no;
	setAttr ".temperature" 6500;
	setAttr ".emissionFocus" 0;
	setAttr ".emissionFocusTint" -type "float3" 0 0 0 ;
	setAttr ".specular" 1;
	setAttr ".diffuse" 1;
	setAttr ".intensityNearDist" 0;
	setAttr ".coneAngle" 90;
	setAttr ".coneSoftness" 0;
	setAttr ".iesProfile" -type "string" "";
	setAttr ".iesProfileScale" 0;
	setAttr ".iesProfileNormalize" no;
	setAttr ".enableShadows" yes;
	setAttr ".shadowColor" -type "float3" 0 0 0 ;
	setAttr ".shadowDistance" -1;
	setAttr ".shadowFalloff" -1;
	setAttr ".shadowFalloffGamma" 1;
	setAttr ".shadowSubset" -type "string" "";
	setAttr ".shadowExcludeSubset" -type "string" "";
	setAttr ".areaNormalize" no;
	setAttr ".traceLightPaths" no;
	setAttr ".thinShadow" yes;
	setAttr ".visibleInRefractionPath" no;
	setAttr ".cheapCaustics" no;
	setAttr ".cheapCausticsExcludeGroup" -type "string" "";
	setAttr ".fixedSampleCount" 0;
	setAttr ".lightGroup" -type "string" "";
	setAttr ".importanceMultiplier" 1;
	setAttr ".rman__lightfilters[0]" -type "float3"  0 0 0;
	setAttr ".cl" -type "float3" 1 1 1 ;
	setAttr ".ed" yes;
	setAttr ".sn" yes;
	setAttr ".lls" 1;
	setAttr ".de" 2;
	setAttr ".urs" yes;
	setAttr ".col" 5;
	setAttr ".hio" no;
	setAttr ".uocol" no;
	setAttr ".oclr" -type "float3" 0 0 0 ;
	setAttr ".rman_coneAngleDepth" 10;
	setAttr ".rman_coneAngleOpacity" 0.5;
createNode transform -n "transform2";
	rename -uid "F4DB40B2-7742-A6D5-FA5D-D6B9E0539084";
	setAttr ".hio" yes;
createNode displayPoints -n "displayPoints2" -p "transform2";
	rename -uid "914178C8-104B-8BFC-AB2A-BFB23CE2992F";
	setAttr -k off ".v";
	setAttr ".boundingBoxes" -type "vectorArray" 0 ;
	setAttr ".hio" yes;
createNode transform -n "pTorus3";
	rename -uid "7A47B1F1-BA45-6212-86EF-94A00D2B62EA";
	setAttr ".t" -type "double3" 2.7957363266636235 1.3578873562295586 2.5990325734684618 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
	setAttr ".rpt" -type "double3" 0.3941378593444822 -0.39413785934448242 0 ;
	setAttr ".sp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
createNode transform -n "transform8" -p "pTorus3";
	rename -uid "5E1BAC85-2C4E-66C0-B670-F1B891654593";
	setAttr ".v" no;
createNode mesh -n "pTorusShape3" -p "transform8";
	rename -uid "DBACC96A-374F-7E9D-1C97-C49471F75E13";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 10 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.85833317041397095 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".pt";
	setAttr ".pt[48]" -type "float3" 0.03513477 -0.064907849 -0.031166604 ;
	setAttr ".pt[49]" -type "float3" 0.031876974 -0.064907849 -0.03099589 ;
	setAttr ".pt[50]" -type "float3" 0.028654853 -0.064907849 -0.030485559 ;
	setAttr ".pt[51]" -type "float3" 0.025503734 -0.064907849 -0.02964123 ;
	setAttr ".pt[52]" -type "float3" 0.022458173 -0.064907849 -0.028472122 ;
	setAttr ".pt[53]" -type "float3" 0.019551437 -0.064907849 -0.026991103 ;
	setAttr ".pt[54]" -type "float3" 0.01681548 -0.064907849 -0.025214329 ;
	setAttr ".pt[55]" -type "float3" 0.014280196 -0.064907849 -0.023161316 ;
	setAttr ".pt[56]" -type "float3" 0.01197345 -0.064907849 -0.020854538 ;
	setAttr ".pt[57]" -type "float3" 0.0099204574 -0.064907849 -0.018319288 ;
	setAttr ".pt[58]" -type "float3" 0.0081436727 -0.064907849 -0.015583325 ;
	setAttr ".pt[59]" -type "float3" 0.0066626295 -0.064907849 -0.012676611 ;
	setAttr ".pt[60]" -type "float3" 0.0054935142 -0.064907849 -0.0096310163 ;
	setAttr ".pt[61]" -type "float3" 0.0046492144 -0.064907849 -0.0064799134 ;
	setAttr ".pt[62]" -type "float3" 0.0041388338 -0.064907849 -0.0032578055 ;
	setAttr ".pt[63]" -type "float3" 0.0039681112 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[66]" -type "float3" 0.085886188 -0.064907849 -0.031166604 ;
	setAttr ".pt[72]" -type "float3" 0.085886188 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[121]" -type "float3" 0.03513477 -0.064907849 0.031166604 ;
	setAttr ".pt[122]" -type "float3" 0.031876974 -0.064907849 0.030995879 ;
	setAttr ".pt[123]" -type "float3" 0.028654853 -0.064907849 0.030485567 ;
	setAttr ".pt[124]" -type "float3" 0.025503734 -0.064907849 0.029641215 ;
	setAttr ".pt[125]" -type "float3" 0.022458173 -0.064907849 0.028472107 ;
	setAttr ".pt[126]" -type "float3" 0.019551437 -0.064907849 0.026991079 ;
	setAttr ".pt[127]" -type "float3" 0.01681548 -0.064907849 0.025214313 ;
	setAttr ".pt[128]" -type "float3" 0.014280196 -0.064907849 0.023161313 ;
	setAttr ".pt[129]" -type "float3" 0.01197345 -0.064907849 0.020854548 ;
	setAttr ".pt[130]" -type "float3" 0.0099204574 -0.064907849 0.018319286 ;
	setAttr ".pt[131]" -type "float3" 0.0081436727 -0.064907849 0.015583316 ;
	setAttr ".pt[132]" -type "float3" 0.0066626295 -0.064907849 0.012676615 ;
	setAttr ".pt[133]" -type "float3" 0.0054935142 -0.064907849 0.0096310098 ;
	setAttr ".pt[134]" -type "float3" 0.0046492144 -0.064907849 0.0064799096 ;
	setAttr ".pt[135]" -type "float3" 0.0041388338 -0.064907849 0.003257805 ;
	setAttr ".pt[138]" -type "float3" 0.085886188 -0.064907849 0.031166604 ;
createNode transform -n "pTorus4";
	rename -uid "F4C5511F-094B-5698-F98A-9DA340FD823A";
	setAttr ".t" -type "double3" 2.7957363266636235 1.3578873562295586 1.1176843793335578 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
	setAttr ".rpt" -type "double3" 0.3941378593444822 -0.39413785934448242 0 ;
	setAttr ".sp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
createNode transform -n "transform6" -p "pTorus4";
	rename -uid "4EC366F6-3C47-C837-AD72-6DB5AFF5B42D";
	setAttr ".v" no;
createNode mesh -n "pTorusShape4" -p "transform6";
	rename -uid "476B6D48-B549-6963-A9E5-F598C6510CCB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 11 ".iog[0].og";
	setAttr ".iog[0].og[4].gcl" -type "componentList" 8 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]";
	setAttr ".iog[0].og[5].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[6].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[7].gcl" -type "componentList" 3 "e[142]" "e[144]" "e[146:147]";
	setAttr ".iog[0].og[10].gcl" -type "componentList" 1 "f[0:143]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 3 ".ciog[0].cog";
	setAttr ".pv" -type "double2" 0.85833317041397095 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 184 ".uvst[0].uvsp[0:183]" -type "float2" 0.73333329 1 0.74999994
		 1 0.76666659 1 0.78333324 1 0.79999989 1 0.81666654 1 0.83333319 1 0.84999985 1 0.8666665
		 1 0.88333315 1 0.8999998 1 0.91666645 1 0.9333331 1 0.94999975 1 0.9666664 1 0.98333305
		 1 0.73333329 0.75 0.74999994 0.75 0.76666659 0.75 0.78333324 0.75 0.79999989 0.75
		 0.81666654 0.75 0.83333319 0.75 0.84999985 0.75 0.8666665 0.75 0.88333315 0.75 0.8999998
		 0.75 0.91666645 0.75 0.9333331 0.75 0.94999975 0.75 0.9666664 0.75 0.98333305 0.75
		 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989 0.5 0.81666654
		 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998 0.5 0.91666645
		 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329 0.25 0.74999994
		 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25 0.83333319 0.25
		 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645 0.25 0.9333331
		 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994 0 0.76666659
		 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665 0 0.88333315
		 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305 0 0.73333329
		 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0.75 0.73333329 0 0.73333329
		 0.25 0.73333329 0.5 0.73333329 0 0.73333329 0.25 0.73333329 0.25 0.73333329 0 0.74999994
		 1 0.73333329 1 0.73333329 0.75 0.74999994 0.75 0.76666659 1 0.76666659 0.75 0.78333324
		 1 0.78333324 0.75 0.79999989 1 0.79999989 0.75 0.81666654 1 0.81666654 0.75 0.83333319
		 1 0.83333319 0.75 0.84999985 1 0.84999985 0.75 0.8666665 1 0.8666665 0.75 0.88333315
		 1 0.88333315 0.75 0.8999998 1 0.8999998 0.75 0.91666645 1 0.91666645 0.75 0.9333331
		 1 0.9333331 0.75 0.94999975 1 0.94999975 0.75 0.9666664 1 0.9666664 0.75 0.98333305
		 1 0.98333305 0.75 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989
		 0.5 0.81666654 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998
		 0.5 0.91666645 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329
		 0.25 0.74999994 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25
		 0.83333319 0.25 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645
		 0.25 0.9333331 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994
		 0 0.76666659 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665
		 0 0.88333315 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305
		 0 0.73333329 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0 0.73333329
		 0.75 0.73333329 0.25 0.73333329 0.5 0.73333329 0.25 0.73333329 0 0.73333329 0.25
		 0.73333329 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".pt";
	setAttr ".pt[48]" -type "float3" 0.03513477 -0.064907849 -0.031166604 ;
	setAttr ".pt[49]" -type "float3" 0.031876974 -0.064907849 -0.03099589 ;
	setAttr ".pt[50]" -type "float3" 0.028654853 -0.064907849 -0.030485559 ;
	setAttr ".pt[51]" -type "float3" 0.025503734 -0.064907849 -0.02964123 ;
	setAttr ".pt[52]" -type "float3" 0.022458173 -0.064907849 -0.028472122 ;
	setAttr ".pt[53]" -type "float3" 0.019551437 -0.064907849 -0.026991103 ;
	setAttr ".pt[54]" -type "float3" 0.01681548 -0.064907849 -0.025214329 ;
	setAttr ".pt[55]" -type "float3" 0.014280196 -0.064907849 -0.023161316 ;
	setAttr ".pt[56]" -type "float3" 0.01197345 -0.064907849 -0.020854538 ;
	setAttr ".pt[57]" -type "float3" 0.0099204574 -0.064907849 -0.018319288 ;
	setAttr ".pt[58]" -type "float3" 0.0081436727 -0.064907849 -0.015583325 ;
	setAttr ".pt[59]" -type "float3" 0.0066626295 -0.064907849 -0.012676611 ;
	setAttr ".pt[60]" -type "float3" 0.0054935142 -0.064907849 -0.0096310163 ;
	setAttr ".pt[61]" -type "float3" 0.0046492144 -0.064907849 -0.0064799134 ;
	setAttr ".pt[62]" -type "float3" 0.0041388338 -0.064907849 -0.0032578055 ;
	setAttr ".pt[63]" -type "float3" 0.0039681112 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[66]" -type "float3" 0.085886188 -0.064907849 -0.031166604 ;
	setAttr ".pt[72]" -type "float3" 0.085886188 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[121]" -type "float3" 0.03513477 -0.064907849 0.031166604 ;
	setAttr ".pt[122]" -type "float3" 0.031876974 -0.064907849 0.030995879 ;
	setAttr ".pt[123]" -type "float3" 0.028654853 -0.064907849 0.030485567 ;
	setAttr ".pt[124]" -type "float3" 0.025503734 -0.064907849 0.029641215 ;
	setAttr ".pt[125]" -type "float3" 0.022458173 -0.064907849 0.028472107 ;
	setAttr ".pt[126]" -type "float3" 0.019551437 -0.064907849 0.026991079 ;
	setAttr ".pt[127]" -type "float3" 0.01681548 -0.064907849 0.025214313 ;
	setAttr ".pt[128]" -type "float3" 0.014280196 -0.064907849 0.023161313 ;
	setAttr ".pt[129]" -type "float3" 0.01197345 -0.064907849 0.020854548 ;
	setAttr ".pt[130]" -type "float3" 0.0099204574 -0.064907849 0.018319286 ;
	setAttr ".pt[131]" -type "float3" 0.0081436727 -0.064907849 0.015583316 ;
	setAttr ".pt[132]" -type "float3" 0.0066626295 -0.064907849 0.012676615 ;
	setAttr ".pt[133]" -type "float3" 0.0054935142 -0.064907849 0.0096310098 ;
	setAttr ".pt[134]" -type "float3" 0.0046492144 -0.064907849 0.0064799096 ;
	setAttr ".pt[135]" -type "float3" 0.0041388338 -0.064907849 0.003257805 ;
	setAttr ".pt[138]" -type "float3" 0.085886188 -0.064907849 0.031166604 ;
	setAttr -s 144 ".vt[0:143]"  0 0.03535533 0.31464475 0.032889366 0.03535533 0.31292099
		 0.065418363 0.03535533 0.307769 0.097230554 0.03535533 0.29924482 0.12797761 0.03535533 0.28744215
		 0.15732241 0.03535533 0.27249032 0.18494356 0.03535533 0.2545529 0.21053839 0.03535533 0.23382658
		 0.23382664 0.03535533 0.21053833 0.25455284 0.03535533 0.1849435 0.27249026 0.03535533 0.15732235
		 0.28744221 0.03535533 0.12797755 0.29924488 0.03535533 0.097230613 0.30776894 0.03535533 0.065418303
		 0.31292105 0.03535533 0.032889307 0.31464469 0.03535533 -7.5649616e-08 0 0.03535533 0.37460828
		 0.0402807 0.03535533 0.37460828 0.080119967 0.03535533 0.37460828 0.11908138 0.03535533 0.37460828
		 0.15673816 0.03535533 0.37460828 0.19267774 0.03535533 0.37460828 0.2740916 0.03535533 0.37460828
		 0.56961167 0.03535533 0.37460828 0.56961167 0.03535533 0.30112225 0.56961167 0.03535533 0.24240094
		 0.56961167 0.03535533 0.19267768 0.56961167 0.03535533 0.1567381 0.56961167 0.03535533 0.11908132
		 0.56961167 0.03535533 0.080119908 0.56961167 0.03535533 0.042046666 0.56961167 0.03535533 -7.5649616e-08
		 0 -0.03535533 0.37460828 0.0402807 -0.03535533 0.37460828 0.080119967 -0.03535533 0.37460828
		 0.11908138 -0.03535533 0.37460828 0.15673816 -0.03535533 0.37460828 0.19267774 -0.03535533 0.37460828
		 0.2740916 -0.03535533 0.37460828 0.56961167 -0.03535533 0.37460828 0.56961167 -0.03535533 0.30112225
		 0.56961167 -0.03535533 0.24240094 0.56961167 -0.03535533 0.19267768 0.56961167 -0.03535533 0.1567381
		 0.56961167 -0.03535533 0.11908132 0.56961167 -0.03535533 0.080119908 0.56961167 -0.03535533 0.042046666
		 0.56961167 -0.03535533 -7.5649616e-08 0 -0.03535533 0.31464463 0.032889366 -0.03535533 0.31292099
		 0.065418363 -0.03535533 0.30776888 0.097230554 -0.03535533 0.29924482 0.12797749 -0.03535533 0.28744215
		 0.15732241 -0.03535533 0.2724902 0.18494356 -0.03535533 0.2545529 0.21053839 -0.03535533 0.23382658
		 0.23382652 -0.03535533 0.21053833 0.25455284 -0.03535533 0.1849435 0.27249026 -0.03535533 0.15732235
		 0.28744221 -0.03535533 0.12797755 0.29924488 -0.03535533 0.097230494 0.30776894 -0.03535533 0.065418303
		 0.31292105 -0.03535533 0.032889307 0.31464458 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 0.31464475
		 -0.51236534 0.03535533 0.37460828 -0.51236534 -0.03535533 0.31464463 -0.51236534 -0.03535533 0.37460828
		 -1.35788739 0.03535533 0.31464475 -1.35788739 0.03535533 0.37460828 -1.35788739 -0.03535533 0.31464463
		 -1.35788739 -0.03535533 0.37460828 -0.51236534 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 -7.5649616e-08
		 -1.35788739 -0.03535533 -7.5649616e-08 -1.35788739 0.03535533 -7.5649616e-08 -3.2154015e-19 0.03535533 -0.3146449
		 0.032889366 0.03535533 -0.31292114 0.065418363 0.03535533 -0.30776915 0.097230554 0.03535533 -0.29924497
		 0.12797761 0.03535533 -0.2874423 0.15732241 0.03535533 -0.27249047 0.18494356 0.03535533 -0.25455305
		 0.21053839 0.03535533 -0.23382673 0.23382664 0.03535533 -0.21053848 0.25455284 0.03535533 -0.18494365
		 0.27249026 0.03535533 -0.1573225 0.28744221 0.03535533 -0.1279777 0.29924488 0.03535533 -0.097230762
		 0.30776894 0.03535533 -0.065418452 0.31292105 0.03535533 -0.032889459 0 0.03535533 -0.37460843
		 0.0402807 0.03535533 -0.37460843 0.080119967 0.03535533 -0.37460843 0.11908138 0.03535533 -0.37460843
		 0.15673816 0.03535533 -0.37460843 0.19267774 0.03535533 -0.37460843 0.2740916 0.03535533 -0.37460843
		 0.56961167 0.03535533 -0.37460843 0.56961167 0.03535533 -0.3011224 0.56961167 0.03535533 -0.24240109
		 0.56961167 0.03535533 -0.19267783 0.56961167 0.03535533 -0.15673825 0.56961167 0.03535533 -0.11908147
		 0.56961167 0.03535533 -0.080120057 0.56961167 0.03535533 -0.042046819 0 -0.03535533 -0.37460843
		 0.0402807 -0.03535533 -0.37460843 0.080119967 -0.03535533 -0.37460843 0.11908138 -0.03535533 -0.37460843
		 0.15673816 -0.03535533 -0.37460843 0.19267774 -0.03535533 -0.37460843 0.2740916 -0.03535533 -0.37460843
		 0.56961167 -0.03535533 -0.37460843 0.56961167 -0.03535533 -0.3011224 0.56961167 -0.03535533 -0.24240109
		 0.56961167 -0.03535533 -0.19267783 0.56961167 -0.03535533 -0.15673825 0.56961167 -0.03535533 -0.11908147
		 0.56961167 -0.03535533 -0.080120057 0.56961167 -0.03535533 -0.042046819 -3.2154025e-19 -0.03535533 -0.31464478
		 0.032889366 -0.03535533 -0.31292114 0.065418363 -0.03535533 -0.30776903 0.097230554 -0.03535533 -0.29924497
		 0.12797749 -0.03535533 -0.2874423 0.15732241 -0.03535533 -0.27249035 0.18494356 -0.03535533 -0.25455305
		 0.21053839 -0.03535533 -0.23382673 0.23382652 -0.03535533 -0.21053848 0.25455284 -0.03535533 -0.18494365
		 0.27249026 -0.03535533 -0.1573225 0.28744221 -0.03535533 -0.1279777 0.29924488 -0.03535533 -0.097230643
		 0.30776894 -0.03535533 -0.065418452 0.31292105 -0.03535533 -0.032889459 -0.51236534 0.03535533 -0.3146449
		 -0.51236534 0.03535533 -0.37460843 -0.51236534 -0.03535533 -0.31464478 -0.51236534 -0.03535533 -0.37460843
		 -1.35788739 0.03535533 -0.3146449 -1.35788739 0.03535533 -0.37460843 -1.35788739 -0.03535533 -0.31464478
		 -1.35788739 -0.03535533 -0.37460843;
	setAttr -s 288 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 16 17 0 17 18 0 18 19 0 19 20 0 20 21 0
		 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0 32 33 0
		 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 42 0 42 43 0 43 44 0
		 44 45 0 45 46 0 46 47 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0
		 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 0 16 0 1 17 1 2 18 1 3 19 1
		 4 20 1 5 21 1 6 22 1 7 23 1 8 24 1 9 25 1 10 26 1 11 27 1 12 28 1 13 29 1 14 30 1
		 15 31 1 16 32 0 17 33 1 18 34 1 19 35 1 20 36 1 21 37 1 22 38 1 23 39 1 24 40 1 25 41 1
		 26 42 1 27 43 1 28 44 1 29 45 1 30 46 1 31 47 1 32 48 0 33 49 1 34 50 1 35 51 1 36 52 1
		 37 53 1 38 54 1 39 55 1 40 56 1 41 57 1 42 58 1 43 59 1 44 60 1 45 61 1 46 62 1 47 63 1
		 48 0 0 49 1 1 50 2 1 51 3 1 52 4 1 53 5 1 54 6 1 55 7 1 56 8 1 57 9 1 58 10 1 59 11 1
		 60 12 1 61 13 1 62 14 1 63 15 1 0 64 0 16 65 0 64 65 0 48 66 0 66 64 0 32 67 0 67 66 0
		 65 67 0 64 68 0 65 69 0 68 69 0 66 70 0 70 68 0 67 71 0 71 70 0 69 71 0 66 72 0 64 73 0
		 72 73 1 70 74 0 72 74 1 68 75 0 74 75 1 73 75 1 76 77 0 76 91 0 91 92 0 77 92 1 77 78 0
		 92 93 0 78 93 1 78 79 0 93 94 0 79 94 1 79 80 0 94 95 0 80 95 1 80 81 0 95 96 0 81 96 1
		 81 82 0 96 97 0;
	setAttr ".ed[166:287]" 82 97 1 82 83 0 97 98 0 83 98 1 83 84 0 98 99 0 84 99 1
		 84 85 0 99 100 0 85 100 1 85 86 0 100 101 0 86 101 1 86 87 0 101 102 0 87 102 1 87 88 0
		 102 103 0 88 103 1 88 89 0 103 104 0 89 104 1 89 90 0 104 105 0 90 105 1 90 15 0
		 105 31 0 91 106 0 106 107 0 92 107 1 107 108 0 93 108 1 108 109 0 94 109 1 109 110 0
		 95 110 1 110 111 0 96 111 1 111 112 0 97 112 1 112 113 0 98 113 1 113 114 0 99 114 1
		 114 115 0 100 115 1 115 116 0 101 116 1 116 117 0 102 117 1 117 118 0 103 118 1 118 119 0
		 104 119 1 119 120 0 105 120 1 120 47 0 106 121 0 121 122 0 107 122 1 122 123 0 108 123 1
		 123 124 0 109 124 1 124 125 0 110 125 1 125 126 0 111 126 1 126 127 0 112 127 1 127 128 0
		 113 128 1 128 129 0 114 129 1 129 130 0 115 130 1 130 131 0 116 131 1 131 132 0 117 132 1
		 132 133 0 118 133 1 133 134 0 119 134 1 134 135 0 120 135 1 135 63 0 121 76 0 122 77 1
		 123 78 1 124 79 1 125 80 1 126 81 1 127 82 1 128 83 1 129 84 1 130 85 1 131 86 1
		 132 87 1 133 88 1 134 89 1 135 90 1 140 141 0 142 140 0 143 142 0 141 143 0 76 136 0
		 136 137 0 91 137 0 121 138 0 138 136 0 106 139 0 139 138 0 137 139 0 136 140 0 137 141 0
		 139 143 0 138 142 0 138 72 0 136 73 0 142 74 0 140 75 0;
	setAttr -s 144 -ch 576 ".fc[0:143]" -type "polyFaces" 
		f 4 -1 60 15 -62
		mu 0 4 1 0 16 17
		f 4 -2 61 16 -63
		mu 0 4 2 1 17 18
		f 4 -3 62 17 -64
		mu 0 4 3 2 18 19
		f 4 -4 63 18 -65
		mu 0 4 4 3 19 20
		f 4 -5 64 19 -66
		mu 0 4 5 4 20 21
		f 4 -6 65 20 -67
		mu 0 4 6 5 21 22
		f 4 -7 66 21 -68
		mu 0 4 7 6 22 23
		f 4 -8 67 22 -69
		mu 0 4 8 7 23 24
		f 4 -9 68 23 -70
		mu 0 4 9 8 24 25
		f 4 -10 69 24 -71
		mu 0 4 10 9 25 26
		f 4 -11 70 25 -72
		mu 0 4 11 10 26 27
		f 4 -12 71 26 -73
		mu 0 4 12 11 27 28
		f 4 -13 72 27 -74
		mu 0 4 13 12 28 29
		f 4 -14 73 28 -75
		mu 0 4 14 13 29 30
		f 4 -15 74 29 -76
		mu 0 4 15 14 30 31
		f 4 -16 76 30 -78
		mu 0 4 17 16 32 33
		f 4 -17 77 31 -79
		mu 0 4 18 17 33 34
		f 4 -18 78 32 -80
		mu 0 4 19 18 34 35
		f 4 -19 79 33 -81
		mu 0 4 20 19 35 36
		f 4 -20 80 34 -82
		mu 0 4 21 20 36 37
		f 4 -21 81 35 -83
		mu 0 4 22 21 37 38
		f 4 -22 82 36 -84
		mu 0 4 23 22 38 39
		f 4 -23 83 37 -85
		mu 0 4 24 23 39 40
		f 4 -24 84 38 -86
		mu 0 4 25 24 40 41
		f 4 -25 85 39 -87
		mu 0 4 26 25 41 42
		f 4 -26 86 40 -88
		mu 0 4 27 26 42 43
		f 4 -27 87 41 -89
		mu 0 4 28 27 43 44
		f 4 -28 88 42 -90
		mu 0 4 29 28 44 45
		f 4 -29 89 43 -91
		mu 0 4 30 29 45 46
		f 4 -30 90 44 -92
		mu 0 4 31 30 46 47
		f 4 -31 92 45 -94
		mu 0 4 33 32 48 49
		f 4 -32 93 46 -95
		mu 0 4 34 33 49 50
		f 4 -33 94 47 -96
		mu 0 4 35 34 50 51
		f 4 -34 95 48 -97
		mu 0 4 36 35 51 52
		f 4 -35 96 49 -98
		mu 0 4 37 36 52 53
		f 4 -36 97 50 -99
		mu 0 4 38 37 53 54
		f 4 -37 98 51 -100
		mu 0 4 39 38 54 55
		f 4 -38 99 52 -101
		mu 0 4 40 39 55 56
		f 4 -39 100 53 -102
		mu 0 4 41 40 56 57
		f 4 -40 101 54 -103
		mu 0 4 42 41 57 58
		f 4 -41 102 55 -104
		mu 0 4 43 42 58 59
		f 4 -42 103 56 -105
		mu 0 4 44 43 59 60
		f 4 -43 104 57 -106
		mu 0 4 45 44 60 61
		f 4 -44 105 58 -107
		mu 0 4 46 45 61 62
		f 4 -45 106 59 -108
		mu 0 4 47 46 62 63
		f 4 -46 108 0 -110
		mu 0 4 49 48 64 65
		f 4 -47 109 1 -111
		mu 0 4 50 49 65 66
		f 4 -48 110 2 -112
		mu 0 4 51 50 66 67
		f 4 -49 111 3 -113
		mu 0 4 52 51 67 68
		f 4 -50 112 4 -114
		mu 0 4 53 52 68 69
		f 4 -51 113 5 -115
		mu 0 4 54 53 69 70
		f 4 -52 114 6 -116
		mu 0 4 55 54 70 71
		f 4 -53 115 7 -117
		mu 0 4 56 55 71 72
		f 4 -54 116 8 -118
		mu 0 4 57 56 72 73
		f 4 -55 117 9 -119
		mu 0 4 58 57 73 74
		f 4 -56 118 10 -120
		mu 0 4 59 58 74 75
		f 4 -57 119 11 -121
		mu 0 4 60 59 75 76
		f 4 -58 120 12 -122
		mu 0 4 61 60 76 77
		f 4 -59 121 13 -123
		mu 0 4 62 61 77 78
		f 4 -60 122 14 -124
		mu 0 4 63 62 78 79
		f 4 -135 -137 -139 -140
		mu 0 4 84 85 86 87
		f 4 -61 124 126 -126
		mu 0 4 16 64 81 80
		f 4 -109 127 128 -125
		mu 0 4 64 48 82 81
		f 4 -93 129 130 -128
		mu 0 4 48 32 83 82
		f 4 -77 125 131 -130
		mu 0 4 32 16 80 83
		f 4 -127 132 134 -134
		mu 0 4 80 81 85 84
		f 4 -131 137 138 -136
		mu 0 4 82 83 87 86
		f 4 -132 133 139 -138
		mu 0 4 83 80 84 87
		f 4 -129 140 142 -142
		mu 0 4 81 82 89 88
		f 4 135 143 -145 -141
		mu 0 4 82 86 90 89
		f 4 136 145 -147 -144
		mu 0 4 86 85 91 90
		f 4 -133 141 147 -146
		mu 0 4 85 81 88 91
		f 4 151 -151 -150 148
		mu 0 4 92 95 94 93
		f 4 154 -154 -152 152
		mu 0 4 96 97 95 92
		f 4 157 -157 -155 155
		mu 0 4 98 99 97 96
		f 4 160 -160 -158 158
		mu 0 4 100 101 99 98
		f 4 163 -163 -161 161
		mu 0 4 102 103 101 100
		f 4 166 -166 -164 164
		mu 0 4 104 105 103 102
		f 4 169 -169 -167 167
		mu 0 4 106 107 105 104
		f 4 172 -172 -170 170
		mu 0 4 108 109 107 106
		f 4 175 -175 -173 173
		mu 0 4 110 111 109 108
		f 4 178 -178 -176 176
		mu 0 4 112 113 111 110
		f 4 181 -181 -179 179
		mu 0 4 114 115 113 112
		f 4 184 -184 -182 182
		mu 0 4 116 117 115 114
		f 4 187 -187 -185 185
		mu 0 4 118 119 117 116
		f 4 190 -190 -188 188
		mu 0 4 120 121 119 118
		f 4 75 -193 -191 191
		mu 0 4 122 123 121 120
		f 4 195 -195 -194 150
		mu 0 4 95 125 124 94
		f 4 197 -197 -196 153
		mu 0 4 97 126 125 95
		f 4 199 -199 -198 156
		mu 0 4 99 127 126 97
		f 4 201 -201 -200 159
		mu 0 4 101 128 127 99
		f 4 203 -203 -202 162
		mu 0 4 103 129 128 101
		f 4 205 -205 -204 165
		mu 0 4 105 130 129 103
		f 4 207 -207 -206 168
		mu 0 4 107 131 130 105
		f 4 209 -209 -208 171
		mu 0 4 109 132 131 107
		f 4 211 -211 -210 174
		mu 0 4 111 133 132 109
		f 4 213 -213 -212 177
		mu 0 4 113 134 133 111
		f 4 215 -215 -214 180
		mu 0 4 115 135 134 113
		f 4 217 -217 -216 183
		mu 0 4 117 136 135 115
		f 4 219 -219 -218 186
		mu 0 4 119 137 136 117
		f 4 221 -221 -220 189
		mu 0 4 121 138 137 119
		f 4 91 -223 -222 192
		mu 0 4 123 139 138 121
		f 4 225 -225 -224 194
		mu 0 4 125 141 140 124
		f 4 227 -227 -226 196
		mu 0 4 126 142 141 125
		f 4 229 -229 -228 198
		mu 0 4 127 143 142 126
		f 4 231 -231 -230 200
		mu 0 4 128 144 143 127
		f 4 233 -233 -232 202
		mu 0 4 129 145 144 128
		f 4 235 -235 -234 204
		mu 0 4 130 146 145 129
		f 4 237 -237 -236 206
		mu 0 4 131 147 146 130
		f 4 239 -239 -238 208
		mu 0 4 132 148 147 131
		f 4 241 -241 -240 210
		mu 0 4 133 149 148 132
		f 4 243 -243 -242 212
		mu 0 4 134 150 149 133
		f 4 245 -245 -244 214
		mu 0 4 135 151 150 134
		f 4 247 -247 -246 216
		mu 0 4 136 152 151 135
		f 4 249 -249 -248 218
		mu 0 4 137 153 152 136
		f 4 251 -251 -250 220
		mu 0 4 138 154 153 137
		f 4 107 -253 -252 222
		mu 0 4 139 155 154 138
		f 4 254 -149 -254 224
		mu 0 4 141 157 156 140
		f 4 255 -153 -255 226
		mu 0 4 142 158 157 141
		f 4 256 -156 -256 228
		mu 0 4 143 159 158 142
		f 4 257 -159 -257 230
		mu 0 4 144 160 159 143
		f 4 258 -162 -258 232
		mu 0 4 145 161 160 144
		f 4 259 -165 -259 234
		mu 0 4 146 162 161 145
		f 4 260 -168 -260 236
		mu 0 4 147 163 162 146
		f 4 261 -171 -261 238
		mu 0 4 148 164 163 147
		f 4 262 -174 -262 240
		mu 0 4 149 165 164 148
		f 4 263 -177 -263 242
		mu 0 4 150 166 165 149
		f 4 264 -180 -264 244
		mu 0 4 151 167 166 150
		f 4 265 -183 -265 246
		mu 0 4 152 168 167 151
		f 4 266 -186 -266 248
		mu 0 4 153 169 168 152
		f 4 267 -189 -267 250
		mu 0 4 154 170 169 153
		f 4 123 -192 -268 252
		mu 0 4 155 171 170 154
		f 4 271 270 269 268
		mu 0 4 172 175 174 173
		f 4 274 -274 -273 149
		mu 0 4 94 177 176 156
		f 4 272 -277 -276 253
		mu 0 4 156 176 178 140
		f 4 275 -279 -278 223
		mu 0 4 140 178 179 124
		f 4 277 -280 -275 193
		mu 0 4 124 179 177 94
		f 4 281 -269 -281 273
		mu 0 4 177 172 173 176
		f 4 283 -271 -283 278
		mu 0 4 178 174 175 179
		f 4 282 -272 -282 279
		mu 0 4 179 175 172 177
		f 4 285 -143 -285 276
		mu 0 4 176 181 180 178
		f 4 284 144 -287 -284
		mu 0 4 178 180 182 174
		f 4 286 146 -288 -270
		mu 0 4 174 182 183 173
		f 4 287 -148 -286 280
		mu 0 4 173 183 181 176;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pTorus5";
	rename -uid "C1CF6683-3847-4502-6BE7-FA9A1131A23B";
	setAttr ".t" -type "double3" 2.7957363266636226 1.3578873562295588 -0.3636638148013458 ;
	setAttr ".r" -type "double3" 0 0 89.999999999999986 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
	setAttr ".rp" -type "double3" -0.39413785934448253 0 -7.5649615682849003e-08 ;
	setAttr ".rpt" -type "double3" 0.39413785934448287 -0.39413785934448264 0 ;
	setAttr ".sp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
	setAttr ".spt" -type "double3" -1.1102230246251568e-16 0 0 ;
createNode transform -n "transform3" -p "pTorus5";
	rename -uid "5ED83EFF-7F4A-BC15-7049-20A7A29F3455";
	setAttr ".v" no;
createNode mesh -n "pTorusShape5" -p "transform3";
	rename -uid "2D43F3C3-2C46-E8CC-9E46-D8A85F322283";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 9 ".iog[0].og";
	setAttr ".iog[0].og[4].gcl" -type "componentList" 8 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]";
	setAttr ".iog[0].og[5].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[6].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[7].gcl" -type "componentList" 3 "e[142]" "e[144]" "e[146:147]";
	setAttr ".iog[0].og[8].gcl" -type "componentList" 1 "f[0:143]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.85833317041397095 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 184 ".uvst[0].uvsp[0:183]" -type "float2" 0.73333329 1 0.74999994
		 1 0.76666659 1 0.78333324 1 0.79999989 1 0.81666654 1 0.83333319 1 0.84999985 1 0.8666665
		 1 0.88333315 1 0.8999998 1 0.91666645 1 0.9333331 1 0.94999975 1 0.9666664 1 0.98333305
		 1 0.73333329 0.75 0.74999994 0.75 0.76666659 0.75 0.78333324 0.75 0.79999989 0.75
		 0.81666654 0.75 0.83333319 0.75 0.84999985 0.75 0.8666665 0.75 0.88333315 0.75 0.8999998
		 0.75 0.91666645 0.75 0.9333331 0.75 0.94999975 0.75 0.9666664 0.75 0.98333305 0.75
		 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989 0.5 0.81666654
		 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998 0.5 0.91666645
		 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329 0.25 0.74999994
		 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25 0.83333319 0.25
		 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645 0.25 0.9333331
		 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994 0 0.76666659
		 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665 0 0.88333315
		 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305 0 0.73333329
		 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0.75 0.73333329 0 0.73333329
		 0.25 0.73333329 0.5 0.73333329 0 0.73333329 0.25 0.73333329 0.25 0.73333329 0 0.74999994
		 1 0.73333329 1 0.73333329 0.75 0.74999994 0.75 0.76666659 1 0.76666659 0.75 0.78333324
		 1 0.78333324 0.75 0.79999989 1 0.79999989 0.75 0.81666654 1 0.81666654 0.75 0.83333319
		 1 0.83333319 0.75 0.84999985 1 0.84999985 0.75 0.8666665 1 0.8666665 0.75 0.88333315
		 1 0.88333315 0.75 0.8999998 1 0.8999998 0.75 0.91666645 1 0.91666645 0.75 0.9333331
		 1 0.9333331 0.75 0.94999975 1 0.94999975 0.75 0.9666664 1 0.9666664 0.75 0.98333305
		 1 0.98333305 0.75 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989
		 0.5 0.81666654 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998
		 0.5 0.91666645 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329
		 0.25 0.74999994 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25
		 0.83333319 0.25 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645
		 0.25 0.9333331 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994
		 0 0.76666659 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665
		 0 0.88333315 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305
		 0 0.73333329 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0 0.73333329
		 0.75 0.73333329 0.25 0.73333329 0.5 0.73333329 0.25 0.73333329 0 0.73333329 0.25
		 0.73333329 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".pt";
	setAttr ".pt[48]" -type "float3" 0.03513477 -0.064907849 -0.031166604 ;
	setAttr ".pt[49]" -type "float3" 0.031876974 -0.064907849 -0.03099589 ;
	setAttr ".pt[50]" -type "float3" 0.028654853 -0.064907849 -0.030485559 ;
	setAttr ".pt[51]" -type "float3" 0.025503734 -0.064907849 -0.02964123 ;
	setAttr ".pt[52]" -type "float3" 0.022458173 -0.064907849 -0.028472122 ;
	setAttr ".pt[53]" -type "float3" 0.019551437 -0.064907849 -0.026991103 ;
	setAttr ".pt[54]" -type "float3" 0.01681548 -0.064907849 -0.025214329 ;
	setAttr ".pt[55]" -type "float3" 0.014280196 -0.064907849 -0.023161316 ;
	setAttr ".pt[56]" -type "float3" 0.01197345 -0.064907849 -0.020854538 ;
	setAttr ".pt[57]" -type "float3" 0.0099204574 -0.064907849 -0.018319288 ;
	setAttr ".pt[58]" -type "float3" 0.0081436727 -0.064907849 -0.015583325 ;
	setAttr ".pt[59]" -type "float3" 0.0066626295 -0.064907849 -0.012676611 ;
	setAttr ".pt[60]" -type "float3" 0.0054935142 -0.064907849 -0.0096310163 ;
	setAttr ".pt[61]" -type "float3" 0.0046492144 -0.064907849 -0.0064799134 ;
	setAttr ".pt[62]" -type "float3" 0.0041388338 -0.064907849 -0.0032578055 ;
	setAttr ".pt[63]" -type "float3" 0.0039681112 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[66]" -type "float3" 0.085886188 -0.064907849 -0.031166604 ;
	setAttr ".pt[72]" -type "float3" 0.085886188 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[121]" -type "float3" 0.03513477 -0.064907849 0.031166604 ;
	setAttr ".pt[122]" -type "float3" 0.031876974 -0.064907849 0.030995879 ;
	setAttr ".pt[123]" -type "float3" 0.028654853 -0.064907849 0.030485567 ;
	setAttr ".pt[124]" -type "float3" 0.025503734 -0.064907849 0.029641215 ;
	setAttr ".pt[125]" -type "float3" 0.022458173 -0.064907849 0.028472107 ;
	setAttr ".pt[126]" -type "float3" 0.019551437 -0.064907849 0.026991079 ;
	setAttr ".pt[127]" -type "float3" 0.01681548 -0.064907849 0.025214313 ;
	setAttr ".pt[128]" -type "float3" 0.014280196 -0.064907849 0.023161313 ;
	setAttr ".pt[129]" -type "float3" 0.01197345 -0.064907849 0.020854548 ;
	setAttr ".pt[130]" -type "float3" 0.0099204574 -0.064907849 0.018319286 ;
	setAttr ".pt[131]" -type "float3" 0.0081436727 -0.064907849 0.015583316 ;
	setAttr ".pt[132]" -type "float3" 0.0066626295 -0.064907849 0.012676615 ;
	setAttr ".pt[133]" -type "float3" 0.0054935142 -0.064907849 0.0096310098 ;
	setAttr ".pt[134]" -type "float3" 0.0046492144 -0.064907849 0.0064799096 ;
	setAttr ".pt[135]" -type "float3" 0.0041388338 -0.064907849 0.003257805 ;
	setAttr ".pt[138]" -type "float3" 0.085886188 -0.064907849 0.031166604 ;
	setAttr -s 144 ".vt[0:143]"  0 0.03535533 0.31464475 0.032889366 0.03535533 0.31292099
		 0.065418363 0.03535533 0.307769 0.097230554 0.03535533 0.29924482 0.12797761 0.03535533 0.28744215
		 0.15732241 0.03535533 0.27249032 0.18494356 0.03535533 0.2545529 0.21053839 0.03535533 0.23382658
		 0.23382664 0.03535533 0.21053833 0.25455284 0.03535533 0.1849435 0.27249026 0.03535533 0.15732235
		 0.28744221 0.03535533 0.12797755 0.29924488 0.03535533 0.097230613 0.30776894 0.03535533 0.065418303
		 0.31292105 0.03535533 0.032889307 0.31464469 0.03535533 -7.5649616e-08 0 0.03535533 0.37460828
		 0.0402807 0.03535533 0.37460828 0.080119967 0.03535533 0.37460828 0.11908138 0.03535533 0.37460828
		 0.15673816 0.03535533 0.37460828 0.19267774 0.03535533 0.37460828 0.2740916 0.03535533 0.37460828
		 0.56961167 0.03535533 0.37460828 0.56961167 0.03535533 0.30112225 0.56961167 0.03535533 0.24240094
		 0.56961167 0.03535533 0.19267768 0.56961167 0.03535533 0.1567381 0.56961167 0.03535533 0.11908132
		 0.56961167 0.03535533 0.080119908 0.56961167 0.03535533 0.042046666 0.56961167 0.03535533 -7.5649616e-08
		 0 -0.03535533 0.37460828 0.0402807 -0.03535533 0.37460828 0.080119967 -0.03535533 0.37460828
		 0.11908138 -0.03535533 0.37460828 0.15673816 -0.03535533 0.37460828 0.19267774 -0.03535533 0.37460828
		 0.2740916 -0.03535533 0.37460828 0.56961167 -0.03535533 0.37460828 0.56961167 -0.03535533 0.30112225
		 0.56961167 -0.03535533 0.24240094 0.56961167 -0.03535533 0.19267768 0.56961167 -0.03535533 0.1567381
		 0.56961167 -0.03535533 0.11908132 0.56961167 -0.03535533 0.080119908 0.56961167 -0.03535533 0.042046666
		 0.56961167 -0.03535533 -7.5649616e-08 0 -0.03535533 0.31464463 0.032889366 -0.03535533 0.31292099
		 0.065418363 -0.03535533 0.30776888 0.097230554 -0.03535533 0.29924482 0.12797749 -0.03535533 0.28744215
		 0.15732241 -0.03535533 0.2724902 0.18494356 -0.03535533 0.2545529 0.21053839 -0.03535533 0.23382658
		 0.23382652 -0.03535533 0.21053833 0.25455284 -0.03535533 0.1849435 0.27249026 -0.03535533 0.15732235
		 0.28744221 -0.03535533 0.12797755 0.29924488 -0.03535533 0.097230494 0.30776894 -0.03535533 0.065418303
		 0.31292105 -0.03535533 0.032889307 0.31464458 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 0.31464475
		 -0.51236534 0.03535533 0.37460828 -0.51236534 -0.03535533 0.31464463 -0.51236534 -0.03535533 0.37460828
		 -1.35788739 0.03535533 0.31464475 -1.35788739 0.03535533 0.37460828 -1.35788739 -0.03535533 0.31464463
		 -1.35788739 -0.03535533 0.37460828 -0.51236534 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 -7.5649616e-08
		 -1.35788739 -0.03535533 -7.5649616e-08 -1.35788739 0.03535533 -7.5649616e-08 -3.2154015e-19 0.03535533 -0.3146449
		 0.032889366 0.03535533 -0.31292114 0.065418363 0.03535533 -0.30776915 0.097230554 0.03535533 -0.29924497
		 0.12797761 0.03535533 -0.2874423 0.15732241 0.03535533 -0.27249047 0.18494356 0.03535533 -0.25455305
		 0.21053839 0.03535533 -0.23382673 0.23382664 0.03535533 -0.21053848 0.25455284 0.03535533 -0.18494365
		 0.27249026 0.03535533 -0.1573225 0.28744221 0.03535533 -0.1279777 0.29924488 0.03535533 -0.097230762
		 0.30776894 0.03535533 -0.065418452 0.31292105 0.03535533 -0.032889459 0 0.03535533 -0.37460843
		 0.0402807 0.03535533 -0.37460843 0.080119967 0.03535533 -0.37460843 0.11908138 0.03535533 -0.37460843
		 0.15673816 0.03535533 -0.37460843 0.19267774 0.03535533 -0.37460843 0.2740916 0.03535533 -0.37460843
		 0.56961167 0.03535533 -0.37460843 0.56961167 0.03535533 -0.3011224 0.56961167 0.03535533 -0.24240109
		 0.56961167 0.03535533 -0.19267783 0.56961167 0.03535533 -0.15673825 0.56961167 0.03535533 -0.11908147
		 0.56961167 0.03535533 -0.080120057 0.56961167 0.03535533 -0.042046819 0 -0.03535533 -0.37460843
		 0.0402807 -0.03535533 -0.37460843 0.080119967 -0.03535533 -0.37460843 0.11908138 -0.03535533 -0.37460843
		 0.15673816 -0.03535533 -0.37460843 0.19267774 -0.03535533 -0.37460843 0.2740916 -0.03535533 -0.37460843
		 0.56961167 -0.03535533 -0.37460843 0.56961167 -0.03535533 -0.3011224 0.56961167 -0.03535533 -0.24240109
		 0.56961167 -0.03535533 -0.19267783 0.56961167 -0.03535533 -0.15673825 0.56961167 -0.03535533 -0.11908147
		 0.56961167 -0.03535533 -0.080120057 0.56961167 -0.03535533 -0.042046819 -3.2154025e-19 -0.03535533 -0.31464478
		 0.032889366 -0.03535533 -0.31292114 0.065418363 -0.03535533 -0.30776903 0.097230554 -0.03535533 -0.29924497
		 0.12797749 -0.03535533 -0.2874423 0.15732241 -0.03535533 -0.27249035 0.18494356 -0.03535533 -0.25455305
		 0.21053839 -0.03535533 -0.23382673 0.23382652 -0.03535533 -0.21053848 0.25455284 -0.03535533 -0.18494365
		 0.27249026 -0.03535533 -0.1573225 0.28744221 -0.03535533 -0.1279777 0.29924488 -0.03535533 -0.097230643
		 0.30776894 -0.03535533 -0.065418452 0.31292105 -0.03535533 -0.032889459 -0.51236534 0.03535533 -0.3146449
		 -0.51236534 0.03535533 -0.37460843 -0.51236534 -0.03535533 -0.31464478 -0.51236534 -0.03535533 -0.37460843
		 -1.35788739 0.03535533 -0.3146449 -1.35788739 0.03535533 -0.37460843 -1.35788739 -0.03535533 -0.31464478
		 -1.35788739 -0.03535533 -0.37460843;
	setAttr -s 288 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 16 17 0 17 18 0 18 19 0 19 20 0 20 21 0
		 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0 32 33 0
		 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 42 0 42 43 0 43 44 0
		 44 45 0 45 46 0 46 47 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0
		 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 0 16 0 1 17 1 2 18 1 3 19 1
		 4 20 1 5 21 1 6 22 1 7 23 1 8 24 1 9 25 1 10 26 1 11 27 1 12 28 1 13 29 1 14 30 1
		 15 31 1 16 32 0 17 33 1 18 34 1 19 35 1 20 36 1 21 37 1 22 38 1 23 39 1 24 40 1 25 41 1
		 26 42 1 27 43 1 28 44 1 29 45 1 30 46 1 31 47 1 32 48 0 33 49 1 34 50 1 35 51 1 36 52 1
		 37 53 1 38 54 1 39 55 1 40 56 1 41 57 1 42 58 1 43 59 1 44 60 1 45 61 1 46 62 1 47 63 1
		 48 0 0 49 1 1 50 2 1 51 3 1 52 4 1 53 5 1 54 6 1 55 7 1 56 8 1 57 9 1 58 10 1 59 11 1
		 60 12 1 61 13 1 62 14 1 63 15 1 0 64 0 16 65 0 64 65 0 48 66 0 66 64 0 32 67 0 67 66 0
		 65 67 0 64 68 0 65 69 0 68 69 0 66 70 0 70 68 0 67 71 0 71 70 0 69 71 0 66 72 0 64 73 0
		 72 73 1 70 74 0 72 74 1 68 75 0 74 75 1 73 75 1 76 77 0 76 91 0 91 92 0 77 92 1 77 78 0
		 92 93 0 78 93 1 78 79 0 93 94 0 79 94 1 79 80 0 94 95 0 80 95 1 80 81 0 95 96 0 81 96 1
		 81 82 0 96 97 0;
	setAttr ".ed[166:287]" 82 97 1 82 83 0 97 98 0 83 98 1 83 84 0 98 99 0 84 99 1
		 84 85 0 99 100 0 85 100 1 85 86 0 100 101 0 86 101 1 86 87 0 101 102 0 87 102 1 87 88 0
		 102 103 0 88 103 1 88 89 0 103 104 0 89 104 1 89 90 0 104 105 0 90 105 1 90 15 0
		 105 31 0 91 106 0 106 107 0 92 107 1 107 108 0 93 108 1 108 109 0 94 109 1 109 110 0
		 95 110 1 110 111 0 96 111 1 111 112 0 97 112 1 112 113 0 98 113 1 113 114 0 99 114 1
		 114 115 0 100 115 1 115 116 0 101 116 1 116 117 0 102 117 1 117 118 0 103 118 1 118 119 0
		 104 119 1 119 120 0 105 120 1 120 47 0 106 121 0 121 122 0 107 122 1 122 123 0 108 123 1
		 123 124 0 109 124 1 124 125 0 110 125 1 125 126 0 111 126 1 126 127 0 112 127 1 127 128 0
		 113 128 1 128 129 0 114 129 1 129 130 0 115 130 1 130 131 0 116 131 1 131 132 0 117 132 1
		 132 133 0 118 133 1 133 134 0 119 134 1 134 135 0 120 135 1 135 63 0 121 76 0 122 77 1
		 123 78 1 124 79 1 125 80 1 126 81 1 127 82 1 128 83 1 129 84 1 130 85 1 131 86 1
		 132 87 1 133 88 1 134 89 1 135 90 1 140 141 0 142 140 0 143 142 0 141 143 0 76 136 0
		 136 137 0 91 137 0 121 138 0 138 136 0 106 139 0 139 138 0 137 139 0 136 140 0 137 141 0
		 139 143 0 138 142 0 138 72 0 136 73 0 142 74 0 140 75 0;
	setAttr -s 144 -ch 576 ".fc[0:143]" -type "polyFaces" 
		f 4 -1 60 15 -62
		mu 0 4 1 0 16 17
		f 4 -2 61 16 -63
		mu 0 4 2 1 17 18
		f 4 -3 62 17 -64
		mu 0 4 3 2 18 19
		f 4 -4 63 18 -65
		mu 0 4 4 3 19 20
		f 4 -5 64 19 -66
		mu 0 4 5 4 20 21
		f 4 -6 65 20 -67
		mu 0 4 6 5 21 22
		f 4 -7 66 21 -68
		mu 0 4 7 6 22 23
		f 4 -8 67 22 -69
		mu 0 4 8 7 23 24
		f 4 -9 68 23 -70
		mu 0 4 9 8 24 25
		f 4 -10 69 24 -71
		mu 0 4 10 9 25 26
		f 4 -11 70 25 -72
		mu 0 4 11 10 26 27
		f 4 -12 71 26 -73
		mu 0 4 12 11 27 28
		f 4 -13 72 27 -74
		mu 0 4 13 12 28 29
		f 4 -14 73 28 -75
		mu 0 4 14 13 29 30
		f 4 -15 74 29 -76
		mu 0 4 15 14 30 31
		f 4 -16 76 30 -78
		mu 0 4 17 16 32 33
		f 4 -17 77 31 -79
		mu 0 4 18 17 33 34
		f 4 -18 78 32 -80
		mu 0 4 19 18 34 35
		f 4 -19 79 33 -81
		mu 0 4 20 19 35 36
		f 4 -20 80 34 -82
		mu 0 4 21 20 36 37
		f 4 -21 81 35 -83
		mu 0 4 22 21 37 38
		f 4 -22 82 36 -84
		mu 0 4 23 22 38 39
		f 4 -23 83 37 -85
		mu 0 4 24 23 39 40
		f 4 -24 84 38 -86
		mu 0 4 25 24 40 41
		f 4 -25 85 39 -87
		mu 0 4 26 25 41 42
		f 4 -26 86 40 -88
		mu 0 4 27 26 42 43
		f 4 -27 87 41 -89
		mu 0 4 28 27 43 44
		f 4 -28 88 42 -90
		mu 0 4 29 28 44 45
		f 4 -29 89 43 -91
		mu 0 4 30 29 45 46
		f 4 -30 90 44 -92
		mu 0 4 31 30 46 47
		f 4 -31 92 45 -94
		mu 0 4 33 32 48 49
		f 4 -32 93 46 -95
		mu 0 4 34 33 49 50
		f 4 -33 94 47 -96
		mu 0 4 35 34 50 51
		f 4 -34 95 48 -97
		mu 0 4 36 35 51 52
		f 4 -35 96 49 -98
		mu 0 4 37 36 52 53
		f 4 -36 97 50 -99
		mu 0 4 38 37 53 54
		f 4 -37 98 51 -100
		mu 0 4 39 38 54 55
		f 4 -38 99 52 -101
		mu 0 4 40 39 55 56
		f 4 -39 100 53 -102
		mu 0 4 41 40 56 57
		f 4 -40 101 54 -103
		mu 0 4 42 41 57 58
		f 4 -41 102 55 -104
		mu 0 4 43 42 58 59
		f 4 -42 103 56 -105
		mu 0 4 44 43 59 60
		f 4 -43 104 57 -106
		mu 0 4 45 44 60 61
		f 4 -44 105 58 -107
		mu 0 4 46 45 61 62
		f 4 -45 106 59 -108
		mu 0 4 47 46 62 63
		f 4 -46 108 0 -110
		mu 0 4 49 48 64 65
		f 4 -47 109 1 -111
		mu 0 4 50 49 65 66
		f 4 -48 110 2 -112
		mu 0 4 51 50 66 67
		f 4 -49 111 3 -113
		mu 0 4 52 51 67 68
		f 4 -50 112 4 -114
		mu 0 4 53 52 68 69
		f 4 -51 113 5 -115
		mu 0 4 54 53 69 70
		f 4 -52 114 6 -116
		mu 0 4 55 54 70 71
		f 4 -53 115 7 -117
		mu 0 4 56 55 71 72
		f 4 -54 116 8 -118
		mu 0 4 57 56 72 73
		f 4 -55 117 9 -119
		mu 0 4 58 57 73 74
		f 4 -56 118 10 -120
		mu 0 4 59 58 74 75
		f 4 -57 119 11 -121
		mu 0 4 60 59 75 76
		f 4 -58 120 12 -122
		mu 0 4 61 60 76 77
		f 4 -59 121 13 -123
		mu 0 4 62 61 77 78
		f 4 -60 122 14 -124
		mu 0 4 63 62 78 79
		f 4 -135 -137 -139 -140
		mu 0 4 84 85 86 87
		f 4 -61 124 126 -126
		mu 0 4 16 64 81 80
		f 4 -109 127 128 -125
		mu 0 4 64 48 82 81
		f 4 -93 129 130 -128
		mu 0 4 48 32 83 82
		f 4 -77 125 131 -130
		mu 0 4 32 16 80 83
		f 4 -127 132 134 -134
		mu 0 4 80 81 85 84
		f 4 -131 137 138 -136
		mu 0 4 82 83 87 86
		f 4 -132 133 139 -138
		mu 0 4 83 80 84 87
		f 4 -129 140 142 -142
		mu 0 4 81 82 89 88
		f 4 135 143 -145 -141
		mu 0 4 82 86 90 89
		f 4 136 145 -147 -144
		mu 0 4 86 85 91 90
		f 4 -133 141 147 -146
		mu 0 4 85 81 88 91
		f 4 151 -151 -150 148
		mu 0 4 92 95 94 93
		f 4 154 -154 -152 152
		mu 0 4 96 97 95 92
		f 4 157 -157 -155 155
		mu 0 4 98 99 97 96
		f 4 160 -160 -158 158
		mu 0 4 100 101 99 98
		f 4 163 -163 -161 161
		mu 0 4 102 103 101 100
		f 4 166 -166 -164 164
		mu 0 4 104 105 103 102
		f 4 169 -169 -167 167
		mu 0 4 106 107 105 104
		f 4 172 -172 -170 170
		mu 0 4 108 109 107 106
		f 4 175 -175 -173 173
		mu 0 4 110 111 109 108
		f 4 178 -178 -176 176
		mu 0 4 112 113 111 110
		f 4 181 -181 -179 179
		mu 0 4 114 115 113 112
		f 4 184 -184 -182 182
		mu 0 4 116 117 115 114
		f 4 187 -187 -185 185
		mu 0 4 118 119 117 116
		f 4 190 -190 -188 188
		mu 0 4 120 121 119 118
		f 4 75 -193 -191 191
		mu 0 4 122 123 121 120
		f 4 195 -195 -194 150
		mu 0 4 95 125 124 94
		f 4 197 -197 -196 153
		mu 0 4 97 126 125 95
		f 4 199 -199 -198 156
		mu 0 4 99 127 126 97
		f 4 201 -201 -200 159
		mu 0 4 101 128 127 99
		f 4 203 -203 -202 162
		mu 0 4 103 129 128 101
		f 4 205 -205 -204 165
		mu 0 4 105 130 129 103
		f 4 207 -207 -206 168
		mu 0 4 107 131 130 105
		f 4 209 -209 -208 171
		mu 0 4 109 132 131 107
		f 4 211 -211 -210 174
		mu 0 4 111 133 132 109
		f 4 213 -213 -212 177
		mu 0 4 113 134 133 111
		f 4 215 -215 -214 180
		mu 0 4 115 135 134 113
		f 4 217 -217 -216 183
		mu 0 4 117 136 135 115
		f 4 219 -219 -218 186
		mu 0 4 119 137 136 117
		f 4 221 -221 -220 189
		mu 0 4 121 138 137 119
		f 4 91 -223 -222 192
		mu 0 4 123 139 138 121
		f 4 225 -225 -224 194
		mu 0 4 125 141 140 124
		f 4 227 -227 -226 196
		mu 0 4 126 142 141 125
		f 4 229 -229 -228 198
		mu 0 4 127 143 142 126
		f 4 231 -231 -230 200
		mu 0 4 128 144 143 127
		f 4 233 -233 -232 202
		mu 0 4 129 145 144 128
		f 4 235 -235 -234 204
		mu 0 4 130 146 145 129
		f 4 237 -237 -236 206
		mu 0 4 131 147 146 130
		f 4 239 -239 -238 208
		mu 0 4 132 148 147 131
		f 4 241 -241 -240 210
		mu 0 4 133 149 148 132
		f 4 243 -243 -242 212
		mu 0 4 134 150 149 133
		f 4 245 -245 -244 214
		mu 0 4 135 151 150 134
		f 4 247 -247 -246 216
		mu 0 4 136 152 151 135
		f 4 249 -249 -248 218
		mu 0 4 137 153 152 136
		f 4 251 -251 -250 220
		mu 0 4 138 154 153 137
		f 4 107 -253 -252 222
		mu 0 4 139 155 154 138
		f 4 254 -149 -254 224
		mu 0 4 141 157 156 140
		f 4 255 -153 -255 226
		mu 0 4 142 158 157 141
		f 4 256 -156 -256 228
		mu 0 4 143 159 158 142
		f 4 257 -159 -257 230
		mu 0 4 144 160 159 143
		f 4 258 -162 -258 232
		mu 0 4 145 161 160 144
		f 4 259 -165 -259 234
		mu 0 4 146 162 161 145
		f 4 260 -168 -260 236
		mu 0 4 147 163 162 146
		f 4 261 -171 -261 238
		mu 0 4 148 164 163 147
		f 4 262 -174 -262 240
		mu 0 4 149 165 164 148
		f 4 263 -177 -263 242
		mu 0 4 150 166 165 149
		f 4 264 -180 -264 244
		mu 0 4 151 167 166 150
		f 4 265 -183 -265 246
		mu 0 4 152 168 167 151
		f 4 266 -186 -266 248
		mu 0 4 153 169 168 152
		f 4 267 -189 -267 250
		mu 0 4 154 170 169 153
		f 4 123 -192 -268 252
		mu 0 4 155 171 170 154
		f 4 271 270 269 268
		mu 0 4 172 175 174 173
		f 4 274 -274 -273 149
		mu 0 4 94 177 176 156
		f 4 272 -277 -276 253
		mu 0 4 156 176 178 140
		f 4 275 -279 -278 223
		mu 0 4 140 178 179 124
		f 4 277 -280 -275 193
		mu 0 4 124 179 177 94
		f 4 281 -269 -281 273
		mu 0 4 177 172 173 176
		f 4 283 -271 -283 278
		mu 0 4 178 174 175 179
		f 4 282 -272 -282 279
		mu 0 4 179 175 172 177
		f 4 285 -143 -285 276
		mu 0 4 176 181 180 178
		f 4 284 144 -287 -284
		mu 0 4 178 180 182 174
		f 4 286 146 -288 -270
		mu 0 4 174 182 183 173
		f 4 287 -148 -286 280
		mu 0 4 173 183 181 176;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pTorus6";
	rename -uid "52AAC7F5-7A40-7A10-38CA-4E86F15864C5";
	setAttr ".t" -type "double3" 2.7957363266636226 1.357887356229559 -1.8450120089362494 ;
	setAttr ".r" -type "double3" 0 0 89.999999999999986 ;
	setAttr ".s" -type "double3" 1.0000000000000007 1.0000000000000002 1 ;
	setAttr ".rp" -type "double3" -0.3941378593444827 0 -7.5649615682849003e-08 ;
	setAttr ".rpt" -type "double3" 0.39413785934448303 -0.39413785934448281 0 ;
	setAttr ".sp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
	setAttr ".spt" -type "double3" -2.7755575615628933e-16 0 0 ;
createNode transform -n "transform4" -p "pTorus6";
	rename -uid "31A8DE53-5840-BE40-7553-A4A495D50D41";
	setAttr ".v" no;
createNode mesh -n "pTorusShape6" -p "transform4";
	rename -uid "1C926EDC-6D46-E1A3-454C-278E41CB3BA8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 9 ".iog[0].og";
	setAttr ".iog[0].og[4].gcl" -type "componentList" 8 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]";
	setAttr ".iog[0].og[5].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[6].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[7].gcl" -type "componentList" 3 "e[142]" "e[144]" "e[146:147]";
	setAttr ".iog[0].og[8].gcl" -type "componentList" 1 "f[0:143]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.79166656732559204 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 184 ".uvst[0].uvsp[0:183]" -type "float2" 0.73333329 1 0.74999994
		 1 0.76666659 1 0.78333324 1 0.79999989 1 0.81666654 1 0.83333319 1 0.84999985 1 0.8666665
		 1 0.88333315 1 0.8999998 1 0.91666645 1 0.9333331 1 0.94999975 1 0.9666664 1 0.98333305
		 1 0.73333329 0.75 0.74999994 0.75 0.76666659 0.75 0.78333324 0.75 0.79999989 0.75
		 0.81666654 0.75 0.83333319 0.75 0.84999985 0.75 0.8666665 0.75 0.88333315 0.75 0.8999998
		 0.75 0.91666645 0.75 0.9333331 0.75 0.94999975 0.75 0.9666664 0.75 0.98333305 0.75
		 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989 0.5 0.81666654
		 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998 0.5 0.91666645
		 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329 0.25 0.74999994
		 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25 0.83333319 0.25
		 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645 0.25 0.9333331
		 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994 0 0.76666659
		 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665 0 0.88333315
		 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305 0 0.73333329
		 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0.75 0.73333329 0 0.73333329
		 0.25 0.73333329 0.5 0.73333329 0 0.73333329 0.25 0.73333329 0.25 0.73333329 0 0.74999994
		 1 0.73333329 1 0.73333329 0.75 0.74999994 0.75 0.76666659 1 0.76666659 0.75 0.78333324
		 1 0.78333324 0.75 0.79999989 1 0.79999989 0.75 0.81666654 1 0.81666654 0.75 0.83333319
		 1 0.83333319 0.75 0.84999985 1 0.84999985 0.75 0.8666665 1 0.8666665 0.75 0.88333315
		 1 0.88333315 0.75 0.8999998 1 0.8999998 0.75 0.91666645 1 0.91666645 0.75 0.9333331
		 1 0.9333331 0.75 0.94999975 1 0.94999975 0.75 0.9666664 1 0.9666664 0.75 0.98333305
		 1 0.98333305 0.75 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989
		 0.5 0.81666654 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998
		 0.5 0.91666645 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329
		 0.25 0.74999994 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25
		 0.83333319 0.25 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645
		 0.25 0.9333331 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994
		 0 0.76666659 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665
		 0 0.88333315 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305
		 0 0.73333329 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0 0.73333329
		 0.75 0.73333329 0.25 0.73333329 0.5 0.73333329 0.25 0.73333329 0 0.73333329 0.25
		 0.73333329 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 35 ".pt";
	setAttr ".pt[48]" -type "float3" 0.03513477 -0.064907849 -0.031166604 ;
	setAttr ".pt[49]" -type "float3" 0.031876974 -0.064907849 -0.03099589 ;
	setAttr ".pt[50]" -type "float3" 0.028654853 -0.064907849 -0.030485559 ;
	setAttr ".pt[51]" -type "float3" 0.025503734 -0.064907849 -0.02964123 ;
	setAttr ".pt[52]" -type "float3" 0.022458173 -0.064907849 -0.028472122 ;
	setAttr ".pt[53]" -type "float3" 0.019551437 -0.064907849 -0.026991103 ;
	setAttr ".pt[54]" -type "float3" 0.01681548 -0.064907849 -0.025214329 ;
	setAttr ".pt[55]" -type "float3" 0.014280196 -0.064907849 -0.023161316 ;
	setAttr ".pt[56]" -type "float3" 0.01197345 -0.064907849 -0.020854538 ;
	setAttr ".pt[57]" -type "float3" 0.0099204574 -0.064907849 -0.018319288 ;
	setAttr ".pt[58]" -type "float3" 0.0081436727 -0.064907849 -0.015583325 ;
	setAttr ".pt[59]" -type "float3" 0.0066626295 -0.064907849 -0.012676611 ;
	setAttr ".pt[60]" -type "float3" 0.0054935142 -0.064907849 -0.0096310163 ;
	setAttr ".pt[61]" -type "float3" 0.0046492144 -0.064907849 -0.0064799134 ;
	setAttr ".pt[62]" -type "float3" 0.0041388338 -0.064907849 -0.0032578055 ;
	setAttr ".pt[63]" -type "float3" 0.0039681112 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[66]" -type "float3" 0.085886188 -0.064907849 -0.031166604 ;
	setAttr ".pt[72]" -type "float3" 0.085886188 -0.064907849 -2.4108173e-09 ;
	setAttr ".pt[121]" -type "float3" 0.03513477 -0.064907849 0.031166604 ;
	setAttr ".pt[122]" -type "float3" 0.031876974 -0.064907849 0.030995879 ;
	setAttr ".pt[123]" -type "float3" 0.028654853 -0.064907849 0.030485567 ;
	setAttr ".pt[124]" -type "float3" 0.025503734 -0.064907849 0.029641215 ;
	setAttr ".pt[125]" -type "float3" 0.022458173 -0.064907849 0.028472107 ;
	setAttr ".pt[126]" -type "float3" 0.019551437 -0.064907849 0.026991079 ;
	setAttr ".pt[127]" -type "float3" 0.01681548 -0.064907849 0.025214313 ;
	setAttr ".pt[128]" -type "float3" 0.014280196 -0.064907849 0.023161313 ;
	setAttr ".pt[129]" -type "float3" 0.01197345 -0.064907849 0.020854548 ;
	setAttr ".pt[130]" -type "float3" 0.0099204574 -0.064907849 0.018319286 ;
	setAttr ".pt[131]" -type "float3" 0.0081436727 -0.064907849 0.015583316 ;
	setAttr ".pt[132]" -type "float3" 0.0066626295 -0.064907849 0.012676615 ;
	setAttr ".pt[133]" -type "float3" 0.0054935142 -0.064907849 0.0096310098 ;
	setAttr ".pt[134]" -type "float3" 0.0046492144 -0.064907849 0.0064799096 ;
	setAttr ".pt[135]" -type "float3" 0.0041388338 -0.064907849 0.003257805 ;
	setAttr ".pt[138]" -type "float3" 0.085886188 -0.064907849 0.031166604 ;
	setAttr -s 144 ".vt[0:143]"  0 0.03535533 0.31464475 0.032889366 0.03535533 0.31292099
		 0.065418363 0.03535533 0.307769 0.097230554 0.03535533 0.29924482 0.12797761 0.03535533 0.28744215
		 0.15732241 0.03535533 0.27249032 0.18494356 0.03535533 0.2545529 0.21053839 0.03535533 0.23382658
		 0.23382664 0.03535533 0.21053833 0.25455284 0.03535533 0.1849435 0.27249026 0.03535533 0.15732235
		 0.28744221 0.03535533 0.12797755 0.29924488 0.03535533 0.097230613 0.30776894 0.03535533 0.065418303
		 0.31292105 0.03535533 0.032889307 0.31464469 0.03535533 -7.5649616e-08 0 0.03535533 0.37460828
		 0.0402807 0.03535533 0.37460828 0.080119967 0.03535533 0.37460828 0.11908138 0.03535533 0.37460828
		 0.15673816 0.03535533 0.37460828 0.19267774 0.03535533 0.37460828 0.2740916 0.03535533 0.37460828
		 0.56961167 0.03535533 0.37460828 0.56961167 0.03535533 0.30112225 0.56961167 0.03535533 0.24240094
		 0.56961167 0.03535533 0.19267768 0.56961167 0.03535533 0.1567381 0.56961167 0.03535533 0.11908132
		 0.56961167 0.03535533 0.080119908 0.56961167 0.03535533 0.042046666 0.56961167 0.03535533 -7.5649616e-08
		 0 -0.03535533 0.37460828 0.0402807 -0.03535533 0.37460828 0.080119967 -0.03535533 0.37460828
		 0.11908138 -0.03535533 0.37460828 0.15673816 -0.03535533 0.37460828 0.19267774 -0.03535533 0.37460828
		 0.2740916 -0.03535533 0.37460828 0.56961167 -0.03535533 0.37460828 0.56961167 -0.03535533 0.30112225
		 0.56961167 -0.03535533 0.24240094 0.56961167 -0.03535533 0.19267768 0.56961167 -0.03535533 0.1567381
		 0.56961167 -0.03535533 0.11908132 0.56961167 -0.03535533 0.080119908 0.56961167 -0.03535533 0.042046666
		 0.56961167 -0.03535533 -7.5649616e-08 0 -0.03535533 0.31464463 0.032889366 -0.03535533 0.31292099
		 0.065418363 -0.03535533 0.30776888 0.097230554 -0.03535533 0.29924482 0.12797749 -0.03535533 0.28744215
		 0.15732241 -0.03535533 0.2724902 0.18494356 -0.03535533 0.2545529 0.21053839 -0.03535533 0.23382658
		 0.23382652 -0.03535533 0.21053833 0.25455284 -0.03535533 0.1849435 0.27249026 -0.03535533 0.15732235
		 0.28744221 -0.03535533 0.12797755 0.29924488 -0.03535533 0.097230494 0.30776894 -0.03535533 0.065418303
		 0.31292105 -0.03535533 0.032889307 0.31464458 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 0.31464475
		 -0.51236534 0.03535533 0.37460828 -0.51236534 -0.03535533 0.31464463 -0.51236534 -0.03535533 0.37460828
		 -1.35788739 0.03535533 0.31464475 -1.35788739 0.03535533 0.37460828 -1.35788739 -0.03535533 0.31464463
		 -1.35788739 -0.03535533 0.37460828 -0.51236534 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 -7.5649616e-08
		 -1.35788739 -0.03535533 -7.5649616e-08 -1.35788739 0.03535533 -7.5649616e-08 -3.2154015e-19 0.03535533 -0.3146449
		 0.032889366 0.03535533 -0.31292114 0.065418363 0.03535533 -0.30776915 0.097230554 0.03535533 -0.29924497
		 0.12797761 0.03535533 -0.2874423 0.15732241 0.03535533 -0.27249047 0.18494356 0.03535533 -0.25455305
		 0.21053839 0.03535533 -0.23382673 0.23382664 0.03535533 -0.21053848 0.25455284 0.03535533 -0.18494365
		 0.27249026 0.03535533 -0.1573225 0.28744221 0.03535533 -0.1279777 0.29924488 0.03535533 -0.097230762
		 0.30776894 0.03535533 -0.065418452 0.31292105 0.03535533 -0.032889459 0 0.03535533 -0.37460843
		 0.0402807 0.03535533 -0.37460843 0.080119967 0.03535533 -0.37460843 0.11908138 0.03535533 -0.37460843
		 0.15673816 0.03535533 -0.37460843 0.19267774 0.03535533 -0.37460843 0.2740916 0.03535533 -0.37460843
		 0.56961167 0.03535533 -0.37460843 0.56961167 0.03535533 -0.3011224 0.56961167 0.03535533 -0.24240109
		 0.56961167 0.03535533 -0.19267783 0.56961167 0.03535533 -0.15673825 0.56961167 0.03535533 -0.11908147
		 0.56961167 0.03535533 -0.080120057 0.56961167 0.03535533 -0.042046819 0 -0.03535533 -0.37460843
		 0.0402807 -0.03535533 -0.37460843 0.080119967 -0.03535533 -0.37460843 0.11908138 -0.03535533 -0.37460843
		 0.15673816 -0.03535533 -0.37460843 0.19267774 -0.03535533 -0.37460843 0.2740916 -0.03535533 -0.37460843
		 0.56961167 -0.03535533 -0.37460843 0.56961167 -0.03535533 -0.3011224 0.56961167 -0.03535533 -0.24240109
		 0.56961167 -0.03535533 -0.19267783 0.56961167 -0.03535533 -0.15673825 0.56961167 -0.03535533 -0.11908147
		 0.56961167 -0.03535533 -0.080120057 0.56961167 -0.03535533 -0.042046819 -3.2154025e-19 -0.03535533 -0.31464478
		 0.032889366 -0.03535533 -0.31292114 0.065418363 -0.03535533 -0.30776903 0.097230554 -0.03535533 -0.29924497
		 0.12797749 -0.03535533 -0.2874423 0.15732241 -0.03535533 -0.27249035 0.18494356 -0.03535533 -0.25455305
		 0.21053839 -0.03535533 -0.23382673 0.23382652 -0.03535533 -0.21053848 0.25455284 -0.03535533 -0.18494365
		 0.27249026 -0.03535533 -0.1573225 0.28744221 -0.03535533 -0.1279777 0.29924488 -0.03535533 -0.097230643
		 0.30776894 -0.03535533 -0.065418452 0.31292105 -0.03535533 -0.032889459 -0.51236534 0.03535533 -0.3146449
		 -0.51236534 0.03535533 -0.37460843 -0.51236534 -0.03535533 -0.31464478 -0.51236534 -0.03535533 -0.37460843
		 -1.35788739 0.03535533 -0.3146449 -1.35788739 0.03535533 -0.37460843 -1.35788739 -0.03535533 -0.31464478
		 -1.35788739 -0.03535533 -0.37460843;
	setAttr -s 288 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 16 17 0 17 18 0 18 19 0 19 20 0 20 21 0
		 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0 32 33 0
		 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 42 0 42 43 0 43 44 0
		 44 45 0 45 46 0 46 47 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0
		 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 0 16 0 1 17 1 2 18 1 3 19 1
		 4 20 1 5 21 1 6 22 1 7 23 1 8 24 1 9 25 1 10 26 1 11 27 1 12 28 1 13 29 1 14 30 1
		 15 31 1 16 32 0 17 33 1 18 34 1 19 35 1 20 36 1 21 37 1 22 38 1 23 39 1 24 40 1 25 41 1
		 26 42 1 27 43 1 28 44 1 29 45 1 30 46 1 31 47 1 32 48 0 33 49 1 34 50 1 35 51 1 36 52 1
		 37 53 1 38 54 1 39 55 1 40 56 1 41 57 1 42 58 1 43 59 1 44 60 1 45 61 1 46 62 1 47 63 1
		 48 0 0 49 1 1 50 2 1 51 3 1 52 4 1 53 5 1 54 6 1 55 7 1 56 8 1 57 9 1 58 10 1 59 11 1
		 60 12 1 61 13 1 62 14 1 63 15 1 0 64 0 16 65 0 64 65 0 48 66 0 66 64 0 32 67 0 67 66 0
		 65 67 0 64 68 0 65 69 0 68 69 0 66 70 0 70 68 0 67 71 0 71 70 0 69 71 0 66 72 0 64 73 0
		 72 73 1 70 74 0 72 74 1 68 75 0 74 75 1 73 75 1 76 77 0 76 91 0 91 92 0 77 92 1 77 78 0
		 92 93 0 78 93 1 78 79 0 93 94 0 79 94 1 79 80 0 94 95 0 80 95 1 80 81 0 95 96 0 81 96 1
		 81 82 0 96 97 0;
	setAttr ".ed[166:287]" 82 97 1 82 83 0 97 98 0 83 98 1 83 84 0 98 99 0 84 99 1
		 84 85 0 99 100 0 85 100 1 85 86 0 100 101 0 86 101 1 86 87 0 101 102 0 87 102 1 87 88 0
		 102 103 0 88 103 1 88 89 0 103 104 0 89 104 1 89 90 0 104 105 0 90 105 1 90 15 0
		 105 31 0 91 106 0 106 107 0 92 107 1 107 108 0 93 108 1 108 109 0 94 109 1 109 110 0
		 95 110 1 110 111 0 96 111 1 111 112 0 97 112 1 112 113 0 98 113 1 113 114 0 99 114 1
		 114 115 0 100 115 1 115 116 0 101 116 1 116 117 0 102 117 1 117 118 0 103 118 1 118 119 0
		 104 119 1 119 120 0 105 120 1 120 47 0 106 121 0 121 122 0 107 122 1 122 123 0 108 123 1
		 123 124 0 109 124 1 124 125 0 110 125 1 125 126 0 111 126 1 126 127 0 112 127 1 127 128 0
		 113 128 1 128 129 0 114 129 1 129 130 0 115 130 1 130 131 0 116 131 1 131 132 0 117 132 1
		 132 133 0 118 133 1 133 134 0 119 134 1 134 135 0 120 135 1 135 63 0 121 76 0 122 77 1
		 123 78 1 124 79 1 125 80 1 126 81 1 127 82 1 128 83 1 129 84 1 130 85 1 131 86 1
		 132 87 1 133 88 1 134 89 1 135 90 1 140 141 0 142 140 0 143 142 0 141 143 0 76 136 0
		 136 137 0 91 137 0 121 138 0 138 136 0 106 139 0 139 138 0 137 139 0 136 140 0 137 141 0
		 139 143 0 138 142 0 138 72 0 136 73 0 142 74 0 140 75 0;
	setAttr -s 144 -ch 576 ".fc[0:143]" -type "polyFaces" 
		f 4 -1 60 15 -62
		mu 0 4 1 0 16 17
		f 4 -2 61 16 -63
		mu 0 4 2 1 17 18
		f 4 -3 62 17 -64
		mu 0 4 3 2 18 19
		f 4 -4 63 18 -65
		mu 0 4 4 3 19 20
		f 4 -5 64 19 -66
		mu 0 4 5 4 20 21
		f 4 -6 65 20 -67
		mu 0 4 6 5 21 22
		f 4 -7 66 21 -68
		mu 0 4 7 6 22 23
		f 4 -8 67 22 -69
		mu 0 4 8 7 23 24
		f 4 -9 68 23 -70
		mu 0 4 9 8 24 25
		f 4 -10 69 24 -71
		mu 0 4 10 9 25 26
		f 4 -11 70 25 -72
		mu 0 4 11 10 26 27
		f 4 -12 71 26 -73
		mu 0 4 12 11 27 28
		f 4 -13 72 27 -74
		mu 0 4 13 12 28 29
		f 4 -14 73 28 -75
		mu 0 4 14 13 29 30
		f 4 -15 74 29 -76
		mu 0 4 15 14 30 31
		f 4 -16 76 30 -78
		mu 0 4 17 16 32 33
		f 4 -17 77 31 -79
		mu 0 4 18 17 33 34
		f 4 -18 78 32 -80
		mu 0 4 19 18 34 35
		f 4 -19 79 33 -81
		mu 0 4 20 19 35 36
		f 4 -20 80 34 -82
		mu 0 4 21 20 36 37
		f 4 -21 81 35 -83
		mu 0 4 22 21 37 38
		f 4 -22 82 36 -84
		mu 0 4 23 22 38 39
		f 4 -23 83 37 -85
		mu 0 4 24 23 39 40
		f 4 -24 84 38 -86
		mu 0 4 25 24 40 41
		f 4 -25 85 39 -87
		mu 0 4 26 25 41 42
		f 4 -26 86 40 -88
		mu 0 4 27 26 42 43
		f 4 -27 87 41 -89
		mu 0 4 28 27 43 44
		f 4 -28 88 42 -90
		mu 0 4 29 28 44 45
		f 4 -29 89 43 -91
		mu 0 4 30 29 45 46
		f 4 -30 90 44 -92
		mu 0 4 31 30 46 47
		f 4 -31 92 45 -94
		mu 0 4 33 32 48 49
		f 4 -32 93 46 -95
		mu 0 4 34 33 49 50
		f 4 -33 94 47 -96
		mu 0 4 35 34 50 51
		f 4 -34 95 48 -97
		mu 0 4 36 35 51 52
		f 4 -35 96 49 -98
		mu 0 4 37 36 52 53
		f 4 -36 97 50 -99
		mu 0 4 38 37 53 54
		f 4 -37 98 51 -100
		mu 0 4 39 38 54 55
		f 4 -38 99 52 -101
		mu 0 4 40 39 55 56
		f 4 -39 100 53 -102
		mu 0 4 41 40 56 57
		f 4 -40 101 54 -103
		mu 0 4 42 41 57 58
		f 4 -41 102 55 -104
		mu 0 4 43 42 58 59
		f 4 -42 103 56 -105
		mu 0 4 44 43 59 60
		f 4 -43 104 57 -106
		mu 0 4 45 44 60 61
		f 4 -44 105 58 -107
		mu 0 4 46 45 61 62
		f 4 -45 106 59 -108
		mu 0 4 47 46 62 63
		f 4 -46 108 0 -110
		mu 0 4 49 48 64 65
		f 4 -47 109 1 -111
		mu 0 4 50 49 65 66
		f 4 -48 110 2 -112
		mu 0 4 51 50 66 67
		f 4 -49 111 3 -113
		mu 0 4 52 51 67 68
		f 4 -50 112 4 -114
		mu 0 4 53 52 68 69
		f 4 -51 113 5 -115
		mu 0 4 54 53 69 70
		f 4 -52 114 6 -116
		mu 0 4 55 54 70 71
		f 4 -53 115 7 -117
		mu 0 4 56 55 71 72
		f 4 -54 116 8 -118
		mu 0 4 57 56 72 73
		f 4 -55 117 9 -119
		mu 0 4 58 57 73 74
		f 4 -56 118 10 -120
		mu 0 4 59 58 74 75
		f 4 -57 119 11 -121
		mu 0 4 60 59 75 76
		f 4 -58 120 12 -122
		mu 0 4 61 60 76 77
		f 4 -59 121 13 -123
		mu 0 4 62 61 77 78
		f 4 -60 122 14 -124
		mu 0 4 63 62 78 79
		f 4 -135 -137 -139 -140
		mu 0 4 84 85 86 87
		f 4 -61 124 126 -126
		mu 0 4 16 64 81 80
		f 4 -109 127 128 -125
		mu 0 4 64 48 82 81
		f 4 -93 129 130 -128
		mu 0 4 48 32 83 82
		f 4 -77 125 131 -130
		mu 0 4 32 16 80 83
		f 4 -127 132 134 -134
		mu 0 4 80 81 85 84
		f 4 -131 137 138 -136
		mu 0 4 82 83 87 86
		f 4 -132 133 139 -138
		mu 0 4 83 80 84 87
		f 4 -129 140 142 -142
		mu 0 4 81 82 89 88
		f 4 135 143 -145 -141
		mu 0 4 82 86 90 89
		f 4 136 145 -147 -144
		mu 0 4 86 85 91 90
		f 4 -133 141 147 -146
		mu 0 4 85 81 88 91
		f 4 151 -151 -150 148
		mu 0 4 92 95 94 93
		f 4 154 -154 -152 152
		mu 0 4 96 97 95 92
		f 4 157 -157 -155 155
		mu 0 4 98 99 97 96
		f 4 160 -160 -158 158
		mu 0 4 100 101 99 98
		f 4 163 -163 -161 161
		mu 0 4 102 103 101 100
		f 4 166 -166 -164 164
		mu 0 4 104 105 103 102
		f 4 169 -169 -167 167
		mu 0 4 106 107 105 104
		f 4 172 -172 -170 170
		mu 0 4 108 109 107 106
		f 4 175 -175 -173 173
		mu 0 4 110 111 109 108
		f 4 178 -178 -176 176
		mu 0 4 112 113 111 110
		f 4 181 -181 -179 179
		mu 0 4 114 115 113 112
		f 4 184 -184 -182 182
		mu 0 4 116 117 115 114
		f 4 187 -187 -185 185
		mu 0 4 118 119 117 116
		f 4 190 -190 -188 188
		mu 0 4 120 121 119 118
		f 4 75 -193 -191 191
		mu 0 4 122 123 121 120
		f 4 195 -195 -194 150
		mu 0 4 95 125 124 94
		f 4 197 -197 -196 153
		mu 0 4 97 126 125 95
		f 4 199 -199 -198 156
		mu 0 4 99 127 126 97
		f 4 201 -201 -200 159
		mu 0 4 101 128 127 99
		f 4 203 -203 -202 162
		mu 0 4 103 129 128 101
		f 4 205 -205 -204 165
		mu 0 4 105 130 129 103
		f 4 207 -207 -206 168
		mu 0 4 107 131 130 105
		f 4 209 -209 -208 171
		mu 0 4 109 132 131 107
		f 4 211 -211 -210 174
		mu 0 4 111 133 132 109
		f 4 213 -213 -212 177
		mu 0 4 113 134 133 111
		f 4 215 -215 -214 180
		mu 0 4 115 135 134 113
		f 4 217 -217 -216 183
		mu 0 4 117 136 135 115
		f 4 219 -219 -218 186
		mu 0 4 119 137 136 117
		f 4 221 -221 -220 189
		mu 0 4 121 138 137 119
		f 4 91 -223 -222 192
		mu 0 4 123 139 138 121
		f 4 225 -225 -224 194
		mu 0 4 125 141 140 124
		f 4 227 -227 -226 196
		mu 0 4 126 142 141 125
		f 4 229 -229 -228 198
		mu 0 4 127 143 142 126
		f 4 231 -231 -230 200
		mu 0 4 128 144 143 127
		f 4 233 -233 -232 202
		mu 0 4 129 145 144 128
		f 4 235 -235 -234 204
		mu 0 4 130 146 145 129
		f 4 237 -237 -236 206
		mu 0 4 131 147 146 130
		f 4 239 -239 -238 208
		mu 0 4 132 148 147 131
		f 4 241 -241 -240 210
		mu 0 4 133 149 148 132
		f 4 243 -243 -242 212
		mu 0 4 134 150 149 133
		f 4 245 -245 -244 214
		mu 0 4 135 151 150 134
		f 4 247 -247 -246 216
		mu 0 4 136 152 151 135
		f 4 249 -249 -248 218
		mu 0 4 137 153 152 136
		f 4 251 -251 -250 220
		mu 0 4 138 154 153 137
		f 4 107 -253 -252 222
		mu 0 4 139 155 154 138
		f 4 254 -149 -254 224
		mu 0 4 141 157 156 140
		f 4 255 -153 -255 226
		mu 0 4 142 158 157 141
		f 4 256 -156 -256 228
		mu 0 4 143 159 158 142
		f 4 257 -159 -257 230
		mu 0 4 144 160 159 143
		f 4 258 -162 -258 232
		mu 0 4 145 161 160 144
		f 4 259 -165 -259 234
		mu 0 4 146 162 161 145
		f 4 260 -168 -260 236
		mu 0 4 147 163 162 146
		f 4 261 -171 -261 238
		mu 0 4 148 164 163 147
		f 4 262 -174 -262 240
		mu 0 4 149 165 164 148
		f 4 263 -177 -263 242
		mu 0 4 150 166 165 149
		f 4 264 -180 -264 244
		mu 0 4 151 167 166 150
		f 4 265 -183 -265 246
		mu 0 4 152 168 167 151
		f 4 266 -186 -266 248
		mu 0 4 153 169 168 152
		f 4 267 -189 -267 250
		mu 0 4 154 170 169 153
		f 4 123 -192 -268 252
		mu 0 4 155 171 170 154
		f 4 271 270 269 268
		mu 0 4 172 175 174 173
		f 4 274 -274 -273 149
		mu 0 4 94 177 176 156
		f 4 272 -277 -276 253
		mu 0 4 156 176 178 140
		f 4 275 -279 -278 223
		mu 0 4 140 178 179 124
		f 4 277 -280 -275 193
		mu 0 4 124 179 177 94
		f 4 281 -269 -281 273
		mu 0 4 177 172 173 176
		f 4 283 -271 -283 278
		mu 0 4 178 174 175 179
		f 4 282 -272 -282 279
		mu 0 4 179 175 172 177
		f 4 285 -143 -285 276
		mu 0 4 176 181 180 178
		f 4 284 144 -287 -284
		mu 0 4 178 180 182 174
		f 4 286 146 -288 -270
		mu 0 4 174 182 183 173
		f 4 287 -148 -286 280
		mu 0 4 173 183 181 176;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pTorus7";
	rename -uid "F0D3015B-3C41-0B0C-1925-00B2223343ED";
	setAttr ".t" -type "double3" 2.2341316878919182 1.3578873562295586 5.7049203515048568 ;
	setAttr ".r" -type "double3" -90 0 90 ;
	setAttr ".rp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
	setAttr ".rpt" -type "double3" 0.3941378593444822 -0.39413785934448242 0 ;
	setAttr ".sp" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
createNode mesh -n "pTorusShape7" -p "pTorus7";
	rename -uid "60C2756B-FC45-0DD4-F7E4-F5883A9FCBE5";
	setAttr -k off ".v";
	setAttr -s 8 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.92499977350234985 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".pt";
	setAttr ".pt[23]" -type "float3" 0.052705795 0 0 ;
	setAttr ".pt[24]" -type "float3" 0.048222259 0 -0.00039944588 ;
	setAttr ".pt[25]" -type "float3" 0.045198679 0 -0.00032155181 ;
	setAttr ".pt[26]" -type "float3" 0.042638399 0 -0.00025559359 ;
	setAttr ".pt[27]" -type "float3" 0.040787835 0 -0.00020791695 ;
	setAttr ".pt[28]" -type "float3" 0.038848899 0 -0.00015796372 ;
	setAttr ".pt[29]" -type "float3" 0.036842752 0 -0.00010628485 ;
	setAttr ".pt[30]" -type "float3" 0.034882288 0 -5.5776287e-05 ;
	setAttr ".pt[31]" -type "float3" 0.032717276 0 2.3979494e-09 ;
	setAttr ".pt[39]" -type "float3" 0.052705795 0 0 ;
	setAttr ".pt[40]" -type "float3" 0.048222259 0 -0.00039944588 ;
	setAttr ".pt[41]" -type "float3" 0.045198679 0 -0.00032155181 ;
	setAttr ".pt[42]" -type "float3" 0.042638399 0 -0.00025559359 ;
	setAttr ".pt[43]" -type "float3" 0.040787835 0 -0.00020791695 ;
	setAttr ".pt[44]" -type "float3" 0.038848899 0 -0.00015796372 ;
	setAttr ".pt[45]" -type "float3" 0.036842752 0 -0.00010628485 ;
	setAttr ".pt[46]" -type "float3" 0.034882288 0 -5.5776287e-05 ;
	setAttr ".pt[47]" -type "float3" 0.032717276 0 2.3979494e-09 ;
	setAttr ".pt[99]" -type "float3" 0.017212318 0 0.00039944542 ;
	setAttr ".pt[100]" -type "float3" 0.020235918 0 0.00032154931 ;
	setAttr ".pt[101]" -type "float3" 0.022796221 0 0.00025558952 ;
	setAttr ".pt[102]" -type "float3" 0.024646748 0 0.00020791712 ;
	setAttr ".pt[103]" -type "float3" 0.02658575 0 0.00015796123 ;
	setAttr ".pt[104]" -type "float3" 0.028591868 0 0.00010628047 ;
	setAttr ".pt[105]" -type "float3" 0.030552309 0 5.577525e-05 ;
	setAttr ".pt[114]" -type "float3" 0.017212318 0 0.00039944542 ;
	setAttr ".pt[115]" -type "float3" 0.020235918 0 0.00032154931 ;
	setAttr ".pt[116]" -type "float3" 0.022796221 0 0.00025558952 ;
	setAttr ".pt[117]" -type "float3" 0.024646748 0 0.00020791712 ;
	setAttr ".pt[118]" -type "float3" 0.02658575 0 0.00015796123 ;
	setAttr ".pt[119]" -type "float3" 0.028591868 0 0.00010628047 ;
	setAttr ".pt[120]" -type "float3" 0.030552309 0 5.577525e-05 ;
	setAttr ".pt[144]" -type "float3" 3.586806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[145]" -type "float3" 3.469447e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[146]" -type "float3" 3.586806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[147]" -type "float3" 3.469447e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[148]" -type "float3" 3.469447e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[149]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[150]" -type "float3" 3.469447e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[151]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[152]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[153]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[154]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[155]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[156]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[157]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[158]" -type "float3" 3.1918912e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[159]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[160]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[161]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[162]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[163]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[164]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[165]" -type "float3" 3.8857806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[166]" -type "float3" 4.1633363e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[167]" -type "float3" 3.8857806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[168]" -type "float3" 3.8857806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[169]" -type "float3" 0.20560434 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[170]" -type "float3" 3.8857806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[171]" -type "float3" 0.20560434 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[172]" -type "float3" 3.586806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[173]" -type "float3" 3.586806e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[174]" -type "float3" 3.3306691e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[175]" -type "float3" 3.3306691e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[176]" -type "float3" 3.3306691e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[177]" -type "float3" 3.3306691e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[178]" -type "float3" 2.220446e-16 -1.1241008e-15 2.8846464 ;
	setAttr ".pt[179]" -type "float3" 2.220446e-16 -1.1241008e-15 2.8846464 ;
createNode mesh -n "polySurfaceShape3" -p "pTorus7";
	rename -uid "2A04FB6D-9747-B914-EFDE-7DBAD8BEF0E6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 8 ".iog[0].og";
	setAttr ".iog[0].og[4].gcl" -type "componentList" 8 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]";
	setAttr ".iog[0].og[5].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[6].gcl" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
	setAttr ".iog[0].og[7].gcl" -type "componentList" 3 "e[142]" "e[144]" "e[146:147]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.79166656732559204 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 184 ".uvst[0].uvsp[0:183]" -type "float2" 0.73333329 1 0.74999994
		 1 0.76666659 1 0.78333324 1 0.79999989 1 0.81666654 1 0.83333319 1 0.84999985 1 0.8666665
		 1 0.88333315 1 0.8999998 1 0.91666645 1 0.9333331 1 0.94999975 1 0.9666664 1 0.98333305
		 1 0.73333329 0.75 0.74999994 0.75 0.76666659 0.75 0.78333324 0.75 0.79999989 0.75
		 0.81666654 0.75 0.83333319 0.75 0.84999985 0.75 0.8666665 0.75 0.88333315 0.75 0.8999998
		 0.75 0.91666645 0.75 0.9333331 0.75 0.94999975 0.75 0.9666664 0.75 0.98333305 0.75
		 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989 0.5 0.81666654
		 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998 0.5 0.91666645
		 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329 0.25 0.74999994
		 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25 0.83333319 0.25
		 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645 0.25 0.9333331
		 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994 0 0.76666659
		 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665 0 0.88333315
		 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305 0 0.73333329
		 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0.75 0.73333329 0 0.73333329
		 0.25 0.73333329 0.5 0.73333329 0 0.73333329 0.25 0.73333329 0.25 0.73333329 0 0.74999994
		 1 0.73333329 1 0.73333329 0.75 0.74999994 0.75 0.76666659 1 0.76666659 0.75 0.78333324
		 1 0.78333324 0.75 0.79999989 1 0.79999989 0.75 0.81666654 1 0.81666654 0.75 0.83333319
		 1 0.83333319 0.75 0.84999985 1 0.84999985 0.75 0.8666665 1 0.8666665 0.75 0.88333315
		 1 0.88333315 0.75 0.8999998 1 0.8999998 0.75 0.91666645 1 0.91666645 0.75 0.9333331
		 1 0.9333331 0.75 0.94999975 1 0.94999975 0.75 0.9666664 1 0.9666664 0.75 0.98333305
		 1 0.98333305 0.75 0.73333329 0.5 0.74999994 0.5 0.76666659 0.5 0.78333324 0.5 0.79999989
		 0.5 0.81666654 0.5 0.83333319 0.5 0.84999985 0.5 0.8666665 0.5 0.88333315 0.5 0.8999998
		 0.5 0.91666645 0.5 0.9333331 0.5 0.94999975 0.5 0.9666664 0.5 0.98333305 0.5 0.73333329
		 0.25 0.74999994 0.25 0.76666659 0.25 0.78333324 0.25 0.79999989 0.25 0.81666654 0.25
		 0.83333319 0.25 0.84999985 0.25 0.8666665 0.25 0.88333315 0.25 0.8999998 0.25 0.91666645
		 0.25 0.9333331 0.25 0.94999975 0.25 0.9666664 0.25 0.98333305 0.25 0.73333329 0 0.74999994
		 0 0.76666659 0 0.78333324 0 0.79999989 0 0.81666654 0 0.83333319 0 0.84999985 0 0.8666665
		 0 0.88333315 0 0.8999998 0 0.91666645 0 0.9333331 0 0.94999975 0 0.9666664 0 0.98333305
		 0 0.73333329 0.75 0.73333329 0 0.73333329 0.25 0.73333329 0.5 0.73333329 0 0.73333329
		 0.75 0.73333329 0.25 0.73333329 0.5 0.73333329 0.25 0.73333329 0 0.73333329 0.25
		 0.73333329 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 144 ".pt[3:143]" -type "float3"  7.4505806e-09 0 0 0 0 0 7.4505806e-09 
		0 0 -7.4505806e-09 0 0 7.4505806e-09 0 0 0 0 0 0 0 0 0 0 0 -7.4505806e-09 0 0 7.4505806e-09 
		0 0 -7.4505806e-09 0 0 -2.2351742e-08 0 0 -1.4901161e-08 0 0 0 0 0 0 0 0 7.4505806e-09 
		0 0 0 0 0 -7.4505806e-09 0 0 0 0 0 -7.4505806e-09 0 0 2.9802322e-08 0 0 -7.4505806e-09 
		0 0 -1.4901161e-08 0 0 0 0 0 -7.4505806e-09 0 0 0 0 0 0 0 0 1.4901161e-08 0 0 0 0 
		0 0 0 0 0 0 0 7.4505806e-09 0 0 0 0 0 -7.4505806e-09 0 0 0 0 0 -7.4505806e-09 0 0 
		2.9802322e-08 0 0 -7.4505806e-09 0 0 -1.4901161e-08 0 0 0 0 0 -7.4505806e-09 0 0 
		0 0 0 0 0 0 1.4901161e-08 0 0 0 0 0 2.9802322e-08 -6.9388939e-18 0 2.2351742e-08 
		-7.4505806e-09 0.0017236248 4.4703484e-08 -7.4505806e-09 3.7252903e-09 2.9802322e-08 
		-7.4505806e-09 -1.4528632e-07 1.5646219e-07 -7.4505806e-09 -1.527369e-07 3.7252903e-08 
		-7.4505806e-09 9.3132257e-08 3.7252903e-08 -7.4505806e-09 -2.2351742e-08 2.2351742e-08 
		-6.9388939e-18 -1.3411045e-07 1.4156103e-07 -6.9388939e-18 -1.1734664e-07 5.2154064e-08 
		0 -1.4901161e-08 5.9604645e-08 0 -1.3038516e-07 2.2351742e-08 0 -1.2665987e-07 1.4901161e-08 
		0 -1.4901161e-08 2.2351742e-08 0 -7.4505806e-09 3.7252903e-08 0 -1.3038516e-07 1.3411045e-07 
		0 -2.4108175e-09 -3.7252903e-09 0 0 -3.7252903e-09 0 0 9.0650786e-08 -6.9388939e-18 
		0 -3.7252903e-09 0 0 0 0 0 0 0 0 0 0 0 0 0 0 3.7252903e-08 -7.4505806e-09 -2.4108173e-09 
		3.7252903e-09 0 0 0 0 0 -3.7252903e-09 0 0 -3.7252903e-09 0 0 0 0 0 3.7252903e-09 
		0 0 0 0 0 7.4505806e-09 0 0 0 0 0 0 0 0 0 0 0 -7.4505806e-09 0 0 7.4505806e-09 0 
		0 0 0 0 0 0 0 0 0 0 1.4901161e-08 0 0 0 0 0 3.7252903e-09 -4.8572257e-17 -0.2364507 
		0 -4.8572257e-17 -0.2364507 3.7252903e-09 -4.8572257e-17 -0.2364507 -3.7252903e-09 
		-4.8572257e-17 -0.2364507 0 -4.8572257e-17 -0.23645072 -3.7252903e-09 -4.8572257e-17 
		-0.2364507 -3.7252903e-09 -4.8572257e-17 -0.23645073 3.7252903e-09 -0.0029366016 
		-0.23645069 0 0 0 0 0 0 7.4505806e-09 0 0 7.4505806e-09 0 0 0 0 0 -7.4505806e-09 
		0 0 0 0 0 3.7252903e-09 -4.8572257e-17 -0.2364507 0 -4.8572257e-17 -0.2364507 3.7252903e-09 
		-4.8572257e-17 -0.2364507 -3.7252903e-09 -4.8572257e-17 -0.2364507 0 -4.8572257e-17 
		-0.23645072 -3.7252903e-09 -4.8572257e-17 -0.2364507 -3.7252903e-09 -4.8572257e-17 
		-0.23645073 3.7252903e-09 -0.0029366016 -0.23645069 0 0 0 0 0 0 7.4505806e-09 0 0 
		7.4505806e-09 0 0 0 0 0 -7.4505806e-09 0 0 0 0 0 2.9802322e-08 -6.9388939e-18 0 2.9802322e-08 
		-7.4505806e-09 -0.0017236359 3.3527613e-08 -7.4505806e-09 3.7252903e-09 2.9802322e-08 
		-7.4505806e-09 1.3038516e-07 1.6391277e-07 -7.4505806e-09 1.3783574e-07 2.2351742e-08 
		-7.4505806e-09 -1.1920929e-07 2.9802322e-08 -7.4505806e-09 7.4505806e-09 3.7252903e-08 
		-6.9388939e-18 1.3411045e-07 1.4156103e-07 -6.9388939e-18 1.2479722e-07 3.7252903e-08 
		0 1.4901161e-08 3.7252903e-08 0 1.1920929e-07 2.9802322e-08 0 1.3038516e-07 2.2351742e-08 
		0 7.4505806e-09 2.9802322e-08 0 7.4505806e-09 2.9802322e-08 0 1.3038516e-07 0 0 0 
		0 -4.8572257e-17 -0.23645073 3.4771428e-08 -7.4505806e-09 0 0 -4.8572257e-17 -0.23645073 
		0 0 0 0 -5.5511151e-17 -0.23645073 -3.7252903e-09 0 0 0 -5.5511151e-17 -0.23645073;
	setAttr -s 144 ".vt[0:143]"  0 0.03535533 0.31464475 0.032889366 0.03535533 0.31292099
		 0.065418363 0.03535533 0.307769 0.097230554 0.03535533 0.29924482 0.12797761 0.03535533 0.28744215
		 0.15732241 0.03535533 0.27249032 0.18494356 0.03535533 0.2545529 0.21053839 0.03535533 0.23382658
		 0.23382664 0.03535533 0.21053833 0.25455284 0.03535533 0.1849435 0.27249026 0.03535533 0.15732235
		 0.28744221 0.03535533 0.12797755 0.29924488 0.03535533 0.097230613 0.30776894 0.03535533 0.065418303
		 0.31292105 0.03535533 0.032889307 0.31464469 0.03535533 -7.5649616e-08 0 0.03535533 0.37460828
		 0.0402807 0.03535533 0.37460828 0.080119967 0.03535533 0.37460828 0.11908138 0.03535533 0.37460828
		 0.15673816 0.03535533 0.37460828 0.19267774 0.03535533 0.37460828 0.2740916 0.03535533 0.37460828
		 0.56961167 0.03535533 0.37460828 0.56961167 0.03535533 0.30112225 0.56961167 0.03535533 0.24240094
		 0.56961167 0.03535533 0.19267768 0.56961167 0.03535533 0.1567381 0.56961167 0.03535533 0.11908132
		 0.56961167 0.03535533 0.080119908 0.56961167 0.03535533 0.042046666 0.56961167 0.03535533 -7.5649616e-08
		 0 -0.03535533 0.37460828 0.0402807 -0.03535533 0.37460828 0.080119967 -0.03535533 0.37460828
		 0.11908138 -0.03535533 0.37460828 0.15673816 -0.03535533 0.37460828 0.19267774 -0.03535533 0.37460828
		 0.2740916 -0.03535533 0.37460828 0.56961167 -0.03535533 0.37460828 0.56961167 -0.03535533 0.30112225
		 0.56961167 -0.03535533 0.24240094 0.56961167 -0.03535533 0.19267768 0.56961167 -0.03535533 0.1567381
		 0.56961167 -0.03535533 0.11908132 0.56961167 -0.03535533 0.080119908 0.56961167 -0.03535533 0.042046666
		 0.56961167 -0.03535533 -7.5649616e-08 0 -0.03535533 0.31464463 0.032889366 -0.03535533 0.31292099
		 0.065418363 -0.03535533 0.30776888 0.097230554 -0.03535533 0.29924482 0.12797749 -0.03535533 0.28744215
		 0.15732241 -0.03535533 0.2724902 0.18494356 -0.03535533 0.2545529 0.21053839 -0.03535533 0.23382658
		 0.23382652 -0.03535533 0.21053833 0.25455284 -0.03535533 0.1849435 0.27249026 -0.03535533 0.15732235
		 0.28744221 -0.03535533 0.12797755 0.29924488 -0.03535533 0.097230494 0.30776894 -0.03535533 0.065418303
		 0.31292105 -0.03535533 0.032889307 0.31464458 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 0.31464475
		 -0.51236534 0.03535533 0.37460828 -0.51236534 -0.03535533 0.31464463 -0.51236534 -0.03535533 0.37460828
		 -1.35788739 0.03535533 0.31464475 -1.35788739 0.03535533 0.37460828 -1.35788739 -0.03535533 0.31464463
		 -1.35788739 -0.03535533 0.37460828 -0.51236534 -0.03535533 -7.5649616e-08 -0.51236534 0.03535533 -7.5649616e-08
		 -1.35788739 -0.03535533 -7.5649616e-08 -1.35788739 0.03535533 -7.5649616e-08 -3.2154015e-19 0.03535533 -0.3146449
		 0.032889366 0.03535533 -0.31292114 0.065418363 0.03535533 -0.30776915 0.097230554 0.03535533 -0.29924497
		 0.12797761 0.03535533 -0.2874423 0.15732241 0.03535533 -0.27249047 0.18494356 0.03535533 -0.25455305
		 0.21053839 0.03535533 -0.23382673 0.23382664 0.03535533 -0.21053848 0.25455284 0.03535533 -0.18494365
		 0.27249026 0.03535533 -0.1573225 0.28744221 0.03535533 -0.1279777 0.29924488 0.03535533 -0.097230762
		 0.30776894 0.03535533 -0.065418452 0.31292105 0.03535533 -0.032889459 0 0.03535533 -0.37460843
		 0.0402807 0.03535533 -0.37460843 0.080119967 0.03535533 -0.37460843 0.11908138 0.03535533 -0.37460843
		 0.15673816 0.03535533 -0.37460843 0.19267774 0.03535533 -0.37460843 0.2740916 0.03535533 -0.37460843
		 0.56961167 0.03535533 -0.37460843 0.56961167 0.03535533 -0.3011224 0.56961167 0.03535533 -0.24240109
		 0.56961167 0.03535533 -0.19267783 0.56961167 0.03535533 -0.15673825 0.56961167 0.03535533 -0.11908147
		 0.56961167 0.03535533 -0.080120057 0.56961167 0.03535533 -0.042046819 0 -0.03535533 -0.37460843
		 0.0402807 -0.03535533 -0.37460843 0.080119967 -0.03535533 -0.37460843 0.11908138 -0.03535533 -0.37460843
		 0.15673816 -0.03535533 -0.37460843 0.19267774 -0.03535533 -0.37460843 0.2740916 -0.03535533 -0.37460843
		 0.56961167 -0.03535533 -0.37460843 0.56961167 -0.03535533 -0.3011224 0.56961167 -0.03535533 -0.24240109
		 0.56961167 -0.03535533 -0.19267783 0.56961167 -0.03535533 -0.15673825 0.56961167 -0.03535533 -0.11908147
		 0.56961167 -0.03535533 -0.080120057 0.56961167 -0.03535533 -0.042046819 -3.2154025e-19 -0.03535533 -0.31464478
		 0.032889366 -0.03535533 -0.31292114 0.065418363 -0.03535533 -0.30776903 0.097230554 -0.03535533 -0.29924497
		 0.12797749 -0.03535533 -0.2874423 0.15732241 -0.03535533 -0.27249035 0.18494356 -0.03535533 -0.25455305
		 0.21053839 -0.03535533 -0.23382673 0.23382652 -0.03535533 -0.21053848 0.25455284 -0.03535533 -0.18494365
		 0.27249026 -0.03535533 -0.1573225 0.28744221 -0.03535533 -0.1279777 0.29924488 -0.03535533 -0.097230643
		 0.30776894 -0.03535533 -0.065418452 0.31292105 -0.03535533 -0.032889459 -0.51236534 0.03535533 -0.3146449
		 -0.51236534 0.03535533 -0.37460843 -0.51236534 -0.03535533 -0.31464478 -0.51236534 -0.03535533 -0.37460843
		 -1.35788739 0.03535533 -0.3146449 -1.35788739 0.03535533 -0.37460843 -1.35788739 -0.03535533 -0.31464478
		 -1.35788739 -0.03535533 -0.37460843;
	setAttr -s 288 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 16 17 0 17 18 0 18 19 0 19 20 0 20 21 0
		 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0 32 33 0
		 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 42 0 42 43 0 43 44 0
		 44 45 0 45 46 0 46 47 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0
		 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 0 16 0 1 17 1 2 18 1 3 19 1
		 4 20 1 5 21 1 6 22 1 7 23 1 8 24 1 9 25 1 10 26 1 11 27 1 12 28 1 13 29 1 14 30 1
		 15 31 1 16 32 0 17 33 1 18 34 1 19 35 1 20 36 1 21 37 1 22 38 1 23 39 1 24 40 1 25 41 1
		 26 42 1 27 43 1 28 44 1 29 45 1 30 46 1 31 47 1 32 48 0 33 49 1 34 50 1 35 51 1 36 52 1
		 37 53 1 38 54 1 39 55 1 40 56 1 41 57 1 42 58 1 43 59 1 44 60 1 45 61 1 46 62 1 47 63 1
		 48 0 0 49 1 1 50 2 1 51 3 1 52 4 1 53 5 1 54 6 1 55 7 1 56 8 1 57 9 1 58 10 1 59 11 1
		 60 12 1 61 13 1 62 14 1 63 15 1 0 64 0 16 65 0 64 65 0 48 66 0 66 64 0 32 67 0 67 66 0
		 65 67 0 64 68 0 65 69 0 68 69 0 66 70 0 70 68 0 67 71 0 71 70 0 69 71 0 66 72 0 64 73 0
		 72 73 1 70 74 0 72 74 1 68 75 0 74 75 1 73 75 1 76 77 0 76 91 0 91 92 0 77 92 1 77 78 0
		 92 93 0 78 93 1 78 79 0 93 94 0 79 94 1 79 80 0 94 95 0 80 95 1 80 81 0 95 96 0 81 96 1
		 81 82 0 96 97 0;
	setAttr ".ed[166:287]" 82 97 1 82 83 0 97 98 0 83 98 1 83 84 0 98 99 0 84 99 1
		 84 85 0 99 100 0 85 100 1 85 86 0 100 101 0 86 101 1 86 87 0 101 102 0 87 102 1 87 88 0
		 102 103 0 88 103 1 88 89 0 103 104 0 89 104 1 89 90 0 104 105 0 90 105 1 90 15 0
		 105 31 0 91 106 0 106 107 0 92 107 1 107 108 0 93 108 1 108 109 0 94 109 1 109 110 0
		 95 110 1 110 111 0 96 111 1 111 112 0 97 112 1 112 113 0 98 113 1 113 114 0 99 114 1
		 114 115 0 100 115 1 115 116 0 101 116 1 116 117 0 102 117 1 117 118 0 103 118 1 118 119 0
		 104 119 1 119 120 0 105 120 1 120 47 0 106 121 0 121 122 0 107 122 1 122 123 0 108 123 1
		 123 124 0 109 124 1 124 125 0 110 125 1 125 126 0 111 126 1 126 127 0 112 127 1 127 128 0
		 113 128 1 128 129 0 114 129 1 129 130 0 115 130 1 130 131 0 116 131 1 131 132 0 117 132 1
		 132 133 0 118 133 1 133 134 0 119 134 1 134 135 0 120 135 1 135 63 0 121 76 0 122 77 1
		 123 78 1 124 79 1 125 80 1 126 81 1 127 82 1 128 83 1 129 84 1 130 85 1 131 86 1
		 132 87 1 133 88 1 134 89 1 135 90 1 140 141 0 142 140 0 143 142 0 141 143 0 76 136 0
		 136 137 0 91 137 0 121 138 0 138 136 0 106 139 0 139 138 0 137 139 0 136 140 0 137 141 0
		 139 143 0 138 142 0 138 72 0 136 73 0 142 74 0 140 75 0;
	setAttr -s 144 -ch 576 ".fc[0:143]" -type "polyFaces" 
		f 4 -1 60 15 -62
		mu 0 4 1 0 16 17
		f 4 -2 61 16 -63
		mu 0 4 2 1 17 18
		f 4 -3 62 17 -64
		mu 0 4 3 2 18 19
		f 4 -4 63 18 -65
		mu 0 4 4 3 19 20
		f 4 -5 64 19 -66
		mu 0 4 5 4 20 21
		f 4 -6 65 20 -67
		mu 0 4 6 5 21 22
		f 4 -7 66 21 -68
		mu 0 4 7 6 22 23
		f 4 -8 67 22 -69
		mu 0 4 8 7 23 24
		f 4 -9 68 23 -70
		mu 0 4 9 8 24 25
		f 4 -10 69 24 -71
		mu 0 4 10 9 25 26
		f 4 -11 70 25 -72
		mu 0 4 11 10 26 27
		f 4 -12 71 26 -73
		mu 0 4 12 11 27 28
		f 4 -13 72 27 -74
		mu 0 4 13 12 28 29
		f 4 -14 73 28 -75
		mu 0 4 14 13 29 30
		f 4 -15 74 29 -76
		mu 0 4 15 14 30 31
		f 4 -16 76 30 -78
		mu 0 4 17 16 32 33
		f 4 -17 77 31 -79
		mu 0 4 18 17 33 34
		f 4 -18 78 32 -80
		mu 0 4 19 18 34 35
		f 4 -19 79 33 -81
		mu 0 4 20 19 35 36
		f 4 -20 80 34 -82
		mu 0 4 21 20 36 37
		f 4 -21 81 35 -83
		mu 0 4 22 21 37 38
		f 4 -22 82 36 -84
		mu 0 4 23 22 38 39
		f 4 -23 83 37 -85
		mu 0 4 24 23 39 40
		f 4 -24 84 38 -86
		mu 0 4 25 24 40 41
		f 4 -25 85 39 -87
		mu 0 4 26 25 41 42
		f 4 -26 86 40 -88
		mu 0 4 27 26 42 43
		f 4 -27 87 41 -89
		mu 0 4 28 27 43 44
		f 4 -28 88 42 -90
		mu 0 4 29 28 44 45
		f 4 -29 89 43 -91
		mu 0 4 30 29 45 46
		f 4 -30 90 44 -92
		mu 0 4 31 30 46 47
		f 4 -31 92 45 -94
		mu 0 4 33 32 48 49
		f 4 -32 93 46 -95
		mu 0 4 34 33 49 50
		f 4 -33 94 47 -96
		mu 0 4 35 34 50 51
		f 4 -34 95 48 -97
		mu 0 4 36 35 51 52
		f 4 -35 96 49 -98
		mu 0 4 37 36 52 53
		f 4 -36 97 50 -99
		mu 0 4 38 37 53 54
		f 4 -37 98 51 -100
		mu 0 4 39 38 54 55
		f 4 -38 99 52 -101
		mu 0 4 40 39 55 56
		f 4 -39 100 53 -102
		mu 0 4 41 40 56 57
		f 4 -40 101 54 -103
		mu 0 4 42 41 57 58
		f 4 -41 102 55 -104
		mu 0 4 43 42 58 59
		f 4 -42 103 56 -105
		mu 0 4 44 43 59 60
		f 4 -43 104 57 -106
		mu 0 4 45 44 60 61
		f 4 -44 105 58 -107
		mu 0 4 46 45 61 62
		f 4 -45 106 59 -108
		mu 0 4 47 46 62 63
		f 4 -46 108 0 -110
		mu 0 4 49 48 64 65
		f 4 -47 109 1 -111
		mu 0 4 50 49 65 66
		f 4 -48 110 2 -112
		mu 0 4 51 50 66 67
		f 4 -49 111 3 -113
		mu 0 4 52 51 67 68
		f 4 -50 112 4 -114
		mu 0 4 53 52 68 69
		f 4 -51 113 5 -115
		mu 0 4 54 53 69 70
		f 4 -52 114 6 -116
		mu 0 4 55 54 70 71
		f 4 -53 115 7 -117
		mu 0 4 56 55 71 72
		f 4 -54 116 8 -118
		mu 0 4 57 56 72 73
		f 4 -55 117 9 -119
		mu 0 4 58 57 73 74
		f 4 -56 118 10 -120
		mu 0 4 59 58 74 75
		f 4 -57 119 11 -121
		mu 0 4 60 59 75 76
		f 4 -58 120 12 -122
		mu 0 4 61 60 76 77
		f 4 -59 121 13 -123
		mu 0 4 62 61 77 78
		f 4 -60 122 14 -124
		mu 0 4 63 62 78 79
		f 4 -135 -137 -139 -140
		mu 0 4 84 85 86 87
		f 4 -61 124 126 -126
		mu 0 4 16 64 81 80
		f 4 -109 127 128 -125
		mu 0 4 64 48 82 81
		f 4 -93 129 130 -128
		mu 0 4 48 32 83 82
		f 4 -77 125 131 -130
		mu 0 4 32 16 80 83
		f 4 -127 132 134 -134
		mu 0 4 80 81 85 84
		f 4 -131 137 138 -136
		mu 0 4 82 83 87 86
		f 4 -132 133 139 -138
		mu 0 4 83 80 84 87
		f 4 -129 140 142 -142
		mu 0 4 81 82 89 88
		f 4 135 143 -145 -141
		mu 0 4 82 86 90 89
		f 4 136 145 -147 -144
		mu 0 4 86 85 91 90
		f 4 -133 141 147 -146
		mu 0 4 85 81 88 91
		f 4 151 -151 -150 148
		mu 0 4 92 95 94 93
		f 4 154 -154 -152 152
		mu 0 4 96 97 95 92
		f 4 157 -157 -155 155
		mu 0 4 98 99 97 96
		f 4 160 -160 -158 158
		mu 0 4 100 101 99 98
		f 4 163 -163 -161 161
		mu 0 4 102 103 101 100
		f 4 166 -166 -164 164
		mu 0 4 104 105 103 102
		f 4 169 -169 -167 167
		mu 0 4 106 107 105 104
		f 4 172 -172 -170 170
		mu 0 4 108 109 107 106
		f 4 175 -175 -173 173
		mu 0 4 110 111 109 108
		f 4 178 -178 -176 176
		mu 0 4 112 113 111 110
		f 4 181 -181 -179 179
		mu 0 4 114 115 113 112
		f 4 184 -184 -182 182
		mu 0 4 116 117 115 114
		f 4 187 -187 -185 185
		mu 0 4 118 119 117 116
		f 4 190 -190 -188 188
		mu 0 4 120 121 119 118
		f 4 75 -193 -191 191
		mu 0 4 122 123 121 120
		f 4 195 -195 -194 150
		mu 0 4 95 125 124 94
		f 4 197 -197 -196 153
		mu 0 4 97 126 125 95
		f 4 199 -199 -198 156
		mu 0 4 99 127 126 97
		f 4 201 -201 -200 159
		mu 0 4 101 128 127 99
		f 4 203 -203 -202 162
		mu 0 4 103 129 128 101
		f 4 205 -205 -204 165
		mu 0 4 105 130 129 103
		f 4 207 -207 -206 168
		mu 0 4 107 131 130 105
		f 4 209 -209 -208 171
		mu 0 4 109 132 131 107
		f 4 211 -211 -210 174
		mu 0 4 111 133 132 109
		f 4 213 -213 -212 177
		mu 0 4 113 134 133 111
		f 4 215 -215 -214 180
		mu 0 4 115 135 134 113
		f 4 217 -217 -216 183
		mu 0 4 117 136 135 115
		f 4 219 -219 -218 186
		mu 0 4 119 137 136 117
		f 4 221 -221 -220 189
		mu 0 4 121 138 137 119
		f 4 91 -223 -222 192
		mu 0 4 123 139 138 121
		f 4 225 -225 -224 194
		mu 0 4 125 141 140 124
		f 4 227 -227 -226 196
		mu 0 4 126 142 141 125
		f 4 229 -229 -228 198
		mu 0 4 127 143 142 126
		f 4 231 -231 -230 200
		mu 0 4 128 144 143 127
		f 4 233 -233 -232 202
		mu 0 4 129 145 144 128
		f 4 235 -235 -234 204
		mu 0 4 130 146 145 129
		f 4 237 -237 -236 206
		mu 0 4 131 147 146 130
		f 4 239 -239 -238 208
		mu 0 4 132 148 147 131
		f 4 241 -241 -240 210
		mu 0 4 133 149 148 132
		f 4 243 -243 -242 212
		mu 0 4 134 150 149 133
		f 4 245 -245 -244 214
		mu 0 4 135 151 150 134
		f 4 247 -247 -246 216
		mu 0 4 136 152 151 135
		f 4 249 -249 -248 218
		mu 0 4 137 153 152 136
		f 4 251 -251 -250 220
		mu 0 4 138 154 153 137
		f 4 107 -253 -252 222
		mu 0 4 139 155 154 138
		f 4 254 -149 -254 224
		mu 0 4 141 157 156 140
		f 4 255 -153 -255 226
		mu 0 4 142 158 157 141
		f 4 256 -156 -256 228
		mu 0 4 143 159 158 142
		f 4 257 -159 -257 230
		mu 0 4 144 160 159 143
		f 4 258 -162 -258 232
		mu 0 4 145 161 160 144
		f 4 259 -165 -259 234
		mu 0 4 146 162 161 145
		f 4 260 -168 -260 236
		mu 0 4 147 163 162 146
		f 4 261 -171 -261 238
		mu 0 4 148 164 163 147
		f 4 262 -174 -262 240
		mu 0 4 149 165 164 148
		f 4 263 -177 -263 242
		mu 0 4 150 166 165 149
		f 4 264 -180 -264 244
		mu 0 4 151 167 166 150
		f 4 265 -183 -265 246
		mu 0 4 152 168 167 151
		f 4 266 -186 -266 248
		mu 0 4 153 169 168 152
		f 4 267 -189 -267 250
		mu 0 4 154 170 169 153
		f 4 123 -192 -268 252
		mu 0 4 155 171 170 154
		f 4 271 270 269 268
		mu 0 4 172 175 174 173
		f 4 274 -274 -273 149
		mu 0 4 94 177 176 156
		f 4 272 -277 -276 253
		mu 0 4 156 176 178 140
		f 4 275 -279 -278 223
		mu 0 4 140 178 179 124
		f 4 277 -280 -275 193
		mu 0 4 124 179 177 94
		f 4 281 -269 -281 273
		mu 0 4 177 172 173 176
		f 4 283 -271 -283 278
		mu 0 4 178 174 175 179
		f 4 282 -272 -282 279
		mu 0 4 179 175 172 177
		f 4 285 -143 -285 276
		mu 0 4 176 181 180 178
		f 4 284 144 -287 -284
		mu 0 4 178 180 182 174
		f 4 286 146 -288 -270
		mu 0 4 174 182 183 173
		f 4 287 -148 -286 280
		mu 0 4 173 183 181 176;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pTorus8";
	rename -uid "BA350EDC-FE4B-15BF-2699-3EA3233DCD1F";
	setAttr ".rp" -type "double3" 2.8281902510811183 0.96374949688507627 -1.1043379863746037 ;
	setAttr ".sp" -type "double3" 2.8281902510811183 0.96374949688507627 -1.1043379863746037 ;
createNode transform -n "transform5" -p "pTorus8";
	rename -uid "A9EEB24F-6E4A-33B9-FDC8-A999D7B53D1A";
	setAttr ".v" no;
createNode mesh -n "pTorus8Shape" -p "transform5";
	rename -uid "836A1555-2D41-7BA0-6661-069C7D6EF1E4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 10 ".iog[0].og";
	setAttr -av ".iog[0].og[5].gid";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.7333332896232605 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pTorus9";
	rename -uid "1433FE10-3843-5786-FC73-68824455B374";
	setAttr ".rp" -type "double3" 2.8281902441824127 0.96374951240814477 -0.36366390431210294 ;
	setAttr ".sp" -type "double3" 2.8281902441824127 0.96374951240814477 -0.36366390431210294 ;
createNode transform -n "transform7" -p "pTorus9";
	rename -uid "9AAD588E-E541-D34D-26DB-E182FDA3255A";
	setAttr ".v" no;
createNode mesh -n "pTorus9Shape" -p "transform7";
	rename -uid "724E979F-DF42-B256-5D41-01A1C1669E36";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 10 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pTorus10";
	rename -uid "FE9ACA2B-A346-77D6-D32A-DDB29E31E9FD";
	setAttr ".t" -type "double3" 0.031323216042642876 0 0 ;
	setAttr ".rp" -type "double3" 2.8281902441824127 0.96374951240814477 0.37701019275534886 ;
	setAttr ".sp" -type "double3" 2.8281902441824127 0.96374951240814477 0.37701019275534886 ;
createNode mesh -n "pTorus10Shape" -p "pTorus10";
	rename -uid "708074E5-F349-5A61-F97D-E2B5B98BE287";
	setAttr -k off ".v";
	setAttr -s 10 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.79166656732559204 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 41 ".pt";
	setAttr ".pt[16]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[17]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[18]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[19]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[20]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[21]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[22]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[23]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[32]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[33]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[34]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[35]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[36]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[37]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[38]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[39]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[65]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[67]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[69]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[71]" -type "float3" 0 0 0.6151793 ;
	setAttr ".pt[379]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[380]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[381]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[382]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[383]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[384]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[385]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[386]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[394]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[395]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[396]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[397]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[398]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[399]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[400]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[401]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[425]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[427]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[429]" -type "float3" 0 0 -1.3691998 ;
	setAttr ".pt[431]" -type "float3" 0 0 -1.3691998 ;
createNode transform -n "pCube2";
	rename -uid "68C5964A-204E-A7FF-6BAA-D3B2411CB103";
	setAttr ".t" -type "double3" 1 0.99775252376740475 -3.8912879753037828 ;
	setAttr ".s" -type "double3" 1.7927390006536357 1.6881266448762924 0.045208477695477048 ;
	setAttr ".rp" -type "double3" 0 7.2164496600635175e-16 8.5320338933342369e-14 ;
	setAttr ".spt" -type "double3" 0 7.2164496600635175e-16 8.5265128291212022e-14 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	rename -uid "0BE26810-D74B-9582-5182-08962642501A";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000001490116119 0.160714291036129 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube6";
	rename -uid "8C7BD452-7848-25B9-D8D1-6BA3AEB9009F";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590074 -5.8908842803442214 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "pCubeShape6" -p "pCube6";
	rename -uid "7D351539-0247-537B-9A13-58AE2245E1F8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "beam";
	rename -uid "342D258D-5D4F-4E09-ED59-4B8ADA5DCE1D";
createNode transform -n "pCube3" -p "beam";
	rename -uid "3A886835-BB44-FA7B-9A5D-66BEB5698D01";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590074 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "pCubeShape3" -p "|beam|pCube3";
	rename -uid "F35CAF4D-974D-324C-2DFE-5982482876B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
createNode transform -n "pCube4" -p "beam";
	rename -uid "570F8427-A446-C77B-8074-18B3CAA59FED";
	setAttr ".t" -type "double3" 1.5117128267902782 1.2049841028367543 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -2.341959037613659 0.52240791879256165 5.7707669087464355 ;
	setAttr ".sp" -type "double3" -0.54627722501754761 2.6910045146942139 31.939294815063477 ;
	setAttr ".spt" -type "double3" -1.7956818125961114 -2.1685965959016524 -26.168527906317042 ;
createNode mesh -n "pCubeShape4" -p "|beam|pCube4";
	rename -uid "B2518956-6E4E-1A45-8390-B9A3436245BC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[8:11]" -type "float3"  0.052375901 0.49401012 0 
		0.052375901 0.49401012 0 0.052375901 -0.046530426 0 0.052375901 -0.046530426 0;
createNode mesh -n "polySurfaceShape8" -p "|beam|pCube4";
	rename -uid "52CA2917-DF47-D276-83FB-F1B48CD101AA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 2.684999 31.770536 
		-0.9695735 3.7968943 31.786789 -0.091733545 2.339 31.770536 -0.96957374 3.4508946 
		31.786789 -0.091733545 2.339 32.091801 -0.96957374 3.4508946 32.108051 -0.091733545 
		2.684999 32.091801 -0.9695735 3.7968943 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube5" -p "beam";
	rename -uid "D755020A-484B-9BC8-A720-5F808D483F2F";
	setAttr ".t" -type "double3" -0.46340017732298922 0.76944171692621266 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 0.13512841007918389 3.1171321414407878 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840964 0.29397593623357654 5.7707669087464355 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.4911302328109741 31.939294815063477 ;
	setAttr ".spt" -type "double3" -0.46133351737311667 -2.1971542965773976 -26.168527906317042 ;
createNode mesh -n "pCubeShape5" -p "|beam|pCube5";
	rename -uid "BEF44E57-544C-46CB-6EB0-6DA6BC03BFA5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 2.6432605 31.770536 
		-0.18895763 2.6432607 31.786789 -0.091733545 2.339 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 2.339 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		2.6432605 32.091801 -0.18895763 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "beam1";
	rename -uid "3E485399-8D44-F039-2D7A-83B2D36F49D4";
	setAttr ".t" -type "double3" 0 0 -1.3858250209387617 ;
createNode transform -n "pCube3" -p "beam1";
	rename -uid "805C643A-834D-9F03-D0A9-92838AAE1D8A";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590074 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "pCubeShape3" -p "|beam1|pCube3";
	rename -uid "969001A0-9545-97AC-AB52-3FB7F8F77906";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube4" -p "beam1";
	rename -uid "1334BE82-BC47-FBFA-B412-BD95C2F34F56";
	setAttr ".t" -type "double3" 1.5117128267902782 1.2049841028367543 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -2.341959037613659 0.52240791879256165 5.7707669087464355 ;
	setAttr ".sp" -type "double3" -0.54627722501754761 2.6910045146942139 31.939294815063477 ;
	setAttr ".spt" -type "double3" -1.7956818125961114 -2.1685965959016524 -26.168527906317042 ;
createNode mesh -n "pCubeShape4" -p "|beam1|pCube4";
	rename -uid "38657C3D-B340-8728-BD69-0CA05DF4D4EA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 18 ".uvst[0].uvsp[0:17]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[8:11]" -type "float3"  0.052375901 0.49401012 0 
		0.052375901 0.49401012 0 0.052375901 -0.046530426 0 0.052375901 -0.046530426 0;
	setAttr -s 12 ".vt[0:11]"  -0.59173357 2.18499851 32.27053833 -0.46957353 3.29689407 32.28678894
		 -0.59173357 2.83899975 32.27053833 -0.46957374 3.95089436 32.28678894 -0.59173357 2.83899975 31.59179878
		 -0.46957374 3.95089436 31.60804939 -0.59173357 2.18499851 31.59179878 -0.46957353 3.29689407 31.60804939
		 -0.46957353 3.29689407 31.60804939 -0.46957353 3.29689407 32.28678894 -0.46957374 3.95089436 31.60804939
		 -0.46957374 3.95089436 32.28678894;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -15 -17 -19 -20
		mu 0 4 14 15 16 17
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape8" -p "|beam1|pCube4";
	rename -uid "65109C96-7148-062C-9708-7D988259B885";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 2.684999 31.770536 
		-0.9695735 3.7968943 31.786789 -0.091733545 2.339 31.770536 -0.96957374 3.4508946 
		31.786789 -0.091733545 2.339 32.091801 -0.96957374 3.4508946 32.108051 -0.091733545 
		2.684999 32.091801 -0.9695735 3.7968943 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube5" -p "beam1";
	rename -uid "5A7B5A52-534E-D396-C358-BEA18151BE89";
	setAttr ".t" -type "double3" -0.46340017732298922 0.76944171692621266 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 0.13512841007918389 3.1171321414407878 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840964 0.29397593623357654 5.7707669087464355 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.4911302328109741 31.939294815063477 ;
	setAttr ".spt" -type "double3" -0.46133351737311667 -2.1971542965773976 -26.168527906317042 ;
createNode mesh -n "pCubeShape5" -p "|beam1|pCube5";
	rename -uid "080CEA05-934D-7A38-BF3F-0EABB6AEE2FE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 2.6432605 31.770536 
		-0.18895763 2.6432607 31.786789 -0.091733545 2.339 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 2.339 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		2.6432605 32.091801 -0.18895763 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "beam2";
	rename -uid "42DDC9E8-124B-29D4-00DD-6AB426D0328D";
	setAttr ".t" -type "double3" 0 0 -2.7716500418775234 ;
createNode transform -n "pCube3" -p "beam2";
	rename -uid "29436657-F74F-56D9-C072-C19FDB677B9B";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590074 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "pCubeShape3" -p "|beam2|pCube3";
	rename -uid "6DFCB259-0C44-ACF8-015E-62A3D0F7F2BD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube4" -p "beam2";
	rename -uid "F8B233F3-B94E-D79F-5FD9-60BB5BD7F4B5";
	setAttr ".t" -type "double3" 1.5117128267902782 1.2049841028367543 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -2.341959037613659 0.52240791879256165 5.7707669087464355 ;
	setAttr ".sp" -type "double3" -0.54627722501754761 2.6910045146942139 31.939294815063477 ;
	setAttr ".spt" -type "double3" -1.7956818125961114 -2.1685965959016524 -26.168527906317042 ;
createNode mesh -n "pCubeShape4" -p "|beam2|pCube4";
	rename -uid "C26AC793-6847-9E59-2DE5-E0BCA27B1C0F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 18 ".uvst[0].uvsp[0:17]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[8:11]" -type "float3"  0.052375901 0.49401012 0 
		0.052375901 0.49401012 0 0.052375901 -0.046530426 0 0.052375901 -0.046530426 0;
	setAttr -s 12 ".vt[0:11]"  -0.59173357 2.18499851 32.27053833 -0.46957353 3.29689407 32.28678894
		 -0.59173357 2.83899975 32.27053833 -0.46957374 3.95089436 32.28678894 -0.59173357 2.83899975 31.59179878
		 -0.46957374 3.95089436 31.60804939 -0.59173357 2.18499851 31.59179878 -0.46957353 3.29689407 31.60804939
		 -0.46957353 3.29689407 31.60804939 -0.46957353 3.29689407 32.28678894 -0.46957374 3.95089436 31.60804939
		 -0.46957374 3.95089436 32.28678894;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -15 -17 -19 -20
		mu 0 4 14 15 16 17
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape8" -p "|beam2|pCube4";
	rename -uid "D81DD632-7A4D-1853-5925-C2B7078D41D5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 2.684999 31.770536 
		-0.9695735 3.7968943 31.786789 -0.091733545 2.339 31.770536 -0.96957374 3.4508946 
		31.786789 -0.091733545 2.339 32.091801 -0.96957374 3.4508946 32.108051 -0.091733545 
		2.684999 32.091801 -0.9695735 3.7968943 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube5" -p "beam2";
	rename -uid "C87A2D36-A943-34DA-2557-4E93F87D2902";
	setAttr ".t" -type "double3" -0.46340017732298922 0.76944171692621266 -5.1960120375416077 ;
	setAttr ".s" -type "double3" 0.13512841007918389 3.1171321414407878 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840964 0.29397593623357654 5.7707669087464355 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.4911302328109741 31.939294815063477 ;
	setAttr ".spt" -type "double3" -0.46133351737311667 -2.1971542965773976 -26.168527906317042 ;
createNode mesh -n "pCubeShape5" -p "|beam2|pCube5";
	rename -uid "AC14F227-7D44-307D-2FF5-80BCF0E46A1E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 2.6432605 31.770536 
		-0.18895763 2.6432607 31.786789 -0.091733545 2.339 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 2.339 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		2.6432605 32.091801 -0.18895763 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube7";
	rename -uid "3D5B22A2-8A46-8737-FB9E-399BFB4F3C6B";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590074 -7.2814177312961057 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "pCubeShape7" -p "pCube7";
	rename -uid "9F3A86B3-7E44-301A-24D0-B9ABCE156A2C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube8";
	rename -uid "F27C7053-8649-E6F0-7816-18B6EA634100";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590076 -8.6719511822479944 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "pCubeShape8" -p "pCube8";
	rename -uid "76B18A46-6742-F871-1128-FA8A796879A6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube9";
	rename -uid "BEC8D5BC-EF4F-53EB-C067-25A6697564D5";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590076 -4.6232519705704798 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "pCubeShape9" -p "pCube9";
	rename -uid "3BD0F464-6A4B-FEDC-46FD-8A86E9B11C5A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube10";
	rename -uid "BB394927-3C42-371F-3FC7-62A3FB605A80";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590076 -3.2394831485831355 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "pCubeShape10" -p "pCube10";
	rename -uid "6B019239-ED4F-49E7-1E0B-67B840FE90CC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube11";
	rename -uid "92354BAD-9945-2CD0-AD47-BBA43AD9330B";
	setAttr ".t" -type "double3" 1.5117128267902782 1.3763607902590076 -2.6812261722653337 ;
	setAttr ".s" -type "double3" 4.2871255296034541 0.19413119373823251 0.18067922107111714 ;
	setAttr ".rp" -type "double3" -0.60167909079840953 0.55687479330414202 5.7417440199895751 ;
	setAttr ".sp" -type "double3" -0.14034557342529297 2.8685487508773804 31.77866268157959 ;
	setAttr ".spt" -type "double3" -0.46133351737311662 -2.3116739575732383 -26.036918661590015 ;
createNode mesh -n "pCubeShape11" -p "pCube11";
	rename -uid "DF538F06-D34A-DB95-3AF7-4FADCF11148B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.091733545 3.7023597 31.770536 
		-0.18895757 2.6432607 31.786789 -0.091733545 3.3980982 31.770536 -0.18895757 2.3389993 
		31.786789 -0.091733545 3.3980982 32.091801 -0.18895757 2.3389993 32.108051 -0.091733545 
		3.7023597 32.091801 -0.18895757 2.6432607 32.108051;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "EC85C6A6-A54B-4DBB-FB63-919196BA23FA";
	setAttr -s 9 ".lnk";
	setAttr -s 9 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "CFF12BBC-544C-0D4C-5A96-038A40CA4344";
createNode displayLayer -n "defaultLayer";
	rename -uid "9A45B3DE-A743-876B-5539-A1A5575971B9";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "1C9BF05F-314E-45B2-7352-D99C52D23E65";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "C777EBB5-7B4B-CA47-47D1-D995DFA8A922";
	setAttr ".g" yes;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "A08818BE-E54E-4579-30AA-F1BED886A6EE";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "218CEFB3-F24E-DA4A-3CB0-DD9487935ED0";
createNode polyCube -n "polyCube1";
	rename -uid "3D47C0EB-D348-A583-7811-019266459F08";
	setAttr ".cuv" 4;
createNode polyChipOff -n "polyChipOff1";
	rename -uid "982E4B51-9641-352C-8AF5-CB8A5045A441";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 2.0502456776410818 0 0 0 0 2.0502456776410818 0 0 0 0 5.8539501523725681 0
		 0 1.0251228388205409 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.0251229 0 ;
	setAttr ".rs" 349826676;
	setAttr ".lt" -type "double3" 0 0 1.2892211252127423 ;
	setAttr ".kft" no;
	setAttr ".dup" no;
createNode polyTweak -n "polyTweak1";
	rename -uid "0E44A603-B24C-484F-5D51-2097A1A0459C";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[3]" -type "float3" 0 -0.10028286 0 ;
	setAttr ".tk[5]" -type "float3" 0 -0.10028286 0 ;
createNode polySeparate -n "polySeparate1";
	rename -uid "16946972-C54A-264E-3648-D6853FDB193C";
	setAttr ".ic" 2;
	setAttr -s 2 ".out";
createNode groupId -n "groupId1";
	rename -uid "6E923FD1-9A4C-72F8-61E7-E9868BCB7F3C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "A322674D-0048-54C1-B89B-9D9F988E7398";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId2";
	rename -uid "A8664A45-C946-99C1-7D32-07B8B72E8CAE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "3E712400-C24A-A2B2-2759-7BB11765FD62";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "D9F39E81-EA4E-F18E-2DF3-FA998A9E51A4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0]";
createNode groupId -n "groupId4";
	rename -uid "E51F7B3E-384D-204A-262A-3E925586DB0D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "3FB0CA36-AF48-AE3E-7C78-75B73AC3EDE5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:4]";
createNode polyNormal -n "polyNormal1";
	rename -uid "2AB33DA0-AB4E-1524-8B43-EB86DEDF8E8C";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".unm" no;
createNode polyNormal -n "polyNormal2";
	rename -uid "2BEC2326-B040-88AA-AC67-C29BE8BA1BAC";
	setAttr ".ics" -type "componentList" 1 "f[0:4]";
createNode polyTorus -n "polyTorus1";
	rename -uid "CA175CBB-FF41-6EC1-5F3B-D5A84EEDFCCE";
	setAttr ".sr" 0.1;
	setAttr ".tw" 45;
	setAttr ".sa" 50;
	setAttr ".sh" 4;
createNode objectSet -n "set1";
	rename -uid "C7D4B60C-6A44-AB29-DC08-5EB0453733E3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "AB940A88-2846-6B08-6B08-DF93465FE161";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "2FD2F3BE-024A-612C-E9C1-4DA541922147";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[24:48]" "e[74:98]" "e[124:148]" "e[174:198]" "e[224:249]" "e[274:299]" "e[324:349]" "e[374:399]";
createNode deleteComponent -n "deleteComponent1";
	rename -uid "5D956465-3043-F8FE-6EAA-BB885C4C6ADF";
	setAttr ".dc" -type "componentList" 4 "f[24:48]" "f[74:98]" "f[124:148]" "f[174:198]";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "20514BB4-4C49-DC07-E5A5-6C8AEA64FB87";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 316\n            -height 278\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 315\n            -height 277\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 316\n            -height 277\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 638\n            -height 600\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n"
		+ "            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n"
		+ "            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n"
		+ "            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n"
		+ "                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n"
		+ "                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n"
		+ "                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n"
		+ "                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -highlightConnections 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n"
		+ "                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 638\\n    -height 600\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 638\\n    -height 600\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "3ED733B4-2540-872F-4C59-A4B74EDBC17B";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCloseBorder -n "polyCloseBorder1";
	rename -uid "5C1A7D5E-594B-9F1F-B0BC-AEAC5001C275";
	setAttr ".ics" -type "componentList" 1 "e[*]";
createNode polyTweak -n "polyTweak2";
	rename -uid "D3EE625D-D445-2A1C-D0BF-E69A7C909E23";
	setAttr ".uopa" yes;
	setAttr -s 52 ".tk[26:77]" -type "float3"  0.09099222 0 0 0.1161877 0
		 0 0.15773851 0 0 0.21498932 0 0 0.28703716 0 0 0.37274602 2.220446e-16 -0.67477077
		 7.4505806e-09 1.2490009e-16 -0.58272582 0 1.110223e-16 -0.50369155 1.4901161e-08
		 9.7144515e-17 -0.43891448 0 8.3266727e-17 -0.38941613 -8.9406967e-08 8.3266727e-17
		 -0.35597703 -2.9802322e-08 6.9388939e-17 -0.33912468 0 6.9388939e-17 -0.3391248 0
		 8.3266727e-17 -0.35597715 0 9.7144515e-17 -0.38941631 0 9.7144515e-17 -0.43891481
		 0 1.110223e-16 -0.50369191 -0.84723085 5.5511151e-17 -0.58272612 -0.74921274 -6.9388939e-17
		 0 -0.66350412 -6.9388939e-17 0 -0.59145641 -6.9388939e-17 0 -0.53420562 -6.9388939e-17
		 0 -0.49265486 -6.9388939e-17 0 -0.46745956 -6.9388939e-17 0 -0.4590168 -6.9388939e-17
		 0 0.082550414 0 0 0.09099222 0 0 0.1161877 0 0 0.15773851 0 0 0.21498932 0 0 0.28703716
		 0 0 0.37274602 2.220446e-16 -0.67477077 7.4505806e-09 1.2490009e-16 -0.58272582 0
		 1.110223e-16 -0.50369155 1.4901161e-08 9.7144515e-17 -0.43891448 0 8.3266727e-17
		 -0.38941613 -2.9802322e-08 8.3266727e-17 -0.35597703 -2.9802322e-08 6.9388939e-17
		 -0.33912468 0 6.9388939e-17 -0.3391248 0 8.3266727e-17 -0.35597715 0 9.7144515e-17
		 -0.38941631 0 9.7144515e-17 -0.43891481 0 1.110223e-16 -0.50369191 -0.84723085 5.5511151e-17
		 -0.58272612 -0.74921274 -6.9388939e-17 0 -0.66350412 -6.9388939e-17 0 -0.59145641
		 -6.9388939e-17 0 -0.53420562 -6.9388939e-17 0 -0.49265486 -6.9388939e-17 0 -0.46745956
		 -6.9388939e-17 0 -0.4590168 -6.9388939e-17 0 0.082550414 0 0;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "D2CF4832-2A4F-533B-87EE-3C8D9881EE7B";
	setAttr ".ics" -type "componentList" 1 "f[100:101]";
	setAttr ".ix" -type "matrix" 1.5152858838336571 0 0 0 0 3.3646105542432129e-16 1.5152858838336571 0
		 0 -1.5152858838336571 3.3646105542432129e-16 0 1.2928521643586794 0.47063593114229862 0.93431024359174064 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.0076246 0.47063586 0.93431026 ;
	setAttr ".rs" 1897297934;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.0251231634875992 0.47063577399743589 0.82716333497236327 ;
	setAttr ".cbx" -type "double3" 3.0403723772601738 0.47063593114229862 1.0414571409213584 ;
createNode polyTorus -n "polyTorus2";
	rename -uid "A7B757E4-7846-E4DC-1E1F-ECB06A8002E7";
	setAttr ".r" 0.5;
	setAttr ".sr" 0.1;
	setAttr ".tw" 45;
	setAttr ".sa" 60;
	setAttr ".sh" 4;
createNode objectSet -n "set2";
	rename -uid "0E6CA0E7-BA42-5550-518C-5C89FF17863B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6";
	rename -uid "43DEF63B-3F49-30D6-A60B-54AAFBC77241";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "5F1D5765-5C40-D729-A89C-1B986844CDC3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[29:58]" "e[89:118]" "e[149:178]" "e[209:238]" "e[269:299]" "e[329:359]" "e[389:419]" "e[449:479]";
createNode deleteComponent -n "deleteComponent2";
	rename -uid "E707DA4B-8841-4958-B977-1385B7E7AE8F";
	setAttr ".dc" -type "componentList" 4 "f[29:58]" "f[89:118]" "f[149:178]" "f[209:238]";
createNode polySplitEdge -n "polySplitEdge1";
	rename -uid "FED6FC96-3A44-ED8A-6206-ABA7674B2C19";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "e[134]" "e[150]" "e[165]" "e[181]" "e[196]" "e[212]" "e[227]" "e[243]";
createNode polyMergeVert -n "polyMergeVert1";
	rename -uid "D90FF8C0-BE4D-9541-4E77-DEAE7C9CFEA6";
	setAttr ".ics" -type "componentList" 2 "vtx[45]" "vtx[125]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 -3.7583501234906547 0 5.1188395462216825 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak3";
	rename -uid "C45E9E0E-4947-CC6F-36EB-56B3EEA312A1";
	setAttr ".uopa" yes;
	setAttr -s 128 ".tk[0:127]" -type "float3"  0.6020183 0 0 0.6020183 0
		 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.30100915 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183 0 0 0.6020183
		 0 0 0.6020183 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.6020183 0 0 0.6020183 0 1.5832484e-08 0.30100915
		 0 1.5832484e-08 0.6020183 0 1.5832484e-08 0.6020183 0 1.5832484e-08;
createNode polyMergeVert -n "polyMergeVert2";
	rename -uid "81BABF8C-CB4A-B358-DC43-5FB24BC999B8";
	setAttr ".ics" -type "componentList" 2 "vtx[14]" "vtx[124]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 -3.7583501234906547 0 5.1188395462216825 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak4";
	rename -uid "E35A8AF0-CC43-29AC-CDD2-64BA82FC47ED";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[14]" -type "float3" 0.30100912 0 2.9802322e-08 ;
	setAttr ".tk[124]" -type "float3" -0.30100915 0 0 ;
createNode polyMergeVert -n "polyMergeVert3";
	rename -uid "915B7F3B-1A4F-1C34-D232-2E8A3967CD6D";
	setAttr ".ics" -type "componentList" 2 "vtx[107]" "vtx[124]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 -3.7583501234906547 0 5.1188395462216825 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak5";
	rename -uid "F57E36AB-B847-6E92-DB51-B289934CC23F";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[107]" -type "float3" 0.30100912 0 0 ;
	setAttr ".tk[124]" -type "float3" -0.30100915 0 -2.9802322e-08 ;
createNode polyMergeVert -n "polyMergeVert4";
	rename -uid "C424AA88-A449-2BD5-2A03-5EA2CA25CD8C";
	setAttr ".ics" -type "componentList" 2 "vtx[76]" "vtx[124]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 -3.7583501234906547 0 5.1188395462216825 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak6";
	rename -uid "F8CB25B8-BC4E-D55F-F081-76B234BFCFDB";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[76]" -type "float3" 0.30100915 0 0 ;
	setAttr ".tk[124]" -type "float3" -0.30100915 0 0 ;
createNode polyCloseBorder -n "polyCloseBorder2";
	rename -uid "9564B322-FA4B-EEC4-F594-C0A2A34B935C";
	setAttr ".ics" -type "componentList" 1 "e[*]";
createNode polyTweak -n "polyTweak7";
	rename -uid "D0759381-6A49-6B19-BE14-5B915DEE7B39";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[14]" -type "float3" 0 0 -0.0036479675 ;
	setAttr ".tk[45]" -type "float3" 0 0 -0.0036479675 ;
	setAttr ".tk[76]" -type "float3" 0 0 -0.0036479675 ;
	setAttr ".tk[107]" -type "float3" 0 0 -0.0036479675 ;
createNode rmanGlobals -s -n "rmanGlobals";
	rename -uid "58AAA6E0-484A-1207-DA4D-209DB397B41D";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".hider_minSamples" 0;
	setAttr ".hider_maxSamples" 128;
	setAttr ".ri_pixelVariance" 0.0099999997764825821;
	setAttr ".hider_darkfalloff" 0.02500000037252903;
	setAttr ".hider_incremental" yes;
	setAttr ".ipr_hider_maxSamples" 64;
	setAttr ".ipr_ri_pixelVariance" 0.05000000074505806;
	setAttr ".ri_maxSpecularDepth" 4;
	setAttr ".ri_maxDiffuseDepth" 1;
	setAttr ".ri_displayFilter" -type "string" "gaussian";
	setAttr ".ri_displayFilterSize" -type "float2" 2 2 ;
	setAttr ".motionBlur" 0;
	setAttr ".cameraBlur" no;
	setAttr ".shutterAngle" 180;
	setAttr ".shutterOpenEnd" 0;
	setAttr ".shutterCloseStart" 1;
	setAttr ".shutterTiming" 0;
	setAttr ".motionSamples" 2;
	setAttr ".displayFilters[0]" -type "string" "";
	setAttr ".sampleFilters[0]" -type "string" "";
	setAttr ".outputAllShaders" no;
	setAttr ".nestedInstancing" no;
	setAttr ".reentrantProcedurals" yes;
	setAttr ".outputShadowAOV" 0;
	setAttr ".enableImagePlaneFilter" yes;
	setAttr ".learnLightSelection" no;
	setAttr ".ri_hider_adaptAll" no;
	setAttr ".osl_batched" 0;
	setAttr ".adaptiveSampler" 0;
	setAttr ".opt_bucket_order" -type "string" "circle";
	setAttr ".limits_bucketsize" -type "long2" 16 16 ;
	setAttr ".limits_othreshold" -type "float3" 0.99599999 0.99599999 0.99599999 ;
	setAttr ".rfm_referenceFrame" 0;
	setAttr ".dice_micropolygonlength" 1;
	setAttr ".dice_watertight" no;
	setAttr ".dice_referenceCameraType" 0;
	setAttr ".dice_referenceCamera" -type "string" "";
	setAttr ".hair_minWidth" 0.5;
	setAttr ".trace_autobias" yes;
	setAttr ".trace_bias" 0.0010000000474974513;
	setAttr ".trace_worldorigin" -type "string" "camera";
	setAttr ".trace_worldoffset" -type "float3" 0 0 0 ;
	setAttr ".opt_cropWindowEnable" no;
	setAttr ".opt_cropWindowTopLeft" -type "float2" 0 0 ;
	setAttr ".opt_cropWindowBottomRight" -type "float2" 1 1 ;
	setAttr ".user_sceneUnits" 1;
	setAttr ".user_iesIgnoreWatts" yes;
	setAttr ".limits_texturememory" 4096;
	setAttr ".limits_geocachememory" 4096;
	setAttr ".limits_opacitycachememory" 2048;
	setAttr ".statistics_level" 1;
	setAttr ".statistics_xmlfilename" -type "string" "";
	setAttr ".lpe_diffuse2" -type "string" "Diffuse,HairDiffuse";
	setAttr ".lpe_diffuse3" -type "string" "Subsurface";
	setAttr ".lpe_specular2" -type "string" "Specular,HairSpecularR";
	setAttr ".lpe_specular3" -type "string" "RoughSpecular,HairSpecularTRT";
	setAttr ".lpe_specular4" -type "string" "Clearcoat";
	setAttr ".lpe_specular5" -type "string" "Iridescence";
	setAttr ".lpe_specular6" -type "string" "Fuzz,HairSpecularGLINTS";
	setAttr ".lpe_specular7" -type "string" "SingleScatter,HairSpecularTT";
	setAttr ".lpe_specular8" -type "string" "Glass";
	setAttr ".lpe_user2" -type "string" "Albedo,DiffuseAlbedo,SubsurfaceAlbedo,HairAlbedo";
	setAttr ".lpe_user3" -type "string" "";
	setAttr ".lpe_user4" -type "string" "";
	setAttr ".lpe_user5" -type "string" "";
	setAttr ".lpe_user6" -type "string" "";
	setAttr ".lpe_user7" -type "string" "";
	setAttr ".lpe_user8" -type "string" "";
	setAttr ".lpe_user9" -type "string" "";
	setAttr ".lpe_user10" -type "string" "";
	setAttr ".lpe_user11" -type "string" "";
	setAttr ".lpe_user12" -type "string" "";
	setAttr ".imageFileFormat" -type "string" "<scene>_<layer>_<camera>_<aov>.<f4>.<ext>";
	setAttr ".ribFileFormat" -type "string" "<camera><layer>.<f4>.rib";
	setAttr ".version" 1;
	setAttr ".take" 1;
	setAttr ".imageOutputDir" -type "string" "<ws>/images/<scene>_v<version>_t<take>";
	setAttr ".ribOutputDir" -type "string" "<ws>/renderman/rib/<scene>/v<version>_t<take>";
	setAttr -s 10 ".UserTokens";
	setAttr ".UserTokens[0].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[0].userTokenValues" -type "string" "";
	setAttr ".UserTokens[1].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[1].userTokenValues" -type "string" "";
	setAttr ".UserTokens[2].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[2].userTokenValues" -type "string" "";
	setAttr ".UserTokens[3].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[3].userTokenValues" -type "string" "";
	setAttr ".UserTokens[4].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[4].userTokenValues" -type "string" "";
	setAttr ".UserTokens[5].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[5].userTokenValues" -type "string" "";
	setAttr ".UserTokens[6].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[6].userTokenValues" -type "string" "";
	setAttr ".UserTokens[7].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[7].userTokenValues" -type "string" "";
	setAttr ".UserTokens[8].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[8].userTokenValues" -type "string" "";
	setAttr ".UserTokens[9].userTokenKeys" -type "string" "";
	setAttr ".UserTokens[9].userTokenValues" -type "string" "";
	setAttr ".rlfData" -type "string" "init";
createNode rmanDisplay -s -n "rmanDefaultDisplay";
	rename -uid "D9EEC36F-A648-2F4C-BA89-29ADD0D2693B";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".enable" yes;
	setAttr ".denoise" no;
	setAttr ".frameMode" 0;
	setAttr ".remapBreakPoint" 0;
	setAttr ".remapMaxValue" 0;
	setAttr ".remapSmoothness" 0;
	setAttr -s 2 ".displayChannels";
	setAttr -l on ".name" -type "string" "beauty";
createNode d_openexr -n "d_openexr";
	rename -uid "FC5B0CAF-E846-1C93-F730-4D9B2C40F141";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".asrgba" yes;
	setAttr ".storage" -type "string" "scanline";
	setAttr ".exrpixeltype" -type "string" "half";
	setAttr ".compression" -type "string" "zips";
	setAttr ".compressionlevel" 45;
createNode rmanDisplayChannel -n "Ci";
	rename -uid "10105B6C-9D4F-3B67-929C-3BB35233FB54";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".enable" yes;
	setAttr ".channelType" -type "string" "color";
	setAttr ".channelSource" -type "string" "Ci";
	setAttr ".lpeLightGroup" -type "string" "";
	setAttr ".filter" -type "string" "inherit from display";
	setAttr ".filterwidth" -type "float2" -1 -1 ;
	setAttr ".statistics" -type "string" "";
	setAttr ".remapBreakPoint" 0;
	setAttr ".remapMaxValue" 0;
	setAttr ".remapSmoothness" 0;
	setAttr -l on ".name" -type "string" "Ci";
createNode rmanDisplayChannel -n "a";
	rename -uid "8D128D32-FA41-A287-441B-9798B2FD96CD";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".enable" yes;
	setAttr ".channelType" -type "string" "float";
	setAttr ".channelSource" -type "string" "a";
	setAttr ".lpeLightGroup" -type "string" "";
	setAttr ".filter" -type "string" "inherit from display";
	setAttr ".filterwidth" -type "float2" -1 -1 ;
	setAttr ".statistics" -type "string" "";
	setAttr ".remapBreakPoint" 0;
	setAttr ".remapMaxValue" 0;
	setAttr ".remapSmoothness" 0;
	setAttr -l on ".name" -type "string" "a";
createNode PxrPathTracer -s -n "PxrPathTracer";
	rename -uid "F481761F-B54A-6FB2-C982-D5B07DFBE1DE";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".maxPathLength" 10;
	setAttr ".maxContinuationLength" -1;
	setAttr ".maxNonStochasticOpacityEvents" 0;
	setAttr ".sampleMode" -type "string" "bxdf";
	setAttr ".numLightSamples" 1;
	setAttr ".numBxdfSamples" 1;
	setAttr ".numIndirectSamples" 1;
	setAttr ".numDiffuseSamples" 1;
	setAttr ".numSpecularSamples" 1;
	setAttr ".numSubsurfaceSamples" 1;
	setAttr ".numRefractionSamples" 1;
	setAttr ".allowCaustics" no;
	setAttr ".accumOpacity" no;
	setAttr ".rouletteDepth" 4;
	setAttr ".rouletteThreshold" 0.20000000298023224;
	setAttr ".clampDepth" 2;
	setAttr ".clampLuminance" 10;
createNode blinn -n "svgBlinn1";
	rename -uid "26BD5703-8B4B-ABB0-4089-EAB0F6142C9D";
	setAttr ".c" -type "float3" 1 1 1 ;
createNode shadingEngine -n "svgBlinn1SG";
	rename -uid "2D475104-DC4B-1236-8F98-A8B99A8256B7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "08CFE0F9-0C4E-5B43-4809-33A8DF2EE2CD";
createNode PxrSurface -n "PxrSurface1";
	rename -uid "C8FC649B-F24C-4E7F-2EBB-B184C2F8FE45";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".inputMaterial" 0;
	setAttr ".diffuseGain" 1;
	setAttr ".diffuseColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".diffuseRoughness" 0;
	setAttr ".diffuseExponent" 1;
	setAttr ".diffuseBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".diffuseDoubleSided" no;
	setAttr ".diffuseBackUseDiffuseColor" yes;
	setAttr ".diffuseBackColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".diffuseTransmitGain" 0;
	setAttr ".diffuseTransmitColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".specularFresnelMode" 0;
	setAttr ".specularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".specularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".specularFresnelShape" 5;
	setAttr ".specularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".specularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".specularRoughness" 0.20000000298023224;
	setAttr ".specularModelType" 0;
	setAttr ".specularAnisotropy" 0;
	setAttr ".specularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".specularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".specularDoubleSided" no;
	setAttr ".roughSpecularFresnelMode" 0;
	setAttr ".roughSpecularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularFresnelShape" 5;
	setAttr ".roughSpecularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".roughSpecularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularRoughness" 0.60000002384185791;
	setAttr ".roughSpecularModelType" 0;
	setAttr ".roughSpecularAnisotropy" 0;
	setAttr ".roughSpecularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularDoubleSided" no;
	setAttr ".clearcoatFresnelMode" 0;
	setAttr ".clearcoatFaceColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatFresnelShape" 5;
	setAttr ".clearcoatIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".clearcoatExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".clearcoatThickness" 0;
	setAttr ".clearcoatAbsorptionTint" -type "float3" 0 0 0 ;
	setAttr ".clearcoatRoughness" 0;
	setAttr ".clearcoatModelType" 0;
	setAttr ".clearcoatAnisotropy" 0;
	setAttr ".clearcoatAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".clearcoatBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".clearcoatDoubleSided" no;
	setAttr ".specularEnergyCompensation" 0;
	setAttr ".clearcoatEnergyCompensation" 0;
	setAttr ".iridescenceFaceGain" 0;
	setAttr ".iridescenceEdgeGain" 0;
	setAttr ".iridescenceFresnelShape" 5;
	setAttr ".iridescenceMode" 0;
	setAttr ".iridescencePrimaryColor" -type "float3" 1 0 0 ;
	setAttr ".iridescenceSecondaryColor" -type "float3" 0 0 1 ;
	setAttr ".iridescenceRoughness" 0.20000000298023224;
	setAttr ".iridescenceAnisotropy" 0;
	setAttr ".iridescenceAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".iridescenceCurve" 1;
	setAttr ".iridescenceScale" 1;
	setAttr ".iridescenceFlip" no;
	setAttr ".iridescenceThickness" 800;
	setAttr ".iridescenceDoubleSided" no;
	setAttr ".fuzzGain" 0;
	setAttr ".fuzzColor" -type "float3" 1 1 1 ;
	setAttr ".fuzzConeAngle" 8;
	setAttr ".fuzzBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".fuzzDoubleSided" no;
	setAttr ".subsurfaceType" 0;
	setAttr ".subsurfaceGain" 0;
	setAttr ".subsurfaceColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".subsurfaceDmfp" 10;
	setAttr ".subsurfaceDmfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".shortSubsurfaceGain" 0;
	setAttr ".shortSubsurfaceColor" -type "float3" 0.89999998 0.89999998 0.89999998 ;
	setAttr ".shortSubsurfaceDmfp" 5;
	setAttr ".longSubsurfaceGain" 0;
	setAttr ".longSubsurfaceColor" -type "float3" 0.80000001 0 0 ;
	setAttr ".longSubsurfaceDmfp" 20;
	setAttr ".subsurfaceDirectionality" 0;
	setAttr ".subsurfaceBleed" 0;
	setAttr ".subsurfaceDiffuseBlend" 0;
	setAttr ".subsurfaceResolveSelfIntersections" no;
	setAttr ".subsurfaceIor" 1.3999999761581421;
	setAttr ".subsurfacePostTint" -type "float3" 1 1 1 ;
	setAttr ".subsurfaceDiffuseSwitch" 1;
	setAttr ".subsurfaceDoubleSided" no;
	setAttr ".subsurfaceTransmitGain" 0;
	setAttr ".considerBackside" yes;
	setAttr ".continuationRayMode" 0;
	setAttr ".maxContinuationHits" 2;
	setAttr ".followTopology" 0;
	setAttr ".subsurfaceSubset" -type "string" "";
	setAttr ".singlescatterGain" 0;
	setAttr ".singlescatterColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".singlescatterMfp" 10;
	setAttr ".singlescatterMfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".singlescatterDirectionality" 0;
	setAttr ".singlescatterIor" 1.2999999523162842;
	setAttr ".singlescatterBlur" 0;
	setAttr ".singlescatterDirectGain" 0;
	setAttr ".singlescatterDirectGainTint" -type "float3" 1 1 1 ;
	setAttr ".singlescatterDoubleSided" no;
	setAttr ".singlescatterConsiderBackside" yes;
	setAttr ".singlescatterContinuationRayMode" 0;
	setAttr ".singlescatterMaxContinuationHits" 2;
	setAttr ".singlescatterDirectGainMode" 0;
	setAttr ".singlescatterSubset" -type "string" "";
	setAttr ".irradianceTint" -type "float3" 1 1 1 ;
	setAttr ".irradianceRoughness" 0;
	setAttr ".unitLength" 0.10000000149011612;
	setAttr ".refractionGain" 0;
	setAttr ".reflectionGain" 0;
	setAttr ".refractionColor" -type "float3" 1 1 1 ;
	setAttr ".glassRoughness" 0.10000000149011612;
	setAttr ".glassAnisotropy" 0;
	setAttr ".glassAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".glassIor" 1.5;
	setAttr ".mwWalkable" no;
	setAttr ".mwIor" -1;
	setAttr ".thinGlass" no;
	setAttr ".ignoreFresnel" no;
	setAttr ".ignoreAccumOpacity" no;
	setAttr ".blocksVolumes" no;
	setAttr ".ssAlbedo" -type "float3" 0 0 0 ;
	setAttr ".extinction" -type "float3" 0 0 0 ;
	setAttr ".g" 0;
	setAttr ".multiScatter" no;
	setAttr ".enableOverlappingVolumes" no;
	setAttr ".glowGain" 0;
	setAttr ".glowColor" -type "float3" 1 1 1 ;
	setAttr ".bumpNormal" -type "float3" 0 0 0 ;
	setAttr ".shadowColor" -type "float3" 0 0 0 ;
	setAttr ".shadowMode" 0;
	setAttr ".presence" 1;
	setAttr ".presenceCached" 1;
	setAttr ".mwStartable" no;
	setAttr ".roughnessMollificationClamp" 32;
	setAttr ".utilityPattern[0]"  0;
createNode shadingEngine -n "PxrSurface1SG";
	rename -uid "16003889-0740-B21C-AE94-8A88E6D2E667";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "41A1BCFC-0E4D-86E3-EDBF-F38E0440CE5F";
createNode lambert -n "lambert2";
	rename -uid "AC633215-324C-0932-3291-3CA1043003C1";
createNode PxrSurface -n "PxrSurface2";
	rename -uid "F47065C6-924B-A848-8FB0-308428CE44EC";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".inputMaterial" 0;
	setAttr ".diffuseGain" 1;
	setAttr ".diffuseColor" -type "float3" 0.2013889 0.2013889 0.2013889 ;
	setAttr ".diffuseRoughness" 0;
	setAttr ".diffuseExponent" 1;
	setAttr ".diffuseBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".diffuseDoubleSided" no;
	setAttr ".diffuseBackUseDiffuseColor" yes;
	setAttr ".diffuseBackColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".diffuseTransmitGain" 0;
	setAttr ".diffuseTransmitColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".specularFresnelMode" 0;
	setAttr ".specularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".specularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".specularFresnelShape" 5;
	setAttr ".specularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".specularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".specularRoughness" 0.20000000298023224;
	setAttr ".specularModelType" 0;
	setAttr ".specularAnisotropy" 0;
	setAttr ".specularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".specularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".specularDoubleSided" no;
	setAttr ".roughSpecularFresnelMode" 0;
	setAttr ".roughSpecularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularFresnelShape" 5;
	setAttr ".roughSpecularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".roughSpecularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularRoughness" 0.60000002384185791;
	setAttr ".roughSpecularModelType" 0;
	setAttr ".roughSpecularAnisotropy" 0;
	setAttr ".roughSpecularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularDoubleSided" no;
	setAttr ".clearcoatFresnelMode" 0;
	setAttr ".clearcoatFaceColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatFresnelShape" 5;
	setAttr ".clearcoatIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".clearcoatExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".clearcoatThickness" 0;
	setAttr ".clearcoatAbsorptionTint" -type "float3" 0 0 0 ;
	setAttr ".clearcoatRoughness" 0;
	setAttr ".clearcoatModelType" 0;
	setAttr ".clearcoatAnisotropy" 0;
	setAttr ".clearcoatAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".clearcoatBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".clearcoatDoubleSided" no;
	setAttr ".specularEnergyCompensation" 0;
	setAttr ".clearcoatEnergyCompensation" 0;
	setAttr ".iridescenceFaceGain" 0;
	setAttr ".iridescenceEdgeGain" 0;
	setAttr ".iridescenceFresnelShape" 5;
	setAttr ".iridescenceMode" 0;
	setAttr ".iridescencePrimaryColor" -type "float3" 1 0 0 ;
	setAttr ".iridescenceSecondaryColor" -type "float3" 0 0 1 ;
	setAttr ".iridescenceRoughness" 0.20000000298023224;
	setAttr ".iridescenceAnisotropy" 0;
	setAttr ".iridescenceAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".iridescenceCurve" 1;
	setAttr ".iridescenceScale" 1;
	setAttr ".iridescenceFlip" no;
	setAttr ".iridescenceThickness" 800;
	setAttr ".iridescenceDoubleSided" no;
	setAttr ".fuzzGain" 0;
	setAttr ".fuzzColor" -type "float3" 1 1 1 ;
	setAttr ".fuzzConeAngle" 8;
	setAttr ".fuzzBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".fuzzDoubleSided" no;
	setAttr ".subsurfaceType" 0;
	setAttr ".subsurfaceGain" 0;
	setAttr ".subsurfaceColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".subsurfaceDmfp" 10;
	setAttr ".subsurfaceDmfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".shortSubsurfaceGain" 0;
	setAttr ".shortSubsurfaceColor" -type "float3" 0.89999998 0.89999998 0.89999998 ;
	setAttr ".shortSubsurfaceDmfp" 5;
	setAttr ".longSubsurfaceGain" 0;
	setAttr ".longSubsurfaceColor" -type "float3" 0.80000001 0 0 ;
	setAttr ".longSubsurfaceDmfp" 20;
	setAttr ".subsurfaceDirectionality" 0;
	setAttr ".subsurfaceBleed" 0;
	setAttr ".subsurfaceDiffuseBlend" 0;
	setAttr ".subsurfaceResolveSelfIntersections" no;
	setAttr ".subsurfaceIor" 1.3999999761581421;
	setAttr ".subsurfacePostTint" -type "float3" 1 1 1 ;
	setAttr ".subsurfaceDiffuseSwitch" 1;
	setAttr ".subsurfaceDoubleSided" no;
	setAttr ".subsurfaceTransmitGain" 0;
	setAttr ".considerBackside" yes;
	setAttr ".continuationRayMode" 0;
	setAttr ".maxContinuationHits" 2;
	setAttr ".followTopology" 0;
	setAttr ".subsurfaceSubset" -type "string" "";
	setAttr ".singlescatterGain" 0;
	setAttr ".singlescatterColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".singlescatterMfp" 10;
	setAttr ".singlescatterMfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".singlescatterDirectionality" 0;
	setAttr ".singlescatterIor" 1.2999999523162842;
	setAttr ".singlescatterBlur" 0;
	setAttr ".singlescatterDirectGain" 0;
	setAttr ".singlescatterDirectGainTint" -type "float3" 1 1 1 ;
	setAttr ".singlescatterDoubleSided" no;
	setAttr ".singlescatterConsiderBackside" yes;
	setAttr ".singlescatterContinuationRayMode" 0;
	setAttr ".singlescatterMaxContinuationHits" 2;
	setAttr ".singlescatterDirectGainMode" 0;
	setAttr ".singlescatterSubset" -type "string" "";
	setAttr ".irradianceTint" -type "float3" 1 1 1 ;
	setAttr ".irradianceRoughness" 0;
	setAttr ".unitLength" 0.10000000149011612;
	setAttr ".refractionGain" 0;
	setAttr ".reflectionGain" 0;
	setAttr ".refractionColor" -type "float3" 1 1 1 ;
	setAttr ".glassRoughness" 0.10000000149011612;
	setAttr ".glassAnisotropy" 0;
	setAttr ".glassAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".glassIor" 1.5;
	setAttr ".mwWalkable" no;
	setAttr ".mwIor" -1;
	setAttr ".thinGlass" no;
	setAttr ".ignoreFresnel" no;
	setAttr ".ignoreAccumOpacity" no;
	setAttr ".blocksVolumes" no;
	setAttr ".ssAlbedo" -type "float3" 0 0 0 ;
	setAttr ".extinction" -type "float3" 0 0 0 ;
	setAttr ".g" 0;
	setAttr ".multiScatter" no;
	setAttr ".enableOverlappingVolumes" no;
	setAttr ".glowGain" 0;
	setAttr ".glowColor" -type "float3" 1 1 1 ;
	setAttr ".bumpNormal" -type "float3" 0 0 0 ;
	setAttr ".shadowColor" -type "float3" 0 0 0 ;
	setAttr ".shadowMode" 0;
	setAttr ".presence" 1;
	setAttr ".presenceCached" 1;
	setAttr ".mwStartable" no;
	setAttr ".roughnessMollificationClamp" 32;
	setAttr ".utilityPattern[0]"  0;
createNode shadingEngine -n "PxrSurface2SG";
	rename -uid "0CE919E2-6442-3A0D-E15C-6D81D97AC1FA";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
	rename -uid "10E8B86E-174E-0CFD-3160-ECB8B3FA39F1";
createNode PxrSurface -n "PxrSurface3";
	rename -uid "318BCB74-5B4E-32E8-60E1-958F39243814";
	setAttr ".cch" no;
	setAttr ".fzn" no;
	setAttr ".ihi" 2;
	setAttr ".nds" 0;
	setAttr ".inputMaterial" 0;
	setAttr ".diffuseGain" 1;
	setAttr ".diffuseColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".diffuseRoughness" 0;
	setAttr ".diffuseExponent" 1;
	setAttr ".diffuseBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".diffuseDoubleSided" no;
	setAttr ".diffuseBackUseDiffuseColor" yes;
	setAttr ".diffuseBackColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".diffuseTransmitGain" 0;
	setAttr ".diffuseTransmitColor" -type "float3" 0.18000001 0.18000001 0.18000001 ;
	setAttr ".specularFresnelMode" 0;
	setAttr ".specularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".specularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".specularFresnelShape" 5;
	setAttr ".specularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".specularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".specularRoughness" 0.20000000298023224;
	setAttr ".specularModelType" 0;
	setAttr ".specularAnisotropy" 0;
	setAttr ".specularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".specularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".specularDoubleSided" no;
	setAttr ".roughSpecularFresnelMode" 0;
	setAttr ".roughSpecularFaceColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularFresnelShape" 5;
	setAttr ".roughSpecularIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".roughSpecularExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularRoughness" 0.60000002384185791;
	setAttr ".roughSpecularModelType" 0;
	setAttr ".roughSpecularAnisotropy" 0;
	setAttr ".roughSpecularAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".roughSpecularDoubleSided" no;
	setAttr ".clearcoatFresnelMode" 0;
	setAttr ".clearcoatFaceColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatEdgeColor" -type "float3" 0 0 0 ;
	setAttr ".clearcoatFresnelShape" 5;
	setAttr ".clearcoatIor" -type "float3" 1.5 1.5 1.5 ;
	setAttr ".clearcoatExtinctionCoeff" -type "float3" 0 0 0 ;
	setAttr ".clearcoatThickness" 0;
	setAttr ".clearcoatAbsorptionTint" -type "float3" 0 0 0 ;
	setAttr ".clearcoatRoughness" 0;
	setAttr ".clearcoatModelType" 0;
	setAttr ".clearcoatAnisotropy" 0;
	setAttr ".clearcoatAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".clearcoatBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".clearcoatDoubleSided" no;
	setAttr ".specularEnergyCompensation" 0;
	setAttr ".clearcoatEnergyCompensation" 0;
	setAttr ".iridescenceFaceGain" 0;
	setAttr ".iridescenceEdgeGain" 0;
	setAttr ".iridescenceFresnelShape" 5;
	setAttr ".iridescenceMode" 0;
	setAttr ".iridescencePrimaryColor" -type "float3" 1 0 0 ;
	setAttr ".iridescenceSecondaryColor" -type "float3" 0 0 1 ;
	setAttr ".iridescenceRoughness" 0.20000000298023224;
	setAttr ".iridescenceAnisotropy" 0;
	setAttr ".iridescenceAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".iridescenceCurve" 1;
	setAttr ".iridescenceScale" 1;
	setAttr ".iridescenceFlip" no;
	setAttr ".iridescenceThickness" 800;
	setAttr ".iridescenceDoubleSided" no;
	setAttr ".fuzzGain" 0;
	setAttr ".fuzzColor" -type "float3" 1 1 1 ;
	setAttr ".fuzzConeAngle" 8;
	setAttr ".fuzzBumpNormal" -type "float3" 0 0 0 ;
	setAttr ".fuzzDoubleSided" no;
	setAttr ".subsurfaceType" 0;
	setAttr ".subsurfaceGain" 0;
	setAttr ".subsurfaceColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".subsurfaceDmfp" 10;
	setAttr ".subsurfaceDmfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".shortSubsurfaceGain" 0;
	setAttr ".shortSubsurfaceColor" -type "float3" 0.89999998 0.89999998 0.89999998 ;
	setAttr ".shortSubsurfaceDmfp" 5;
	setAttr ".longSubsurfaceGain" 0;
	setAttr ".longSubsurfaceColor" -type "float3" 0.80000001 0 0 ;
	setAttr ".longSubsurfaceDmfp" 20;
	setAttr ".subsurfaceDirectionality" 0;
	setAttr ".subsurfaceBleed" 0;
	setAttr ".subsurfaceDiffuseBlend" 0;
	setAttr ".subsurfaceResolveSelfIntersections" no;
	setAttr ".subsurfaceIor" 1.3999999761581421;
	setAttr ".subsurfacePostTint" -type "float3" 1 1 1 ;
	setAttr ".subsurfaceDiffuseSwitch" 1;
	setAttr ".subsurfaceDoubleSided" no;
	setAttr ".subsurfaceTransmitGain" 0;
	setAttr ".considerBackside" yes;
	setAttr ".continuationRayMode" 0;
	setAttr ".maxContinuationHits" 2;
	setAttr ".followTopology" 0;
	setAttr ".subsurfaceSubset" -type "string" "";
	setAttr ".singlescatterGain" 0;
	setAttr ".singlescatterColor" -type "float3" 0.82999998 0.79100001 0.75300002 ;
	setAttr ".singlescatterMfp" 10;
	setAttr ".singlescatterMfpColor" -type "float3" 0.85100001 0.55699998 0.39500001 ;
	setAttr ".singlescatterDirectionality" 0;
	setAttr ".singlescatterIor" 1.2999999523162842;
	setAttr ".singlescatterBlur" 0;
	setAttr ".singlescatterDirectGain" 0;
	setAttr ".singlescatterDirectGainTint" -type "float3" 1 1 1 ;
	setAttr ".singlescatterDoubleSided" no;
	setAttr ".singlescatterConsiderBackside" yes;
	setAttr ".singlescatterContinuationRayMode" 0;
	setAttr ".singlescatterMaxContinuationHits" 2;
	setAttr ".singlescatterDirectGainMode" 0;
	setAttr ".singlescatterSubset" -type "string" "";
	setAttr ".irradianceTint" -type "float3" 1 1 1 ;
	setAttr ".irradianceRoughness" 0;
	setAttr ".unitLength" 0.10000000149011612;
	setAttr ".refractionGain" 0;
	setAttr ".reflectionGain" 0;
	setAttr ".refractionColor" -type "float3" 1 1 1 ;
	setAttr ".glassRoughness" 0.10000000149011612;
	setAttr ".glassAnisotropy" 0;
	setAttr ".glassAnisotropyDirection" -type "float3" 0 0 0 ;
	setAttr ".glassIor" 1.5;
	setAttr ".mwWalkable" no;
	setAttr ".mwIor" -1;
	setAttr ".thinGlass" no;
	setAttr ".ignoreFresnel" no;
	setAttr ".ignoreAccumOpacity" no;
	setAttr ".blocksVolumes" no;
	setAttr ".ssAlbedo" -type "float3" 0 0 0 ;
	setAttr ".extinction" -type "float3" 0 0 0 ;
	setAttr ".g" 0;
	setAttr ".multiScatter" no;
	setAttr ".enableOverlappingVolumes" no;
	setAttr ".glowGain" 0;
	setAttr ".glowColor" -type "float3" 1 1 1 ;
	setAttr ".bumpNormal" -type "float3" 0 0 0 ;
	setAttr ".shadowColor" -type "float3" 0 0 0 ;
	setAttr ".shadowMode" 0;
	setAttr ".presence" 1;
	setAttr ".presenceCached" 1;
	setAttr ".mwStartable" no;
	setAttr ".roughnessMollificationClamp" 32;
	setAttr ".utilityPattern[0]"  0;
createNode polyTorus -n "polyTorus3";
	rename -uid "51837BA3-D649-8C25-B2FC-F19ECFA6BB85";
	setAttr ".r" 0.35;
	setAttr ".sr" 0.05;
	setAttr ".tw" 45;
	setAttr ".sa" 60;
	setAttr ".sh" 4;
createNode objectSet -n "set3";
	rename -uid "F494F39E-8C40-B139-D413-09B396660B91";
	setAttr ".ihi" 0;
	setAttr -s 8 ".dsm";
	setAttr -s 8 ".gn";
createNode groupId -n "groupId37";
	rename -uid "B9060686-EE43-C83B-7B40-FEAC9797315D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "8BB12470-7D48-7C1A-61B5-A79ECE476C21";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[14:43]" "e[74:103]" "e[134:163]" "e[194:223]" "e[254:284]" "e[314:344]" "e[374:404]" "e[434:464]";
createNode deleteComponent -n "deleteComponent3";
	rename -uid "8DAD1742-B243-6866-07D1-EB88B49419A3";
	setAttr ".dc" -type "componentList" 4 "f[14:43]" "f[74:103]" "f[134:163]" "f[194:223]";
createNode objectSet -n "set4";
	rename -uid "136B2405-1444-8176-CEEC-988B4E6BB670";
	setAttr ".ihi" 0;
	setAttr -s 8 ".dsm";
	setAttr -s 8 ".gn";
createNode groupId -n "groupId38";
	rename -uid "3794C6C8-1540-E53B-A8C4-62B5FDABA63F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "D7C0D796-0F48-D72D-A6EF-949A44FE0E65";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 9 "e[0:13]" "e[29:43]" "e[59:73]" "e[89:103]" "e[119:134]" "e[150:165]" "e[181:196]" "e[212:227]" "e[243]";
createNode deleteComponent -n "deleteComponent4";
	rename -uid "88E44117-1249-0EBB-1088-1CA24D5BFE38";
	setAttr ".dc" -type "componentList" 5 "f[0:13]" "f[29:43]" "f[59:73]" "f[89:103]" "f[119]";
createNode polyCloseBorder -n "polyCloseBorder3";
	rename -uid "E5116CEE-6B44-E5EF-95A4-F4AB1C3E00B2";
	setAttr ".ics" -type "componentList" 1 "e[*]";
createNode polyTweak -n "polyTweak8";
	rename -uid "0A57E989-A746-81F9-68B7-7990A54F2C18";
	setAttr ".uopa" yes;
	setAttr -s 28 ".tk";
	setAttr ".tk[17]" -type "float3" 0 0 0.0021110449 ;
	setAttr ".tk[18]" -type "float3" 0 0 0.0084209712 ;
	setAttr ".tk[19]" -type "float3" 0 0 0.018860666 ;
	setAttr ".tk[20]" -type "float3" 0 0 0.033315774 ;
	setAttr ".tk[21]" -type "float3" 0 0 0.051627874 ;
	setAttr ".tk[22]" -type "float3" 0.047585391 2.0816682e-17 0.073596388 ;
	setAttr ".tk[23]" -type "float3" 0.12750223 2.7755576e-17 0.098980576 ;
	setAttr ".tk[24]" -type "float3" 0.098980449 2.0816682e-17 0.043269221 ;
	setAttr ".tk[25]" -type "float3" 0.073596291 1.3877788e-17 0.015894815 ;
	setAttr ".tk[26]" -type "float3" 0.051627807 1.3877788e-17 0 ;
	setAttr ".tk[27]" -type "float3" 0.033315711 6.9388939e-18 0 ;
	setAttr ".tk[28]" -type "float3" 0.018860603 6.9388939e-18 0 ;
	setAttr ".tk[29]" -type "float3" 0.0084209088 0 0 ;
	setAttr ".tk[30]" -type "float3" 0.0021110117 0 0.0017660903 ;
	setAttr ".tk[33]" -type "float3" 0 0 0.0021110449 ;
	setAttr ".tk[34]" -type "float3" 0 0 0.0084209712 ;
	setAttr ".tk[35]" -type "float3" 0 0 0.018860666 ;
	setAttr ".tk[36]" -type "float3" 0 0 0.033315774 ;
	setAttr ".tk[37]" -type "float3" 0 0 0.051627874 ;
	setAttr ".tk[38]" -type "float3" 0.047585391 2.0816682e-17 0.073596388 ;
	setAttr ".tk[39]" -type "float3" 0.12750223 2.7755576e-17 0.098980576 ;
	setAttr ".tk[40]" -type "float3" 0.098980449 2.0816682e-17 0.043269221 ;
	setAttr ".tk[41]" -type "float3" 0.073596291 1.3877788e-17 0.015894815 ;
	setAttr ".tk[42]" -type "float3" 0.051627807 1.3877788e-17 0 ;
	setAttr ".tk[43]" -type "float3" 0.033315711 6.9388939e-18 0 ;
	setAttr ".tk[44]" -type "float3" 0.018860603 6.9388939e-18 0 ;
	setAttr ".tk[45]" -type "float3" 0.0084209088 0 0 ;
	setAttr ".tk[46]" -type "float3" 0.0021110117 0 0.0017660903 ;
createNode objectSet -n "set5";
	rename -uid "F577FA09-D441-4BA0-B9D3-9FA172A09AE3";
	setAttr ".ihi" 0;
	setAttr -s 8 ".dsm";
	setAttr -s 8 ".gn";
createNode groupId -n "groupId39";
	rename -uid "B28D303E-3A4F-7AB7-6E3C-CEA0D3EEDE29";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "457ABA93-6C4D-E9D3-A193-9CA6326EFC6D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
createNode deleteComponent -n "deleteComponent5";
	rename -uid "43EB14D8-A64E-B0AD-6DB1-0383ED3B2BAA";
	setAttr ".dc" -type "componentList" 1 "f[61]";
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "30FB7285-2044-1759-2D7C-E0A123C4BB6C";
	setAttr ".ics" -type "componentList" 1 "f[60]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 2.7957363266636235 1.2059839130104333 0.92726954502800107 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.7957363 1.2059839 1.3345377 ;
	setAttr ".rs" 801951903;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.7603809859742028 1.2059839160281047 1.2419142095539868 ;
	setAttr ".cbx" -type "double3" 2.8310916710783345 1.2059839234038048 1.4271613625977337 ;
createNode polyTweak -n "polyTweak9";
	rename -uid "092DCC75-7E4E-FD92-4786-D99BA53A4755";
	setAttr ".uopa" yes;
	setAttr -s 21 ".tk";
	setAttr ".tk[0]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[16]" -type "float3" -2.9802322e-08 0 0.11453643 ;
	setAttr ".tk[17]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[18]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[19]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[20]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[21]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[22]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[23]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[32]" -type "float3" -2.9802322e-08 0 0.11453643 ;
	setAttr ".tk[33]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[34]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[35]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[36]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[37]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[38]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[39]" -type "float3" 0 0 0.11453643 ;
	setAttr ".tk[48]" -type "float3" -2.9802322e-08 0 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "93B692F0-C546-845F-232A-75A8F0913F05";
	setAttr ".ics" -type "componentList" 1 "f[60]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 2.7957363266636235 1.2059839130104333 0.92726954502800107 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.7957363 0.6936186 1.3345377 ;
	setAttr ".rs" 2011245933;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.7603809971500737 0.69361857182390985 1.2419141797516644 ;
	setAttr ".cbx" -type "double3" 2.8310916561771733 0.69361857182390985 1.4271613625977337 ;
createNode polyTweak -n "polyTweak10";
	rename -uid "E7616B91-3E46-2987-02FC-DB9CBD9C6FCD";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[64:67]" -type "float3"  -0.51236534 -8.3266727e-17
		 0 -0.51236534 -8.3266727e-17 0 -0.51236534 -8.3266727e-17 0 -0.51236534 -8.3266727e-17
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "9A836E55-C348-A342-D728-24B508757164";
	setAttr ".ics" -type "componentList" 1 "f[66]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 2.7957363266636235 1.5421437383354455 0.92726954502800107 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.7957363 0.51488924 1.2419143 ;
	setAttr ".rs" 1611558153;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.7603809971500732 3.5828426447892525e-08 1.2419141797516644 ;
	setAttr ".cbx" -type "double3" 2.8310916561771733 1.0297783971489221 1.241914298960954 ;
createNode polyTweak -n "polyTweak11";
	rename -uid "8A7303F4-4A46-22FE-4402-E0B33E69D008";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[68]" -type "float3" -1.0297784 -6.7307271e-16 0 ;
	setAttr ".tk[69]" -type "float3" -1.0297784 -6.7307271e-16 0 ;
	setAttr ".tk[70]" -type "float3" -1.0297784 -6.7307271e-16 0 ;
	setAttr ".tk[71]" -type "float3" -1.0297784 -6.7307271e-16 0 ;
createNode objectSet -n "set6";
	rename -uid "B55F7438-704B-B922-FC13-1888B279A6D1";
	setAttr ".ihi" 0;
	setAttr -s 8 ".dsm";
	setAttr -s 8 ".gn";
createNode groupId -n "groupId40";
	rename -uid "9FE20736-8A42-3BEE-5376-1A87798E56DC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts11";
	rename -uid "3E36C4F7-A040-7598-206F-A4B555B3768D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "e[142]" "e[144]" "e[146:147]";
createNode polyTweak -n "polyTweak12";
	rename -uid "22405D84-3A40-B3DE-847F-51BD4EF77548";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[72]" -type "float3" 0 0 -0.31464472 ;
	setAttr ".tk[73]" -type "float3" 0 0 -0.31464472 ;
	setAttr ".tk[74]" -type "float3" 0 0 -0.31464472 ;
	setAttr ".tk[75]" -type "float3" 0 0 -0.31464472 ;
createNode deleteComponent -n "deleteComponent6";
	rename -uid "74E632DE-0642-808B-3EBB-67ABD2A5BB32";
	setAttr ".dc" -type "componentList" 1 "f[66]";
createNode polyMirror -n "polyMirror1";
	rename -uid "5A352607-8442-C11C-B361-5997022D8954";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 2.795736326663623 1.3578873562295586 1.0942815106624331 1;
	setAttr ".p" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
	setAttr ".a" 2;
	setAttr ".ma" 1;
	setAttr ".mtt" 1;
	setAttr ".cm" yes;
	setAttr ".fnf" 72;
	setAttr ".lnf" 143;
	setAttr ".pc" -type "double3" -0.39413785934448242 0 -7.5649615682849003e-08 ;
createNode polyTweak -n "polyTweak13";
	rename -uid "1936C8A8-E946-4609-2AB0-39AFE2E4F7B2";
	setAttr ".uopa" yes;
	setAttr -s 47 ".tk";
	setAttr ".tk[16]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[17]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[18]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[19]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[20]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[21]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[22]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[23]" -type "float3" 0.18425632 3.469447e-17 -0.12528354 ;
	setAttr ".tk[24]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[25]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[26]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[27]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[28]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[29]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[30]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[31]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[32]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[33]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[34]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[35]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[36]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[37]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[38]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[39]" -type "float3" 0.18425632 3.469447e-17 -0.12528354 ;
	setAttr ".tk[40]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[41]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[42]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[43]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[44]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[45]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[46]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[47]" -type "float3" 0.18425632 3.469447e-17 0 ;
	setAttr ".tk[65]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[67]" -type "float3" 0 0 -0.12528354 ;
	setAttr ".tk[68]" -type "float3" 0.18425636 4.7878368e-16 0 ;
	setAttr ".tk[69]" -type "float3" 0.18425636 4.7878368e-16 -0.12528354 ;
	setAttr ".tk[70]" -type "float3" 0.18425636 4.7878368e-16 0 ;
	setAttr ".tk[71]" -type "float3" 0.18425636 4.7878368e-16 -0.12528354 ;
	setAttr ".tk[74]" -type "float3" 0.18425636 4.7878368e-16 0 ;
	setAttr ".tk[75]" -type "float3" 0.18425636 4.7878368e-16 0 ;
createNode groupId -n "groupId41";
	rename -uid "3FB9C73F-164E-406F-B012-73B10E4E199A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId42";
	rename -uid "3A800111-2E45-3453-4FCE-85BE95489F46";
	setAttr ".ihi" 0;
createNode groupId -n "groupId43";
	rename -uid "779998B5-F548-8E40-EA94-53BAA4BFE311";
	setAttr ".ihi" 0;
createNode groupId -n "groupId44";
	rename -uid "6A283094-6F44-14BF-CD75-6BABD1386E81";
	setAttr ".ihi" 0;
createNode groupId -n "groupId45";
	rename -uid "5CD7CE87-A149-1D5D-DB1A-CEABA270111B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId46";
	rename -uid "552E8282-7647-5544-8889-79A09BD9901C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId47";
	rename -uid "883CED9F-8C44-D39A-A4F3-BC8C48529EDA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId48";
	rename -uid "318DEDDF-A145-D7B9-033A-0D924E5C91B6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId49";
	rename -uid "4D6A390D-2B40-C447-0F83-DFBAF0C3FAC5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId50";
	rename -uid "54FE206D-8E46-358D-A80D-99B44721C5B1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId51";
	rename -uid "30F1B991-0A40-299C-3CF2-47B1D50EDBFE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId52";
	rename -uid "620CE80F-AC46-087C-99F7-E585AC21B318";
	setAttr ".ihi" 0;
createNode polyUnite -n "polyUnite1";
	rename -uid "DA648144-6C48-D55D-E602-90965F80F228";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId57";
	rename -uid "0D43BE3A-2E43-60B8-5E7A-E19FD2202C88";
	setAttr ".ihi" 0;
createNode groupId -n "groupId58";
	rename -uid "F7BF0501-174D-8955-C531-869FB3FF4D43";
	setAttr ".ihi" 0;
createNode groupId -n "groupId59";
	rename -uid "8884329C-6A4C-2D36-A27A-71A2B5E13255";
	setAttr ".ihi" 0;
createNode groupId -n "groupId60";
	rename -uid "348BFA20-E944-16E0-3A4A-5AA4CFE44737";
	setAttr ".ihi" 0;
createNode groupId -n "groupId61";
	rename -uid "95212C67-144B-01CD-F3D1-CA97A8013413";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "C3331297-644A-3616-0E2C-33BCDFF29992";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 16 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]" "e[348]" "e[364]" "e[380]" "e[396]" "e[437]" "e[481]" "e[511]" "e[541]";
createNode groupId -n "groupId62";
	rename -uid "0177AC27-1C40-2FB0-DF06-A1A1DA373F8A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts13";
	rename -uid "D4CC4910-AB4C-53A5-4B83-FD9008B4E8AF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[75]" "e[91]" "e[107]" "e[123]" "e[363]" "e[379]" "e[395]" "e[411]";
createNode groupId -n "groupId63";
	rename -uid "B8091BEA-FE47-5E7F-8956-2D9D5741CCB1";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts14";
	rename -uid "5307EB0A-1941-B531-6FB4-DFBE1E8C372D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[75]" "e[91]" "e[107]" "e[123]" "e[363]" "e[379]" "e[395]" "e[411]";
createNode groupId -n "groupId64";
	rename -uid "77AF1E7F-0F41-8017-8F84-06A01EAECF06";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts15";
	rename -uid "9A0A57A7-A14A-C431-AAE6-93A311202958";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "e[142]" "e[144]" "e[146:147]" "e[430]" "e[432]" "e[434:435]";
createNode groupId -n "groupId65";
	rename -uid "E3A9F1C5-E74B-5D96-FC6D-3BBAED337F66";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts16";
	rename -uid "C3B7E84A-7E45-C4C5-1C05-479F47B7713E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:287]";
createNode deleteComponent -n "deleteComponent7";
	rename -uid "418E9B81-FB4A-FFDF-09C1-58B5C2AF5007";
	setAttr ".dc" -type "componentList" 6 "f[15:21]" "f[64]" "f[67]" "f[231:237]" "f[280]" "f[283]";
createNode polyBridgeEdge -n "polyBridgeEdge1";
	rename -uid "748EDE8C-1341-E5D3-C92D-8CA1D3AC214D";
	setAttr ".ics" -type "componentList" 18 "e[15:21]" "e[30:36]" "e[76]" "e[118]" "e[122]" "e[125]" "e[129]" "e[430]" "e[433]" "e[436]" "e[439]" "e[442]" "e[445]" "e[448]" "e[473:480]" "e[547]" "e[550]" "e[553:554]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 71;
	setAttr ".sv2" 285;
	setAttr ".rev" yes;
	setAttr ".d" 1;
createNode polyBridgeEdge -n "polyBridgeEdge2";
	rename -uid "D2527D2C-624E-7804-8CE9-9CB859A8E166";
	setAttr ".ics" -type "componentList" 2 "e[131]" "e[544]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 69;
	setAttr ".sv2" 287;
	setAttr ".d" 1;
createNode polyUnite -n "polyUnite2";
	rename -uid "407FE17A-EA4E-39D3-39D1-F8A4B17EC888";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId66";
	rename -uid "2A1E3FD9-7548-9EE1-0A83-93AC638457D3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId67";
	rename -uid "BD137EEA-3442-9157-5730-BB83C7ADB8F9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId68";
	rename -uid "99B093EA-1046-70AE-ED19-3FAF68479629";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts17";
	rename -uid "66DD233E-B84E-4483-1F0C-5C9B0B4FE488";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 22 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]" "e[348]" "e[373]" "e[389]" "e[429]" "e[473]" "e[503]" "e[533]" "e[628]" "e[644]" "e[660]" "e[676]" "e[717]" "e[784]" "e[814]";
createNode groupId -n "groupId69";
	rename -uid "A46B80DC-5343-DEB5-2F2F-59BE73FF006C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts18";
	rename -uid "32A653AF-A849-AA66-FA74-979D4FD47677";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[75]" "e[91]" "e[107]" "e[123]" "e[363]" "e[372]" "e[388]" "e[404]" "e[643]" "e[659]" "e[675]" "e[691]";
createNode groupId -n "groupId70";
	rename -uid "B255EF1D-2C4C-28DF-F8E2-69B32F50D9C4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts19";
	rename -uid "057A549A-564D-9F3C-265E-73950D39BB48";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[75]" "e[91]" "e[107]" "e[123]" "e[363]" "e[372]" "e[388]" "e[404]" "e[643]" "e[659]" "e[675]" "e[691]";
createNode groupId -n "groupId71";
	rename -uid "C98A3FD9-F142-3F9F-37CB-2BB94F132BC5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts20";
	rename -uid "7E2F9E96-FF4A-EDEC-87F7-E2A3372A3B8A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 9 "e[142]" "e[144]" "e[146:147]" "e[422]" "e[424]" "e[426:427]" "e[710]" "e[712]" "e[714:715]";
createNode groupId -n "groupId72";
	rename -uid "78F3D9CA-A34E-BE7F-2225-FDAFD48C4C8B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts21";
	rename -uid "A6B0B33F-4349-B2CC-DADC-63860D36322F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:433]";
createNode deleteComponent -n "deleteComponent8";
	rename -uid "2C8A373C-9C4B-9DB7-8B79-17A4B54E58C7";
	setAttr ".dc" -type "componentList" 6 "f[87:93]" "f[136]" "f[139]" "f[294:300]" "f[343]" "f[346]";
createNode polyBridgeEdge -n "polyBridgeEdge3";
	rename -uid "D5D0521F-624B-7C89-609F-17AE72B4D08F";
	setAttr ".ics" -type "componentList" 18 "e[150]" "e[153]" "e[156]" "e[159]" "e[162]" "e[165]" "e[168]" "e[193:200]" "e[267]" "e[270]" "e[273:274]" "e[575:581]" "e[590:596]" "e[636]" "e[678]" "e[682]" "e[685]" "e[689]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 141;
	setAttr ".sv2" 359;
	setAttr ".rev" yes;
	setAttr ".d" 1;
createNode polyBridgeEdge -n "polyBridgeEdge4";
	rename -uid "7BA04A57-914E-C414-46CF-7183F2DF919F";
	setAttr ".ics" -type "componentList" 2 "e[264]" "e[691]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 143;
	setAttr ".sv2" 357;
	setAttr ".d" 1;
createNode polyUnite -n "polyUnite3";
	rename -uid "60085F76-1C43-92AC-FFC2-1AA35CBFC161";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId73";
	rename -uid "57656EEC-7743-ECFB-1BE5-94BBEC4EAAA2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts22";
	rename -uid "A0054933-1644-382B-BACC-5EA78CA35329";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:143]";
createNode groupId -n "groupId74";
	rename -uid "D93460D0-354F-61F0-8673-539DD82225BA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId75";
	rename -uid "0382DE87-A348-B74E-9FAB-DB8E12A1865C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts23";
	rename -uid "C052238D-9242-91E8-D813-20827F7F497A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 28 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]" "e[348]" "e[364]" "e[380]" "e[396]" "e[437]" "e[504]" "e[534]" "e[628]" "e[653]" "e[669]" "e[709]" "e[753]" "e[783]" "e[813]" "e[908]" "e[933]" "e[949]" "e[989]" "e[1056]" "e[1086]";
createNode groupId -n "groupId76";
	rename -uid "BD7304AE-454B-6C88-A618-4781F486A753";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts24";
	rename -uid "E5C3BD19-DE4E-F631-D02E-D4B908CFE4FE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 16 "e[75]" "e[91]" "e[107]" "e[123]" "e[363]" "e[379]" "e[395]" "e[411]" "e[643]" "e[652]" "e[668]" "e[684]" "e[923]" "e[932]" "e[948]" "e[964]";
createNode groupId -n "groupId77";
	rename -uid "19079929-2D44-55D5-A473-85944386FBC4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts25";
	rename -uid "BFD776D6-4F48-2ED7-8E96-879637EB71CF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 16 "e[75]" "e[91]" "e[107]" "e[123]" "e[363]" "e[379]" "e[395]" "e[411]" "e[643]" "e[652]" "e[668]" "e[684]" "e[923]" "e[932]" "e[948]" "e[964]";
createNode groupId -n "groupId78";
	rename -uid "FE37F942-EB40-7F73-508F-CF8603307762";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts26";
	rename -uid "08962B58-6F4D-8EE3-A230-5A80C64C352C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[142]" "e[144]" "e[146:147]" "e[430]" "e[432]" "e[434:435]" "e[702]" "e[704]" "e[706:707]" "e[982]" "e[984]" "e[986:987]";
createNode groupId -n "groupId79";
	rename -uid "4E0A6A17-3C40-69D7-9F5A-46B86E9A2053";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts27";
	rename -uid "615592BB-4B4D-645F-5137-399D207250C7";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:579]";
createNode deleteComponent -n "deleteComponent9";
	rename -uid "7A8D1E90-164A-42F3-015D-64B0997FEB19";
	setAttr ".dc" -type "componentList" 6 "f[87:93]" "f[136]" "f[139]" "f[159:165]" "f[208]" "f[211]";
createNode polyBridgeEdge -n "polyBridgeEdge5";
	rename -uid "AF46EB85-5D43-D250-0CEF-B3999F781C3B";
	setAttr ".ics" -type "componentList" 18 "e[150]" "e[153]" "e[156]" "e[159]" "e[162]" "e[165]" "e[168]" "e[193:200]" "e[267]" "e[270]" "e[273:274]" "e[295:301]" "e[310:316]" "e[356]" "e[398]" "e[402]" "e[405]" "e[409]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 141;
	setAttr ".sv2" 215;
	setAttr ".rev" yes;
	setAttr ".d" 1;
createNode polyBridgeEdge -n "polyBridgeEdge6";
	rename -uid "723421CE-CB48-4CDE-EFE9-E186F8491C7C";
	setAttr ".ics" -type "componentList" 2 "e[264]" "e[411]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 143;
	setAttr ".sv2" 213;
	setAttr ".d" 1;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "80E079AF-FD48-013B-6653-B9B0B5AF67CF";
	setAttr ".ics" -type "componentList" 3 "f[15:21]" "f[64]" "f[67]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -2.2204460492503131e-16 0 -1 0
		 -1 2.2204460492503131e-16 2.2204460492503131e-16 0 2.2341316122423018 1.3578873562295586 3.5858838520789247 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.8595233 0.96374947 3.5858839 ;
	setAttr ".rs" 388464941;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.8595233339677653 -3.1046137216605985e-08 3.5505285225653749 ;
	setAttr ".cbx" -type "double3" 1.8595233339677657 1.9274990248162895 3.6212391815924745 ;
createNode groupId -n "groupId80";
	rename -uid "D5267501-3141-DFC2-3E97-869C0C3BE6BE";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts28";
	rename -uid "114477FD-BB4A-5CC3-5C53-2A967970425B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[60]" "e[76]" "e[92]" "e[108]" "e[149]" "e[193]" "e[223]" "e[253]";
createNode groupId -n "groupId81";
	rename -uid "0B91BE33-3E46-C119-C4CA-C98140AD70EF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts29";
	rename -uid "9EEB835B-0B4A-01C8-A7E4-B5B2AD2EF766";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
createNode groupId -n "groupId82";
	rename -uid "38FB8D98-DB43-DD95-E624-0F9B7FA00420";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts30";
	rename -uid "AF2A445A-AA4E-EC6B-63A7-C9B2670CD74B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "e[75]" "e[91]" "e[107]" "e[123]";
createNode groupId -n "groupId83";
	rename -uid "50A1CA2E-7040-AF0C-7A1C-E292F6F236AD";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts31";
	rename -uid "9739C0F8-A24F-517F-7C1F-D9A716C5BCD1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "e[142]" "e[144]" "e[146:147]";
createNode polyChipOff -n "polyChipOff2";
	rename -uid "9C4409AF-7940-754E-2600-7D8116CBBFCB";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 2.0502456776410818 0 0 0 0 2.0502456776410818 0 0 0 0 7.1776411407192819 0
		 0 1.0251228388205409 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.91003388 1.0665517 0 ;
	setAttr ".rs" 1965329630;
	setAttr ".kft" no;
	setAttr ".dup" no;
createNode polyTweak -n "polyTweak14";
	rename -uid "553FC49A-094C-2802-92D6-379DEF4E477B";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[1]" -type "float3" 0.88773155 0 0 ;
	setAttr ".tk[2]" -type "float3" 0 0.040413585 0 ;
	setAttr ".tk[3]" -type "float3" 0.88773155 0.040413585 0 ;
	setAttr ".tk[4]" -type "float3" 0 0.040413585 0 ;
	setAttr ".tk[5]" -type "float3" 0.88773155 0.040413585 0 ;
	setAttr ".tk[7]" -type "float3" 0.88773155 0 0 ;
createNode polySeparate -n "polySeparate2";
	rename -uid "B8FA55D5-164C-6A9D-5A4B-2F9C61E0D108";
	setAttr ".ic" 2;
	setAttr -s 2 ".out";
createNode groupId -n "groupId84";
	rename -uid "DAFA3457-F845-26DD-C5E3-59B429330A0E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts32";
	rename -uid "9D394F73-4D45-B446-527B-21A1F1CD6C8F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "f[0]" "f[1]" "f[2]" "f[3]";
createNode groupId -n "groupId85";
	rename -uid "0A6AE5D3-8544-F684-E66E-7CB84BB6F269";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts33";
	rename -uid "35BB097A-B145-95CF-5BE9-B78EBDF0C674";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0]";
createNode groupId -n "groupId86";
	rename -uid "E2186B98-2F4A-A1FA-64E3-B88429F311CF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts34";
	rename -uid "F6BBBE9E-8946-CE15-5380-7FA855A9CABF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0]";
createNode objectSet -n "polySurfaceShape5HiddenFacesSet";
	rename -uid "DA96B3BB-D04C-8084-A961-30BCED0DCF81";
	setAttr ".ihi" 0;
createNode polyChipOff -n "polyChipOff3";
	rename -uid "5C2310C5-F34A-04EE-CC40-FAB0572F4A00";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 2.0502456776410818 0 0 0 0 2.0502456776410818 0 0 0 0 7.1776411407192819 0
		 0 1.0251228388205409 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.0251229 0 ;
	setAttr ".rs" 994369986;
	setAttr ".kft" no;
	setAttr ".dup" no;
createNode polySeparate -n "polySeparate3";
	rename -uid "59FA228D-E045-886E-E4B3-51B9D74FBA45";
	setAttr ".ic" 2;
	setAttr -s 2 ".out";
createNode groupId -n "groupId87";
	rename -uid "12C40570-844D-2612-A82F-5499B533C125";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts35";
	rename -uid "072E534C-4549-8124-4D55-B8A291E02C49";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "f[0]" "f[1]" "f[2]";
createNode groupId -n "groupId88";
	rename -uid "EA0F5168-6D40-898B-D65E-D8AEC380515A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts36";
	rename -uid "6E64FD83-DE43-1D95-B699-7196B4BA66CE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0]";
createNode polyCube -n "polyCube2";
	rename -uid "3016DD67-6B42-9745-D9E4-598C5C1AF136";
	setAttr ".sw" 3;
	setAttr ".sh" 7;
	setAttr ".cuv" 4;
createNode objectSet -n "set7";
	rename -uid "F40465A3-744B-D9E3-5DC9-10843DB1ECFB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId89";
	rename -uid "C86AA542-7F47-78CB-ED98-9997EBA7294E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts37";
	rename -uid "40F7B882-514A-C618-677B-97A46876A7D8";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 9 "e[0:2]" "e[21:48]" "e[51:52]" "e[55:56]" "e[59:60]" "e[63:64]" "e[67:68]" "e[71:72]" "e[75:123]";
createNode polyTweak -n "polyTweak15";
	rename -uid "EBD1C032-494F-55E0-A4E5-B0A3D0B7E30D";
	setAttr ".uopa" yes;
	setAttr -s 57 ".tk";
	setAttr ".tk[1]" -type "float3" -0.28145987 0 9.5367432e-07 ;
	setAttr ".tk[2]" -type "float3" 0.28145987 0 9.5367432e-07 ;
	setAttr ".tk[4]" -type "float3" 0 -0.090498686 0 ;
	setAttr ".tk[5]" -type "float3" -0.28145987 -0.090498686 9.5367432e-07 ;
	setAttr ".tk[6]" -type "float3" 0.28145987 -0.090498686 9.5367432e-07 ;
	setAttr ".tk[7]" -type "float3" 0 -0.090498686 0 ;
	setAttr ".tk[8]" -type "float3" 0 0.026919618 0 ;
	setAttr ".tk[9]" -type "float3" -0.28145987 0.026919611 9.5367432e-07 ;
	setAttr ".tk[10]" -type "float3" 0.28145987 0.026919611 9.5367432e-07 ;
	setAttr ".tk[11]" -type "float3" 0 0.026919618 0 ;
	setAttr ".tk[12]" -type "float3" 0 -0.063092239 0 ;
	setAttr ".tk[13]" -type "float3" -0.28145987 -0.063092239 9.5367432e-07 ;
	setAttr ".tk[14]" -type "float3" 0.28145987 -0.063092239 9.5367432e-07 ;
	setAttr ".tk[15]" -type "float3" 0 -0.063092239 0 ;
	setAttr ".tk[16]" -type "float3" 0 0.063092239 0 ;
	setAttr ".tk[17]" -type "float3" -0.28145987 0.063092247 9.5367432e-07 ;
	setAttr ".tk[18]" -type "float3" 0.28145987 0.063092247 9.5367432e-07 ;
	setAttr ".tk[19]" -type "float3" 0 0.063092239 0 ;
	setAttr ".tk[20]" -type "float3" 0 -0.026919613 0 ;
	setAttr ".tk[21]" -type "float3" -0.28145987 -0.0269196 9.5367432e-07 ;
	setAttr ".tk[22]" -type "float3" 0.28145987 -0.0269196 9.5367432e-07 ;
	setAttr ".tk[23]" -type "float3" 0 -0.026919613 0 ;
	setAttr ".tk[24]" -type "float3" 0 0.090498686 0 ;
	setAttr ".tk[25]" -type "float3" -0.28145987 0.090498686 9.5367432e-07 ;
	setAttr ".tk[26]" -type "float3" 0.28145987 0.090498686 9.5367432e-07 ;
	setAttr ".tk[27]" -type "float3" 0 0.090498686 0 ;
	setAttr ".tk[29]" -type "float3" -0.28145987 0 9.5367432e-07 ;
	setAttr ".tk[30]" -type "float3" 0.28145987 0 9.5367432e-07 ;
	setAttr ".tk[33]" -type "float3" -0.28145987 0 9.5367432e-07 ;
	setAttr ".tk[34]" -type "float3" 0.28145987 0 9.5367432e-07 ;
	setAttr ".tk[36]" -type "float3" 0 0.090498686 0 ;
	setAttr ".tk[37]" -type "float3" -0.28145987 0.090498686 9.5367432e-07 ;
	setAttr ".tk[38]" -type "float3" 0.28145987 0.090498686 9.5367432e-07 ;
	setAttr ".tk[39]" -type "float3" 0 0.090498686 0 ;
	setAttr ".tk[40]" -type "float3" 0 -0.026919613 0 ;
	setAttr ".tk[41]" -type "float3" -0.28145987 -0.026919613 9.5367432e-07 ;
	setAttr ".tk[42]" -type "float3" 0.28145987 -0.026919613 9.5367432e-07 ;
	setAttr ".tk[43]" -type "float3" 0 -0.026919613 0 ;
	setAttr ".tk[44]" -type "float3" 0 0.063092239 0 ;
	setAttr ".tk[45]" -type "float3" -0.28145987 0.063092239 9.5367432e-07 ;
	setAttr ".tk[46]" -type "float3" 0.28145987 0.063092239 9.5367432e-07 ;
	setAttr ".tk[47]" -type "float3" 0 0.063092239 0 ;
	setAttr ".tk[48]" -type "float3" 0 -0.063092239 0 ;
	setAttr ".tk[49]" -type "float3" -0.28145987 -0.063092239 9.5367432e-07 ;
	setAttr ".tk[50]" -type "float3" 0.28145987 -0.063092239 9.5367432e-07 ;
	setAttr ".tk[51]" -type "float3" 0 -0.063092239 0 ;
	setAttr ".tk[52]" -type "float3" 0 0.026919618 0 ;
	setAttr ".tk[53]" -type "float3" -0.28145987 0.026919618 9.5367432e-07 ;
	setAttr ".tk[54]" -type "float3" 0.28145987 0.026919618 9.5367432e-07 ;
	setAttr ".tk[55]" -type "float3" 0 0.026919618 0 ;
	setAttr ".tk[56]" -type "float3" 0 -0.090498686 0 ;
	setAttr ".tk[57]" -type "float3" -0.28145987 -0.090498686 9.5367432e-07 ;
	setAttr ".tk[58]" -type "float3" 0.28145987 -0.090498686 9.5367432e-07 ;
	setAttr ".tk[59]" -type "float3" 0 -0.090498686 0 ;
	setAttr ".tk[61]" -type "float3" -0.28145987 0 9.5367432e-07 ;
	setAttr ".tk[62]" -type "float3" 0.28145987 0 9.5367432e-07 ;
createNode deleteComponent -n "deleteComponent10";
	rename -uid "4AC18854-1A47-2DDA-E34E-3F981C6B2176";
	setAttr ".dc" -type "componentList" 1 "f[21:61]";
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "175BEF5F-A34B-97B7-8883-7483BA236617";
	setAttr ".ics" -type "componentList" 4 "f[0:3]" "f[5:9]" "f[11:15]" "f[17:20]";
	setAttr ".ix" -type "matrix" 1.7927390006536357 0 0 0 0 1.6881266448762924 0 0 0 0 0.045208477695477048 0
		 1 0.99775252376740542 -3.8912879753036975 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1 0.99775255 -3.8686838 ;
	setAttr ".rs" 417857167;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.10363049967318216 0.15368920132925923 -3.8686837364559592 ;
	setAttr ".cbx" -type "double3" 1.8963695003268177 1.8418158462055516 -3.868683693341795 ;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "5B857152-D443-9F32-BE7D-9581FA15C546";
	setAttr ".ics" -type "componentList" 6 "f[0:3]" "f[5:6]" "f[8:9]" "f[11:12]" "f[14:15]" "f[17:20]";
	setAttr ".ix" -type "matrix" 1.7927390006536357 0 0 0 0 1.6881266448762924 0 0 0 0 0.045208477695477048 0
		 1 0.99775252376740542 -3.8912879753036975 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1 0.99775255 -3.8686838 ;
	setAttr ".rs" 2075353444;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.10363049967318216 0.15368920132925923 -3.868683865798451 ;
	setAttr ".cbx" -type "double3" 1.8963695003268177 1.8418159468257405 -3.868683865798451 ;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "71D90BDA-D64D-622A-595F-D68F46A412AD";
	setAttr ".ics" -type "componentList" 2 "f[7]" "f[13]";
	setAttr ".ix" -type "matrix" 1.7927390006536357 0 0 0 0 1.6881266448762924 0 0 0 0 0.045208477695477048 0
		 1 0.99775252376740542 -3.8912879753036975 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1 0.99775255 -3.8686838 ;
	setAttr ".rs" 1875044674;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.19662610196657548 0.68145482110769739 -3.868683865798451 ;
	setAttr ".cbx" -type "double3" 1.8033739514612104 1.3140502767372078 -3.868683865798451 ;
createNode polyTweak -n "polyTweak16";
	rename -uid "8FF9FD99-0446-535B-01E5-FCB542F04BD9";
	setAttr ".uopa" yes;
	setAttr -s 146 ".tk";
	setAttr ".tk[32]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[33]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[34]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[35]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[36]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[37]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[38]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[39]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[40]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[41]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[42]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[43]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[44]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[45]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[46]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[47]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[48]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[49]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[50]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[51]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[52]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[53]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[54]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[55]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[56]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[57]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[58]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[59]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[60]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[61]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[62]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[63]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[64]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[65]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[66]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[67]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[68]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[69]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[70]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[71]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[72]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[73]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[74]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[75]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[76]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[77]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[78]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[79]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[80]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[81]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[82]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[83]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[84]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[85]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[86]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[87]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[88]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[89]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[90]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[91]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[92]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[93]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[94]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[95]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[96]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[97]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[98]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[99]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[100]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[101]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[102]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[103]" -type "float3" 0 0 4.7683716e-06 ;
	setAttr ".tk[104]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[105]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[106]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[107]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[108]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[109]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[110]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[111]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[112]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[113]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[114]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[115]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[116]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[117]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[118]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[119]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[120]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[121]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[122]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[123]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[124]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[125]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[126]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[127]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[128]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[129]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[130]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[131]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[132]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[133]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[134]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[135]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[136]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[137]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[138]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[139]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[140]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[141]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[142]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[143]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[144]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[145]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[146]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[147]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[148]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[149]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[150]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[151]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[152]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[153]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[154]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[155]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[156]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[157]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[158]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[159]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[160]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[161]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[162]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[163]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[164]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[165]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[166]" -type "float3" 0 0 11.469094 ;
	setAttr ".tk[167]" -type "float3" 0 0 11.469094 ;
createNode polyCube -n "polyCube3";
	rename -uid "1438A646-8C4E-0899-A7A2-179979F91978";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	rename -uid "097D8D56-F14F-24F7-2231-CDA1FF883044";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 4.2871255296034541 0 0 0 0 0.19413119373823251 0 0 0 0 0.18067922107111714 0
		 1.5117128267902782 1.2049841028367552 -5.1960120375416112 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.50140822 1.9084951 0.57622296 ;
	setAttr ".rs" 1039667726;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.50140872566456052 1.8450141312356436 0.51490605092712283 ;
	setAttr ".cbx" -type "double3" -0.50140770353418329 1.9719759867412758 0.637539839102784 ;
createNode polyTweak -n "polyTweak17";
	rename -uid "F418AA7F-E645-7932-7283-9FAD0C7C7BCD";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk";
	setAttr ".tk[168]" -type "float3" 0 -0.0078565385 9.5456944 ;
	setAttr ".tk[169]" -type "float3" 0 -0.0078565385 9.5456944 ;
	setAttr ".tk[170]" -type "float3" 0 -0.0078565385 9.5456944 ;
	setAttr ".tk[171]" -type "float3" 0 -0.0078565385 9.5456944 ;
	setAttr ".tk[172]" -type "float3" -0.0003367741 0.0050554695 9.5456944 ;
	setAttr ".tk[173]" -type "float3" -0.00044833013 -0.0099612791 9.5456944 ;
	setAttr ".tk[174]" -type "float3" 0.0003367741 -0.0099678524 9.5456944 ;
	setAttr ".tk[175]" -type "float3" 0.00044833025 0.0050488887 9.5456944 ;
createNode deleteComponent -n "deleteComponent11";
	rename -uid "4021BDB6-1B40-5731-DD0B-A9B0216D4231";
	setAttr ".dc" -type "componentList" 1 "f[16]";
createNode deleteComponent -n "deleteComponent12";
	rename -uid "A0CC6A15-E44A-677C-41B2-839F72EFA6EA";
	setAttr ".dc" -type "componentList" 1 "f[10]";
createNode deleteComponent -n "deleteComponent13";
	rename -uid "54F3842C-AF4C-BB47-0A13-E3B62CCB5034";
	setAttr ".dc" -type "componentList" 1 "f[4]";
createNode polyCloseBorder -n "polyCloseBorder4";
	rename -uid "D8A1455F-FC46-6917-31A7-7386F9E7236A";
	setAttr ".ics" -type "componentList" 1 "e[*]";
createNode deleteComponent -n "deleteComponent14";
	rename -uid "9D7E6CDB-4D4C-2AD9-699B-DEB8DD482A3F";
	setAttr ".dc" -type "componentList" 1 "f[162]";
createNode deleteComponent -n "deleteComponent15";
	rename -uid "A32222D6-B54F-0DA0-FB4B-D980ECFBC501";
	setAttr ".dc" -type "componentList" 1 "f[164]";
createNode deleteComponent -n "deleteComponent16";
	rename -uid "D65E3772-804A-F61D-B8D9-F6A5CD324AE2";
	setAttr ".dc" -type "componentList" 1 "f[163]";
createNode deleteComponent -n "deleteComponent17";
	rename -uid "51C894A0-8147-C614-B248-ADAE1F3D94B9";
	setAttr ".dc" -type "componentList" 1 "f[162]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".aoon" yes;
	setAttr ".msaa" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 5 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 7 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -s 5 ".r";
select -ne :lightList1;
	setAttr -s 2 ".l";
select -ne :initialShadingGroup;
	setAttr -s 38 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 19 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "renderman";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultLightSet;
	setAttr -s 2 ".dsm";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHideFaceDataSet;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "polyNormal1.out" "polySurfaceShape1.i";
connectAttr "groupId3.id" "polySurfaceShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape1.iog.og[0].gco";
connectAttr "groupParts35.og" "polySurfaceShape6.i";
connectAttr "groupId87.id" "polySurfaceShape6.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape6.iog.og[0].gco";
connectAttr "groupParts36.og" "polySurfaceShape7.i";
connectAttr "groupId88.id" "polySurfaceShape7.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape7.iog.og[0].gco";
connectAttr "polyChipOff3.out" "polySurfaceShape4.i";
connectAttr "groupId84.id" "polySurfaceShape4.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape4.iog.og[0].gco";
connectAttr "groupParts34.og" "polySurfaceShape5.i";
connectAttr "groupId85.id" "polySurfaceShape5.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape5.iog.og[0].gco";
connectAttr "groupId86.id" "polySurfaceShape5.iog.og[1].gid";
connectAttr "polyChipOff2.out" "polySurfaceShape2.i";
connectAttr "groupId4.id" "polySurfaceShape2.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape2.iog.og[0].gco";
connectAttr "groupId1.id" "pCubeShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "groupParts1.og" "pCubeShape1.i";
connectAttr "groupId2.id" "pCubeShape1.ciog.cog[0].cgid";
connectAttr "groupId5.id" "pTorusShape1.iog.og[0].gid";
connectAttr "set1.mwc" "pTorusShape1.iog.og[0].gco";
connectAttr "polyExtrudeFace1.out" "pTorusShape1.i";
connectAttr "groupId6.id" "pTorusShape2.iog.og[0].gid";
connectAttr "set2.mwc" "pTorusShape2.iog.og[0].gco";
connectAttr "polyCloseBorder2.out" "pTorusShape2.i";
connectAttr "groupId37.id" "pTorusShape3.iog.og[4].gid";
connectAttr "set3.mwc" "pTorusShape3.iog.og[4].gco";
connectAttr "groupId38.id" "pTorusShape3.iog.og[5].gid";
connectAttr "set4.mwc" "pTorusShape3.iog.og[5].gco";
connectAttr "groupId39.id" "pTorusShape3.iog.og[6].gid";
connectAttr "set5.mwc" "pTorusShape3.iog.og[6].gco";
connectAttr "groupId40.id" "pTorusShape3.iog.og[7].gid";
connectAttr "set6.mwc" "pTorusShape3.iog.og[7].gco";
connectAttr "groupId73.id" "pTorusShape3.iog.og[8].gid";
connectAttr ":initialShadingGroup.mwc" "pTorusShape3.iog.og[8].gco";
connectAttr "groupParts22.og" "pTorusShape3.i";
connectAttr "groupId74.id" "pTorusShape3.ciog.cog[0].cgid";
connectAttr "groupId41.id" "pTorusShape4.iog.og[4].gid";
connectAttr "set3.mwc" "pTorusShape4.iog.og[4].gco";
connectAttr "groupId42.id" "pTorusShape4.iog.og[5].gid";
connectAttr "set4.mwc" "pTorusShape4.iog.og[5].gco";
connectAttr "groupId43.id" "pTorusShape4.iog.og[6].gid";
connectAttr "set5.mwc" "pTorusShape4.iog.og[6].gco";
connectAttr "groupId44.id" "pTorusShape4.iog.og[7].gid";
connectAttr "set6.mwc" "pTorusShape4.iog.og[7].gco";
connectAttr "groupId66.id" "pTorusShape4.iog.og[10].gid";
connectAttr ":initialShadingGroup.mwc" "pTorusShape4.iog.og[10].gco";
connectAttr "groupId67.id" "pTorusShape4.ciog.cog[2].cgid";
connectAttr "groupId45.id" "pTorusShape5.iog.og[4].gid";
connectAttr "set3.mwc" "pTorusShape5.iog.og[4].gco";
connectAttr "groupId46.id" "pTorusShape5.iog.og[5].gid";
connectAttr "set4.mwc" "pTorusShape5.iog.og[5].gco";
connectAttr "groupId47.id" "pTorusShape5.iog.og[6].gid";
connectAttr "set5.mwc" "pTorusShape5.iog.og[6].gco";
connectAttr "groupId48.id" "pTorusShape5.iog.og[7].gid";
connectAttr "set6.mwc" "pTorusShape5.iog.og[7].gco";
connectAttr "groupId59.id" "pTorusShape5.iog.og[8].gid";
connectAttr ":initialShadingGroup.mwc" "pTorusShape5.iog.og[8].gco";
connectAttr "groupId60.id" "pTorusShape5.ciog.cog[0].cgid";
connectAttr "groupId49.id" "pTorusShape6.iog.og[4].gid";
connectAttr "set3.mwc" "pTorusShape6.iog.og[4].gco";
connectAttr "groupId50.id" "pTorusShape6.iog.og[5].gid";
connectAttr "set4.mwc" "pTorusShape6.iog.og[5].gco";
connectAttr "groupId51.id" "pTorusShape6.iog.og[6].gid";
connectAttr "set5.mwc" "pTorusShape6.iog.og[6].gco";
connectAttr "groupId52.id" "pTorusShape6.iog.og[7].gid";
connectAttr "set6.mwc" "pTorusShape6.iog.og[7].gco";
connectAttr "groupId57.id" "pTorusShape6.iog.og[8].gid";
connectAttr ":initialShadingGroup.mwc" "pTorusShape6.iog.og[8].gco";
connectAttr "groupId58.id" "pTorusShape6.ciog.cog[0].cgid";
connectAttr "groupId80.id" "pTorusShape7.iog.og[4].gid";
connectAttr "set3.mwc" "pTorusShape7.iog.og[4].gco";
connectAttr "groupId81.id" "pTorusShape7.iog.og[5].gid";
connectAttr "set4.mwc" "pTorusShape7.iog.og[5].gco";
connectAttr "groupId82.id" "pTorusShape7.iog.og[6].gid";
connectAttr "set5.mwc" "pTorusShape7.iog.og[6].gco";
connectAttr "groupId83.id" "pTorusShape7.iog.og[7].gid";
connectAttr "set6.mwc" "pTorusShape7.iog.og[7].gco";
connectAttr "polyExtrudeFace5.out" "pTorusShape7.i";
connectAttr "polyBridgeEdge2.out" "pTorus8Shape.i";
connectAttr "groupId61.id" "pTorus8Shape.iog.og[0].gid";
connectAttr "set3.mwc" "pTorus8Shape.iog.og[0].gco";
connectAttr "groupId62.id" "pTorus8Shape.iog.og[1].gid";
connectAttr "set4.mwc" "pTorus8Shape.iog.og[1].gco";
connectAttr "groupId63.id" "pTorus8Shape.iog.og[2].gid";
connectAttr "set5.mwc" "pTorus8Shape.iog.og[2].gco";
connectAttr "groupId64.id" "pTorus8Shape.iog.og[3].gid";
connectAttr "set6.mwc" "pTorus8Shape.iog.og[3].gco";
connectAttr "groupId65.id" "pTorus8Shape.iog.og[4].gid";
connectAttr ":initialShadingGroup.mwc" "pTorus8Shape.iog.og[4].gco";
connectAttr "polyBridgeEdge4.out" "pTorus9Shape.i";
connectAttr "groupId68.id" "pTorus9Shape.iog.og[0].gid";
connectAttr "set3.mwc" "pTorus9Shape.iog.og[0].gco";
connectAttr "groupId69.id" "pTorus9Shape.iog.og[1].gid";
connectAttr "set4.mwc" "pTorus9Shape.iog.og[1].gco";
connectAttr "groupId70.id" "pTorus9Shape.iog.og[2].gid";
connectAttr "set5.mwc" "pTorus9Shape.iog.og[2].gco";
connectAttr "groupId71.id" "pTorus9Shape.iog.og[3].gid";
connectAttr "set6.mwc" "pTorus9Shape.iog.og[3].gco";
connectAttr "groupId72.id" "pTorus9Shape.iog.og[4].gid";
connectAttr ":initialShadingGroup.mwc" "pTorus9Shape.iog.og[4].gco";
connectAttr "polyBridgeEdge6.out" "pTorus10Shape.i";
connectAttr "groupId75.id" "pTorus10Shape.iog.og[0].gid";
connectAttr "set3.mwc" "pTorus10Shape.iog.og[0].gco";
connectAttr "groupId76.id" "pTorus10Shape.iog.og[1].gid";
connectAttr "set4.mwc" "pTorus10Shape.iog.og[1].gco";
connectAttr "groupId77.id" "pTorus10Shape.iog.og[2].gid";
connectAttr "set5.mwc" "pTorus10Shape.iog.og[2].gco";
connectAttr "groupId78.id" "pTorus10Shape.iog.og[3].gid";
connectAttr "set6.mwc" "pTorus10Shape.iog.og[3].gco";
connectAttr "groupId79.id" "pTorus10Shape.iog.og[4].gid";
connectAttr ":initialShadingGroup.mwc" "pTorus10Shape.iog.og[4].gco";
connectAttr "groupId89.id" "pCubeShape2.iog.og[1].gid";
connectAttr "set7.mwc" "pCubeShape2.iog.og[1].gco";
connectAttr "deleteComponent17.og" "pCubeShape2.i";
connectAttr "polyCube3.out" "|beam|pCube3|pCubeShape3.i";
connectAttr "polyExtrudeFace9.out" "|beam|pCube4|pCubeShape4.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "svgBlinn1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "PxrSurface1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "PxrSurface2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "svgBlinn1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "PxrSurface1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "PxrSurface2SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyTweak1.out" "polyChipOff1.ip";
connectAttr "pCubeShape1.wm" "polyChipOff1.mp";
connectAttr "polyCube1.out" "polyTweak1.ip";
connectAttr "pCubeShape1.o" "polySeparate1.ip";
connectAttr "polyChipOff1.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polySeparate1.out[0]" "groupParts2.ig";
connectAttr "groupId3.id" "groupParts2.gi";
connectAttr "polySeparate1.out[1]" "groupParts3.ig";
connectAttr "groupId4.id" "groupParts3.gi";
connectAttr "groupParts2.og" "polyNormal1.ip";
connectAttr "groupParts3.og" "polyNormal2.ip";
connectAttr "groupId5.msg" "set1.gn" -na;
connectAttr "pTorusShape1.iog.og[0]" "set1.dsm" -na;
connectAttr "polyTorus1.out" "groupParts4.ig";
connectAttr "groupId5.id" "groupParts4.gi";
connectAttr "groupParts4.og" "deleteComponent1.ig";
connectAttr "polyTweak2.out" "polyCloseBorder1.ip";
connectAttr "deleteComponent1.og" "polyTweak2.ip";
connectAttr "polyCloseBorder1.out" "polyExtrudeFace1.ip";
connectAttr "pTorusShape1.wm" "polyExtrudeFace1.mp";
connectAttr "groupId6.msg" "set2.gn" -na;
connectAttr "pTorusShape2.iog.og[0]" "set2.dsm" -na;
connectAttr "polyTorus2.out" "groupParts5.ig";
connectAttr "groupId6.id" "groupParts5.gi";
connectAttr "groupParts5.og" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polySplitEdge1.ip";
connectAttr "polyTweak3.out" "polyMergeVert1.ip";
connectAttr "pTorusShape2.wm" "polyMergeVert1.mp";
connectAttr "polySplitEdge1.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polyMergeVert2.ip";
connectAttr "pTorusShape2.wm" "polyMergeVert2.mp";
connectAttr "polyMergeVert1.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polyMergeVert3.ip";
connectAttr "pTorusShape2.wm" "polyMergeVert3.mp";
connectAttr "polyMergeVert2.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polyMergeVert4.ip";
connectAttr "pTorusShape2.wm" "polyMergeVert4.mp";
connectAttr "polyMergeVert3.out" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polyCloseBorder2.ip";
connectAttr "polyMergeVert4.out" "polyTweak7.ip";
connectAttr ":rmanDefaultDisplay.msg" ":rmanGlobals.displays[0]";
connectAttr ":PxrPathTracer.msg" ":rmanGlobals.ri_integrator";
connectAttr "d_openexr.msg" ":rmanDefaultDisplay.displayType";
connectAttr "Ci.msg" ":rmanDefaultDisplay.displayChannels[0]";
connectAttr "a.msg" ":rmanDefaultDisplay.displayChannels[1]";
connectAttr "svgBlinn1.oc" "svgBlinn1SG.ss";
connectAttr "svgBlinn1SG.msg" "materialInfo1.sg";
connectAttr "svgBlinn1.msg" "materialInfo1.m";
connectAttr "PxrSurface1.oc" "PxrSurface1SG.rman__surface";
connectAttr "lambert2.oc" "PxrSurface1SG.ss";
connectAttr "PxrSurface1SG.msg" "materialInfo2.sg";
connectAttr "lambert2.msg" "materialInfo2.m";
connectAttr "PxrSurface2.oc" "PxrSurface2SG.rman__surface";
connectAttr "PxrSurface3.oc" "PxrSurface2SG.ss";
connectAttr "PxrSurface2SG.msg" "materialInfo3.sg";
connectAttr "PxrSurface3.msg" "materialInfo3.m";
connectAttr "PxrSurface3.msg" "materialInfo3.t" -na;
connectAttr "groupId37.msg" "set3.gn" -na;
connectAttr "groupId41.msg" "set3.gn" -na;
connectAttr "groupId45.msg" "set3.gn" -na;
connectAttr "groupId49.msg" "set3.gn" -na;
connectAttr "groupId61.msg" "set3.gn" -na;
connectAttr "groupId68.msg" "set3.gn" -na;
connectAttr "groupId75.msg" "set3.gn" -na;
connectAttr "groupId80.msg" "set3.gn" -na;
connectAttr "pTorusShape3.iog.og[4]" "set3.dsm" -na;
connectAttr "pTorusShape4.iog.og[4]" "set3.dsm" -na;
connectAttr "pTorusShape5.iog.og[4]" "set3.dsm" -na;
connectAttr "pTorusShape6.iog.og[4]" "set3.dsm" -na;
connectAttr "pTorus8Shape.iog.og[0]" "set3.dsm" -na;
connectAttr "pTorus9Shape.iog.og[0]" "set3.dsm" -na;
connectAttr "pTorus10Shape.iog.og[0]" "set3.dsm" -na;
connectAttr "pTorusShape7.iog.og[4]" "set3.dsm" -na;
connectAttr "polyTorus3.out" "groupParts8.ig";
connectAttr "groupId37.id" "groupParts8.gi";
connectAttr "groupParts8.og" "deleteComponent3.ig";
connectAttr "groupId38.msg" "set4.gn" -na;
connectAttr "groupId42.msg" "set4.gn" -na;
connectAttr "groupId46.msg" "set4.gn" -na;
connectAttr "groupId50.msg" "set4.gn" -na;
connectAttr "groupId62.msg" "set4.gn" -na;
connectAttr "groupId69.msg" "set4.gn" -na;
connectAttr "groupId76.msg" "set4.gn" -na;
connectAttr "groupId81.msg" "set4.gn" -na;
connectAttr "pTorusShape3.iog.og[5]" "set4.dsm" -na;
connectAttr "pTorusShape4.iog.og[5]" "set4.dsm" -na;
connectAttr "pTorusShape5.iog.og[5]" "set4.dsm" -na;
connectAttr "pTorusShape6.iog.og[5]" "set4.dsm" -na;
connectAttr "pTorus8Shape.iog.og[1]" "set4.dsm" -na;
connectAttr "pTorus9Shape.iog.og[1]" "set4.dsm" -na;
connectAttr "pTorus10Shape.iog.og[1]" "set4.dsm" -na;
connectAttr "pTorusShape7.iog.og[5]" "set4.dsm" -na;
connectAttr "deleteComponent3.og" "groupParts9.ig";
connectAttr "groupId38.id" "groupParts9.gi";
connectAttr "groupParts9.og" "deleteComponent4.ig";
connectAttr "polyTweak8.out" "polyCloseBorder3.ip";
connectAttr "deleteComponent4.og" "polyTweak8.ip";
connectAttr "groupId39.msg" "set5.gn" -na;
connectAttr "groupId43.msg" "set5.gn" -na;
connectAttr "groupId47.msg" "set5.gn" -na;
connectAttr "groupId51.msg" "set5.gn" -na;
connectAttr "groupId63.msg" "set5.gn" -na;
connectAttr "groupId70.msg" "set5.gn" -na;
connectAttr "groupId77.msg" "set5.gn" -na;
connectAttr "groupId82.msg" "set5.gn" -na;
connectAttr "pTorusShape3.iog.og[6]" "set5.dsm" -na;
connectAttr "pTorusShape4.iog.og[6]" "set5.dsm" -na;
connectAttr "pTorusShape5.iog.og[6]" "set5.dsm" -na;
connectAttr "pTorusShape6.iog.og[6]" "set5.dsm" -na;
connectAttr "pTorus8Shape.iog.og[2]" "set5.dsm" -na;
connectAttr "pTorus9Shape.iog.og[2]" "set5.dsm" -na;
connectAttr "pTorus10Shape.iog.og[2]" "set5.dsm" -na;
connectAttr "pTorusShape7.iog.og[6]" "set5.dsm" -na;
connectAttr "polyCloseBorder3.out" "groupParts10.ig";
connectAttr "groupId39.id" "groupParts10.gi";
connectAttr "groupParts10.og" "deleteComponent5.ig";
connectAttr "polyTweak9.out" "polyExtrudeFace2.ip";
connectAttr "pTorusShape3.wm" "polyExtrudeFace2.mp";
connectAttr "deleteComponent5.og" "polyTweak9.ip";
connectAttr "polyTweak10.out" "polyExtrudeFace3.ip";
connectAttr "pTorusShape3.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polyExtrudeFace4.ip";
connectAttr "pTorusShape3.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak11.ip";
connectAttr "groupId40.msg" "set6.gn" -na;
connectAttr "groupId44.msg" "set6.gn" -na;
connectAttr "groupId48.msg" "set6.gn" -na;
connectAttr "groupId52.msg" "set6.gn" -na;
connectAttr "groupId64.msg" "set6.gn" -na;
connectAttr "groupId71.msg" "set6.gn" -na;
connectAttr "groupId78.msg" "set6.gn" -na;
connectAttr "groupId83.msg" "set6.gn" -na;
connectAttr "pTorusShape3.iog.og[7]" "set6.dsm" -na;
connectAttr "pTorusShape4.iog.og[7]" "set6.dsm" -na;
connectAttr "pTorusShape5.iog.og[7]" "set6.dsm" -na;
connectAttr "pTorusShape6.iog.og[7]" "set6.dsm" -na;
connectAttr "pTorus8Shape.iog.og[3]" "set6.dsm" -na;
connectAttr "pTorus9Shape.iog.og[3]" "set6.dsm" -na;
connectAttr "pTorus10Shape.iog.og[3]" "set6.dsm" -na;
connectAttr "pTorusShape7.iog.og[7]" "set6.dsm" -na;
connectAttr "polyExtrudeFace4.out" "groupParts11.ig";
connectAttr "groupId40.id" "groupParts11.gi";
connectAttr "groupParts11.og" "polyTweak12.ip";
connectAttr "polyTweak12.out" "deleteComponent6.ig";
connectAttr "polyTweak13.out" "polyMirror1.ip";
connectAttr "pTorus3.sp" "polyMirror1.sp";
connectAttr "pTorusShape3.wm" "polyMirror1.mp";
connectAttr "deleteComponent6.og" "polyTweak13.ip";
connectAttr "pTorusShape6.o" "polyUnite1.ip[0]";
connectAttr "pTorusShape5.o" "polyUnite1.ip[1]";
connectAttr "pTorusShape6.wm" "polyUnite1.im[0]";
connectAttr "pTorusShape5.wm" "polyUnite1.im[1]";
connectAttr "polyUnite1.out" "groupParts12.ig";
connectAttr "groupId61.id" "groupParts12.gi";
connectAttr "groupParts12.og" "groupParts13.ig";
connectAttr "groupId62.id" "groupParts13.gi";
connectAttr "groupParts13.og" "groupParts14.ig";
connectAttr "groupId63.id" "groupParts14.gi";
connectAttr "groupParts14.og" "groupParts15.ig";
connectAttr "groupId64.id" "groupParts15.gi";
connectAttr "groupParts15.og" "groupParts16.ig";
connectAttr "groupId65.id" "groupParts16.gi";
connectAttr "groupParts16.og" "deleteComponent7.ig";
connectAttr "deleteComponent7.og" "polyBridgeEdge1.ip";
connectAttr "pTorus8Shape.wm" "polyBridgeEdge1.mp";
connectAttr "polyBridgeEdge1.out" "polyBridgeEdge2.ip";
connectAttr "pTorus8Shape.wm" "polyBridgeEdge2.mp";
connectAttr "pTorusShape4.o" "polyUnite2.ip[0]";
connectAttr "pTorus8Shape.o" "polyUnite2.ip[1]";
connectAttr "pTorusShape4.wm" "polyUnite2.im[0]";
connectAttr "pTorus8Shape.wm" "polyUnite2.im[1]";
connectAttr "polyUnite2.out" "groupParts17.ig";
connectAttr "groupId68.id" "groupParts17.gi";
connectAttr "groupParts17.og" "groupParts18.ig";
connectAttr "groupId69.id" "groupParts18.gi";
connectAttr "groupParts18.og" "groupParts19.ig";
connectAttr "groupId70.id" "groupParts19.gi";
connectAttr "groupParts19.og" "groupParts20.ig";
connectAttr "groupId71.id" "groupParts20.gi";
connectAttr "groupParts20.og" "groupParts21.ig";
connectAttr "groupId72.id" "groupParts21.gi";
connectAttr "groupParts21.og" "deleteComponent8.ig";
connectAttr "deleteComponent8.og" "polyBridgeEdge3.ip";
connectAttr "pTorus9Shape.wm" "polyBridgeEdge3.mp";
connectAttr "polyBridgeEdge3.out" "polyBridgeEdge4.ip";
connectAttr "pTorus9Shape.wm" "polyBridgeEdge4.mp";
connectAttr "pTorusShape3.o" "polyUnite3.ip[0]";
connectAttr "pTorus9Shape.o" "polyUnite3.ip[1]";
connectAttr "pTorusShape3.wm" "polyUnite3.im[0]";
connectAttr "pTorus9Shape.wm" "polyUnite3.im[1]";
connectAttr "polyMirror1.out" "groupParts22.ig";
connectAttr "groupId73.id" "groupParts22.gi";
connectAttr "polyUnite3.out" "groupParts23.ig";
connectAttr "groupId75.id" "groupParts23.gi";
connectAttr "groupParts23.og" "groupParts24.ig";
connectAttr "groupId76.id" "groupParts24.gi";
connectAttr "groupParts24.og" "groupParts25.ig";
connectAttr "groupId77.id" "groupParts25.gi";
connectAttr "groupParts25.og" "groupParts26.ig";
connectAttr "groupId78.id" "groupParts26.gi";
connectAttr "groupParts26.og" "groupParts27.ig";
connectAttr "groupId79.id" "groupParts27.gi";
connectAttr "groupParts27.og" "deleteComponent9.ig";
connectAttr "deleteComponent9.og" "polyBridgeEdge5.ip";
connectAttr "pTorus10Shape.wm" "polyBridgeEdge5.mp";
connectAttr "polyBridgeEdge5.out" "polyBridgeEdge6.ip";
connectAttr "pTorus10Shape.wm" "polyBridgeEdge6.mp";
connectAttr "groupParts31.og" "polyExtrudeFace5.ip";
connectAttr "pTorusShape7.wm" "polyExtrudeFace5.mp";
connectAttr "polySurfaceShape3.o" "groupParts28.ig";
connectAttr "groupId80.id" "groupParts28.gi";
connectAttr "groupParts28.og" "groupParts29.ig";
connectAttr "groupId81.id" "groupParts29.gi";
connectAttr "groupParts29.og" "groupParts30.ig";
connectAttr "groupId82.id" "groupParts30.gi";
connectAttr "groupParts30.og" "groupParts31.ig";
connectAttr "groupId83.id" "groupParts31.gi";
connectAttr "polyTweak14.out" "polyChipOff2.ip";
connectAttr "polySurfaceShape2.wm" "polyChipOff2.mp";
connectAttr "polyNormal2.out" "polyTweak14.ip";
connectAttr "polySurfaceShape2.o" "polySeparate2.ip";
connectAttr "polySeparate2.out[0]" "groupParts32.ig";
connectAttr "groupId84.id" "groupParts32.gi";
connectAttr "polySeparate2.out[1]" "groupParts33.ig";
connectAttr "groupId85.id" "groupParts33.gi";
connectAttr "groupParts33.og" "groupParts34.ig";
connectAttr "groupId86.id" "groupParts34.gi";
connectAttr "groupParts32.og" "polyChipOff3.ip";
connectAttr "polySurfaceShape4.wm" "polyChipOff3.mp";
connectAttr "polySurfaceShape4.o" "polySeparate3.ip";
connectAttr "polySeparate3.out[0]" "groupParts35.ig";
connectAttr "groupId87.id" "groupParts35.gi";
connectAttr "polySeparate3.out[1]" "groupParts36.ig";
connectAttr "groupId88.id" "groupParts36.gi";
connectAttr "groupId89.msg" "set7.gn" -na;
connectAttr "pCubeShape2.iog.og[1]" "set7.dsm" -na;
connectAttr "polyCube2.out" "groupParts37.ig";
connectAttr "groupId89.id" "groupParts37.gi";
connectAttr "groupParts37.og" "polyTweak15.ip";
connectAttr "polyTweak15.out" "deleteComponent10.ig";
connectAttr "deleteComponent10.og" "polyExtrudeFace6.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace6.mp";
connectAttr "polyExtrudeFace6.out" "polyExtrudeFace7.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace7.mp";
connectAttr "polyTweak16.out" "polyExtrudeFace8.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace8.mp";
connectAttr "polyExtrudeFace7.out" "polyTweak16.ip";
connectAttr "|beam|pCube4|polySurfaceShape8.o" "polyExtrudeFace9.ip";
connectAttr "|beam|pCube4|pCubeShape4.wm" "polyExtrudeFace9.mp";
connectAttr "polyExtrudeFace8.out" "polyTweak17.ip";
connectAttr "polyTweak17.out" "deleteComponent11.ig";
connectAttr "deleteComponent11.og" "deleteComponent12.ig";
connectAttr "deleteComponent12.og" "deleteComponent13.ig";
connectAttr "deleteComponent13.og" "polyCloseBorder4.ip";
connectAttr "polyCloseBorder4.out" "deleteComponent14.ig";
connectAttr "deleteComponent14.og" "deleteComponent15.ig";
connectAttr "deleteComponent15.og" "deleteComponent16.ig";
connectAttr "deleteComponent16.og" "deleteComponent17.ig";
connectAttr "svgBlinn1SG.pa" ":renderPartition.st" -na;
connectAttr "PxrSurface1SG.pa" ":renderPartition.st" -na;
connectAttr "PxrSurface2SG.pa" ":renderPartition.st" -na;
connectAttr "svgBlinn1.msg" ":defaultShaderList1.s" -na;
connectAttr "PxrSurface1.msg" ":defaultShaderList1.s" -na;
connectAttr "PxrSurface2.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr ":rmanGlobals.msg" ":defaultRenderingList1.r" -na;
connectAttr ":rmanDefaultDisplay.msg" ":defaultRenderingList1.r" -na;
connectAttr "d_openexr.msg" ":defaultRenderingList1.r" -na;
connectAttr ":PxrPathTracer.msg" ":defaultRenderingList1.r" -na;
connectAttr "PxrSphereLightShape.msg" ":lightList1.l" -na;
connectAttr "PxrSphereLight1Shape.msg" ":lightList1.l" -na;
connectAttr "pCubeShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape2.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape7.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape6.iog.og[8]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape6.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape5.iog.og[8]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape5.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorus8Shape.iog.og[4]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape4.iog.og[10]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape4.ciog.cog[2]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorus9Shape.iog.og[4]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape3.iog.og[8]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorusShape3.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pTorus10Shape.iog.og[4]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape4.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape5.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape6.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape7.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|beam|pCube3|pCubeShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|beam|pCube4|pCubeShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|beam|pCube5|pCubeShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|beam1|pCube3|pCubeShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|beam1|pCube4|pCubeShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|beam1|pCube5|pCubeShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|beam2|pCube3|pCubeShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|beam2|pCube4|pCubeShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|beam2|pCube5|pCubeShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape7.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape8.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape9.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape10.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape11.iog" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId57.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId58.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId59.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId60.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId65.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId66.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId67.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId72.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId73.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId74.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId79.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId84.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId85.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId87.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId88.msg" ":initialShadingGroup.gn" -na;
connectAttr "PxrSphereLight.iog" ":defaultLightSet.dsm" -na;
connectAttr "PxrSphereLight1.iog" ":defaultLightSet.dsm" -na;
connectAttr "polySurfaceShape5HiddenFacesSet.msg" ":defaultHideFaceDataSet.dnsm"
		 -na;
connectAttr "groupId86.msg" ":defaultLastHiddenSet.gn" -na;
connectAttr "polySurfaceShape5.iog.og[1]" ":defaultLastHiddenSet.dsm" -na;
// End of bottega-3.ma
