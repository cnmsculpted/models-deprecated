//Maya ASCII 2018 scene
//Name: woodthing2.ma
//Last modified: Fri, Nov 09, 2018 11:05:44 PM
//Codeset: UTF-8
requires maya "2018";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Mac OS X 10.11.4";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "1D8EFB0A-844F-651E-6EFC-8488967DE100";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 21.633047327836941 15.571739761963068 -6.3154515563402693 ;
	setAttr ".r" -type "double3" -24.938352907734554 106.19999999999121 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "E15EB25B-F64B-509F-98F3-9B91D06B7E11";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 24.96437796341155;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.10487951095817971 5.0456875347136627 0 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "7287017C-3C4C-B5BC-1CB4-DEBE95AB4E56";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.055981636047363281 1000.1174281071113 -0.075422525405663909 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "ED84A022-0E4D-A234-AA8B-E5A992E01501";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 990.26991586193287;
	setAttr ".ow" 8.7134371611604671;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -0.055981636047363281 9.8475122451782227 -0.075422525405883789 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "C699A91F-3540-E1BA-859C-21847098D1A7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.10487951095817971 5.0456875347136627 1000.2863275742429 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "79D6E07F-4C4E-6DB2-62CA-BCA3397BB48F";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.2863275742429;
	setAttr ".ow" 16.975246620629161;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" -0.10487951095817971 5.0456875347136627 0 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "5F2A030F-E54C-2B42-43AB-379FBB64735C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1642560048582 3.1520529336201912 2.2204460492503131e-13 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "312A520A-C543-414D-268E-259A2CA663FB";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.164256004858;
	setAttr ".ow" 19.016376293311605;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 0 3.1520529336201912 0 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "woodthing2";
	rename -uid "8556B6CC-AB4C-96DD-BF59-A8BA96C28B96";
	setAttr ".rp" -type "double3" -0.10487961769104004 5.0456876754760742 0 ;
	setAttr ".sp" -type "double3" -0.10487961769104004 5.0456876754760742 0 ;
createNode mesh -n "woodthing2Shape" -p "woodthing2";
	rename -uid "46506E23-B74F-8A32-0167-A6BD61585EA0";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000002235174179 0.47955806198297068 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape1" -p "woodthing2";
	rename -uid "141D9B4F-9348-F0AF-5181-7FA1C78C9AFF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:107]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 252 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.375 0 0.375 0.25 0.625 0.25
		 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0.25 0.875
		 0 0.125 0 0.125 0.25 0.375 0 0.375 0.25 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375
		 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0.25 0.875 0 0.125 0 0.125 0.25 0.375 0 0.375
		 0.25 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1
		 0.875 0.25 0.875 0 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625
		 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125
		 0.25 0.375 0 0.375 0.25 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 1 0.625 1 0.875 0.25 0.875 0 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25
		 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875
		 0.25 0.125 0 0.125 0.25 0.375 0 0.375 0.25 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5
		 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0.25 0.875 0 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75
		 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75
		 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25 0.375 0 0.375 0.25 0.625 0.25 0.625 0 0.375 0.5 0.625
		 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0.25 0.875 0 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25;
	setAttr ".uvst[0].uvsp[250:251]" 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 144 ".vt[0:143]"  2.46498895 8.28005028 -2.62326121 2.80174756 8.29947662 -2.62326121
		 2.34348679 9.63913345 -2.49580336 2.68024468 9.65855694 -2.49580336 2.34348679 9.63913345 2.47889781
		 2.68024468 9.65855694 2.47889781 2.46498895 8.28005028 2.62436628 2.80174756 8.29947662 2.62436628
		 -2.5047164 8.28265858 -2.62591195 -2.84154129 8.30046463 -2.62985754 -2.39136982 9.63860321 -2.50339794
		 -2.728194 9.65640831 -2.50734329 -2.38732505 9.63936234 2.52846909 -2.72408295 9.65878677 2.52846909
		 -2.50882745 8.28028107 2.60398698 -2.84558582 8.29970551 2.60398698 -3.19938421 1.76880884 3.077565908
		 -3.53561854 1.795825 3.077993631 -3.047242165 3.12700844 2.94897652 -3.38347602 3.15402389 2.94940448
		 -3.047242165 3.21975255 -2.90786386 -3.38347602 3.246768 -2.90743613 -3.19938421 1.86664116 -3.10059714
		 -3.53561854 1.89365733 -3.10016942 3.15136456 1.88697934 3.1059761 3.48770189 1.91247594 3.1100769
		 3.0074470043 3.2366209 2.93278027 3.34378362 3.26211667 2.9368813 3.0033862591 3.11042356 -2.99076128
		 3.33962011 3.13743639 -2.99133992 3.15552831 1.75283706 -3.050626278 3.49176264 1.77985036 -3.05120492
		 3.059151173 1.81846547 2.89225602 3.059151173 1.8359611 3.22912025 2.90948129 3.17822099 2.77854252
		 2.90948129 3.19571567 3.11540651 -2.93217015 3.17822099 2.77854252 -2.93217015 3.19571567 3.11540651
		 -3.10298991 1.81846547 2.89225602 -3.10298991 1.8359611 3.22912025 3.059151173 1.81846547 -2.89225602
		 3.059151173 1.8359611 -3.22912025 2.97971559 3.17822099 -2.77854252 2.97971559 3.19571567 -3.11540651
		 -2.99038076 3.17822099 -2.77854252 -2.99038076 3.19571567 -3.11540651 -3.079059362 1.81846547 -2.89225602
		 -3.079059362 1.8359611 -3.22912025 2.43042254 8.27803802 2.3452425 2.43042254 8.29569817 2.68209815
		 2.31129456 9.63773727 2.23086071 2.31129456 9.65539742 2.56771564 -2.33829951 9.63773727 2.23086071
		 -2.33829951 9.65539742 2.56771564 -2.47426128 8.27803802 2.3452425 -2.47426128 8.29569817 2.68209815
		 2.43042254 8.27803802 -2.3452425 2.43042254 8.29569817 -2.68209815 2.3671968 9.63773727 -2.23086071
		 2.3671968 9.65539742 -2.56771564 -2.3846314 9.63773727 -2.23086071 -2.3846314 9.65539742 -2.56771564
		 -2.45521402 8.27803802 -2.3452425 -2.45521402 8.29569817 -2.68209815 -3.69283795 2.051929235 -2.2889688
		 -3.41936946 2.035569906 -2.30336356 -3.025822878 9.038413048 2.44275022 -2.75235415 9.022053719 2.42835546
		 -3.034506559 9.52717304 1.72230971 -2.76103806 9.51081371 1.70791495 -3.70152187 2.54068923 -3.0094091892
		 -3.42805314 2.5243299 -3.023803949 -2.78558183 9.65695095 2.47581196 -1.51112676 9.65695095 2.48932695
		 -2.78558183 10.03807354 2.47581196 -1.51112676 10.03807354 2.48932695 -2.73132968 10.03807354 -2.640172
		 -1.45687461 10.03807354 -2.62665701 -2.73132968 9.65695095 -2.640172 -1.45687461 9.65695095 -2.62665701
		 -1.38275635 9.65695095 2.48758411 -0.10826814 9.65695095 2.47768784 -1.38275635 10.03807354 2.48758411
		 -0.10826814 10.03807354 2.47768784 -1.42248213 10.03807354 -2.62853289 -0.14799392 10.03807354 -2.63842916
		 -1.42248213 9.65695095 -2.62853289 -0.14799392 9.65695095 -2.63842916 -0.013493039 9.65695095 2.48903561
		 1.26096821 9.65695095 2.47612834 -0.013493039 10.03807354 2.48903561 1.26096821 10.03807354 2.47612834
		 -0.065305486 10.03807354 -2.62697339 1.20915568 10.03807354 -2.63988066 -0.065305486 9.65695095 -2.62697339
		 1.20915568 9.65695095 -2.63988066 1.39918089 9.65695095 2.49006987 2.67361856 9.65695095 2.47499895
		 1.39918089 10.03807354 2.49006987 2.67361856 10.03807354 2.47499895 1.33868194 10.03807354 -2.625844
		 2.6131196 10.03807354 -2.64091492 1.33868194 9.65695095 -2.625844 2.6131196 9.65695095 -2.64091492
		 3.098353863 2.50594497 -2.31304169 2.82549119 2.47755694 -2.31304169 2.4348979 8.88302326 1.056429982
		 2.16203523 8.85463524 1.056429982 2.47680926 8.48017597 1.82711351 2.20394659 8.45178795 1.82711351
		 3.14026523 2.10309768 -1.54235828 2.86740255 2.074709654 -1.54235828 -3.38213992 0.053301811 -2.64264584
		 -2.40841389 0.053301811 -2.64264584 -2.37025809 9.66843796 -1.83314002 -1.39653289 9.66843796 -1.83314002
		 -2.37025809 9.66843796 -2.22924662 -1.39653289 9.66843796 -2.22924662 -3.38213992 0.053301811 -3.038752317
		 -2.40841389 0.053301811 -3.038752317 -3.38213992 0.053301811 2.64264512 -2.40841389 0.053301811 2.64264512
		 -2.37025809 9.66843796 1.83313978 -1.39653289 9.66843796 1.83313978 -2.37025809 9.66843796 2.22924614
		 -1.39653289 9.66843796 2.22924614 -3.38213992 0.053301811 3.038751602 -2.40841389 0.053301811 3.038751602
		 3.38213968 0.053301811 -2.64264584 2.40841389 0.053301811 -2.64264584 2.37025809 9.66843796 -1.83314002
		 1.39653301 9.66843796 -1.83314002 2.37025809 9.66843796 -2.22924662 1.39653301 9.66843796 -2.22924662
		 3.38213968 0.053301811 -3.038752317 2.40841389 0.053301811 -3.038752317 3.38213968 0.053301811 2.64264512
		 2.40841389 0.053301811 2.64264512 2.37025809 9.66843796 1.83313978 1.39653301 9.66843796 1.83313978
		 2.37025809 9.66843796 2.22924614 1.39653301 9.66843796 2.22924614 3.38213968 0.053301811 3.038751602
		 2.40841389 0.053301811 3.038751602;
	setAttr -s 216 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 3 0 2 3 0 0 2 0 3 5 0 4 5 0 2 4 0 5 7 0 6 7 0
		 4 6 0 7 1 0 6 0 0 8 10 0 10 11 0 9 11 0 8 9 0 10 12 0 12 13 0 11 13 0 12 14 0 14 15 0
		 13 15 0 14 8 0 15 9 0 16 17 0 17 19 0 18 19 0 16 18 0 19 21 0 20 21 0 18 20 0 21 23 0
		 22 23 0 20 22 0 23 17 0 22 16 0 24 26 0 26 27 0 25 27 0 24 25 0 26 28 0 28 29 0 27 29 0
		 28 30 0 30 31 0 29 31 0 30 24 0 31 25 0 32 33 0 33 35 0 34 35 0 32 34 0 35 37 0 36 37 0
		 34 36 0 37 39 0 38 39 0 36 38 0 39 33 0 38 32 0 40 42 0 42 43 0 41 43 0 40 41 0 42 44 0
		 44 45 0 43 45 0 44 46 0 46 47 0 45 47 0 46 40 0 47 41 0 48 49 0 49 51 0 50 51 0 48 50 0
		 51 53 0 52 53 0 50 52 0 53 55 0 54 55 0 52 54 0 55 49 0 54 48 0 56 58 0 58 59 0 57 59 0
		 56 57 0 58 60 0 60 61 0 59 61 0 60 62 0 62 63 0 61 63 0 62 56 0 63 57 0 64 65 0 66 67 0
		 68 69 0 70 71 0 64 66 0 65 67 0 66 68 0 67 69 0 68 70 0 69 71 0 70 64 0 71 65 0 72 73 0
		 74 75 0 76 77 0 78 79 0 72 74 0 73 75 0 74 76 0 75 77 0 76 78 0 77 79 0 78 72 0 79 73 0
		 80 81 0 82 83 0 84 85 0 86 87 0 80 82 0 81 83 0 82 84 0 83 85 0 84 86 0 85 87 0 86 80 0
		 87 81 0 88 89 0 90 91 0 92 93 0 94 95 0 88 90 0 89 91 0 90 92 0 91 93 0 92 94 0 93 95 0
		 94 88 0 95 89 0 96 97 0 98 99 0 100 101 0 102 103 0 96 98 0 97 99 0 98 100 0 99 101 0
		 100 102 0 101 103 0 102 96 0 103 97 0 104 105 0 106 107 0 108 109 0 110 111 0 104 106 0
		 105 107 0 106 108 0 107 109 0 108 110 0 109 111 0;
	setAttr ".ed[166:215]" 110 104 0 111 105 0 112 113 0 114 115 0 116 117 0 118 119 0
		 112 114 0 113 115 0 114 116 0 115 117 0 116 118 0 117 119 0 118 112 0 119 113 0 120 121 0
		 121 123 0 122 123 0 120 122 0 123 125 0 124 125 0 122 124 0 125 127 0 126 127 0 124 126 0
		 127 121 0 126 120 0 128 129 0 129 131 0 130 131 0 128 130 0 131 133 0 132 133 0 130 132 0
		 133 135 0 134 135 0 132 134 0 135 129 0 134 128 0 136 138 0 138 139 0 137 139 0 136 137 0
		 138 140 0 140 141 0 139 141 0 140 142 0 142 143 0 141 143 0 142 136 0 143 137 0;
	setAttr -s 108 -ch 432 ".fc[0:107]" -type "polyFaces" 
		f 4 3 2 -2 -1
		mu 0 4 0 1 2 3
		f 4 6 5 -5 -3
		mu 0 4 1 4 5 2
		f 4 9 8 -8 -6
		mu 0 4 4 6 7 5
		f 4 11 0 -11 -9
		mu 0 4 6 8 9 7
		f 4 1 4 7 10
		mu 0 4 3 2 10 11
		f 4 -10 -7 -4 -12
		mu 0 4 12 13 1 0
		f 4 15 14 -14 -13
		mu 0 4 14 17 16 15
		f 4 13 18 -18 -17
		mu 0 4 15 16 19 18
		f 4 17 21 -21 -20
		mu 0 4 18 19 21 20
		f 4 20 23 -16 -23
		mu 0 4 20 21 23 22
		f 4 -24 -22 -19 -15
		mu 0 4 17 25 24 16
		f 4 22 12 16 19
		mu 0 4 26 14 15 27
		f 4 27 26 -26 -25
		mu 0 4 28 29 30 31
		f 4 30 29 -29 -27
		mu 0 4 29 32 33 30
		f 4 33 32 -32 -30
		mu 0 4 32 34 35 33
		f 4 35 24 -35 -33
		mu 0 4 34 36 37 35
		f 4 25 28 31 34
		mu 0 4 31 30 38 39
		f 4 -34 -31 -28 -36
		mu 0 4 40 41 29 28
		f 4 39 38 -38 -37
		mu 0 4 42 43 44 45
		f 4 37 42 -42 -41
		mu 0 4 45 44 46 47
		f 4 41 45 -45 -44
		mu 0 4 47 46 48 49
		f 4 44 47 -40 -47
		mu 0 4 49 48 50 51
		f 4 -48 -46 -43 -39
		mu 0 4 43 52 53 44
		f 4 46 36 40 43
		mu 0 4 54 42 45 55
		f 4 51 50 -50 -49
		mu 0 4 56 57 58 59
		f 4 54 53 -53 -51
		mu 0 4 57 60 61 58
		f 4 57 56 -56 -54
		mu 0 4 60 62 63 61
		f 4 59 48 -59 -57
		mu 0 4 62 64 65 63
		f 4 49 52 55 58
		mu 0 4 59 58 66 67
		f 4 -58 -55 -52 -60
		mu 0 4 68 69 57 56
		f 4 63 62 -62 -61
		mu 0 4 70 71 72 73
		f 4 61 66 -66 -65
		mu 0 4 73 72 74 75
		f 4 65 69 -69 -68
		mu 0 4 75 74 76 77
		f 4 68 71 -64 -71
		mu 0 4 77 76 78 79
		f 4 -72 -70 -67 -63
		mu 0 4 71 80 81 72
		f 4 70 60 64 67
		mu 0 4 82 70 73 83
		f 4 75 74 -74 -73
		mu 0 4 84 85 86 87
		f 4 78 77 -77 -75
		mu 0 4 85 88 89 86
		f 4 81 80 -80 -78
		mu 0 4 88 90 91 89
		f 4 83 72 -83 -81
		mu 0 4 90 92 93 91
		f 4 73 76 79 82
		mu 0 4 87 86 94 95
		f 4 -82 -79 -76 -84
		mu 0 4 96 97 85 84
		f 4 87 86 -86 -85
		mu 0 4 98 99 100 101
		f 4 85 90 -90 -89
		mu 0 4 101 100 102 103
		f 4 89 93 -93 -92
		mu 0 4 103 102 104 105
		f 4 92 95 -88 -95
		mu 0 4 105 104 106 107
		f 4 -96 -94 -91 -87
		mu 0 4 99 108 109 100
		f 4 94 84 88 91
		mu 0 4 110 98 101 111
		f 4 96 101 -98 -101
		mu 0 4 112 113 114 115
		f 4 97 103 -99 -103
		mu 0 4 115 114 116 117
		f 4 98 105 -100 -105
		mu 0 4 117 116 118 119
		f 4 99 107 -97 -107
		mu 0 4 119 118 120 121
		f 4 -108 -106 -104 -102
		mu 0 4 113 122 123 114
		f 4 106 100 102 104
		mu 0 4 124 112 115 125
		f 4 108 113 -110 -113
		mu 0 4 126 127 128 129
		f 4 109 115 -111 -115
		mu 0 4 129 128 130 131
		f 4 110 117 -112 -117
		mu 0 4 131 130 132 133
		f 4 111 119 -109 -119
		mu 0 4 133 132 134 135
		f 4 -120 -118 -116 -114
		mu 0 4 127 136 137 128
		f 4 118 112 114 116
		mu 0 4 138 126 129 139
		f 4 120 125 -122 -125
		mu 0 4 140 141 142 143
		f 4 121 127 -123 -127
		mu 0 4 143 142 144 145
		f 4 122 129 -124 -129
		mu 0 4 145 144 146 147
		f 4 123 131 -121 -131
		mu 0 4 147 146 148 149
		f 4 -132 -130 -128 -126
		mu 0 4 141 150 151 142
		f 4 130 124 126 128
		mu 0 4 152 140 143 153
		f 4 132 137 -134 -137
		mu 0 4 154 155 156 157
		f 4 133 139 -135 -139
		mu 0 4 157 156 158 159
		f 4 134 141 -136 -141
		mu 0 4 159 158 160 161
		f 4 135 143 -133 -143
		mu 0 4 161 160 162 163
		f 4 -144 -142 -140 -138
		mu 0 4 155 164 165 156
		f 4 142 136 138 140
		mu 0 4 166 154 157 167
		f 4 144 149 -146 -149
		mu 0 4 168 169 170 171
		f 4 145 151 -147 -151
		mu 0 4 171 170 172 173
		f 4 146 153 -148 -153
		mu 0 4 173 172 174 175
		f 4 147 155 -145 -155
		mu 0 4 175 174 176 177
		f 4 -156 -154 -152 -150
		mu 0 4 169 178 179 170
		f 4 154 148 150 152
		mu 0 4 180 168 171 181
		f 4 156 161 -158 -161
		mu 0 4 182 183 184 185
		f 4 157 163 -159 -163
		mu 0 4 185 184 186 187
		f 4 158 165 -160 -165
		mu 0 4 187 186 188 189
		f 4 159 167 -157 -167
		mu 0 4 189 188 190 191
		f 4 -168 -166 -164 -162
		mu 0 4 183 192 193 184
		f 4 166 160 162 164
		mu 0 4 194 182 185 195
		f 4 168 173 -170 -173
		mu 0 4 196 197 198 199
		f 4 169 175 -171 -175
		mu 0 4 199 198 200 201
		f 4 170 177 -172 -177
		mu 0 4 201 200 202 203
		f 4 171 179 -169 -179
		mu 0 4 203 202 204 205
		f 4 -180 -178 -176 -174
		mu 0 4 197 206 207 198
		f 4 178 172 174 176
		mu 0 4 208 196 199 209
		f 4 183 182 -182 -181
		mu 0 4 210 211 212 213
		f 4 186 185 -185 -183
		mu 0 4 211 214 215 212
		f 4 189 188 -188 -186
		mu 0 4 214 216 217 215
		f 4 191 180 -191 -189
		mu 0 4 216 218 219 217
		f 4 181 184 187 190
		mu 0 4 213 212 220 221
		f 4 -190 -187 -184 -192
		mu 0 4 222 223 211 210
		f 4 195 194 -194 -193
		mu 0 4 224 225 226 227
		f 4 198 197 -197 -195
		mu 0 4 225 228 229 226
		f 4 201 200 -200 -198
		mu 0 4 228 230 231 229
		f 4 203 192 -203 -201
		mu 0 4 230 232 233 231
		f 4 193 196 199 202
		mu 0 4 227 226 234 235
		f 4 -202 -199 -196 -204
		mu 0 4 236 237 225 224
		f 4 207 206 -206 -205
		mu 0 4 238 239 240 241
		f 4 205 210 -210 -209
		mu 0 4 241 240 242 243
		f 4 209 213 -213 -212
		mu 0 4 243 242 244 245
		f 4 212 215 -208 -215
		mu 0 4 245 244 246 247
		f 4 -216 -214 -211 -207
		mu 0 4 239 248 249 240
		f 4 214 204 208 211
		mu 0 4 250 238 241 251;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "A90B7A8F-8648-FC25-014C-8FBD15C2F906";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "9F16BA84-3242-F016-9C29-CF8A36B3B88A";
createNode displayLayer -n "defaultLayer";
	rename -uid "A420A974-F145-CDA8-0AF9-2EBF51B0E222";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "39EAAB2F-C746-9283-3FAD-06AE78AFF92A";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "DE38CB01-CB49-6345-D26D-578DFBDAFFAD";
	setAttr ".g" yes;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "DBA03DB7-9344-BC58-BF1F-0DB6A6FE8F4A";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "F3F39D42-A44F-DC33-1B30-CE8AB284D90C";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "536A9E2A-074E-E211-C107-A38EC36EDB00";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 449\n            -height 279\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n"
		+ "            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n"
		+ "            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 449\n            -height 278\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n"
		+ "            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 449\n            -height 278\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n"
		+ "            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 321\n            -height 697\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n"
		+ "            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n"
		+ "            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n"
		+ "            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n"
		+ "            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n"
		+ "                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n"
		+ "                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n"
		+ "                -classicMode 1\n                -valueLinesToggle 1\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n"
		+ "                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n"
		+ "                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n"
		+ "                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n"
		+ "                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -highlightConnections 0\n"
		+ "                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 321\\n    -height 697\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 321\\n    -height 697\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "26E924B6-EC45-AA26-DADE-A798E3A602AD";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode renderLayerManager -n "woodthing1:renderLayerManager";
	rename -uid "AD0AE983-AD4A-7ADA-C050-E7BB16F31C4F";
createNode renderLayer -n "woodthing1:defaultRenderLayer";
	rename -uid "E7A07D57-B744-CCE5-13A0-AD95CB38313B";
	setAttr ".g" yes;
createNode renderLayerManager -n "woodthingtop:renderLayerManager";
	rename -uid "494A0BC6-5B47-6201-7950-3593342C49EC";
createNode renderLayer -n "woodthingtop:defaultRenderLayer";
	rename -uid "46C77D2B-E74F-ABAF-E67E-9384805034CA";
	setAttr ".g" yes;
createNode renderLayerManager -n "woodthingtop1:renderLayerManager";
	rename -uid "4C3CF75D-4A44-6D42-0F50-EA955DAFB0EC";
createNode renderLayer -n "woodthingtop1:defaultRenderLayer";
	rename -uid "C6247D9A-6743-E356-4A4E-87962546E8B7";
	setAttr ".g" yes;
createNode renderLayerManager -n "woodthing3:renderLayerManager";
	rename -uid "987693BF-3346-A118-274D-088E1355D3BA";
createNode renderLayer -n "woodthing3:defaultRenderLayer";
	rename -uid "9DBD367B-6C4A-C80B-B1A8-418ED6216AC3";
	setAttr ".g" yes;
createNode polyAutoProj -n "polyAutoProj1";
	rename -uid "E7822B94-1B49-7BE1-E803-13B3DEAC7172";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:107]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".s" -type "double3" 9.984771728515625 9.984771728515625 9.984771728515625 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode groupId -n "groupId1";
	rename -uid "5D0C42ED-0F46-479D-EECA-14B402BAFF19";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "81ED1132-7749-FB08-C537-0E9F2B50D5E9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:107]";
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "11A6DE33-E34F-A5D9-C3E2-728F2D8DFB98";
	setAttr ".uopa" yes;
	setAttr -s 428 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" -0.25735676 -0.14214584 -0.24476317
		 -0.14099495 -0.24465381 -0.094908476 -0.25724137 -0.093530945 -0.24657844 -0.14171986
		 -0.23399089 -0.14034232 -0.23410027 -0.094255872 -0.24669382 -0.093104959 -0.54873043
		 -0.11634386 -0.54907173 -0.16483088 -0.53647619 -0.16421805 -0.53617662 -0.11756543
		 0.23096463 -0.0024274543 0.23124392 -0.050877895 0.24379954 -0.049676213 0.24355909
		 -0.003060146 0.60362965 -0.33153158 0.61611867 -0.32942712 0.61303675 -0.27513632
		 0.60037857 -0.27426222 0.71971589 -0.33220139 0.73237401 -0.33132729 0.73545593 -0.27703643
		 0.72296691 -0.27493203 0.83861029 -0.49033833 0.84281433 -0.54747963 0.85532629 -0.54572874
		 0.85125417 -0.49075156 0.52532977 -0.17211573 0.52106869 -0.22921516 0.53371441 -0.22881851
		 0.53784078 -0.17388318 -0.11079945 -0.0054998277 -0.11483135 0.0073042992 -0.1179636
		 0.0062082489 -0.11393164 -0.0065958006 0.84207171 0.02352665 0.83803976 0.010722656
		 0.8411721 0.0096265459 0.845204 0.022430515 0.61983734 0.054024745 0.62296957 0.052928627
		 0.62700152 0.065732643 0.62386936 0.066828772 0.31459114 0.066645131 0.31145898 0.065549076
		 0.31549084 0.052745037 0.31862316 0.053841073 0.0021622919 0.049239431 -0.0018926307
		 0.062045433 -0.0050251726 0.060943596 -0.00097028178 0.048137691 0.46004739 0.060905032
		 0.45599252 0.04809903 0.45912513 0.04699729 0.46318001 0.059803195 0.49682876 0.0557504
		 0.49996126 0.054648638 0.50401628 0.067454629 0.50088364 0.068556391 -0.13648114
		 0.072308421 -0.13961373 0.071206674 -0.13555883 0.058400683 -0.13242623 0.059502505
		 -0.4033649 -0.33213207 -0.39013723 -0.34890655 -0.22745278 -0.22082813 -0.24068041
		 -0.2040537 -0.0048771552 -0.3469846 0.0083504291 -0.33021015 -0.15433398 -0.20213175
		 -0.16756165 -0.21890618 -0.032127593 -0.13710603 -0.032127593 -0.18450142 -0.028596811
		 -0.18450142 -0.028596811 -0.13710603 -0.10114578 -0.079376377 -0.10114578 -0.12677178
		 -0.097615004 -0.12677178 -0.097615004 -0.079376377 -0.35658851 -0.29855451 -0.35658851
		 -0.34595114 -0.35305771 -0.34595114 -0.35305771 -0.29855451 -0.6174702 -0.29636928
		 -0.6174702 -0.34376588 -0.6139394 -0.34376588 -0.6139394 -0.29636928 -0.77090842
		 -0.26281038 -0.77090842 -0.31020606 -0.76737761 -0.31020606 -0.76737761 -0.26281038
		 0.040727906 -0.1726345 0.040727906 -0.22003014 0.044258717 -0.22003014 0.044258717
		 -0.1726345 -0.24115339 -0.07202965 -0.24115339 -0.1194244 -0.23762259 -0.1194244
		 -0.23762259 -0.07202965 -0.51546568 -0.069609769 -0.51546568 -0.1170045 -0.51193494
		 -0.1170045 -0.51193494 -0.069609769 0.18583696 -0.34505531 0.19729459 -0.3310338
		 0.081423678 -0.23549119 0.069966063 -0.24951278 0.26065969 -0.33180228 0.27211729
		 -0.34582388 0.38798821 -0.2502813 0.37653056 -0.2362597 0.20364252 0.0023867039 0.20725097
		 0.00093385729 0.23514311 0.091494881 0.23153466 0.092947729 0.20583451 0.0028254492
		 0.20944296 0.0042782924 0.1815508 0.094839312 0.17794238 0.093386471 0.03172446 0.0037730106
		 0.0038323274 0.094334058 0.00022387475 0.09288121 0.028116034 0.0023201609 -0.021867838
		 0.001006027 0.0060242657 0.091567047 0.002415813 0.093019888 -0.02547629 0.0024588723
		 -0.17169407 0.0033541957 -0.1995862 0.093915224 -0.20319466 0.092462376 -0.17530254
		 0.0019013461 -0.22528642 0.0014246408 -0.19739425 0.091985673 -0.20100269 0.093438521
		 -0.22889489 0.0028774948 -0.40661326 0.0022802206 -0.4030048 0.00082737522 -0.37511265
		 0.091388389 -0.37872115 0.092841268 -0.4044213 0.0019966674 -0.40081283 0.0034495129
		 -0.42870495 0.094010554 -0.43231341 0.092557706 -0.22663659 -0.075476132 -0.22663659
		 -0.1215627 -0.22351679 -0.1215627 -0.22351679 -0.075476132 -0.33379909 -0.093614355
		 -0.33379909 -0.14222938 -0.33067927 -0.14222938 -0.33067927 -0.093614355 -0.24122488
		 -0.30197144 -0.24434516 -0.3019225 -0.24449168 -0.34857529 -0.24137188 -0.34858763
		 -0.18878238 -0.11694746 -0.19190215 -0.1169363 -0.19203787 -0.16542371 -0.18891732
		 -0.16539825 -0.13700053 -0.14171152 -0.13700162 -0.08745268 -0.1401165 -0.087456733
		 -0.1401154 -0.14171553 0.67735255 -0.33150801 0.67735356 -0.27427241 0.67423856 -0.27426836
		 0.67423761 -0.33150402 0.29365897 -0.192599 0.29677474 -0.19264746 0.2969206 -0.13772731
		 0.29380569 -0.13772218 0.9043768 -0.27690428 0.90749156 -0.27691922 0.90762991 -0.21983998
		 0.9045139 -0.21986836 0.0085175121 -0.14437419 0.0085175121 -0.090256095 0.0053967214
		 -0.090256095 0.0053967214 -0.14437419 0.72149789 -0.0047293552 0.72149789 -0.061816573
		 0.72461867 -0.061816573 0.72461867 -0.0047293552 0.40346402 -0.24660899 0.40658483
		 -0.24660899 0.40658483 -0.1913009 0.40346402 -0.1913009 0.65417117 -0.2178452 0.65105039
		 -0.2178452 0.65105039 -0.27471071 0.65417117 -0.27471071 -0.37280086 -0.10345115
		 -0.37280086 -0.060376473 -0.37592155 -0.060376473 -0.37592155 -0.10345115 -0.4410744
		 -0.071116745 -0.4410744 -0.11655459 -0.43795371 -0.11655459 -0.43795371 -0.071116745
		 0.057240948 -0.10176923 0.060361538 -0.10176923 0.060361538 -0.057747427 0.057240948
		 -0.057747427 -0.11109127 -0.047330607 -0.11421197 -0.047330607 -0.11421197 -0.092592023
		 -0.11109127 -0.092592023 -0.0055949925 -0.047901206 0.0062149037 -0.047436476 0.0043496448
		 -2.8564687e-05 -0.0074602901 -0.00049321138 -0.15025793 -0.043135867 -0.13844807
		 -0.043600529 -0.13658266 0.0038073829 -0.1483926 0.004272114 -0.2481627 -0.1799608
		 -0.23635393 -0.18030098 -0.23498809 -0.13289766 -0.24679692 -0.13255747 -0.28565168
		 -0.12673768 -0.27384296 -0.12639745 -0.2752088 -0.078994043 -0.28701761 -0.079334363
		 -0.90501678 0.065567426 -0.89320701 0.065123685 -0.89142567 0.11253071 -0.90323538
		 0.11297452 -0.62812352 -0.11258975 -0.61631387 -0.11214601 -0.61809528 -0.064738944
		 -0.62990499 -0.065182686 -0.61808312 0.065226495 -0.6062727 0.064708337 -0.60419255
		 0.11211869 -0.61600304 0.11263683 -0.5381574 0.06681589 -0.52634692 0.067334063 -0.528427
		 0.11474447 -0.54023755 0.11422624 0.56565833 -0.04919365 0.56315345 -0.048454951
		 0.56145179 -0.055643421 0.56395668 -0.05638212 0.54641616 -0.0086980416 0.54391122
		 -0.0094367424 0.54561287 -0.016625153 0.54811776 -0.015886521 0.55980462 0.076248839
		 0.55980462 0.067228071;
	setAttr ".uvtk[250:427]" 0.56347424 0.067228071 0.56347424 0.076248839 0.28510606
		 0.079368316 0.28510606 0.070347555 0.28877565 0.070347555 0.28877565 0.079368316
		 0.48222458 0.072630249 0.4785549 0.072630249 0.4785549 0.063609481 0.48222458 0.063609481
		 -0.18963182 0.070959099 -0.19330142 0.070959099 -0.19330142 0.061938327 -0.18963182
		 0.061938327 0.24381429 0.063870512 0.24748388 0.063870512 0.24748388 0.072891273
		 0.24381429 0.072891273 -0.20548019 0.066036299 -0.2018106 0.066036299 -0.2018106
		 0.075057074 -0.20548019 0.075057074 -0.20022538 0.066291742 -0.20022538 0.075312503
		 -0.20389499 0.075312503 -0.20389499 0.066291742 -0.19864023 0.066792704 -0.19864023
		 0.07581348 -0.20230983 0.07581348 -0.20230983 0.066792704 -0.27024087 -0.06578628
		 -0.26594111 -0.078613579 -0.26280519 -0.07745114 -0.26710492 -0.064623922 -0.044511467
		 -0.0053696125 -0.048811235 -0.018196924 -0.0456753 -0.019359278 -0.04137557 -0.00653206
		 0.5470708 0.12275462 0.54393762 0.12384657 0.53992164 0.11107849 0.54305482 0.10998667
		 0.14121881 -0.0054015075 0.13808291 -0.0065639298 0.14238261 -0.019391162 0.14551851
		 -0.018228725 -0.12715751 -0.090123273 -0.13250984 -0.07716997 -0.13566387 -0.078590699
		 -0.13031153 -0.091543943 0.64434409 0.060919754 0.64969867 0.07382717 0.64654481
		 0.075254627 0.64119035 0.062347155 0.53644371 0.11114138 0.53959388 0.10978515 0.54466122
		 0.1226223 0.54151106 0.12397856 0.053781103 -0.091476552 0.056935031 -0.090055041
		 0.051582493 -0.07710731 0.048428528 -0.078528732 0.8357811 -0.058891155 0.8483808
		 -0.057529554 0.84848797 -0.0034114579 0.83589417 -0.0018039886 0.76274186 -0.21122678
		 0.750148 -0.21283424 0.75025517 -0.26695231 0.76285487 -0.26831394 0.3698405 -0.17990136
		 0.36979213 -0.12303588 0.35719582 -0.12386815 0.35724282 -0.17917615 0.23168392 -0.074286275
		 0.2316355 -0.13115177 0.24423321 -0.13042659 0.24428019 -0.075118549 -0.094637997
		 -0.080312796 -0.082038701 -0.079241537 -0.08192803 -0.036166858 -0.094521306 -0.034874938
		 0.36607921 0.064480565 0.353486 0.063188583 0.35359669 0.020113895 0.36619598 0.019042695
		 0.28899869 0.018636573 0.28894877 0.063898005 0.27635309 0.063230224 0.27640158 0.019208454
		 0.39452153 0.064489804 0.39447168 0.019228386 0.40706876 0.019800266 0.40711725 0.063822031
		 0.40237674 0.075020812 0.40237674 0.063214041 0.40590757 0.063214041 0.40590757 0.075020812
		 0.16991851 0.07912156 0.16991851 0.067314759 0.17344932 0.067314759 0.17344932 0.07912156
		 0.60334933 0.12184807 0.60334933 0.110041 0.60688013 0.110041 0.60688013 0.12184807
		 0.63694841 0.075503506 0.63694841 0.063696422 0.64047915 0.063696422 0.64047915 0.075503506
		 0.32886213 0.07760907 0.32886213 0.065802231 0.33239293 0.065802231 0.33239293 0.07760907
		 0.029251693 0.0795312 0.029251693 0.067724369 0.032782484 0.067724369 0.032782484
		 0.0795312 0.030867178 0.079781719 0.030867178 0.067975111 0.034397978 0.067975111
		 0.034397978 0.079781719 0.032482654 0.079781719 0.032482654 0.067975111 0.036013454
		 0.067975111 0.036013454 0.079781719 0.436829 -0.26981932 0.43423694 -0.27079538 0.45704812
		 -0.33137605 0.45964018 -0.33040005 0.51523608 -0.27140471 0.51264399 -0.27042869
		 0.48983279 -0.33100936 0.49242494 -0.33198541 0.00041098159 0.00089618564 0.0091935787
		 0.0053892964 -0.026047263 0.096782982 -0.034829862 0.092289872 -0.063067958 0.005313098
		 -0.054285374 0.00081999035 -0.019044543 0.09221366 -0.027827131 0.09670677 -0.15331799
		 0.097545639 -0.18855882 0.0061519779 -0.17977622 0.0016588768 -0.14453538 0.093052566
		 -0.21660201 0.093677402 -0.18136117 0.0022837603 -0.17257859 0.0067768744 -0.2078194
		 0.098170549 -0.33331019 8.1442056e-05 -0.29806948 0.091475084 -0.30685201 0.095968194
		 -0.34209278 0.0045745475 -0.32611266 0.0064355973 -0.36135337 0.097829252 -0.37013599
		 0.09333618 -0.33489525 0.0019425005 -0.48684433 0.097510554 -0.49562687 0.093017444
		 -0.46038607 0.0016237778 -0.4516035 0.0061168619 -0.47964662 0.093175396 -0.48842922
		 0.097668536 -0.52367002 0.0062748445 -0.51488745 0.0017817349 0.052760914 -0.13690308
		 0.05527544 -0.13548839 0.034516551 -0.089622378 0.032002091 -0.091036998 0.056748088
		 -0.14336257 0.059262548 -0.14194793 -0.46003133 -0.14253266 -0.45751688 -0.14394736
		 -0.45352975 -0.13748781 -0.45604432 -0.13607316 -0.43277082 -0.091621809 -0.43528536
		 -0.090207115;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -s 5 ".r";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyTweakUV1.out" "woodthing2Shape.i";
connectAttr "groupId1.id" "woodthing2Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "woodthing2Shape.iog.og[0].gco";
connectAttr "polyTweakUV1.uvtk[0]" "woodthing2Shape.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "woodthing1:renderLayerManager.rlmi[0]" "woodthing1:defaultRenderLayer.rlid"
		;
connectAttr "woodthingtop:renderLayerManager.rlmi[0]" "woodthingtop:defaultRenderLayer.rlid"
		;
connectAttr "woodthingtop1:renderLayerManager.rlmi[0]" "woodthingtop1:defaultRenderLayer.rlid"
		;
connectAttr "woodthing3:renderLayerManager.rlmi[0]" "woodthing3:defaultRenderLayer.rlid"
		;
connectAttr "groupParts1.og" "polyAutoProj1.ip";
connectAttr "woodthing2Shape.wm" "polyAutoProj1.mp";
connectAttr "polySurfaceShape1.o" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polyAutoProj1.out" "polyTweakUV1.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "woodthing1:defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "woodthingtop:defaultRenderLayer.msg" ":defaultRenderingList1.r" -na
		;
connectAttr "woodthingtop1:defaultRenderLayer.msg" ":defaultRenderingList1.r" -na
		;
connectAttr "woodthing3:defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "woodthing2Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
// End of woodthing2.ma
