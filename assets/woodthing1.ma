//Maya ASCII 2018 scene
//Name: woodthing1.ma
//Last modified: Fri, Nov 09, 2018 11:04:43 PM
//Codeset: UTF-8
requires maya "2018";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Mac OS X 10.11.4";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "1D8EFB0A-844F-651E-6EFC-8488967DE100";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -17.731312956148216 10.511971730717262 -12.977359318244979 ;
	setAttr ".r" -type "double3" -16.538352729410025 233.79999999997517 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "E15EB25B-F64B-509F-98F3-9B91D06B7E11";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 22.921232831223175;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0 3.9872801303863525 0 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "7287017C-3C4C-B5BC-1CB4-DEBE95AB4E56";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.80995302220284637 1000.1222610314594 0.25022305585246091 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "ED84A022-0E4D-A234-AA8B-E5A992E01501";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 992.37350566473219;
	setAttr ".ow" 11.059859068669018;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -2.5815322576367072 7.7487553667269324 0 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "C699A91F-3540-E1BA-859C-21847098D1A7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 8.7646162859934229 1000.2323466318512 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "79D6E07F-4C4E-6DB2-62CA-BCA3397BB48F";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.2323466318512;
	setAttr ".ow" 11.635151226482495;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 0 8.7646162859934229 0 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "5F2A030F-E54C-2B42-43AB-379FBB64735C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1365369612346 2.4226627664713423 -2.4899248705820618 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "312A520A-C543-414D-268E-259A2CA663FB";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1365369612344;
	setAttr ".ow" 4.0489580668743494;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 0 2.2494521141052246 0 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "woodthing1";
	rename -uid "632B3F2C-0F48-05FC-916D-F89E7486AC54";
	setAttr ".rp" -type "double3" 0 3.9872801303863525 0 ;
	setAttr ".sp" -type "double3" 0 3.9872801303863525 0 ;
createNode mesh -n "woodthing1Shape" -p "woodthing1";
	rename -uid "E45CDBA8-4243-0C51-7AC4-04A0779E9C75";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.47749482013750821 0.50000002246815711 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape1" -p "woodthing1";
	rename -uid "670B6311-1E48-1F73-3C5A-16B5E55559EC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:95]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.47749482013750821 0.50000002246815711 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 224 ".uvst[0].uvsp[0:223]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75
		 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25 0.375 0 0.375 0.25 0.625 0.25 0.625 0 0.375 0.5 0.625
		 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0.25 0.875 0 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.375 0.25 0.625 0.25 0.625
		 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0.25 0.875 0 0.125
		 0 0.125 0.25 0.375 0 0.375 0.25 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75
		 0.625 0.75 0.375 1 0.625 1 0.875 0.25 0.875 0 0.125 0 0.125 0.25 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25 0.375 0 0.375 0.25 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75
		 0.625 0.75 0.375 1 0.625 1 0.875 0.25 0.875 0 0.125 0 0.125 0.25 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[24:31]" -type "float3"  0.0082188295 0 0.0023041712 
		0.0082083838 0 -0.0023411813 0.0082188295 0 0.0023041712 0.0082083838 0 -0.0023411813 
		-0.0082083838 0 0.0023411906 -0.0082188295 0 -0.0023041763 -0.0082083838 0 0.0023411906 
		-0.0082188295 0 -0.0023041763;
	setAttr -s 128 ".vt[0:127]"  -3.21272779 7.52294827 2.75407696 -1.66805899 7.52294827 2.76852036
		 -3.21272779 7.97456264 2.75407696 -1.66805899 7.97456264 2.76852036 -3.16108942 7.97456264 -2.76852036
		 -1.61642051 7.97456264 -2.75407696 -3.16108942 7.52294827 -2.76852036 -1.61642051 7.52294827 -2.75407696
		 -1.59405017 7.52294827 2.72354412 -0.049388885 7.52294827 2.73876238 -1.59405017 7.97456264 2.72354412
		 -0.049388885 7.97456264 2.73876238 -1.54023433 7.97456264 -2.73876238 0.0044269562 7.97456264 -2.72354412
		 -1.54023433 7.52294827 -2.73876238 0.0044269562 7.52294827 -2.72354412 0.061721921 7.52294827 2.69337606
		 1.60645783 7.52294827 2.69236898 0.061721921 7.97456264 2.69337606 1.60645783 7.97456264 2.69236898
		 0.058210254 7.97456264 -2.69236898 1.60294616 7.97456264 -2.69337606 0.058210254 7.52294827 -2.69236898
		 1.60294616 7.52294827 -2.69337606 1.67033911 7.52294827 2.73186398 3.21507478 7.52294827 2.73070621
		 1.67033911 7.97456264 2.73186398 3.21507478 7.97456264 2.73070621 1.66624427 7.97456264 -2.73070621
		 3.21097994 7.97456264 -2.73186398 1.66624427 7.52294827 -2.73070621 3.21097994 7.52294827 -2.73186398
		 -3.86999154 -2.3841858e-06 -2.66266465 -2.81470966 -2.3841858e-06 -2.66266465 -2.81156445 7.5233202 -1.81753469
		 -1.75628388 7.5233202 -1.81753469 -2.81156445 7.5233202 -2.2468164 -1.75628388 7.5233202 -2.2468164
		 -3.86999154 -2.3841858e-06 -3.091948271 -2.81470966 -2.3841858e-06 -3.091948271 3.86999178 -2.3841858e-06 -2.66266465
		 2.81470966 -2.3841858e-06 -2.66266465 2.81156445 7.5233202 -1.81753469 1.75628364 7.5233202 -1.81753469
		 2.81156445 7.5233202 -2.2468164 1.75628364 7.5233202 -2.2468164 3.86999178 -2.3841858e-06 -3.091948271
		 2.81470966 -2.3841858e-06 -3.091948271 -3.86999154 -2.3841858e-06 2.66266513 -2.81470966 -2.3841858e-06 2.66266513
		 -2.81156445 7.5233202 1.81753469 -1.75628388 7.5233202 1.81753469 -2.81156445 7.5233202 2.24681664
		 -1.75628388 7.5233202 2.24681664 -3.86999154 -2.3841858e-06 3.091948509 -2.81470966 -2.3841858e-06 3.091948509
		 3.86999178 -2.3841858e-06 2.66266513 2.81470966 -2.3841858e-06 2.66266513 2.81156445 7.5233202 1.81753469
		 1.75628364 7.5233202 1.81753469 2.81156445 7.5233202 2.24681664 1.75628364 7.5233202 2.24681664
		 3.86999178 -2.3841858e-06 3.091948509 2.81470966 -2.3841858e-06 3.091948509 3.95621514 1.53043938 2.92977667
		 3.95545363 1.56876826 3.26543903 3.84029818 3.084991455 2.75200152 3.83953667 3.1233201 3.087664127
		 -3.90515113 2.93113565 2.75200152 -3.9059124 2.9694643 3.087664127 -3.92454052 1.37389565 2.92977667
		 -3.92530179 1.41222465 3.26543903 3.16014647 5.96650982 2.42156649 3.16014647 6.0046458244 2.75725222
		 3.0054366589 7.52316427 2.24472332 3.0054366589 7.52014875 2.58040857 -3.032927275 7.52316427 2.24472332
		 -3.032927275 7.52014875 2.58040857 -3.20949912 5.96650982 2.42156649 -3.20949912 6.0046458244 2.75725222
		 3.94955087 1.49468994 -2.92977667 3.94913602 1.53302455 -3.26543903 3.8476975 3.050227165 -2.75200152
		 3.84728265 3.088561058 -3.087664127 -3.8988266 2.96642447 -2.75200152 -3.89924121 3.0047585964 -3.087664127
		 -3.93229818 1.40942371 -2.92977667 -3.93271303 1.44775772 -3.26543903 3.16014647 5.96650982 -2.42156649
		 3.16014647 6.0046458244 -2.75725222 3.07803607 7.52316427 -2.24472332 3.07803607 7.52014875 -2.58040857
		 -3.093097925 7.52316427 -2.24472332 -3.093097925 7.52014875 -2.58040857 -3.18476272 5.96650982 -2.42156649
		 -3.18476272 6.0046458244 -2.75725222 -3.029358864 5.96468592 -2.2789197 -3.36391687 6.011699677 -2.2789197
		 -2.81134844 7.52198887 -2.19136238 -3.14590597 7.52197266 -2.19136238 -2.81134844 7.52198887 2.21843386
		 -3.14590597 7.52197266 2.21843386 -3.029358864 5.96468592 2.38056207 -3.36391687 6.011699677 2.38056207
		 -3.67141366 1.37315142 -2.87051249 -4.005979538 1.42010021 -2.87099338 -3.45368791 2.9261086 -2.7326653
		 -3.7882545 2.97305727 -2.73314619 -3.45368791 2.98218441 2.74178171 -3.7882545 3.029133081 2.74130082
		 -3.67141366 1.43212557 2.88688445 -4.005979538 1.47907448 2.88640356 3.029358864 5.96468592 -2.2789197
		 3.36391687 6.011699677 -2.2789197 2.81134844 7.52198887 -2.19136238 3.14590597 7.52197266 -2.19136238
		 2.81134844 7.52198887 2.21843386 3.14590597 7.52197266 2.21843386 3.029358864 5.96468592 2.38056207
		 3.36391687 6.011699677 2.38056207 3.67141366 1.40259659 -2.87884951 4.005979538 1.44954789 -2.87884951
		 3.45368791 2.95406055 -2.72510314 3.7882545 3.0010113716 -2.72510314 3.45368791 2.95406055 2.74963117
		 3.7882545 3.0010113716 2.74963117 3.67141366 1.40259659 2.87884951 4.005979538 1.44954789 2.87884951;
	setAttr -s 192 ".ed";
	setAttr ".ed[0:165]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0 3 5 0 4 6 0
		 5 7 0 6 0 0 7 1 0 8 9 0 10 11 0 12 13 0 14 15 0 8 10 0 9 11 0 10 12 0 11 13 0 12 14 0
		 13 15 0 14 8 0 15 9 0 16 17 0 18 19 0 20 21 0 22 23 0 16 18 0 17 19 0 18 20 0 19 21 0
		 20 22 0 21 23 0 22 16 0 23 17 0 24 25 0 26 27 0 28 29 0 30 31 0 24 26 0 25 27 0 26 28 0
		 27 29 0 28 30 0 29 31 0 30 24 0 31 25 0 32 33 0 34 35 0 36 37 0 38 39 0 32 34 0 33 35 0
		 34 36 0 35 37 0 36 38 0 37 39 0 38 32 0 39 33 0 40 41 0 41 43 0 42 43 0 40 42 0 43 45 0
		 44 45 0 42 44 0 45 47 0 46 47 0 44 46 0 47 41 0 46 40 0 48 49 0 49 51 0 50 51 0 48 50 0
		 51 53 0 52 53 0 50 52 0 53 55 0 54 55 0 52 54 0 55 49 0 54 48 0 56 58 0 58 59 0 57 59 0
		 56 57 0 58 60 0 60 61 0 59 61 0 60 62 0 62 63 0 61 63 0 62 56 0 63 57 0 64 65 0 65 67 0
		 66 67 0 64 66 0 67 69 0 68 69 0 66 68 0 69 71 0 70 71 0 68 70 0 71 65 0 70 64 0 72 73 0
		 73 75 0 74 75 0 72 74 0 75 77 0 76 77 0 74 76 0 77 79 0 78 79 0 76 78 0 79 73 0 78 72 0
		 80 82 0 82 83 0 81 83 0 80 81 0 82 84 0 84 85 0 83 85 0 84 86 0 86 87 0 85 87 0 86 80 0
		 87 81 0 88 90 0 90 91 0 89 91 0 88 89 0 90 92 0 92 93 0 91 93 0 92 94 0 94 95 0 93 95 0
		 94 88 0 95 89 0 96 98 0 98 99 0 97 99 0 96 97 0 98 100 0 100 101 0 99 101 0 100 102 0
		 102 103 0 101 103 0 102 96 0 103 97 0 104 106 0 106 107 0 105 107 0 104 105 0 106 108 0
		 108 109 0 107 109 0 108 110 0 110 111 0 109 111 0;
	setAttr ".ed[166:191]" 110 104 0 111 105 0 112 113 0 113 115 0 114 115 0 112 114 0
		 115 117 0 116 117 0 114 116 0 117 119 0 118 119 0 116 118 0 119 113 0 118 112 0 120 121 0
		 121 123 0 122 123 0 120 122 0 123 125 0 124 125 0 122 124 0 125 127 0 126 127 0 124 126 0
		 127 121 0 126 120 0;
	setAttr -s 96 -ch 384 ".fc[0:95]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 12 17 -14 -17
		mu 0 4 14 15 16 17
		f 4 13 19 -15 -19
		mu 0 4 17 16 18 19
		f 4 14 21 -16 -21
		mu 0 4 19 18 20 21
		f 4 15 23 -13 -23
		mu 0 4 21 20 22 23
		f 4 -24 -22 -20 -18
		mu 0 4 15 24 25 16
		f 4 22 16 18 20
		mu 0 4 26 14 17 27
		f 4 24 29 -26 -29
		mu 0 4 28 29 30 31
		f 4 25 31 -27 -31
		mu 0 4 31 30 32 33
		f 4 26 33 -28 -33
		mu 0 4 33 32 34 35
		f 4 27 35 -25 -35
		mu 0 4 35 34 36 37
		f 4 -36 -34 -32 -30
		mu 0 4 29 38 39 30
		f 4 34 28 30 32
		mu 0 4 40 28 31 41
		f 4 36 41 -38 -41
		mu 0 4 42 43 44 45
		f 4 37 43 -39 -43
		mu 0 4 45 44 46 47
		f 4 38 45 -40 -45
		mu 0 4 47 46 48 49
		f 4 39 47 -37 -47
		mu 0 4 49 48 50 51
		f 4 -48 -46 -44 -42
		mu 0 4 43 52 53 44
		f 4 46 40 42 44
		mu 0 4 54 42 45 55
		f 4 48 53 -50 -53
		mu 0 4 56 57 58 59
		f 4 49 55 -51 -55
		mu 0 4 59 58 60 61
		f 4 50 57 -52 -57
		mu 0 4 61 60 62 63
		f 4 51 59 -49 -59
		mu 0 4 63 62 64 65
		f 4 -60 -58 -56 -54
		mu 0 4 57 66 67 58
		f 4 58 52 54 56
		mu 0 4 68 56 59 69
		f 4 63 62 -62 -61
		mu 0 4 70 71 72 73
		f 4 66 65 -65 -63
		mu 0 4 71 74 75 72
		f 4 69 68 -68 -66
		mu 0 4 74 76 77 75
		f 4 71 60 -71 -69
		mu 0 4 76 78 79 77
		f 4 61 64 67 70
		mu 0 4 73 72 80 81
		f 4 -70 -67 -64 -72
		mu 0 4 82 83 71 70
		f 4 75 74 -74 -73
		mu 0 4 84 85 86 87
		f 4 78 77 -77 -75
		mu 0 4 85 88 89 86
		f 4 81 80 -80 -78
		mu 0 4 88 90 91 89
		f 4 83 72 -83 -81
		mu 0 4 90 92 93 91
		f 4 73 76 79 82
		mu 0 4 87 86 94 95
		f 4 -82 -79 -76 -84
		mu 0 4 96 97 85 84
		f 4 87 86 -86 -85
		mu 0 4 98 99 100 101
		f 4 85 90 -90 -89
		mu 0 4 101 100 102 103
		f 4 89 93 -93 -92
		mu 0 4 103 102 104 105
		f 4 92 95 -88 -95
		mu 0 4 105 104 106 107
		f 4 -96 -94 -91 -87
		mu 0 4 99 108 109 100
		f 4 94 84 88 91
		mu 0 4 110 98 101 111
		f 4 99 98 -98 -97
		mu 0 4 112 113 114 115
		f 4 102 101 -101 -99
		mu 0 4 113 116 117 114
		f 4 105 104 -104 -102
		mu 0 4 116 118 119 117
		f 4 107 96 -107 -105
		mu 0 4 118 120 121 119
		f 4 97 100 103 106
		mu 0 4 115 114 122 123
		f 4 -106 -103 -100 -108
		mu 0 4 124 125 113 112
		f 4 111 110 -110 -109
		mu 0 4 126 127 128 129
		f 4 114 113 -113 -111
		mu 0 4 127 130 131 128
		f 4 117 116 -116 -114
		mu 0 4 130 132 133 131
		f 4 119 108 -119 -117
		mu 0 4 132 134 135 133
		f 4 109 112 115 118
		mu 0 4 129 128 136 137
		f 4 -118 -115 -112 -120
		mu 0 4 138 139 127 126
		f 4 123 122 -122 -121
		mu 0 4 140 141 142 143
		f 4 121 126 -126 -125
		mu 0 4 143 142 144 145
		f 4 125 129 -129 -128
		mu 0 4 145 144 146 147
		f 4 128 131 -124 -131
		mu 0 4 147 146 148 149
		f 4 -132 -130 -127 -123
		mu 0 4 141 150 151 142
		f 4 130 120 124 127
		mu 0 4 152 140 143 153
		f 4 135 134 -134 -133
		mu 0 4 154 155 156 157
		f 4 133 138 -138 -137
		mu 0 4 157 156 158 159
		f 4 137 141 -141 -140
		mu 0 4 159 158 160 161
		f 4 140 143 -136 -143
		mu 0 4 161 160 162 163
		f 4 -144 -142 -139 -135
		mu 0 4 155 164 165 156
		f 4 142 132 136 139
		mu 0 4 166 154 157 167
		f 4 147 146 -146 -145
		mu 0 4 168 169 170 171
		f 4 145 150 -150 -149
		mu 0 4 171 170 172 173
		f 4 149 153 -153 -152
		mu 0 4 173 172 174 175
		f 4 152 155 -148 -155
		mu 0 4 175 174 176 177
		f 4 -156 -154 -151 -147
		mu 0 4 169 178 179 170
		f 4 154 144 148 151
		mu 0 4 180 168 171 181
		f 4 159 158 -158 -157
		mu 0 4 182 183 184 185
		f 4 157 162 -162 -161
		mu 0 4 185 184 186 187
		f 4 161 165 -165 -164
		mu 0 4 187 186 188 189
		f 4 164 167 -160 -167
		mu 0 4 189 188 190 191
		f 4 -168 -166 -163 -159
		mu 0 4 183 192 193 184
		f 4 166 156 160 163
		mu 0 4 194 182 185 195
		f 4 171 170 -170 -169
		mu 0 4 196 197 198 199
		f 4 174 173 -173 -171
		mu 0 4 197 200 201 198
		f 4 177 176 -176 -174
		mu 0 4 200 202 203 201
		f 4 179 168 -179 -177
		mu 0 4 202 204 205 203
		f 4 169 172 175 178
		mu 0 4 199 198 206 207
		f 4 -178 -175 -172 -180
		mu 0 4 208 209 197 196
		f 4 183 182 -182 -181
		mu 0 4 210 211 212 213
		f 4 186 185 -185 -183
		mu 0 4 211 214 215 212
		f 4 189 188 -188 -186
		mu 0 4 214 216 217 215
		f 4 191 180 -191 -189
		mu 0 4 216 218 219 217
		f 4 181 184 187 190
		mu 0 4 213 212 220 221
		f 4 -190 -187 -184 -192
		mu 0 4 222 223 211 210;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "167EB5DD-B349-2CD6-B0CF-87801BFFA637";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "2E734A7B-B34E-3BD9-7636-2197F6E20C03";
createNode displayLayer -n "defaultLayer";
	rename -uid "A420A974-F145-CDA8-0AF9-2EBF51B0E222";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "C6B835FB-8F43-7885-1C1D-11A714A28D73";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "DE38CB01-CB49-6345-D26D-578DFBDAFFAD";
	setAttr ".g" yes;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "8E57F00C-4649-E4C5-230B-A5BE34DA0967";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "8BEF0E1A-454D-EF91-BBDC-16B85F512308";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "536A9E2A-074E-E211-C107-A38EC36EDB00";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 449\n            -height 279\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n"
		+ "            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n"
		+ "            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 449\n            -height 278\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n"
		+ "            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 449\n            -height 278\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n"
		+ "            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 321\n            -height 697\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n"
		+ "            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n"
		+ "            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n"
		+ "            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n"
		+ "            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n"
		+ "                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n"
		+ "                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n"
		+ "                -classicMode 1\n                -valueLinesToggle 1\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n"
		+ "                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n"
		+ "                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n"
		+ "                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n"
		+ "                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -highlightConnections 0\n"
		+ "                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 321\\n    -height 697\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 321\\n    -height 697\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "26E924B6-EC45-AA26-DADE-A798E3A602AD";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyAutoProj -n "polyAutoProj1";
	rename -uid "C26A55CF-6C49-A8FE-E442-96B3C05CDEB6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:95]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".s" -type "double3" 8.0119590759277344 8.0119590759277344 8.0119590759277344 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode groupId -n "groupId1";
	rename -uid "B38800C3-F144-2B0D-FF1A-C8BB2944AD6A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "810AACFE-8743-BC45-A8DB-8A8BEC33BC55";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:95]";
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "FEB749CE-BF42-56C6-5E38-DE959EB8CE2F";
	setAttr ".uopa" yes;
	setAttr -s 384 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 0.27234021 -0.22530952 0.27234021 -0.24816158
		 0.27420899 -0.24816158 0.27420899 -0.22530952 -0.20959724 -0.21556079 -0.20959724
		 -0.23841301 -0.2077287 -0.23841301 -0.2077287 -0.21556079 -0.61151826 0.021754893
		 -0.61151826 -0.00084774062 -0.6096496 -0.00084774062 -0.6096496 0.021754893 -0.67277807
		 0.024438156 -0.67277807 0.0018355278 -0.67090952 0.0018355278 -0.67090952 0.024438156
		 -0.52089882 -0.17611463 -0.52089882 -0.19840042 -0.51903015 -0.19840042 -0.51903015
		 -0.17611463 -0.62759167 -0.18263195 -0.62759167 -0.20491782 -0.62572306 -0.20491782
		 -0.62572306 -0.18263195 -0.7502988 0.019964157 -0.7502988 -0.0026392264 -0.74843007
		 -0.0026392264 -0.74843007 0.019964157 -0.6633032 0.021743389 -0.6633032 -0.00086023135
		 -0.66143465 -0.00086023135 -0.66143465 0.021743389 0.19885093 0.0041905344 0.20051344
		 0.0021636805 0.23276156 0.035289694 0.23109913 0.037316471 0.19750097 0.0020848506
		 0.19916347 0.00411169 0.1669153 0.037237693 0.1652528 0.035210852 -0.0093902871 0.0045115273
		 -0.041638605 0.03763748 -0.043301057 0.035610635 -0.011052747 0.0024846632 -0.075236589
		 0.0030512291 -0.042988345 0.036177278 -0.044650789 0.038204063 -0.076899081 0.0050780987
		 0.12952428 -0.28578547 0.097276092 -0.25265956 0.095613651 -0.25468636 0.12786187
		 -0.28781229 0.17383038 -0.28819305 0.20607865 -0.25506699 0.20441607 -0.25303996
		 0.17216785 -0.28616613 0.42117715 -0.2857272 0.42283973 -0.28775409 0.45508787 -0.25462818
		 0.45342547 -0.25260136 0.52997977 -0.28689858 0.5316422 -0.28487176 0.49939406 -0.25174585
		 0.49773133 -0.25377241 -0.041643914 0.051078301 -0.048403833 0.05793817 -0.049884811
		 0.056478243 -0.043124769 0.049618505 -0.28049716 0.026719006 -0.2872566 0.019848809
		 -0.28577569 0.018391907 -0.27901641 0.025262108 -0.13481386 0.039601088 -0.14183939
		 0.046463877 -0.14311561 0.044790529 -0.13629384 0.038086019 -0.090358138 0.048421338
		 -0.097383693 0.041558485 -0.09590365 0.040043443 -0.089081898 0.046747986 -0.095651269
		 0.022587445 -0.094170287 0.021128748 -0.087410673 0.02799247 -0.088891558 0.029451113
		 0.032410409 0.029471662 0.030929461 0.028014369 0.037688773 0.021145282 0.039169844
		 0.022602512 -0.051651776 0.04371126 -0.0501718 0.042196237 -0.043350138 0.048900802
		 -0.044626188 0.050574061 -0.1028629 0.048817333 -0.10413916 0.047143854 -0.097317286
		 0.040439337 -0.095837414 0.04195451 -0.30481315 -0.17262031 -0.30420858 -0.19190001
		 -0.29798058 -0.19103317 -0.29855266 -0.17278661 0.14635667 -0.17437139 0.14572829
		 -0.19365099 0.15218361 -0.19349864 0.1527781 -0.17525214 0.26996282 -0.19398904 0.26756203
		 -0.21782528 0.27403885 -0.21793717 0.2763215 -0.19527194 0.50262344 -0.19585784 0.50502437
		 -0.2196945 0.51138324 -0.21841101 0.50910056 -0.19574617 0.26280284 0.0041275457
		 0.26906341 0.0042939782 0.26963544 0.022540597 0.26340738 0.023407344 -0.06067377
		 0.0077564754 -0.054251876 0.0086372318 -0.05484651 0.026883667 -0.061302125 0.027035784
		 0.072419576 -0.19117655 0.078835234 -0.1904977 0.078684546 -0.16784373 0.072261259
		 -0.16735159 0.079206675 -0.21404189 0.085630052 -0.21354973 0.085780583 -0.19089608
		 0.079365142 -0.19021721 -0.023594858 -0.21212964 -0.01720004 -0.21158044 -0.019163476
		 -0.18871807 -0.025558235 -0.18926728 -0.33718404 -0.23924646 -0.33078951 -0.23979551
		 -0.3288261 -0.21693324 -0.33522072 -0.21638413 -0.061446104 -0.0026425705 -0.055051289
		 -0.0020639023 -0.057097457 0.020549841 -0.063492425 0.019971244 -0.27689901 -0.0010645024
		 -0.27050391 -0.0016432254 -0.26845768 0.020970581 -0.27485266 0.021549305 -0.078136601
		 0.017759813 -0.071744621 0.017721497 -0.071611047 0.04000742 -0.078003086 0.040045679
		 -0.32339862 -0.19319372 -0.31700641 -0.19315538 -0.31713998 -0.17086944 -0.32353207
		 -0.17090778 0.081586584 -0.0005787547 0.087979101 -0.00079947308 0.088759385 0.021805702
		 0.082366988 0.022026474 -0.65734977 -0.001810509 -0.65095752 -0.001589736 -0.65173769
		 0.021015424 -0.65813017 0.020794697 0.46082869 -0.0019337618 0.46082869 -0.0063004852
		 0.46260491 -0.0063004852 0.46260491 -0.0019337618 0.65902764 -0.0013554676 0.65902764
		 -0.005722113 0.66080421 -0.005722113 0.66080421 -0.0013554676 0.65893155 -0.0045097438
		 0.66070795 -0.0045097438 0.66070795 -0.00014299758 0.65893155 -0.00014299758 0.41606319
		 -0.0047229198 0.41783991 -0.0047229198 0.41783991 -0.00035623676 0.41606319 -0.00035623676
		 0.41774321 -0.00088249345 0.41596705 -0.00088249345 0.41596705 -0.0052491585 0.41774321
		 -0.0052491585 0.61594236 -0.0017311835 0.61416596 -0.0017311835 0.61416596 -0.0060978672
		 0.61594236 -0.0060978672 0.080268092 0.034999795 0.080268092 0.03936645 0.078491509
		 0.03936645 0.078491509 0.034999795 0.20433518 0.040481646 0.20433518 0.044848327
		 0.20255898 0.044848327 0.20255898 0.040481646 0.40927079 0.0026761142 0.40927213
		 0.034726217 0.4078832 0.034729425 0.40788177 0.0026793259 0.15294071 0.035480153
		 0.15294208 0.0028701003 0.15433103 0.0028733069 0.15432966 0.035483375 0.28645417
		 0.025433326 0.28645417 0.050419591 0.285065 0.050419591 0.285065 0.025433326 -0.048247024
		 0.061485089 -0.048247024 0.035127901 -0.046857934 0.035127901 -0.046857934 0.061485089
		 0.40982381 0.0027060474 0.41121268 0.0027077871 0.41121188 0.034762342 0.409823 0.034760568
		 0.29047456 0.035292692 0.28908572 0.03529444 0.28908494 0.0026798919 0.29047388 0.0026781482
		 0.69268113 0.025299959 0.69406974 0.025299959 0.69406974 0.050835818 0.69268113 0.050835818
		 0.25040272 0.050176896 0.24901378 0.050176896 0.24901378 0.023922145 0.25040272 0.023922145
		 0.10691991 0.005349881 0.10553535 0.005349881 0.10553535 -0.012897528 0.10691991
		 -0.012897528 -0.17768429 -0.19510923 -0.17906862 -0.19510923 -0.17906862 -0.21438985
		 -0.17768429 -0.21438985 0.083912477 0.015348634 0.082528129 0.015350486 0.082526892
		 -0.0073020686 0.0839113 -0.0073039811 0.60524911 0.04985648 0.60386485 0.049854673
		 0.60386604 0.026031004 0.60525024 0.026033076 0.32982296 0.025316065 0.32982296 0.0070686261
		 0.33120725 0.0070686261 0.33120725 0.025316065 -0.36756393 -0.17253406 -0.36756393
		 -0.19181468 -0.36617932 -0.19181468 -0.36617932 -0.17253406 0.082446866 0.014254063
		 0.082446866 -0.0083997501;
	setAttr ".uvtk[250:383]" 0.083831146 -0.0083997501 0.083831146 0.014254063
		 0.34261051 0.054135729 0.34261051 0.030310821 0.34399492 0.030310821 0.34399492 0.054135729
		 0.75053406 0.0099703521 0.75053406 0.003578627 0.75240308 0.003578627 0.75240308
		 0.0099703521 0.12386007 0.012432698 0.12386007 0.0060409652 0.12572853 0.0060409652
		 0.12572853 0.012432698 0.15340674 -0.0088580493 0.15340674 -0.015249751 0.15527536
		 -0.015249751 0.15527536 -0.0088580493 0.26634711 -0.0048624682 0.26634711 -0.011254182
		 0.26821578 -0.011254182 0.26821578 -0.0048624682 0.20915413 0.050799411 0.20915413
		 0.044407405 0.2110229 0.044407405 0.2110229 0.050799411 0.070956945 0.0508334 0.070956945
		 0.044441424 0.072825663 0.044441424 0.072825663 0.0508334 0.67404044 0.073057331
		 0.67404044 0.066665389 0.6759088 0.066665389 0.6759088 0.073057331 0.72116297 0.073286459
		 0.72116297 0.066894509 0.72303182 0.066894509 0.72303182 0.073286459 -0.03240202
		 0.00066628336 -0.028486164 0.0069839777 -0.069598816 0.04123728 -0.073514648 0.034919705
		 -0.11095621 0.0086939316 -0.1070405 0.0023762276 -0.065927811 0.036629532 -0.069843575
		 0.042947225 -0.25430235 0.0010379254 -0.21318974 0.035291255 -0.21710552 0.041609015
		 -0.25821823 0.0073556006 -0.24877805 0.0079588089 -0.28989065 0.042212233 -0.29380655
		 0.035894543 -0.25269377 0.0016411052 -0.33809417 -0.24770464 -0.37920693 -0.28195795
		 -0.37529102 -0.28827569 -0.33417845 -0.25402245 -0.24294235 -0.25516316 -0.20182966
		 -0.28941649 -0.19791383 -0.28309879 -0.23902658 -0.24884549 0.0058693532 -0.24869998
		 0.0019534235 -0.25501755 0.043066137 -0.28927076 0.046981938 -0.2829532 0.18324649
		 -0.25362059 0.17933065 -0.24730279 0.13821803 -0.2815558 0.14213377 -0.2878736 -0.18281528
		 0.0027035985 -0.17630023 0.0019108722 -0.17060065 0.03402542 -0.1770162 0.035379119
		 -0.17661363 0.032797523 -0.18302916 0.031443831 -0.1773296 -0.00067065645 -0.17081451
		 0.00012207279 0.2672708 0.021515636 0.27354476 0.022126561 0.27366117 0.047113065
		 0.26739359 0.047872756 0.56114268 0.049097877 0.55470467 0.04833651 0.55482477 0.023350105
		 0.56126916 0.022740904 0.13221508 -2.153609e-10 0.12912606 0.032633975 0.12270142
		 0.03181567 0.1257374 -0.00025799221 0.12914649 0.034318358 0.12605749 0.001684504
		 0.13253511 0.0014265036 0.13557102 0.033500079 0.71355522 0.026296664 0.71350271
		 0.052551452 0.70723224 0.052159961 0.70728344 0.026623955 0.39798242 0.052754749
		 0.39792827 0.026499579 0.40437043 0.02682622 0.40442312 0.052361995 0.18178731 0.045123845
		 0.18026453 0.046983156 0.17189302 0.040073879 0.1731299 0.038040675 0.061009757 0.03705579
		 0.059772696 0.03502271 0.06814421 0.028113434 0.069667093 0.029972576 -0.11251252
		 0.028383557 -0.11403498 0.030166628 -0.12231326 0.023100212 -0.12079035 0.021317085
		 -0.23294662 0.027403086 -0.23446961 0.02561583 -0.22619045 0.01856041 -0.22466795
		 0.020347416 0.42392546 0.042484969 0.43258277 0.035401706 0.43381986 0.037434742
		 0.42544827 0.044344015 0.48539722 0.043405972 0.47673976 0.036322773 0.47826275 0.034463603
		 0.48663419 0.041372884 -0.054684311 0.03032608 -0.046406064 0.023265176 -0.04488318
		 0.025050336 -0.053161822 0.032111235 0.00083767669 0.030186517 -0.0074407677 0.023125751
		 -0.0059181866 0.021340286 0.0023603032 0.028401244;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyTweakUV1.out" "woodthing1Shape.i";
connectAttr "groupId1.id" "woodthing1Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "woodthing1Shape.iog.og[0].gco";
connectAttr "polyTweakUV1.uvtk[0]" "woodthing1Shape.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "groupParts1.og" "polyAutoProj1.ip";
connectAttr "woodthing1Shape.wm" "polyAutoProj1.mp";
connectAttr "polySurfaceShape1.o" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polyAutoProj1.out" "polyTweakUV1.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "woodthing1Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
// End of woodthing1.ma
